import pandas as pd

from yatpkg.kinematics.model import Model
from yatpkg.math.filters import Butterworth
from yatpkg.util.data import StorageIO, MocapFlags, StorageType, Yatsdo, TRC, OSIMStorage
from yatpkg.math.transformation import Cloud
import os
from tqdm import tqdm
from yatpkg.kinematics.model import OsimModel
import numpy as np
import copy
from scipy.spatial.transform import Rotation
import time
from scipy.optimize import minimize
from scipy.signal import find_peaks
import matplotlib.pyplot as mp
from opensim.simbody import Vector
from opensim import MarkerWeight, InverseKinematicsTool, IKMarkerTask
from multiprocessing import Pool
from shutil import copyfile


class Omodel:
    def __init__(self, model_path):
        self.osim_model = OsimModel(model_path)
        cn = self.osim_model.osim.getStateVariableNames()
        self.state_variable_names = [cn.get(c) for c in range(0, cn.getSize())]
        self.state_variable_names_processed = [c.split("/")[-2] if 'value' in c else 'N_A' for c in self.state_variable_names]

        m = self.osim_model.markerset
        self.osim_markers = [f for f in m.marker_set]
        self.__markerset__ = {}
        for x in m.marker_set:
            p = m.marker_set[x].location_in_ground
            # print("{0}: {1}, {2}, {3}".format(x, p[0], p[1], p[2]))
            self.__markerset__[x] = p
        self.locked = ["mtp_r", "mtp_l"]
        self.locked_id = []
        for f in range(0, len(self.osim_model.joint_names)):
            y = self.osim_model.joint_names[f]
            if y in self.locked:
                self.locked_id.append(1)
            else:
                self.locked_id.append(0)

    @property
    def markerset(self):
        return pd.DataFrame(self.__markerset__)

    def update(self):
        m = self.osim_model.markerset
        for x in m.marker_set:
            self.__markerset__[x] = copy.deepcopy(m.marker_set[x].location_in_ground)
        pass

    def translate_model(self, vxyz, joint_name='ground_pelvis'):
        j = self.osim_model.jointset[joint_name]
        nxp = [0, 0, 0, 0, 0, 0]
        nxp[j.coord['pelvis_tx'][1]] = vxyz[0]
        nxp[j.coord['pelvis_ty'][1]] = vxyz[1]
        nxp[j.coord['pelvis_tz'][1]] = vxyz[2]
        j.set_joint(nxp)

    def set_joint(self, joint_name, v):
        j = None
        if isinstance(joint_name, int):
            j = self.osim_model.jointset[self.osim_model.joint_names[joint_name]]
        elif isinstance(joint_name, str):
            j = self.osim_model.jointset[joint_name]
        if j is not None:
            b = j.set_joint(v)
            if b == 0:
                self.update()
            elif b == -1:
                print("Can not set {0} due to num of coord is not the same: v({1}) != {0}({2})".format(joint_name, len(v), j.num_coord))
        else:
            print("Can not set {0} due to it not being in the model.".format(joint_name))

    @staticmethod
    def joint_worker(g):
        v = g[1]
        j = g[0]
        r = []
        for c in j.coord:
            if not ('tx' in c or 'ty' in c or 'tz' in c):
                r.append(np.pi * (v[c] / 180.0))
            else:
                r.append(v[c])

        j.set_joint(r)

    def set_joints(self, v):
        if isinstance(v, pd.DataFrame) or isinstance(v, pd.Series):
            ret = [0 for n in range(0, len(self.state_variable_names))]
            cols = None
            if isinstance(v, pd.DataFrame):
                cols = [c for c in v.columns if 'time' not in c]
            if isinstance(v, pd.Series):
                cols = [c for c in v.index if 'time' not in c]
            kl = {c: self.state_variable_names_processed.index(c) for c in cols}
            for c in cols:
                vx = 0
                if isinstance(v, pd.DataFrame):
                    vx = float(v[c].iloc[0])
                    if not ('tx' in c or 'ty' in c or 'tz' in v[c]):
                        vx = np.pi * (vx / 180.0)
                if isinstance(v, pd.Series):
                    vx = float(v[c])
                    if not ('tx' in c or 'ty' in c or 'tz' in c):
                        vx = np.pi * (vx / 180.0)

                ret[kl[c]] = vx
            "set by coordinate"
            # for jn in self.osim_model.joint_names:
            #     j = self.osim_model.jointset[jn]
            #     ic = cols
            #     if ic is not None:
            #         # assumes sto file order
            #         r = [np.pi * (float(v[c].iloc[0])/180.0) if not ('tx' in c or 'ty' in c or 'tz' in c) else float(v[c].iloc[0]) for c in j.coord]
            #         cx = [c for c in j.coord]
            #         for ix in range(0, len(cx)):
            #             j.set_joint(r, ix, )
            #         pass
            # jobs = []
            # for jn in self.osim_model.joint_names:
            #     j = self.osim_model.jointset[jn]
            #     ic = None
            #     if isinstance(v, pd.Series):
            #         ic = [c for c in v.index]
            #     elif isinstance(v, pd.DataFrame):
            #         ic = [c for c in v.columns]
            #     if ic is not None:
            #         r = {c: np.pi * (v[c]/180.0) if not ('tx' in c or 'ty' in c or 'tz' in c) else v[c] for c in j.coord}
            #         idc = {c: self.state_variable_names_processed.index(c) for c in j.coord}
            #         for c in idc:
            #             ret[idc[c]] = float(r[c])
            #         pass
            vx1 = Vector(ret)
            self.osim_model.osim.setStateVariableValues(self.osim_model.state, vx1)
            self.osim_model.osim.assemble(self.osim_model.state)
            # self.osim_model.re_init()
            self.update()
            pass

    def obj_func(self, para, tx):
        """
        Basic obj function compare model markers with trc file markers
        :param para: joint angles
        :param tx: trc markers
        :return: abs difference
        """
        self.set_joints(para)
        self.update()
        yx = 1000*self.markerset.to_numpy()
        residuals = yx - tx
        return np.abs(residuals)


class Pelvis:
    def __init__(self, marker_set):
        self.marker_labels = ["R_ASIS", "L_ASIS", "R_PSIS", "L_PSIS"]
        self.pelvis_set = None
        try:
            self.pelvis_set = {mx: marker_set[mx] for mx in self.marker_labels}
        except KeyError:
            pass

    def origin(self, t):
        nx = np.zeros([4, 3])
        for mx in range(0, len(self.marker_labels)):
            nx[mx, :] = self.pelvis_set[self.marker_labels[mx]].iloc[t, :]
        return np.mean(nx, axis=0)/1000.0

    def get_markers(self, t):
        nx = np.zeros([4, 3])
        for mx in range(0, len(self.marker_labels)):
            nx[mx, :] = self.pelvis_set[self.marker_labels[mx]].iloc[t, :]

        return pd.DataFrame(data= (nx/1000.0).T, columns=self.marker_labels)

class QuickAnalysis:
    def __init__(self):
        pass

    @staticmethod
    def stride(mot_file: str, model: Omodel):
        """
        Function extract strides from mot file using Omodel
        :param mot_file:
        :param model:
        :return:
        """
        sto = StorageIO.load(mot_file, StorageType.mot)
        markers = {}
        leg = {"knee_angle_r": "right_",
               "knee_angle_l": "left_"
               }
        n = model.osim_model.joint_names
        toes = ["R_DP1", "L_DP1"]
        heel = ["R_Heel", "L_Heel"]
        for frame in range(0, sto.data.shape[0]):
            print(frame)
            te = sto.data.iloc[frame, 0]
            d = sto.data.iloc[frame, 1:]
            model.set_joints(d)
            model.update()
            markers[str(te)] = copy.deepcopy(model.markerset)
        tk = [t0 for t0 in markers]
        marker_names = [c for c in markers['0.0'].columns]
        marker_idx = {toes[0]: marker_names.index(toes[0]),
                      toes[1]: marker_names.index(toes[1]),
                      heel[0]: marker_names.index(heel[0]),
                      heel[1]: marker_names.index(heel[1]),
                      }
        frames = np.zeros([len(tk), model.markerset.shape[0] * model.markerset.shape[1] +1])
        for mn in range(0, len(tk)):
            m = markers[tk[mn]]
            nx = m.to_numpy()
            frames[mn, 0] = float(tk[mn])
            ret = np.squeeze(nx.reshape([1, m.shape[1]*3], order='F'))
            frames[mn, 1:] = np.array([float(ret[c]) for c in range(0, ret.shape[0])])

        x = Yatsdo(frames)  # displacement
        xd = Yatsdo(x.get_samples_dev(x.x))     # velocity
        xdd = Yatsdo(xd.get_samples_dev(xd.x)) # acceleration
        dta = {}
        mp.figure()
        for keyw in ["knee_angle_r", "knee_angle_l"]:
            print(keyw)
            dta[keyw] = []
            kd_ = sto.data[keyw]
            ar, _ = find_peaks(kd_, distance=10, height=np.max(kd_) * 0.5)
            nkd_ = -kd_ - np.min(-kd_)
            ar0, _ = find_peaks(nkd_, distance=20, height=np.max(nkd_) * 0.1)

            #a = xdd.data[:, -6:-3]
            hel = heel[0]
            hel_idx = marker_idx[hel]
            toe0 = toes[0]
            toe_idx = marker_idx[toe0]

            if "knee_angle_l" in keyw:
                hel = heel[1]
                hel_idx = marker_idx[hel]
                toe0 = toes[1]
                toe_idx = marker_idx[toe0]
                # a = xdd.data[:, -3:]
                # v = xd.data[:, -3:]
                # dx = x.data[:, -3:]
                # a = xdd.data[:, 1+marker_idx[heel[1]] * 3:1+marker_idx[heel[1]] * 3 + 3]
                # v = xd.data[:, 1+marker_idx[heel[1]] * 3:1+marker_idx[heel[1]] * 3 + 3]
                # dx = x.data[:, 1+marker_idx[heel[1]] * 3:1+marker_idx[heel[1]] * 3 + 3]
            a = xdd.data[:, 1 + hel_idx * 3:1 + hel_idx * 3 + 3]
            v = xd.data[:, 1 + hel_idx * 3:1 + hel_idx * 3 + 3]
            dx = x.data[:, 1 + hel_idx * 3:1 + hel_idx * 3 + 3]
            na = [np.linalg.norm(a[i, 0:3:2]) for i in range(0, a.shape[0])]
            nab = Butterworth.butter_low_filter(na, 6, 100)
            nv = [np.linalg.norm(v[i, 0:3:2]) for i in range(0, a.shape[0])]
            nvb = Butterworth.butter_low_filter(nv, 6, 100)
            nx = [np.linalg.norm(dx[i, 1]) for i in range(0, a.shape[0])]

            at = xdd.data[:, 1+toe_idx*3:1+toe_idx*3+3]
            vt = xd.data[:, 1+toe_idx*3:1+toe_idx*3+3]
            dxt = x.data[:, 1+toe_idx*3:1+toe_idx*3+3]
            # if "knee_angle_l" in keyw:
            #     at = xdd.data[:, 1+marker_idx[toes[1]]*3:1+marker_idx[toes[1]]*3+3]
            #     vt = xd.data[:, 1+marker_idx[toes[1]]*3:1+marker_idx[toes[1]]*3+3]
            #     dxt = x.data[:, 1+marker_idx[toes[1]]*3:1+marker_idx[toes[1]]*3+3]



            nat = [np.linalg.norm(at[i, 0:3:2]) for i in range(0, a.shape[0])]
            nabt = Butterworth.butter_low_filter(nat, 6, 100)
            nvt = [np.linalg.norm(vt[i, 0:3:2]) for i in range(0, a.shape[0])]
            nvbt = Butterworth.butter_low_filter(nvt, 6, 100)
            nxt = [np.linalg.norm(dxt[i, 1]) for i in range(0, a.shape[0])]

            mp.plot(nab, label=keyw+"_nabt")
            mp.plot(nvb, label=keyw+"_nvbt")

            dta[keyw].append(kd_)
            dta[keyw].append(nkd_)
            dta[keyw].append(nab)
            dta[keyw].append(nvb)
            dta[keyw].append(nx)
            dta[keyw].append(ar)
            dta[keyw].append(ar0)
            dta[keyw].append(nabt)
            dta[keyw].append(nvbt)
            dta[keyw].append(nxt)
            dta[keyw].append([keyw+"_kd", keyw+"_nkd", keyw+"_na", keyw+"_nv", keyw+"_nx", keyw+"_kd_peak", keyw+"_nkd_peak", keyw+"_nat", keyw+"_nvt", keyw+"_nxt"])
        mp.legend()
        mp.show()
        mp.figure()
        # mp.plot(dta["knee_angle_r"][0], label=dta["knee_angle_r"][10][0])
        mp.plot(dta["knee_angle_r"][1], label=dta["knee_angle_r"][10][1])
        # mp.plot(dta["knee_angle_r"][2], '_', label=dta["knee_angle_r"][10][2])
        # mp.plot(dta["knee_angle_r"][3], '_', label=dta["knee_angle_r"][10][3])
        # mp.plot(dta["knee_angle_r"][4], '_', label=dta["knee_angle_r"][10][4])
        #mp.plot(dta["knee_angle_r"][5], dta["knee_angle_r"][0].iloc[dta["knee_angle_r"][5]], 'x' , label=dta["knee_angle_r"][10][5])
        mp.plot(dta["knee_angle_r"][6], dta["knee_angle_r"][1].iloc[dta["knee_angle_r"][6]], 'o' , label=dta["knee_angle_r"][10][6])
        # mp.plot(dta["knee_angle_r"][7], '.', label=dta["knee_angle_r"][10][7])
        mp.plot(dta["knee_angle_r"][8], '.', label=dta["knee_angle_r"][10][8])
        # mp.plot(dta["knee_angle_r"][9], '.', label=dta["knee_angle_r"][10][9])
        # mp.plot(dta["knee_angle_l"][0], label=dta["knee_angle_l"][10][0])
        mp.plot(dta["knee_angle_l"][1], label=dta["knee_angle_l"][10][1])
        # mp.plot(dta["knee_angle_l"][2], '_', label=dta["knee_angle_l"][10][2])
        # mp.plot(dta["knee_angle_l"][3], '_', label=dta["knee_angle_l"][10][3])
        # mp.plot(dta["knee_angle_l"][4], '_', label=dta["knee_angle_l"][10][4])
        #mp.plot(dta["knee_angle_l"][5], dta["knee_angle_l"][1].iloc[dta["knee_angle_l"][5]], 'x', label=dta["knee_angle_l"][10][5])
        mp.plot(dta["knee_angle_l"][6], dta["knee_angle_l"][1].iloc[dta["knee_angle_l"][6]], 'o', label=dta["knee_angle_l"][10][6])
        #mp.plot(dta["knee_angle_l"][7], '.', label=dta["knee_angle_l"][10][7])
        mp.plot(dta["knee_angle_l"][8], '.', label=dta["knee_angle_l"][10][8])
        # mp.plot(dta["knee_angle_l"][9], '.', label=dta["knee_angle_l"][10][9])
        mp.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')
        mp.tight_layout()
        mp.show()
        print()
        pass


class QuickFixes:
    @staticmethod
    def repair_trc(src, save_loc, fill=False):
        """
        This method is used to fix trc files exported from nexus. Nexus puts an extra tab space in the header.
        :param src: file or dir of files or list of files that need to be fixed
        :param save_loc: directory location of save
        :return:
        """
        if not os.path.exists(save_loc):
            os.makedirs(save_loc)
        if isinstance(src, list):
            bar = tqdm(total=len(src), desc="fixing trc files", ascii=False,
                       ncols=100,
                       colour="#6e5b5b")
            for f in src:
                filepath = os.path.split(f)
                t = StorageIO.trc_reader(f, fill_data=fill)
                t.write(save_loc+filepath[1])
                bar.update(1)
            bar.close()
        if isinstance(src, str):
            if os.path.isdir(src):
                if not src.endswith("/") or src.endswith("\\"):
                    src += "/"
                    d = [src + f for f in os.listdir(src) if f.endswith('.trc')]
                    bar = tqdm(total=len(d), desc="fixing trc files", ascii=False,
                               ncols=100,
                               colour="#6e5b5b")
                    for f in d:
                        filepath = os.path.split(f)
                        t = StorageIO.trc_reader(f, fill_data=fill)
                        t.write(save_loc + filepath[1])
                        bar.update(1)
                    bar.close()
                else:
                    filepath = os.path.split(src)
                    t = StorageIO.trc_reader(src)
                    t.write(save_loc + filepath[1])


def trc_analysis():
    global t, ik

    trc = "M:/Mocap/P012/New Session/Gap_Fill/Straight normal 1.trc"
    trc_data = StorageIO.trc_reader(trc)
    toes = ['L_DP1', 'R_DP1']
    heels = ['L_Heel', 'R_Heel']
    toes_L = np.zeros([trc_data.marker_set[toes[0]].shape[0], 3])
    toes_L[:, 0] = trc_data.x
    toes_L[:, 1:] = trc_data.marker_set[toes[0]].iloc[:, 0:3:2]
    toe_l = Yatsdo(pd.DataFrame(data=toes_L, columns=["time", "x", "z"]))
    heels_L = np.zeros([trc_data.marker_set[heels[0]].shape[0], 3])
    heels_L[:, 0] = trc_data.x
    heels_L[:, 1:] = trc_data.marker_set[heels[0]].iloc[:, 0:3:2]
    heel_l = Yatsdo(pd.DataFrame(data=heels_L, columns=["time", "x", "z"]))
    toes_R = np.zeros([trc_data.marker_set[toes[1]].shape[0], 3])
    toes_R[:, 0] = trc_data.x
    toes_R[:, 1:] = trc_data.marker_set[toes[1]].iloc[:, 0:3:2]
    toe_R = Yatsdo(pd.DataFrame(data=toes_R, columns=["time", "x", "z"]))
    heels_R = np.zeros([trc_data.marker_set[heels[1]].shape[0], 3])
    heels_R[:, 0] = trc_data.x
    heels_R[:, 1:] = trc_data.marker_set[heels[1]].iloc[:, 0:3:2]
    heel_R = Yatsdo(pd.DataFrame(data=heels_R, columns=["time", "x", "z"]))
    t0 = toe_l.get_samples_dev(toe_l.x)
    t1 = np.array([[t0[c, 0], np.linalg.norm(t0[c, 1:])] for c in range(0, t0.shape[0])])
    t1[:, 1] = Butterworth.butter_low_filter(t1[:, 1], 6, 100)
    t1[:, 1] = t1[:, 1] / np.max(t1[:, 1])
    toev_l = Yatsdo(t1)
    h0 = heel_l.get_samples_dev(heel_l.x)
    h1 = np.array([[h0[c, 0], np.linalg.norm(h0[c, 1:])] for c in range(0, h0.shape[0])])
    h1[:, 1] = Butterworth.butter_low_filter(h1[:, 1], 6, 100)
    h1[:, 1] = h1[:, 1] / np.max(h1[:, 1])
    heelv_l = Yatsdo(h1)
    t2 = toe_R.get_samples_dev(toe_R.x)
    t3 = np.array([[t2[c, 0], np.linalg.norm(t2[c, 1:])] for c in range(0, t2.shape[0])])
    t3[:, 1] = Butterworth.butter_low_filter(t3[:, 1], 6, 100)
    t3[:, 1] = t3[:, 1] / np.max(t3[:, 1])
    toev_R = Yatsdo(t3)
    h2 = heel_R.get_samples_dev(heel_R.x)
    h3 = np.array([[h2[c, 0], np.linalg.norm(h2[c, 1:])] for c in range(0, h2.shape[0])])
    h3[:, 1] = Butterworth.butter_low_filter(h3[:, 1], 6, 100)
    h3[:, 1] = h3[:, 1] / np.max(h3[:, 1])
    heelv_R = Yatsdo(h3)
    t4 = toev_R.get_samples_dev(toe_R.x)
    t5 = np.array([[t4[c, 0], np.linalg.norm(t4[c, 1:])] for c in range(0, t4.shape[0])])
    t5[:, 1] = Butterworth.butter_low_filter(t5[:, 1], 6, 100)
    t5[:, 1] = t5[:, 1] / np.max(t5[:, 1])
    toea_R = Yatsdo(t5)
    h4 = heelv_R.get_samples_dev(heel_R.x)
    h5 = np.array([[h4[c, 0], np.linalg.norm(h4[c, 1:])] for c in range(0, h4.shape[0])])
    h5[:, 1] = Butterworth.butter_low_filter(h5[:, 1], 6, 100)
    h5[:, 1] = h5[:, 1] / np.max(h5[:, 1])
    heela_R = Yatsdo(h5)
    t6 = toev_l.get_samples_dev(toe_l.x)
    t7 = np.array([[t6[c, 0], np.linalg.norm(t6[c, 1:])] for c in range(0, t6.shape[0])])
    t7[:, 1] = Butterworth.butter_low_filter(t7[:, 1], 6, 100)
    t7[:, 1] = t7[:, 1] / np.max(t7[:, 1])
    toea_l = Yatsdo(t7)
    h6 = heelv_l.get_samples_dev(heel_l.x)
    h7 = np.array([[h6[c, 0], np.linalg.norm(h6[c, 1:])] for c in range(0, h6.shape[0])])
    h7[:, 1] = Butterworth.butter_low_filter(h7[:, 1], 6, 100)
    h7[:, 1] = h7[:, 1] / np.max(h7[:, 1])
    heela_l = Yatsdo(h7)
    mp.figure()
    mp.plot(toev_l.data[:, 0], toev_l.data[:, 1:], label="toev_l")
    mp.plot(heelv_l.data[:, 0], heelv_l.data[:, 1:], label="heelv_l")
    mp.plot(toev_R.data[:, 0], toev_R.data[:, 1:], label="toev_R")
    mp.plot(heelv_R.data[:, 0], heelv_R.data[:, 1:], label="heelv_R")
    mp.plot(toea_R.data[:, 0], toea_R.data[:, 1:], label="toea_R")
    mp.plot(heela_R.data[:, 0], heela_R.data[:, 1:], label="heela_R")
    mp.plot(toea_l.data[:, 0], toea_l.data[:, 1:], label="toea_l")
    mp.plot(heela_l.data[:, 0], heela_l.data[:, 1:], label="heela_l")
    mp.legend()
    mp.show()

def test0():
    ik = "M:/Mocap/P012/New Session/Gap_Fill/motion_files/Straight AR 1.sto"


    trc = "M:/Mocap/P012/New Session/Gap_Fill/Straight AR 1.trc"
    imu = "M:/Mocap/Movella/P012/Straight AR 1/Pelvis_imu_ori.csv"
    imu_v = "M:/Mocap/Movella/P012/Straight AR 1/Pelvis_imu_vec3.csv"

    sto = StorageIO.load(ik, StorageType.mot)
    sto_x = Yatsdo(sto.data)
    trc_x = TRC.read(trc)
    imu_data = pd.read_csv(imu)
    imu_vec = pd.read_csv(imu_v)
    model = Omodel()
    markers = {}
    for frame in range(0, sto.data.shape[0]):
        print(frame)
    te = sto.data.iloc[frame, 0]
    d = sto.data.iloc[frame, 1:]
    model.set_joints(d)
    model.update()
    markers[str(te)] = copy.deepcopy(model.markerset)
    tk = [t0 for t0 in markers]
    frames = np.zeros([len(tk), model.markerset.shape[0] * model.markerset.shape[1] + 1])
    col_ = None
    for mn in range(0, len(tk)):
        m = markers[tk[mn]]
    if col_ is None:
        col_ = [c for c in m.columns]
    nx = m.to_numpy()
    frames[mn, 0] = float(tk[mn])
    ret = np.squeeze(nx.reshape([1, m.shape[1] * 3], order='F'))
    frames[mn, 1:] = np.array([float(ret[c]) for c in range(0, ret.shape[0])])
    col = ["Time"]
    for c in col_:
        col.append(c + "_X")
    col.append(c + "_Y")
    col.append(c + "_Z")
    data = pd.DataFrame(data=frames, columns=col)
    get_list = [c for c in col if "R_PSIS" in c or "L_PSIS" in c or "R_ASIS" in c or "L_ASIS" in c]
    get_p = [c for c in col if "R_PSIS" in c or "L_PSIS" in c]
    get_list.insert(0, "Time")
    pelvis_data = data[get_list]
    pik = {}
    clo = [c for c in pelvis_data.columns if 'Time' not in c]
    for i in range(0, len(clo)):
        cl = clo[i]
    if "R_ASIS" in cl:
        try:
            pik["R_ASIS"].append(i)
        except KeyError:
            pik["R_ASIS"] = [i]
    if "L_ASIS" in cl:
        try:
            pik["L_ASIS"].append(i)
        except KeyError:
            pik["L_ASIS"] = [i]
    if "R_PSIS" in cl:
        try:
            pik["R_PSIS"].append(i)
        except KeyError:
            pik["R_PSIS"] = [i]
    if "L_PSIS" in cl:
        try:
            pik["L_PSIS"].append(i)
        except KeyError:
            pik["L_PSIS"] = [i]
    pelvis_markers = pelvis_data.iloc[0, 1:].to_numpy()
    pmm = pelvis_data[get_p].to_numpy()
    pelvis_p = pmm[0, :]
    pelvis_markers0 = np.squeeze(pelvis_markers.reshape([3, 4], order='F'))
    mean_pelvis = np.mean(np.squeeze(pelvis_p.reshape([3, 2], order='F')), 1)
    pelvis_markers0 = pelvis_markers0 - np.atleast_2d(mean_pelvis).T
    imu_data.iloc[:, 0] = imu_data.iloc[:, 0] - imu_data.iloc[0, 0]
    imu_vec.iloc[:, 0] = imu_vec.iloc[:, 0] - imu_vec.iloc[0, 0]
    steps = int(imu_data.iloc[-1, 0] / sto_x.dt)
    dx = [n * sto_x.dt for n in range(0, steps)]
    pelvis_imu = Yatsdo(imu_data)
    pelvis_imu_resample = Yatsdo(pelvis_imu.get_samples(dx, as_pandas=True))

    pelvis_imu_vec3 = Yatsdo(imu_vec)
    pelvis_imu_vec3_resample = Yatsdo(pelvis_imu_vec3.get_samples(dx, as_pandas=True))

    ref_pelvis = None
    pelvis_markers1 = None
    traj = []
    angles = []
    for i in range(0, steps):
        q = pelvis_imu_resample.data[i, 1:]
        indx = pelvis_imu_vec3_resample.column_labels.index("m_position_X")
        p = pelvis_imu_vec3_resample.data[i, indx:indx + 3]
        q0 = [q[1], q[2], q[3], q[0]]
        r0 = Rotation.from_quat(q0)
        e0 = r0.as_euler('xyz', degrees=True)
        angles.append(q)
        m0 = r0.as_matrix()
        t0 = np.eye(4)
        t0[:3, :3] = m0
        t0[:3, 3] = p / 1000.0
        if ref_pelvis is None:
            ref_pelvis = t0
            # pelvis_markers1 = pelvis_markers0
        m1 = np.matmul(t0, np.linalg.inv(ref_pelvis))
        pt = np.ones([4, pelvis_markers0.shape[1]])
        pt[:3, :] = pelvis_markers0
        tra = np.matmul(m1, pt)
        # pelvis_markers1 = tra[:3, :]
        traj.append((tra[:3, :]).reshape(1, pelvis_markers0.shape[1] * 3, order='F'))
        pass
    a = np.concatenate(traj)
    e = np.array(angles)
    b = trc_x.data
    pel = {}
    for i in range(0, len(trc_x.column_labels)):
        cl = trc_x.column_labels[i]
        if "R_ASIS" in cl:
            try:
                pel["R_ASIS"].append(i)
            except KeyError:
                pel["R_ASIS"] = [i]
        if "L_ASIS" in cl:
            try:
                pel["L_ASIS"].append(i)
            except KeyError:
                pel["L_ASIS"] = [i]
        if "R_PSIS" in cl:
            try:
                pel["R_PSIS"].append(i)
            except KeyError:
                pel["R_PSIS"] = [i]
        if "L_PSIS" in cl:
            try:
                pel["L_PSIS"].append(i)
            except KeyError:
                pel["L_PSIS"] = [i]

    for i in pik:
        trc_x.data[:trc_x.data.shape[0], pel[i]] = a[:trc_x.shape[0], pik[i]] * 1000
    trc_x.update()
    trc_x.write("M:/Mocap/P012/New Session/Gap_Fill/test/Straight AR 2.trc")
    mp.figure()
    mp.plot(a)
    mp.show()


def reshape_3xm_data(joints: str):
    jc = os.path.split(joints)
    jcf = jc[1][:jc[1].index(".csv")]
    jcfo = "{0}/{1}_2.csv".format(jc[0], jcf)
    j = pd.read_csv(joints)
    for i in range(0, j.shape[0]):
        c = j.to_numpy()[i, 1:]
        d = c.reshape([3, int(c.shape[0] / 3)])
        e = d.reshape([1, c.shape[0]], order='F')
        j.iloc[i, 1:] = e
    j.to_csv(jcfo, index=False)


def fixing_joint_file():
    movella = "M:/Mocap/Movella/"
    mov = [f for f in os.listdir(movella) if os.path.isdir(movella + f) and "P031" in f]
    mov.sort()
    tasks = {
        f: [g for g in os.listdir("{0}/{1}/".format(movella, f)) if os.path.isdir("{0}/{1}/{2}".format(movella, f, g))]
        for f in mov}
    # joint_file = "M:/Mocap/Movella/P012/Straight AR 1/joint_angles.csv"
    num = len([d for d in tasks])
    bar = tqdm(total=num, desc="fixing_joint_file", ascii=False,
               ncols=100,
               colour="#6e5b5b")
    for f in tasks:
        for tk in tasks[f]:
            joint_file = "{0}{1}/{2}/joint_angles.csv".format(movella, f, tk)
            if os.path.exists(joint_file):
                reshape_3xm_data(joint_file)
        bar.update(1)
    bar.close()


def fixing_imu_vec3_file():
    movella = "M:/Mocap/Movella/"
    mov = [f for f in os.listdir(movella) if os.path.isdir(movella + f) and "P031" in f]
    mov.sort()
    tasks = {
        f: [g for g in os.listdir("{0}/{1}/".format(movella, f)) if os.path.isdir("{0}/{1}/{2}".format(movella, f, g))]
        for f in mov}
    # imu_file = "M:/Mocap/Movella/P012/Straight normal 1/Pelvis_imu_vec3.csv"
    # reshape_3xm_data(imu_file)
    num = len([d for d in tasks])
    bar = tqdm(total=num, desc="fixing_imu_vec3_file", ascii=False,
               ncols=100,
               colour="#6e5b5b")
    for f in tasks:
        for tk in tasks[f]:
            fold = [l for l in os.listdir("{0}{1}/{2}/".format(movella, f, tk)) if "_imu_vec3.csv" in l]
            for c in fold:
                imu_file = "{0}{1}/{2}/{3}".format(movella, f, tk, c)
                if os.path.exists(imu_file):
                    reshape_3xm_data(imu_file)
        bar.update(1)
    bar.close()


def grab_new_exports():
    movella = "M:/Mocap/Movella/"
    movella_re = "M:/Mocap/Movella_Re/"
    mov = [f for f in os.listdir(movella) if os.path.isdir(movella + f)]
    mov.sort()
    tasks = {
        f: [g for g in os.listdir("{0}/{1}/".format(movella, f)) if os.path.isdir("{0}/{1}/{2}".format(movella, f, g))]
        for f in mov}
    # imu_file = "M:/Mocap/Movella/P012/Straight normal 1/Pelvis_imu_vec3.csv"
    # reshape_3xm_data(imu_file)
    counter = 0
    num = len([d for d in tasks])
    bar = tqdm(total=num, desc="calculating number of files", ascii=False,
               ncols=100,
               colour="#6e5b5b")
    for f in tasks:
        for tk in tasks[f]:
            wkdir = "{0}{1}/{2}/".format(movella, f, tk)
            files_imu_vec3 = [l for l in os.listdir(wkdir) if
                              l.endswith("_imu_vec3_2.csv")]

            files_imu_joint = [l for l in os.listdir(wkdir) if
                               l.endswith("joint_angles_2.csv")]
            counter += len(files_imu_joint)
            file_imu_ori = [l for l in os.listdir(wkdir) if
                            l.endswith("_imu_ori.csv")]
            if len(files_imu_vec3) == 0 or len(files_imu_joint) == 0 or len(file_imu_ori) == 0:
                continue
            counter += len(files_imu_vec3)
            counter += len(files_imu_joint)
            counter += len(file_imu_ori)
        bar.update(1)
    bar.close()

    bar2 = tqdm(total=counter, desc="exporting files", ascii=False,
               ncols=100,
               colour="#6e5b5b")
    for f in tasks:
        for tk in tasks[f]:
            wkdir = "{0}{1}/{2}/".format(movella, f, tk)
            outdir = "{0}{1}/{2}/".format(movella_re, f, tk)
            if not os.path.exists(outdir):
                os.makedirs(outdir)
            files_imu_vec3 = [l for l in os.listdir(wkdir) if
                              l.endswith("_imu_vec3_2.csv")]
            files_imu_joint = [l for l in os.listdir(wkdir) if
                              l.endswith("joint_angles_2.csv")]
            file_imu_ori = [l for l in os.listdir(wkdir) if
                              l.endswith("_imu_ori.csv")]
            if len(files_imu_vec3) == 0 or len(files_imu_joint) == 0 or len(file_imu_ori) == 0:
                continue
            for j in files_imu_vec3:
                filename_in = "{0}{1}/{2}/{3}".format(movella, f, tk, j)
                filename_out = "{0}{1}/{2}/{3}".format(movella_re, f, tk, j)
                copyfile(filename_in, filename_out)
                bar2.update(1)

            for j in files_imu_joint:
                filename_in = "{0}{1}/{2}/{3}".format(movella, f, tk, j)
                filename_out = "{0}{1}/{2}/{3}".format(movella_re, f, tk, j)
                copyfile(filename_in, filename_out)
                bar2.update(1)

            for j in file_imu_ori:
                filename_in = "{0}{1}/{2}/{3}".format(movella, f, tk, j)
                filename_out = "{0}{1}/{2}/{3}".format(movella_re, f, tk, j)
                copyfile(filename_in, filename_out)
                bar2.update(1)

            pass
    bar2.close()


def get_position():
    movella_re = "M:/Mocap/Movella_Re/"
    mov = [f for f in os.listdir(movella_re) if os.path.isdir(movella_re + f)]
    mov.sort()
    tasks = {
        f: [g for g in os.listdir("{0}/{1}/".format(movella_re, f)) if os.path.isdir("{0}/{1}/{2}".format(movella_re, f, g))]
        for f in mov}
    # imu_file = "M:/Mocap/Movella/P012/Straight normal 1/Pelvis_imu_vec3.csv"
    # reshape_3xm_data(imu_file)
    counter = 0
    num = len([d for d in tasks])
    bar = tqdm(total=num, desc="calculating number of files", ascii=False,
               ncols=100,
               colour="#6e5b5b")
    for f in tasks:
        for tk in tasks[f]:
            wkdir = "{0}{1}/{2}/".format(movella_re, f, tk)
            if os.path.exists(wkdir+"imu_pos.trc"):
                continue
            files_imu_vec3 = {l[:l.rindex('.')]: pd.read_csv(wkdir+l) for l in os.listdir(wkdir) if
                              l.endswith("_imu_vec3_2.csv")}
            imu_cols = [c for c in files_imu_vec3]
            try:
                t = files_imu_vec3[imu_cols[0]]['time']
                imu = np.zeros([len(t), len(imu_cols)*3+1])
                imu[:, 0] = np.round(t - t[0], 5)
                imu_pd_col = ['time']
                for i in range(0, len(imu_cols)):
                    imu_pd_col.append(imu_cols[i].split('_')[0]+"_X")
                    imu[:, i * 3 + 1] = files_imu_vec3[imu_cols[i]]['m_position_X']
                    imu_pd_col.append(imu_cols[i].split('_')[0]+"_Y")
                    imu[:, i * 3 + 2] = files_imu_vec3[imu_cols[i]]['m_position_Y']
                    imu_pd_col.append(imu_cols[i].split('_')[0]+"_Z")
                    imu[:, i * 3 + 3] = files_imu_vec3[imu_cols[i]]['m_position_Z']
                imu_pd = pd.DataFrame(data=imu, columns=imu_pd_col)
                trc = TRC(data=imu_pd)
                headers = trc.headers
                dt = np.mean((imu[1:, 0]-imu[0:-1, 0]))
                headers[MocapFlags.DataRate] = 1/dt
                headers[MocapFlags.CameraRate] = 1 / dt
                headers[MocapFlags.NumFrames] = imu.shape[0]
                headers[MocapFlags.NumMarkers] = len(imu_cols)
                headers[MocapFlags.Units] = 'm'
                headers[MocapFlags.OrigDataRate] = 1 / dt
                headers[MocapFlags.OrigDataStartFrame] = 0
                headers[MocapFlags.OrigNumFrames] = imu.shape[0]
                trc.headers = headers
                trc.update()
                trc.write(wkdir+"imu_pos.trc")
            except IndexError:
                pass
            except KeyError:
                pass
            except ValueError:
                pass
            pass
        bar.update(1)
    bar.close()


def export_as_sto():
    fixing_joint_file()
    fixing_imu_vec3_file()
    omap = pd.read_excel("M:/Documents/movella2opensimMap.xlsx")
    movella_joints = omap['movella'].to_list()
    opensim_joints = omap['Opensim'].to_list()
    ik_template_sto = "M:/Mocap/P012/New Session/Gap_Fill/motion_files/Straight AR 1.sto"
    opensim_sto = StorageIO.load(ik_template_sto)
    opensim_ik_list = opensim_sto.data
    opensim_shortlist = [c for c in range(0, len(movella_joints)) if str(opensim_joints[c]) != 'nan']
    movella = "M:/Mocap/Movella/"
    mov = [f for f in os.listdir(movella) if os.path.isdir(movella + f) if int(f[1:]) > 25]
    mov.sort()
    tasks = {
        f: [g for g in os.listdir("{0}/{1}/".format(movella, f)) if os.path.isdir("{0}/{1}/{2}".format(movella, f, g))]
        for f in mov}
    num = len([d for d in tasks])
    bar = tqdm(total=num, desc="convert to sto", ascii=False,
               ncols=100,
               colour="#6e5b5b")
    for f in tasks:
        for tk in tasks[f]:
            joint_file = "{0}{1}/{2}/joint_angles_2.csv".format(movella, f, tk)
            # if int(f[1:]) > 25:
            #     joint_file = "{0}{1}/{2}/joint_angles.csv".format(movella, f, tk)
            if not os.path.exists(joint_file):
                continue
            jc = os.path.split(joint_file)
            jcf = jc[1][:jc[1].index(".csv")]
            jcfo = "{0}/{1}_opensim.sto".format(jc[0], jcf)
            j = pd.read_csv(joint_file)
            get_data = j[[movella_joints[c] for c in opensim_shortlist]]
            cols = [opensim_joints[c] for c in opensim_shortlist]
            get_data.columns = cols
            data = np.zeros([get_data.shape[0], opensim_ik_list.shape[1]])
            output_cols = [c for c in opensim_ik_list.columns]

            for c in range(0, len(cols)):
                try:
                    b = get_data.iloc[:, c]
                    e = output_cols.index(cols[c])

                    data[:, e] = b.to_list()
                    if "knee" in cols[c].lower():
                        data[:, e] = -1.0*data[:, e]
                except ValueError:
                    print(c)
                    pass
                except AttributeError:
                    print(c)
                    pass
            opensim_sto.data = pd.DataFrame(data=data,
                                            columns=output_cols)
            opensim_sto.info["header"][2] = "nRows={0}\n".format(get_data.shape[0])
            opensim_sto.write_mot(jcfo)
        bar.update(1)
    bar.close()
    pass


def run_bk():
    b = Model("C:/Users/tyeu008/Documents/Repos/bluepython/yatpkg_src/yatpkg/kinematics/articulating_ssm_gui/models/Full_body_template.osim")
    # QuickFixes.repair_trc(["M:/Mocap/P003/Session 1/"+f for f in os.listdir("M:/Mocap/P003/Session 1/") if f.endswith('.trc')], "M:/Mocap/P003/Session 1/repaired_filled/", fill=True)
    # t = StorageIO.trc_reader("M:/Mocap/P012/New Session/static01.trc")
    # t.z_up_to_y_up()
    # t.write("M:/Mocap/P012/New Session/static01_y.trc")
    # trc_analysis()
    # QuickAnalysis.stride(ik, t)

    # ik = "M:/Mocap/P012/New Session/Gap_Fill/motion_files/Straight AR 1.sto"
    # st0 = OSIMStorage.read(ik)
    # export_as_sto()
    # m = t.markerset
    #
    # print("\n")
    # jc = [0 for f in t.locked_id]
    # trc = StorageIO.trc_reader("M:/Mocap/P012/New Session/static01.trc")
    # pelvis_bone = Pelvis(trc.marker_set)
    # for x in range(0, int(trc.headers[MocapFlags.NumFrames.value])):
    #     n = pelvis_bone.origin(x)
    #     c0 = t.markerset[pelvis_bone.marker_labels]
    #     nx = pelvis_bone.get_markers(x)
    #     nt = nx.to_numpy()
    #     ct = c0.to_numpy()
    #     r = Cloud.rigid_body_transform(ct, nt)
    #     rx = Rotation.from_matrix(r[0:3, 0:3])
    #     re = rx.as_euler('xzy', degrees=False)
    #     t.set_joint("ground_pelvis", [re[0], re[1], re[2], r[3, 0], r[3, 1], r[3, 2]])
    #
    #     t.update()
    #     c = t.markerset[pelvis_bone.marker_labels]
    #     df = c-nx
    #     print(n)
    # t.update()
    # rows = trc.data.shape[0]
    # dt = trc.marker_set
    # mc = [c for c in m.columns]
    # dpi = np.zeros(m.shape)
    # i = 0
    # for n in mc:
    #     x = dt[n]
    #     dpi[:, i] = x.iloc[1, :]
    #     i += 1
    # joints = np.squeeze(np.zeros([1, len(t.locked_id)]))
    # t.obj_func(joints, dpi)


def test1():
    """
    Read c3d file
    :return:
    """
    ike = "M:/Mocap/P012/New Session/Gap_Fill/motion_files/Straight AR 1.sto"
    sto = StorageIO.load(ike, StorageType.mot)
    sto_x = Yatsdo(sto.data)
    dt = sto_x.get_samples_dev(sto_x.x)
    dfp = pd.DataFrame(data=dt, columns=sto_x.col_labels)
    ik_cols = sto_x.col_labels
    root = "M:/Mocap/P012/New Session/"
    omodel = "M:/Models/P012.osim"
    model = Omodel(omodel)
    jointnames = model.osim_model.joint_names
    trc = TRC.create_from_c3d(root+"Straight normal 1.c3d")
    trc.z_up_to_y_up()
    trc.update()
    markers = copy.deepcopy(model.markerset)

    i = 0

    order = [c for c in markers.columns]
    trc_data_raw = np.zeros(markers.shape)
    for c in range(0, len(order)):
        idxc = c
        idx = order[c]
        print(order[c])
        m = trc.marker_set[order[c]].to_numpy()
        nm = m[i, :]/1000.0
        trc_data_raw[:, c] = nm
    trc_frame = pd.DataFrame(data=trc_data_raw, columns=order)
    co = [c for c in range(0, len(order)) if not np.isnan(trc_data_raw[0, c])]

    tfx = trc_frame.to_numpy()
    tf = tfx[:, co]
    mf = markers.iloc[:, co].to_numpy()
    r0 = Cloud.rigid_body_transform(mf, tf)
    mfo = np.ones([4, mf.shape[1]])
    mfo[:3, :] = mf
    mf0 = np.matmul(r0, mfo)
    r1 = Cloud.rigid_body_transform(tf, mf0[:3, :])
    check = np.sum(np.abs(r1 - np.eye(4)))
    if not (check < 1e-10):
        return

    def cost(x, jname, tf0, co0, cols=None):
        if jname == 'all':
            v = pd.DataFrame(data=np.atleast_2d(x), columns=cols)
            model.set_joints(v)
            model.update()
        else:
            model.set_joint(jname, x)
        m0 = model.markerset.iloc[:, co0].to_numpy()
        t0 = tf0[:, co0]
        error = [np.linalg.norm(t0[:, v] - m0[:, v]) for v in range(0, m0.shape[1])]
        ret = np.abs([e*e for e in error])
        print(x)
        weight = 100
        if np.abs(x[0]) > np.pi:
            return weight
        if x.shape[0] > 1:
            if np.abs(x[1]) > np.pi:
                return weight
        if x.shape[0] > 2:
            if np.abs(x[2]) > np.pi:
                return weight
        return np.mean(ret)

    # pelvis first
    pelvis = ["R_ASIS", "L_ASIS", "R_PSIS", "L_PSIS"]
    gpc = [c for c in range(0, len(order)) if not np.isnan(trc_data_raw[i, c]) and order[c] in pelvis]
    m0 = model.markerset.iloc[:, gpc].to_numpy()
    t0 = trc_frame.iloc[:, gpc].to_numpy()
    mx = Cloud.rigid_body_transform(m0, t0)
    m0a = np.ones([4, m0.shape[1]])
    m0a[:3, :] = m0
    m0b = np.matmul(mx, m0a)
    r = Rotation.from_matrix(mx[:3, :3])
    zxy = r.as_euler('zxy')
    gp = np.array([zxy[2], zxy[0], zxy[1], mx[0, 3], mx[1, 3], mx[2, 3]])
    gp = np.array([0.0 for c in range(1, len(ik_cols)-1)])
    # results = minimize(cost, gp, method="SLSQP", args=('all', tfx, co, [ik_cols[c] for c in range(1, len(ik_cols)-1)]))
    results = minimize(cost, gp, args=('all', tfx, co, [ik_cols[c] for c in range(1, len(ik_cols) - 1)]))
    gp = results.x

    hip_r = [0 for x in range(0, 3)]
    leg_r = ['R_LatKnee', 'R_FibHead', 'R_MedKnee', 'R_MidShank', 'R_DP1', 'R_MT2', 'R_MT5', 'R_MedAnkle', 'R_LatAnkle',
             'R_Heel']
    # hip_m = ['R_ASIS', 'L_ASIS', 'R_PSIS', 'L_PSIS', leg_m[0], leg_m[2]]
    hip_m = [leg_r[0], leg_r[1], leg_r[2], leg_r[3]]
    hip_rim = [c for c in range(0, len(order)) if not np.isnan(trc_data_raw[i, c]) and order[c] in hip_m]
    results = minimize(cost, hip_r, method="SLSQP", args=('hip_r', tfx, hip_rim,))
    hip_r = results.x
    hip_l = [0 for x in range(0, 3)]
    hip_l = copy.deepcopy(hip_r)
    leg_l = ['L_LatKnee', 'L_FibHead', 'L_MedKnee', 'L_MidShank', 'L_DP1', 'L_MT2', 'L_MT5', 'L_MedAnkle', 'L_LatAnkle',
              'L_Heel']
    hip_m = [leg_l[0], leg_l[1], leg_l[2]]
    hip_lim = [c for c in range(0, len(order)) if not np.isnan(trc_data_raw[i, c]) and order[c] in hip_m]
    results = minimize(cost, hip_l, method="SLSQP", args=('hip_l', tfx, hip_lim, ))
    hip_l = results.x

    knee_r = np.array([0 for x in range(0, 1)])
    knee_r_m = [leg_r[3], leg_r[7], leg_r[8]]
    # knee_r_m = [leg_r[3]]
    knee_m = [c for c in range(0, len(order)) if not np.isnan(trc_data_raw[i, c]) and order[c] in knee_r_m]
    results = minimize(cost, knee_r, method="SLSQP", args=('walker_knee_r', tfx, knee_m,))
    knee_r = results.x

    knee_l = np.array([0 for x in range(0, 1)])
    knee_l_m = [leg_l[3], leg_l[7], leg_l[8]]
    # knee_l_m = [leg_l[3]]
    knee_m = [c for c in range(0, len(order)) if not np.isnan(trc_data_raw[i, c]) and order[c] in knee_l_m]
    results = minimize(cost, knee_l, method="SLSQP", args=('walker_knee_l', tfx, knee_m,))
    knee_l = results.x
    print(gp)
    print(hip_r)
    print(knee_r)
    print(hip_l)
    print(knee_l)
    v = np.array([np.arccos(np.cos(gp[ix])) for ix in range(0, 3)])
    gp[:3] = v * (180/np.pi)
    hip_r = hip_r * (180/np.pi)
    hip_l = hip_l * (180 / np.pi)
    knee_r = knee_r * (180 / np.pi)
    knee_l = knee_l * (180 / np.pi)
    print(gp)
    print(hip_r)
    print(knee_r)
    print(hip_l)
    print(knee_l)


def process_trial(wkdir="M:/test/", out_root="M:/osim/", omodel="M:/Models/P011.osim", trial_c3d="Straight normal 1.c3d", root="M:/Mocap/P011/New Session/", template='M:/template/Straight normal 1.xml'):
    check = [f for f in os.listdir(wkdir) if f.endswith(".trc") or f.endswith('.sto')]
    bar = tqdm(total=len(check), desc="Clean up working dir {0}".format(wkdir), ascii=False,
               ncols=100,
               colour="#6e5b5b")
    for c in check:
        p = wkdir+c
        os.remove(p)
        bar.update(1)
    bar.close()

    model = Omodel(omodel)
    mk = copy.deepcopy(model.markerset)

    # read task
    trc_name = trial_c3d[:trial_c3d.rindex('.c3d')]
    trc = TRC.create_from_c3d(root + trial_c3d)
    trc.z_up_to_y_up()
    trc.write("{0}{1}.trc".format(wkdir, trc_name))
    x = trc.data[:, 1]
    factor = 1.0
    if trc.headers['Units'] == 'mm':
        factor = 1000.0
    previous_sto = None
    previous_sto2 = None
    # run through marker set for unlabels
    unlabelled = {m: trc.marker_set[m] for m in trc.marker_set if m.startswith("*")}
    ik = InverseKinematicsTool(template)
    b = ik.get_output_motion_file()
    bf = "{0}/{1}.sto".format(b[:b.rindex("/")], trc_name)
    ik.set_output_motion_file(bf)
    k = bf
    kp = os.path.split(k)
    for i in range(0, x.shape[0]):
        data_copied = copy.deepcopy(trc)
        data_copied.headers['Units'] = 'm'
        t = x[i]
        ik.setStartTime(t)
        ik.setEndTime(t)
        dat = {}
        out = {}
        applies = {}
        weights = {}
        num_markers = 0
        mks = None
        if previous_sto is not None:
            filename = "{2}/{0}_{1:06d}.sto".format(kp[1][:kp[1].rindex(".sto")], int(x[i - 1] * 1000), kp[0])
            if not os.path.exists(filename):
                os.listdir(kp[0])
            if not os.path.exists(filename):
                continue
            sto_t = StorageIO.load(filename, StorageType.mot)
            v = sto_t.data.iloc[0, 1:]
            model.set_joints(v)
            model.update()
            mks = copy.deepcopy(model.markerset)
        mks2 = None
        if previous_sto2 is not None:
            filename = "{2}/{0}_{1:06d}.sto".format(kp[1][:kp[1].rindex(".sto")], int(x[i - 2] * 1000), kp[0])
            sto_t = StorageIO.load(filename, StorageType.mot)
            v = sto_t.data.iloc[0, 1:]
            model.set_joints(v)
            model.update()
            mks2 = copy.deepcopy(model.markerset)

        for m in trc.marker_set:
            if m in mk:
                marker = trc.marker_set[m]
                da = np.atleast_2d(marker.to_numpy()[i, :])/factor
                dat[m] = pd.DataFrame(data=da, columns=[c for c in marker.columns])
                nans = [1 for v in range(0, 3) if np.isnan(dat[m].iloc[0, v])]
                if np.sum(nans) == 3:
                    applies[m] = False
                    weights[m] = 0.01
                    if num_markers < 36 and previous_sto is not None:
                        ka = np.array(mks[m].to_list())
                        k0 = ka
                        if previous_sto2 is not None:
                            k2 = np.array(mks2[m].to_list())
                            g = (ka-k2) / trc.dt
                            k0 = g*(trc.dt*2) + k2
                        un = {u: (np.array(unlabelled[u].iloc[i, :].to_list()))/1000.0 for u in unlabelled}
                        un_filter = {u: un[u] for u in un if not np.isnan(un[u][0])}
                        order_un = [n for n in un_filter]
                        km = [np.linalg.norm(un_filter[n]-k0) for n in un_filter]
                        check = [1 if c < 0.05 else 0 for c in km]
                        try:
                            n = check.index(1)
                        except ValueError:
                            n = -1
                        if n > -1:
                            k0 = un_filter[order_un[n]]
                            weights[m] = 0.5
                        else:
                            if m.lower() == 'head':
                                weights[m] = 0.2
                            elif m.lower() == 'sternum':
                                weights[m] = 0.6
                            elif m.lower() == 'l_medknee':
                                weights[m] = 0.6
                            elif m.lower() == 'r_medknee':
                                weights[m] = 0.6
                            elif m.lower() == 'l_medankle':
                                weights[m] = 0.6
                            elif m.lower() == 'r_medankle':
                                weights[m] = 0.6
                            else:
                                weights[m] = 1
                        out[m] = pd.DataFrame(data=np.atleast_2d(k0), columns=[c for c in marker.columns])
                        applies[m] = True

                        num_markers += 1
                        pass
                else:
                    out[m] = dat[m]
                    applies[m] = True
                    weights[m] = 1
                    num_markers += 1
        print("time:{0} > number of markers:{1}".format(t, num_markers))

        filename = "{2}/{0}_{1:06d}.sto".format(kp[1][:kp[1].rindex(".sto")], int(t * 1000), kp[0])
        data_copied.marker_set = out
        data_copied.data = np.zeros([1, num_markers*3+2])
        data_copied.data[0, 0] = 1
        data_copied.data[0, 1] = t
        data_copied.update_from_markerset()
        marker_filename = "{1}{2}_{0:06d}.trc".format(int(t*1000), wkdir, trc_name)
        data_copied.write(marker_filename)
        ik.setMarkerDataFileName(marker_filename)

        if previous_sto is not None:
            previous_sto2 = previous_sto
        previous_sto = filename
        ik.set_output_motion_file(filename)
        print("frame: {0} - {1}".format(ik.getStartTime(), ik.getEndTime()))
        tasks = ik.getIKTaskSet()
        ik.get_marker_file()

        count = 0
        for m in tasks:
            apl = applies[m.getName()]
            w = weights[m.getName()]
            m.setApply(apl)
            m.setWeight(w)
            count += 1
        try:
            ik.run()
        except RuntimeError:
            continue
        pass
    try:
        export(wkdir=wkdir, trial=trc_name, out_root=out_root, model_file=omodel)
    except IndexError:
        pass
    pass


def sto_loader(f):
    return StorageIO.load(f, StorageType.mot)


def trc_loader(f):
    return TRC.read(f)


def export(wkdir="M:/test/", out_root="M:/osim/", trial="test", model_file="M:/Models/P012.osim"):
    path_part = os.path.split(model_file)
    idx = path_part[1][:path_part[1].rindex('.osim')]
    out_dir = "{0}{1}/".format(out_root, idx)
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    ike = "M:/template/Straight normal 1.sto"
    ste = OSIMStorage.read(ike)
    template_trc = "M:/template/Straight normal 1.trc"
    temp_trc = TRC.read(template_trc)
    trc_out = copy.deepcopy(temp_trc)
    model = Omodel(model_file)
    sto_n = [f for f in os.listdir(wkdir) if f.endswith(".sto") and not f.startswith("_") and not f.startswith("test")]
    sto_nx = [int(f.split("_")[-1][:f.split("_")[-1].rindex('.sto')]) for f in sto_n]
    sto_ref = [int(f.split("_")[-1][:f.split("_")[-1].rindex('.sto')]) for f in sto_n]
    sto_nx.sort()
    sto_h = [sto_ref.index(f) for f in sto_nx]
    sto_g = [wkdir+sto_n[f] for f in sto_h]

    print("Load Sto ...", end="")
    with Pool(4) as p:
        sto_data = p.map(sto_loader, sto_g)
    print(" Done!")

    def convert_str_2_int(f):
        try:
            return int(f.split("_")[-1][:f.split("_")[-1].rindex('.trc')])
        except ValueError:
            return None

    def search(l, t):
        idx = -1
        for k0 in l:
            if k0 == t:
                return idx
            idx += 1
        return idx

    # trc_n = [f for f in os.listdir(wkdir) if f.endswith(".trc") and not f.startswith("_") and not f.endswith('test.trc')]
    # trc_ref = [convert_str_2_int(f) if convert_str_2_int(f) is not None else -1 for f in trc_n]
    # trc_h = [search(trc_ref, f) for f in sto_nx]
    # trc_g = [wkdir+trc_n[f] for f in trc_h]
    def is_number(v):
        sp = v.split("_")[-1]
        return sp[:sp.rindex(".")].isnumeric()

    trc_n = [f for f in os.listdir(wkdir) if f.endswith(".trc") and not f.startswith("_") if is_number(f)]
    trc_nx = [int(f.split("_")[-1][:f.split("_")[-1].rindex('.trc')]) for f in trc_n]
    trc_ref = [int(f.split("_")[-1][:f.split("_")[-1].rindex('.trc')]) for f in trc_n]
    trc_nx.sort()
    trc_h = [trc_ref.index(f) for f in trc_nx]
    trc_g = [wkdir + trc_n[f] for f in trc_h]
    print("Load Trc ...", end="")
    with Pool(4) as p:
        trc_data = p.map(trc_loader, trc_g)
    print(" Done!")

    sto_d = np.zeros([len(sto_data), ste.data.shape[1]])
    trc_d = np.zeros([len(sto_data), len(temp_trc.col_labels)])
    trc_model = pd.DataFrame(data=trc_d, columns=temp_trc.col_labels)
    for s in range(0, len(sto_data)):
        print(s)
        trc_model.iloc[s, 0] = int(s +1)
        tr = trc_data[s]
        st = sto_data[s]
        model.set_joints(st.data.iloc[:1])
        model.update()
        mk = copy.deepcopy(model.markerset)
        model_markers = [c for c in mk.columns]
        trc_markers = [c for c in tr.marker_set]
        for c in model_markers:
            dx = mk[c]
            idex = [z for z in range(0, len(temp_trc.col_labels)) if temp_trc.col_labels[z].startswith(c)]
            if c in trc_markers:
                itdex = [z for z in range(0, len(tr.col_labels)) if tr.col_labels[z].startswith(c)]
                tx = tr.data[0, itdex]
                trc_model.iloc[s, idex] = tx
            else:
                trc_model.iloc[s, idex] = dx.to_list()
            trc_model.iloc[s, idex] = dx.to_list()
            # if 'Epicondyle' in c:
            #     trc_model.iloc[s, idex] = dx.to_list()
        f = st.data.iloc[0, :].to_list()
        trc_model.iloc[s, 1] = st.data.iloc[0, 0]
        sto_d[s, :] = f
    ste.data = sto_d
    ste.update()
    ste.write("{0}{1}.sto".format(out_dir, trial))
    trc_out.data = trc_model.to_numpy()
    trc_out.headers['Units'] = 'm'
    trc_out.update()
    trc_out.write("{0}{1}.trc".format(out_dir, trial))


if __name__ == "__main__":
    # omodel = "M:/Models/P012.osim"
    # ike = "M:/Mocap/P012/New Session/Gap_Fill/motion_files/Straight AR 1.sto"
    #
    # # load model
    # model = Omodel(omodel)
    #
    # # load sto file
    # ste = OSIMStorage.read(ike)
    #
    # # loop through frames
    # for v in range(0, ste.data.shape[0]):
    #     # set frame data
    #     model.set_joints(ste.data[v, 1:])
    #     # update model
    #     model.update()
    #     # get marker data
    #     mk = copy.deepcopy(model.markerset)
    export_as_sto()
    # test1()
    # cd3_files = [f for f in os.listdir("M:/Mocap/P011/New Session/") if f.endswith(".c3d")]
    # done_folder = "M:/osim/P011"
    # if not os.path.exists(done_folder):
    #     os.makedirs(done_folder)
    # dones = [f[:f.rindex(".trc")] for f in os.listdir(done_folder) if f.endswith(".trc")]
    # c3d_done_check = [d for d in cd3_files if d[:d.rindex(".")] not in dones]
    # for c in c3d_done_check:
    #     process_trial(trial_c3d=c)
    # export(trial="Straight normal 1")

    # fixing_imu_vec3_file()
    # grab_new_exports()
    # get_position()
    pass
