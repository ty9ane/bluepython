import numpy as np
import pandas as pd


def simple_exf_reader(filepath, delimit='\n'):
    buffer = []
    try:
        f = open(filepath, "r")
        stream = f.readline()
        while len(stream) > 0:
            buffer.append(stream)
            stream = f.readline()
    except OSError:
        print('cannot open', filepath)
    finally:
        f.close()
    nodes = []
    for b in range(0, len(buffer)):
        if buffer[b].startswith("Node:"):
            idn = buffer[b]
            index = b
            try:
                x = buffer[b + 1].split()
                y = buffer[b + 2].split()
                z = buffer[b + 3].split()
                x_r = [float(r) for r in x]
                y_r = [float(r) for r in y]
                z_r = [float(r) for r in z]
                block = np.array([x_r, y_r, z_r])
                point = block.T
                if len(block.shape) > 1:
                    if block.shape[0] * block.shape[1] > 3:
                        point = block[:, 0]
                nodes.append([idn, index, np.squeeze(point)])
            except ValueError:
                pass

    points = pd.DataFrame(data=np.array([b[2] for b in nodes]), columns=["X", "Y", "Z"])
    return {"buffer": nodes, "points": points}


if __name__ == '__main__':
    ref = simple_exf_reader(filepath='E:/mapclient/workflow/data/transformed_sample_femur_left_29.exf')
    ref['points'].to_csv("E:/mapclient/workflow/ref.csv")
    fitted = simple_exf_reader(filepath='E:/mapclient/workflow/fitted/transformed_sample_femur_29_Fitted.exf')
    fitted['points'].to_csv("E:/mapclient/workflow/fitted.csv")
    pass
