import os

import pandas as pd
from numpy.ma.extras import average

from yatpkg.util.data import TRC, JSONSUtl, Yatsdo
from yatpkg.math.transformation import Cloud, PCAModel, Trig
from yatpkg.math.filters import Butterworth
from omc.models.sheep.bones import Lumbar
from omc.tools.marker_tools import MarkerMerger, AnteriorStylus, PosteriorStylus,MarkerTracer
import numpy as np
from scipy.spatial.transform import Rotation
import pandas
from enum import Enum
import matplotlib.pyplot as plt



class AnteriorStylusLabels(Enum):
    S29 = [0, 1, 2, 3]
    S30 = [0, 1, 2, 3]
    S31 = [0, 1, 2, 3]
    S32 = [0, 1, 2, 3]


def extract_landmarks(landmarks_frames, c3dfile=None, trc_out=None, json_out=None, anterior=True, merge_markers=True, trc_in=None, static=1):
    trc = trc_in
    if c3dfile is not None:
        trc = TRC.create_from_c3d(c3dfile)
        if merge_markers:
            merger = MarkerMerger(trc)
            new_trc = merger.run()
            new_trc.write(trc_out)
            trc = new_trc
    if trc is None:
        return
    a = AnteriorStylus()
    if not anterior:
        a = PosteriorStylus()
    landmarks = {'a': [0, 0, 0], 'b': [0, 0, 0], 'c': [0, 0, 0], 'd': [0, 0, 0]}
    s1 = trc.marker_set['S1']
    s2 = trc.marker_set['S2']
    s3 = trc.marker_set['S3']
    s4 = trc.marker_set['S4']
    l3L = 1000*trc.marker_set['L3Left'].to_numpy()[static, :]
    l3R = 1000*trc.marker_set['L3Right'].to_numpy()[static, :]
    l3M = 1000*trc.marker_set['L3Mid'].to_numpy()[static, :]
    l4L = 1000 * trc.marker_set['L4Left'].to_numpy()[static, :]
    l4R = 1000 * trc.marker_set['L4Right'].to_numpy()[static, :]
    l4M = 1000 * trc.marker_set['L3Mid'].to_numpy()[static, :]
    l5L = 1000*trc.marker_set['L5Left'].to_numpy()[static, :]
    l5R = 1000*trc.marker_set['L5Right'].to_numpy()[static, :]
    l5M = 1000 * trc.marker_set['L3Mid'].to_numpy()[static, :]
    print()
    for landmark in landmarks_frames:
        tracked = np.zeros([3, 4])
        st = landmarks_frames[landmark][0]
        ed = landmarks_frames[landmark][1]
        s1_block = s1.iloc[st: ed].to_numpy()
        s2_block = s2.iloc[st: ed].to_numpy()
        s3_block = s3.iloc[st: ed].to_numpy()
        s4_block = s4.iloc[st: ed].to_numpy()
        tracked[:, 0] = np.nanmean(s1_block, axis=0)
        tracked[:, 1] = np.nanmean(s2_block, axis=0)
        tracked[:, 2] = np.nanmean(s3_block, axis=0)
        tracked[:, 3] = np.nanmean(s4_block, axis=0)
        tracked_milli = 1000*tracked
        c = Cloud.rigid_body_transform(a.tracker[:, :-1], tracked_milli[:, :-1])

        ap = np.ones([4, 5])
        ap[:3, :-1] = a.tracker
        ap[:3, -1] = a.point
        s = np.matmul(c, ap)
        print(s)
        landmarks[landmark] = s[:3, -1].tolist()
    bapple = {"landmarks_position": landmarks, "landmark_frames": landmarks_frames,
              'l3': {'l': l3L.tolist(), 'r': l3R.tolist(), 'm': l3M.tolist()},
              'l4': {'l': l4L.tolist(), 'r': l4R.tolist(), 'm': l4M.tolist()},
              'l5': {'l': l5L.tolist(), 'r': l5R.tolist(), 'm': l5M.tolist()}}
    JSONSUtl.write_json(json_out, bapple)
    return bapple


def L3(args):
    v0 = np.array(args['aa']) - np.array(args['pa'])
    v0_norm = v0 / np.linalg.norm(v0)
    v1_mid = (args['l'] + args['r'])/2
    v0_mid = (np.array(args['aa']) + np.array(args['pa']))/2
    v1a = v1_mid - v0_mid
    v1a_norm = v1a / np.linalg.norm(v1a)
    v2 = np.cross(v0_norm, v1a_norm)
    v1b = np.cross(v2, v0_norm)
    ret = np.zeros([3, 4])
    ret[:, 1] = v0_norm
    ret[:, 2] = v1b
    ret[:, 3] = v2
    # origin = np.atleast_2d(v0_mid+v1_mid)/2
    origin = np.atleast_2d(v1_mid)
    ret = 100 * ret + origin.T
    ret[:, 0] = origin
    print('L3')
    print(ret)
    return ret


def L4(args):
    v0 = np.array(args['ac']) - np.array(args['pc'])
    v1 = np.array(args['pb']) - np.array(args['pc'])
    v2 = np.cross(v0, v1)
    v1 = np.cross(v2, v0)
    ret = np.zeros([3, 4])
    ret[:, 1] = v0 / np.linalg.norm(v0)
    ret[:, 2] = v1 / np.linalg.norm(v1)
    ret[:, 3] = v2 / np.linalg.norm(v2)
    # origin = (np.atleast_2d(args['ac'])+np.atleast_2d(args['pc'])+np.atleast_2d(args['ab'])+np.atleast_2d(args['pb']))/4
    v0_mid = (np.array(args['ac']) + np.array(args['pc'])) / 2
    v1_mid = (args['l'] + args['r']) / 2
    v3_mid = (np.array(args['ab']) + np.array(args['pb'])) / 2
    # origin = np.atleast_2d(v0_mid+v1_mid+v3_mid)/3
    origin = np.atleast_2d(v1_mid)
    ret = 100*ret + origin.T
    print('L4')
    print(ret)
    ret[:, 0] = origin
    return ret


def L5(args):
    v0 = np.array(args['ad']) - np.array(args['pd'])
    v0_norm = v0 / np.linalg.norm(v0)
    v1_mid = (args['l'] + args['r']) / 2
    v0_mid = (np.array(args['ad']) + np.array(args['pd'])) / 2
    v1a = v0_mid - v1_mid
    v1a_norm = v1a / np.linalg.norm(v1a)
    v2 = np.cross(v0_norm, v1a_norm)
    v1b = np.cross(v2, v0_norm)
    ret = np.zeros([3, 4])
    ret[:, 1] = v0_norm
    ret[:, 2] = v1b
    ret[:, 3] = v2
    # origin = np.atleast_2d(v0_mid + v1_mid) / 2
    origin = np.atleast_2d(v1_mid)
    ret = 100 * ret + origin.T
    print('L5')
    print(ret)
    ret[:, 0] = origin
    return ret


def test():
    y0 = 42.656 * np.sin(1 * (np.pi / 180))
    y1 = 53.95 * np.sin(1 * (np.pi / 180))
    y2 = -48.03 * np.sin(1 * (np.pi / 180))
    y3 = 27.764 * np.sin(1 * (np.pi / 180))
    print(y0)
    print(y1)
    print(y2)
    print(y3)


def merger_land():
    root = "E:/bones/sheep/Optitrack data/"
    c3d_2_trc = [f for f in os.listdir(root)]
    out_dir = "E:/bones/sheep/labelleddata/"
    for trc in c3d_2_trc:
        print(trc)
        file_out = trc[:trc.rindex(".c3d")]+".trc"
        trc_data = TRC.create_from_c3d(root+trc)
        merger = MarkerMerger(trc_data)
        new_trc = merger.run()
        new_trc.write(out_dir+file_out)


def S30_calibration(inf0=None):
    landmarks_frames = inf0['Anterior landmarks']

    a = extract_landmarks(
        landmarks_frames=landmarks_frames,
        c3dfile='E:/bones/sheep/Optitrack data/S30 L3L4L5 Anterior Landmarks - labelled.c3d',
        trc_out='E:/bones/sheep/labelleddata/S30 L3L4L5 Anterior Landmarks - labelled.trc',
        json_out='E:/bones/sheep/labelleddata/S30 L3L4L5 Anterior Landmarks.json',
        anterior=True)

    landmarks_frames = inf0['Posterior landmarks']
    p = extract_landmarks(
        landmarks_frames=landmarks_frames,
        c3dfile='E:/bones/sheep/Optitrack data/S30 L3L4L5 Posterior Landmarks - Labelled.c3d',
        trc_out='E:/bones/sheep/labelleddata/S30 L3L4L5 Posterior Landmarks - Labelled.trc',
        json_out='E:/bones/sheep/labelleddata/S30 L3L4L5 Posterior Landmarks.json',
        anterior=False)
    l3_args = {'aa': a['landmarks_position']['a'], 'pa': p['landmarks_position']['a'],
               "l": (np.array(a['l3']['l']) + np.array(p['l3']['l'])) / 2,
               'r': (np.array(a['l3']['r']) + np.array(p['l3']['r'])) / 2}
    l4_args = {'pc': p['landmarks_position']['c'], 'ac': a['landmarks_position']['c'],
               "ab": a['landmarks_position']['b'], "pb": p['landmarks_position']['b'],
               "l": (np.array(a['l4']['l'] + np.array(p['l4']['l']))) / 2,
               'r': (np.array(a['l4']['r']) + np.array(p['l4']['r'])) / 2
               }
    l5_args = {'ad': a['landmarks_position']['d'], 'pd': p['landmarks_position']['d'],
               "l": (np.array(a['l5']['l'] + np.array(p['l5']['l']))) / 2,
               'r': (np.array(a['l5']['r']) + np.array(p['l5']['r'])) / 2}
    world = np.eye(3, 3)
    frame_l3 = L3(l3_args)
    frame_l4 = L4(l4_args)
    frame_l5 = L5(l5_args)
    track_l3 = np.zeros([3, 3])
    track_l3[:, 0] = np.array(a['l3']['l'])
    track_l3[:, 1] = np.array(a['l3']['m'])
    track_l3[:, 2] = np.array(a['l3']['r'])
    track_l4 = np.zeros([3, 3])
    track_l4[:, 0] = np.array(a['l4']['l'])
    track_l4[:, 1] = np.array(a['l4']['m'])
    track_l4[:, 2] = np.array(a['l4']['r'])
    track_l5 = np.zeros([3, 3])
    track_l5[:, 0] = np.array(a['l5']['l'])
    track_l5[:, 1] = np.array(a['l5']['m'])
    track_l5[:, 2] = np.array(a['l5']['r'])
    frame_l3_zeroed = frame_l3 - (np.atleast_2d(frame_l3[:, 0])).T
    frame_l3_zeroed[:, 1] = frame_l3_zeroed[:, 1] / np.linalg.norm(frame_l3_zeroed[:, 1])
    frame_l3_zeroed[:, 2] = frame_l3_zeroed[:, 2] / np.linalg.norm(frame_l3_zeroed[:, 2])
    frame_l3_zeroed[:, 3] = frame_l3_zeroed[:, 3] / np.linalg.norm(frame_l3_zeroed[:, 3])
    frame_l4_zeroed = frame_l4 - (np.atleast_2d(frame_l4[:, 0])).T
    frame_l4_zeroed[:, 1] = frame_l4_zeroed[:, 1] / np.linalg.norm(frame_l4_zeroed[:, 1])
    frame_l4_zeroed[:, 2] = frame_l4_zeroed[:, 2] / np.linalg.norm(frame_l4_zeroed[:, 2])
    frame_l4_zeroed[:, 3] = frame_l4_zeroed[:, 3] / np.linalg.norm(frame_l4_zeroed[:, 3])
    frame_l5_zeroed = frame_l5 - (np.atleast_2d(frame_l5[:, 0])).T
    frame_l5_zeroed[:, 1] = frame_l5_zeroed[:, 1] / np.linalg.norm(frame_l5_zeroed[:, 1])
    frame_l5_zeroed[:, 2] = frame_l5_zeroed[:, 2] / np.linalg.norm(frame_l5_zeroed[:, 2])
    frame_l5_zeroed[:, 3] = frame_l5_zeroed[:, 3] / np.linalg.norm(frame_l5_zeroed[:, 3])
    transform_l3 = Cloud.rigid_body_transform(frame_l3_zeroed[:, 1:], world)
    transform_l4 = Cloud.rigid_body_transform(frame_l4_zeroed[:, 1:], world)
    transform_l5 = Cloud.rigid_body_transform(frame_l5_zeroed[:, 1:], world)
    # zeroed
    track_l3 = track_l3 - (np.atleast_2d(frame_l3[:, 0])).T
    track_l4 = track_l4 - (np.atleast_2d(frame_l4[:, 0])).T
    track_l5 = track_l5 - (np.atleast_2d(frame_l5[:, 0])).T
    l3a = np.matmul(transform_l3[:3, :3], frame_l3_zeroed)
    l4a = np.matmul(transform_l4[:3, :3], frame_l4_zeroed)
    l5a = np.matmul(transform_l5[:3, :3], frame_l5_zeroed)
    # Create reference for tracking markers
    track_l3_ref = np.matmul(transform_l3[:3, :3], track_l3)
    track_l4_ref = np.matmul(transform_l4[:3, :3], track_l4)
    track_l5_ref = np.matmul(transform_l5[:3, :3], track_l5)
    np.savez('E:/bones/sheep/labelleddata/S30_track_l3_ref.npz', a=track_l3_ref)
    np.savez('E:/bones/sheep/labelleddata/S30_track_l4_ref.npz', a=track_l4_ref)
    np.savez('E:/bones/sheep/labelleddata/S30_track_l5_ref.npz', a=track_l5_ref)


def S31_calibration(inf0=None):
    landmarks_frames = inf0['Anterior landmarks']

    a = extract_landmarks(
        landmarks_frames=landmarks_frames,
        c3dfile='E:/bones/sheep/Optitrack data/S31 L3L4L5 Anterior Landmarks - labelled.c3d',
        trc_out='E:/bones/sheep/labelleddata/S31 L3L4L5 Anterior Landmarks - labelled.trc',
        json_out='E:/bones/sheep/labelleddata/S31 L3L4L5 Anterior Landmarks.json',
        anterior=True)

    landmarks_frames = inf0['Posterior landmarks']
    p = extract_landmarks(
        landmarks_frames=landmarks_frames,
        c3dfile='E:/bones/sheep/Optitrack data/S31 L3L4L5 Posterior Landmarks - Labelled.c3d',
        trc_out='E:/bones/sheep/labelleddata/S31 L3L4L5 Posterior Landmarks - Labelled.trc',
        json_out='E:/bones/sheep/labelleddata/S31 L3L4L5 Posterior Landmarks.json',
        anterior=False)
    l3_args = {'aa': a['landmarks_position']['a'], 'pa': p['landmarks_position']['a'],
               "l": (np.array(a['l3']['l']) + np.array(p['l3']['l'])) / 2,
               'r': (np.array(a['l3']['r']) + np.array(p['l3']['r'])) / 2}
    l4_args = {'pc': p['landmarks_position']['c'], 'ac': a['landmarks_position']['c'],
               "ab": a['landmarks_position']['b'], "pb": p['landmarks_position']['b'],
               "l": (np.array(a['l4']['l'] + np.array(p['l4']['l']))) / 2,
               'r': (np.array(a['l4']['r']) + np.array(p['l4']['r'])) / 2
               }
    l5_args = {'ad': a['landmarks_position']['d'], 'pd': p['landmarks_position']['d'],
               "l": (np.array(a['l5']['l'] + np.array(p['l5']['l']))) / 2,
               'r': (np.array(a['l5']['r']) + np.array(p['l5']['r'])) / 2}
    world = np.eye(3, 3)
    frame_l3 = L3(l3_args)
    frame_l4 = L4(l4_args)
    frame_l5 = L5(l5_args)
    track_l3 = np.zeros([3, 3])
    track_l3[:, 0] = np.array(a['l3']['l'])
    track_l3[:, 1] = np.array(a['l3']['m'])
    track_l3[:, 2] = np.array(a['l3']['r'])
    track_l4 = np.zeros([3, 3])
    track_l4[:, 0] = np.array(a['l4']['l'])
    track_l4[:, 1] = np.array(a['l4']['m'])
    track_l4[:, 2] = np.array(a['l4']['r'])
    track_l5 = np.zeros([3, 3])
    track_l5[:, 0] = np.array(a['l5']['l'])
    track_l5[:, 1] = np.array(a['l5']['m'])
    track_l5[:, 2] = np.array(a['l5']['r'])
    frame_l3_zeroed = frame_l3 - (np.atleast_2d(frame_l3[:, 0])).T
    frame_l3_zeroed[:, 1] = frame_l3_zeroed[:, 1] / np.linalg.norm(frame_l3_zeroed[:, 1])
    frame_l3_zeroed[:, 2] = frame_l3_zeroed[:, 2] / np.linalg.norm(frame_l3_zeroed[:, 2])
    frame_l3_zeroed[:, 3] = frame_l3_zeroed[:, 3] / np.linalg.norm(frame_l3_zeroed[:, 3])
    frame_l4_zeroed = frame_l4 - (np.atleast_2d(frame_l4[:, 0])).T
    frame_l4_zeroed[:, 1] = frame_l4_zeroed[:, 1] / np.linalg.norm(frame_l4_zeroed[:, 1])
    frame_l4_zeroed[:, 2] = frame_l4_zeroed[:, 2] / np.linalg.norm(frame_l4_zeroed[:, 2])
    frame_l4_zeroed[:, 3] = frame_l4_zeroed[:, 3] / np.linalg.norm(frame_l4_zeroed[:, 3])
    frame_l5_zeroed = frame_l5 - (np.atleast_2d(frame_l5[:, 0])).T
    frame_l5_zeroed[:, 1] = frame_l5_zeroed[:, 1] / np.linalg.norm(frame_l5_zeroed[:, 1])
    frame_l5_zeroed[:, 2] = frame_l5_zeroed[:, 2] / np.linalg.norm(frame_l5_zeroed[:, 2])
    frame_l5_zeroed[:, 3] = frame_l5_zeroed[:, 3] / np.linalg.norm(frame_l5_zeroed[:, 3])
    transform_l3 = Cloud.rigid_body_transform(frame_l3_zeroed[:, 1:], world)
    transform_l4 = Cloud.rigid_body_transform(frame_l4_zeroed[:, 1:], world)
    transform_l5 = Cloud.rigid_body_transform(frame_l5_zeroed[:, 1:], world)
    # zeroed
    track_l3 = track_l3 - (np.atleast_2d(frame_l3[:, 0])).T
    track_l4 = track_l4 - (np.atleast_2d(frame_l4[:, 0])).T
    track_l5 = track_l5 - (np.atleast_2d(frame_l5[:, 0])).T
    l3a = np.matmul(transform_l3[:3, :3], frame_l3_zeroed)
    l4a = np.matmul(transform_l4[:3, :3], frame_l4_zeroed)
    l5a = np.matmul(transform_l5[:3, :3], frame_l5_zeroed)
    # Create reference for tracking markers
    track_l3_ref = np.matmul(transform_l3[:3, :3], track_l3)
    track_l4_ref = np.matmul(transform_l4[:3, :3], track_l4)
    track_l5_ref = np.matmul(transform_l5[:3, :3], track_l5)
    np.savez('E:/bones/sheep/labelleddata/S31_track_l3_ref.npz', a=track_l3_ref)
    np.savez('E:/bones/sheep/labelleddata/S31_track_l4_ref.npz', a=track_l4_ref)
    np.savez('E:/bones/sheep/labelleddata/S31_track_l5_ref.npz', a=track_l5_ref)


def S32_calibration(inf0=None):
    landmarks_frames = inf0['Anterior landmarks']

    a = extract_landmarks(
        landmarks_frames=landmarks_frames,
        c3dfile='E:/bones/sheep/Optitrack data/S32 L3L4L5 Anterior Landmarks - labelled.c3d',
        trc_out='E:/bones/sheep/labelleddata/S32 L3L4L5 Anterior Landmarks - labelled.trc',
        json_out='E:/bones/sheep/labelleddata/S32 L3L4L5 Anterior Landmarks.json',
        anterior=True)

    landmarks_frames = inf0['Posterior landmarks']
    p = extract_landmarks(
        landmarks_frames=landmarks_frames,
        c3dfile='E:/bones/sheep/Optitrack data/S32 L3L4L5 Posterior Landmarks - Labelled.c3d',
        trc_out='E:/bones/sheep/labelleddata/S32 L3L4L5 Posterior Landmarks - Labelled.trc',
        json_out='E:/bones/sheep/labelleddata/S32 L3L4L5 Posterior Landmarks.json',
        anterior=False)
    l3_args = {'aa': a['landmarks_position']['a'], 'pa': p['landmarks_position']['a'],
               "l": (np.array(a['l3']['l']) + np.array(p['l3']['l'])) / 2,
               'r': (np.array(a['l3']['r']) + np.array(p['l3']['r'])) / 2}
    l4_args = {'pc': p['landmarks_position']['c'], 'ac': a['landmarks_position']['c'],
               "ab": a['landmarks_position']['b'], "pb": p['landmarks_position']['b'],
               "l": (np.array(a['l4']['l'] + np.array(p['l4']['l']))) / 2,
               'r': (np.array(a['l4']['r']) + np.array(p['l4']['r'])) / 2
               }
    l5_args = {'ad': a['landmarks_position']['d'], 'pd': p['landmarks_position']['d'],
               "l": (np.array(a['l5']['l'] + np.array(p['l5']['l']))) / 2,
               'r': (np.array(a['l5']['r']) + np.array(p['l5']['r'])) / 2}
    world = np.eye(3, 3)
    frame_l3 = L3(l3_args)
    frame_l4 = L4(l4_args)
    frame_l5 = L5(l5_args)
    track_l3 = np.zeros([3, 3])
    track_l3[:, 0] = np.array(a['l3']['l'])
    track_l3[:, 1] = np.array(a['l3']['m'])
    track_l3[:, 2] = np.array(a['l3']['r'])
    track_l4 = np.zeros([3, 3])
    track_l4[:, 0] = np.array(a['l4']['l'])
    track_l4[:, 1] = np.array(a['l4']['m'])
    track_l4[:, 2] = np.array(a['l4']['r'])
    track_l5 = np.zeros([3, 3])
    track_l5[:, 0] = np.array(a['l5']['l'])
    track_l5[:, 1] = np.array(a['l5']['m'])
    track_l5[:, 2] = np.array(a['l5']['r'])
    frame_l3_zeroed = frame_l3 - (np.atleast_2d(frame_l3[:, 0])).T
    frame_l3_zeroed[:, 1] = frame_l3_zeroed[:, 1] / np.linalg.norm(frame_l3_zeroed[:, 1])
    frame_l3_zeroed[:, 2] = frame_l3_zeroed[:, 2] / np.linalg.norm(frame_l3_zeroed[:, 2])
    frame_l3_zeroed[:, 3] = frame_l3_zeroed[:, 3] / np.linalg.norm(frame_l3_zeroed[:, 3])
    frame_l4_zeroed = frame_l4 - (np.atleast_2d(frame_l4[:, 0])).T
    frame_l4_zeroed[:, 1] = frame_l4_zeroed[:, 1] / np.linalg.norm(frame_l4_zeroed[:, 1])
    frame_l4_zeroed[:, 2] = frame_l4_zeroed[:, 2] / np.linalg.norm(frame_l4_zeroed[:, 2])
    frame_l4_zeroed[:, 3] = frame_l4_zeroed[:, 3] / np.linalg.norm(frame_l4_zeroed[:, 3])
    frame_l5_zeroed = frame_l5 - (np.atleast_2d(frame_l5[:, 0])).T
    frame_l5_zeroed[:, 1] = frame_l5_zeroed[:, 1] / np.linalg.norm(frame_l5_zeroed[:, 1])
    frame_l5_zeroed[:, 2] = frame_l5_zeroed[:, 2] / np.linalg.norm(frame_l5_zeroed[:, 2])
    frame_l5_zeroed[:, 3] = frame_l5_zeroed[:, 3] / np.linalg.norm(frame_l5_zeroed[:, 3])
    transform_l3 = Cloud.rigid_body_transform(frame_l3_zeroed[:, 1:], world)
    transform_l4 = Cloud.rigid_body_transform(frame_l4_zeroed[:, 1:], world)
    transform_l5 = Cloud.rigid_body_transform(frame_l5_zeroed[:, 1:], world)
    # zeroed
    track_l3 = track_l3 - (np.atleast_2d(frame_l3[:, 0])).T
    track_l4 = track_l4 - (np.atleast_2d(frame_l4[:, 0])).T
    track_l5 = track_l5 - (np.atleast_2d(frame_l5[:, 0])).T
    l3a = np.matmul(transform_l3[:3, :3], frame_l3_zeroed)
    l4a = np.matmul(transform_l4[:3, :3], frame_l4_zeroed)
    l5a = np.matmul(transform_l5[:3, :3], frame_l5_zeroed)
    # Create reference for tracking markers
    track_l3_ref = np.matmul(transform_l3[:3, :3], track_l3)
    track_l4_ref = np.matmul(transform_l4[:3, :3], track_l4)
    track_l5_ref = np.matmul(transform_l5[:3, :3], track_l5)
    np.savez('E:/bones/sheep/labelleddata/S32_track_l3_ref.npz', a=track_l3_ref)
    np.savez('E:/bones/sheep/labelleddata/S32_track_l4_ref.npz', a=track_l4_ref)
    np.savez('E:/bones/sheep/labelleddata/S32_track_l5_ref.npz', a=track_l5_ref)


def S33_calibration(inf0=None):
    landmarks_frames = inf0['Anterior landmarks']

    a = extract_landmarks(
        landmarks_frames=landmarks_frames,
        c3dfile='E:/bones/sheep/Optitrack data/S33 L3L4L5 Anterior Landmarks - labelled.c3d',
        trc_out='E:/bones/sheep/labelleddata/S33 L3L4L5 Anterior Landmarks - labelled.trc',
        json_out='E:/bones/sheep/labelleddata/S33 L3L4L5 Anterior Landmarks.json',
        anterior=True)

    landmarks_frames = inf0['Posterior landmarks']
    p = extract_landmarks(
        landmarks_frames=landmarks_frames,
        c3dfile='E:/bones/sheep/Optitrack data/S33 L3L4L5 Posterior Landmarks - Labelled.c3d',
        trc_out='E:/bones/sheep/labelleddata/S33 L3L4L5 Posterior Landmarks - Labelled.trc',
        json_out='E:/bones/sheep/labelleddata/S33 L3L4L5 Posterior Landmarks.json',
        anterior=False)
    l3_args = {'aa': a['landmarks_position']['a'], 'pa': p['landmarks_position']['a'],
               "l": (np.array(a['l3']['l']) + np.array(p['l3']['l'])) / 2,
               'r': (np.array(a['l3']['r']) + np.array(p['l3']['r'])) / 2}
    l4_args = {'pc': p['landmarks_position']['c'], 'ac': a['landmarks_position']['c'],
               "ab": a['landmarks_position']['b'], "pb": p['landmarks_position']['b'],
               "l": (np.array(a['l4']['l'] + np.array(p['l4']['l']))) / 2,
               'r': (np.array(a['l4']['r']) + np.array(p['l4']['r'])) / 2
               }
    l5_args = {'ad': a['landmarks_position']['d'], 'pd': p['landmarks_position']['d'],
               "l": (np.array(a['l5']['l'] + np.array(p['l5']['l']))) / 2,
               'r': (np.array(a['l5']['r']) + np.array(p['l5']['r'])) / 2}
    world = np.eye(3, 3)
    frame_l3 = L3(l3_args)
    frame_l4 = L4(l4_args)
    frame_l5 = L5(l5_args)
    track_l3 = np.zeros([3, 3])
    track_l3[:, 0] = np.array(a['l3']['l'])
    track_l3[:, 1] = np.array(a['l3']['m'])
    track_l3[:, 2] = np.array(a['l3']['r'])
    track_l4 = np.zeros([3, 3])
    track_l4[:, 0] = np.array(a['l4']['l'])
    track_l4[:, 1] = np.array(a['l4']['m'])
    track_l4[:, 2] = np.array(a['l4']['r'])
    track_l5 = np.zeros([3, 3])
    track_l5[:, 0] = np.array(a['l5']['l'])
    track_l5[:, 1] = np.array(a['l5']['m'])
    track_l5[:, 2] = np.array(a['l5']['r'])
    frame_l3_zeroed = frame_l3 - (np.atleast_2d(frame_l3[:, 0])).T
    frame_l3_zeroed[:, 1] = frame_l3_zeroed[:, 1] / np.linalg.norm(frame_l3_zeroed[:, 1])
    frame_l3_zeroed[:, 2] = frame_l3_zeroed[:, 2] / np.linalg.norm(frame_l3_zeroed[:, 2])
    frame_l3_zeroed[:, 3] = frame_l3_zeroed[:, 3] / np.linalg.norm(frame_l3_zeroed[:, 3])
    frame_l4_zeroed = frame_l4 - (np.atleast_2d(frame_l4[:, 0])).T
    frame_l4_zeroed[:, 1] = frame_l4_zeroed[:, 1] / np.linalg.norm(frame_l4_zeroed[:, 1])
    frame_l4_zeroed[:, 2] = frame_l4_zeroed[:, 2] / np.linalg.norm(frame_l4_zeroed[:, 2])
    frame_l4_zeroed[:, 3] = frame_l4_zeroed[:, 3] / np.linalg.norm(frame_l4_zeroed[:, 3])
    frame_l5_zeroed = frame_l5 - (np.atleast_2d(frame_l5[:, 0])).T
    frame_l5_zeroed[:, 1] = frame_l5_zeroed[:, 1] / np.linalg.norm(frame_l5_zeroed[:, 1])
    frame_l5_zeroed[:, 2] = frame_l5_zeroed[:, 2] / np.linalg.norm(frame_l5_zeroed[:, 2])
    frame_l5_zeroed[:, 3] = frame_l5_zeroed[:, 3] / np.linalg.norm(frame_l5_zeroed[:, 3])
    transform_l3 = Cloud.rigid_body_transform(frame_l3_zeroed[:, 1:], world)
    transform_l4 = Cloud.rigid_body_transform(frame_l4_zeroed[:, 1:], world)
    transform_l5 = Cloud.rigid_body_transform(frame_l5_zeroed[:, 1:], world)
    # zeroed
    track_l3 = track_l3 - (np.atleast_2d(frame_l3[:, 0])).T
    track_l4 = track_l4 - (np.atleast_2d(frame_l4[:, 0])).T
    track_l5 = track_l5 - (np.atleast_2d(frame_l5[:, 0])).T
    l3a = np.matmul(transform_l3[:3, :3], frame_l3_zeroed)
    l4a = np.matmul(transform_l4[:3, :3], frame_l4_zeroed)
    l5a = np.matmul(transform_l5[:3, :3], frame_l5_zeroed)
    # Create reference for tracking markers
    track_l3_ref = np.matmul(transform_l3[:3, :3], track_l3)
    track_l4_ref = np.matmul(transform_l4[:3, :3], track_l4)
    track_l5_ref = np.matmul(transform_l5[:3, :3], track_l5)
    np.savez('E:/bones/sheep/labelleddata/S33_track_l3_ref.npz', a=track_l3_ref)
    np.savez('E:/bones/sheep/labelleddata/S33_track_l4_ref.npz', a=track_l4_ref)
    np.savez('E:/bones/sheep/labelleddata/S33_track_l5_ref.npz', a=track_l5_ref)


def calibrate_tracking_markers():
    landmarks_frames = {'a': [1382, 2066], 'b': [2452, 3071], 'c': [3527, 4111], 'd': [4552, 5137]}
    event = {'hold': [15161, 22400], 'compression': [25519, 27268]}
    a = extract_landmarks(
        landmarks_frames=landmarks_frames,
        c3dfile='E:/bones/sheep/labelleddata/S29 L3L4L5 Anterior Landmarks - labelled.c3d',
        trc_out='E:/bones/sheep/labelleddata/S29 L3L4L5 Anterior Landmarks - labelled.trc',
        json_out='E:/bones/sheep/labelleddata/S29 L3L4L5 Anterior Landmarks.json',
        anterior=True)
    landmarks_frames = {'a': [1493, 2009], 'b': [2445, 3034], 'c': [3551, 4075], 'd': [5380, 5879]}
    p = extract_landmarks(
        landmarks_frames=landmarks_frames,
        c3dfile='E:/bones/sheep/labelleddata/S29 L3L4L5 Posterior Landmarks - Labelled.c3d',
        trc_out='E:/bones/sheep/labelleddata/S29 L3L4L5 Posterior Landmarks - Labelled.trc',
        json_out='E:/bones/sheep/labelleddata/S29 L3L4L5 Posterior Landmarks.json',
        anterior=False)
    l3_args = {'aa': a['landmarks_position']['a'], 'pa': p['landmarks_position']['a'],
               "l": (np.array(a['l3']['l']) + np.array(p['l3']['l'])) / 2,
               'r': (np.array(a['l3']['r']) + np.array(p['l3']['r'])) / 2}
    l4_args = {'pc': p['landmarks_position']['c'], 'ac': a['landmarks_position']['c'],
               "ab": a['landmarks_position']['b'], "pb": p['landmarks_position']['b'],
               "l": (np.array(a['l4']['l'] + np.array(p['l4']['l']))) / 2,
               'r': (np.array(a['l4']['r']) + np.array(p['l4']['r'])) / 2
               }
    l5_args = {'ad': a['landmarks_position']['d'], 'pd': p['landmarks_position']['d'],
               "l": (np.array(a['l5']['l'] + np.array(p['l5']['l']))) / 2,
               'r': (np.array(a['l5']['r']) + np.array(p['l5']['r'])) / 2}
    world = np.eye(3, 3)
    frame_l3 = L3(l3_args)
    frame_l4 = L4(l4_args)
    frame_l5 = L5(l5_args)
    track_l3 = np.zeros([3, 3])
    track_l3[:, 0] = np.array(a['l3']['l'])
    track_l3[:, 1] = np.array(a['l3']['m'])
    track_l3[:, 2] = np.array(a['l3']['r'])
    track_l4 = np.zeros([3, 3])
    track_l4[:, 0] = np.array(a['l4']['l'])
    track_l4[:, 1] = np.array(a['l4']['m'])
    track_l4[:, 2] = np.array(a['l4']['r'])
    track_l5 = np.zeros([3, 3])
    track_l5[:, 0] = np.array(a['l5']['l'])
    track_l5[:, 1] = np.array(a['l5']['m'])
    track_l5[:, 2] = np.array(a['l5']['r'])
    frame_l3_zeroed = frame_l3 - (np.atleast_2d(frame_l3[:, 0])).T
    frame_l3_zeroed[:, 1] = frame_l3_zeroed[:, 1] / np.linalg.norm(frame_l3_zeroed[:, 1])
    frame_l3_zeroed[:, 2] = frame_l3_zeroed[:, 2] / np.linalg.norm(frame_l3_zeroed[:, 2])
    frame_l3_zeroed[:, 3] = frame_l3_zeroed[:, 3] / np.linalg.norm(frame_l3_zeroed[:, 3])
    frame_l4_zeroed = frame_l4 - (np.atleast_2d(frame_l4[:, 0])).T
    frame_l4_zeroed[:, 1] = frame_l4_zeroed[:, 1] / np.linalg.norm(frame_l4_zeroed[:, 1])
    frame_l4_zeroed[:, 2] = frame_l4_zeroed[:, 2] / np.linalg.norm(frame_l4_zeroed[:, 2])
    frame_l4_zeroed[:, 3] = frame_l4_zeroed[:, 3] / np.linalg.norm(frame_l4_zeroed[:, 3])
    frame_l5_zeroed = frame_l5 - (np.atleast_2d(frame_l5[:, 0])).T
    frame_l5_zeroed[:, 1] = frame_l5_zeroed[:, 1] / np.linalg.norm(frame_l5_zeroed[:, 1])
    frame_l5_zeroed[:, 2] = frame_l5_zeroed[:, 2] / np.linalg.norm(frame_l5_zeroed[:, 2])
    frame_l5_zeroed[:, 3] = frame_l5_zeroed[:, 3] / np.linalg.norm(frame_l5_zeroed[:, 3])
    transform_l3 = Cloud.rigid_body_transform(frame_l3_zeroed[:, 1:], world)
    transform_l4 = Cloud.rigid_body_transform(frame_l4_zeroed[:, 1:], world)
    transform_l5 = Cloud.rigid_body_transform(frame_l5_zeroed[:, 1:], world)
    # zeroed
    track_l3 = track_l3 - (np.atleast_2d(frame_l3[:, 0])).T
    track_l4 = track_l4 - (np.atleast_2d(frame_l4[:, 0])).T
    track_l5 = track_l5 - (np.atleast_2d(frame_l5[:, 0])).T
    l3a = np.matmul(transform_l3[:3, :3], frame_l3_zeroed)
    l4a = np.matmul(transform_l4[:3, :3], frame_l4_zeroed)
    l5a = np.matmul(transform_l5[:3, :3], frame_l5_zeroed)
    # Create reference for tracking markers
    track_l3_ref = np.matmul(transform_l3[:3, :3], track_l3)
    track_l4_ref = np.matmul(transform_l4[:3, :3], track_l4)
    track_l5_ref = np.matmul(transform_l5[:3, :3], track_l5)
    np.savez('E:/bones/sheep/labelleddata/S29_track_l3_ref.npz', a=track_l3_ref)
    np.savez('E:/bones/sheep/labelleddata/S29_track_l4_ref.npz', a=track_l4_ref)
    np.savez('E:/bones/sheep/labelleddata/S29_track_l5_ref.npz', a=track_l5_ref)


def S29tracking():
    end_frame = 27739
    event = {'hold': [15161, 22400], 'compression': [25519, 27268]}
    trc = TRC.read('E:/bones/sheep/labelleddata/S29 L3L4L5 Flexion Hold 350N Compression - Labelled.trc')
    track_l3_ = np.load('E:/bones/sheep/labelleddata/S29_track_l3_ref.npz')
    track_l3_ref = np.asarray(track_l3_['a'])
    track_l3_.close()
    track_l4_ = np.load('E:/bones/sheep/labelleddata/S29_track_l4_ref.npz')
    track_l4_ref = np.asarray(track_l4_['a'])
    track_l4_.close()
    track_l5_ = np.load('E:/bones/sheep/labelleddata/S29_track_l5_ref.npz')
    track_l5_ref = np.asarray(track_l5_['a'])
    track_l5_.close()
    l3l = 1000 * trc.marker_set['L3Left']
    l3m = 1000 * trc.marker_set['L3Mid']
    l3r = 1000 * trc.marker_set['L3Right']
    l4l = 1000 * trc.marker_set['L4Left']
    l4m = 1000 * trc.marker_set['L4Mid']
    l4r = 1000 * trc.marker_set['L4Right']
    l5l = 1000 * trc.marker_set['L5Left']
    l5m = 1000 * trc.marker_set['L5Mid']
    l5r = 1000 * trc.marker_set['L5Right']
    ar = np.ones([4, 3])
    ar[:3, :3] = np.eye(3, 3)
    l3_frame = np.zeros([l3l.shape[0], 9])
    l4_frame = np.zeros([l4l.shape[0], 9])
    l5_frame = np.zeros([l5l.shape[0], 9])
    l3l4_list = []
    l4l5_list = []
    time_list = []
    for i in range(15092, end_frame):
        l3 = np.zeros([3, 3])
        l3[:, 0] = l3l.iloc[i, :]
        l3[:, 1] = l3m.iloc[i, :]
        l3[:, 2] = l3r.iloc[i, :]
        time_list.append(trc.data[i, 1])

        t = Cloud.rigid_body_transform(track_l3_ref, l3)
        tr0 = np.matmul(t, ar)
        l3_frame[i, :] = tr0[:3, :3].reshape([1, 9], order='F')

        l4 = np.zeros([3, 3])
        l4[:, 0] = l4l.iloc[i, :]
        l4[:, 1] = l4m.iloc[i, :]
        l4[:, 2] = l4r.iloc[i, :]

        t = Cloud.rigid_body_transform(track_l4_ref, l3)
        tr1 = np.matmul(t, ar)
        l4_frame[i, :] = tr1[:3, :3].reshape([1, 9], order='F')

        l5 = np.zeros([3, 3])
        l5[:, 0] = l5l.iloc[i, :]
        l5[:, 1] = l5m.iloc[i, :]
        l5[:, 2] = l5r.iloc[i, :]

        t = Cloud.rigid_body_transform(track_l5_ref, l5)
        tr2 = np.matmul(t, ar)
        l5_frame[i, :] = tr2[:3, :3].reshape([1, 9], order='F')

        r1 = Cloud.rigid_body_transform(tr1[:3, :3], tr0[:3, :3])
        r2 = Cloud.rigid_body_transform(tr1[:3, :3], tr2[:3, :3])

        l3l4 = Rotation.from_matrix(r1[:3, :3])
        l3l4_euler = l3l4.as_euler('xyz', True)
        l3l4_list.append(l3l4_euler)
        l4l5 = Rotation.from_matrix(r2[:3, :3])
        l4l5_euler = l4l5.as_euler('xyz', True)
        l4l5_list.append(l4l5_euler)
        pass
    p = np.atleast_2d(np.concatenate(l3l4_list)).reshape([len(l3l4_list), 3])
    q = np.atleast_2d(np.concatenate(l4l5_list)).reshape([len(l4l5_list), 3])
    trc.export('S29 L3L4L5 Flexion Hold 350N Compression.pineapple', 'E:/bones/sheep/labelleddata/')
    b2 = Yatsdo.load('E:/bones/sheep/labelleddata/S29 L3L4L5 Flexion Hold 350N Compression.pineapple')
    return b2


def S30tracking():
    end_frame = 25046
    trc = TRC.read('E:/bones/sheep/labelleddata/S30 L3L4L5 Flexion Hold 350N Compression - Labelled.trc')
    track_l3_ = np.load('E:/bones/sheep/labelleddata/S30_track_l3_ref.npz')
    track_l3_ref = np.asarray(track_l3_['a'])
    track_l3_.close()
    track_l4_ = np.load('E:/bones/sheep/labelleddata/S30_track_l4_ref.npz')
    track_l4_ref = np.asarray(track_l4_['a'])
    track_l4_.close()
    track_l5_ = np.load('E:/bones/sheep/labelleddata/S30_track_l5_ref.npz')
    track_l5_ref = np.asarray(track_l5_['a'])
    track_l5_.close()
    l3l = 1000 * trc.marker_set['L3Left']
    l3m = 1000 * trc.marker_set['L3Mid']
    l3r = 1000 * trc.marker_set['L3Right']
    l4l = 1000 * trc.marker_set['L4Left']
    l4m = 1000 * trc.marker_set['L4Mid']
    l4r = 1000 * trc.marker_set['L4Right']
    l5l = 1000 * trc.marker_set['L5Left']
    l5m = 1000 * trc.marker_set['L5Mid']
    l5r = 1000 * trc.marker_set['L5Right']
    ar = np.ones([4, 3])
    ar[:3, :3] = np.eye(3, 3)
    l3_frame = np.zeros([l3l.shape[0], 9])
    l4_frame = np.zeros([l4l.shape[0], 9])
    l5_frame = np.zeros([l5l.shape[0], 9])
    l3l4_list = []
    l4l5_list = []
    time_list = []
    for i in range(15092, end_frame):
        l3 = np.zeros([3, 3])
        l3[:, 0] = l3l.iloc[i, :]
        l3[:, 1] = l3m.iloc[i, :]
        l3[:, 2] = l3r.iloc[i, :]
        time_list.append(trc.data[i, 1])

        t = Cloud.rigid_body_transform(track_l3_ref, l3)
        tr0 = np.matmul(t, ar)
        l3_frame[i, :] = tr0[:3, :3].reshape([1, 9], order='F')

        l4 = np.zeros([3, 3])
        l4[:, 0] = l4l.iloc[i, :]
        l4[:, 1] = l4m.iloc[i, :]
        l4[:, 2] = l4r.iloc[i, :]

        t = Cloud.rigid_body_transform(track_l4_ref, l3)
        tr1 = np.matmul(t, ar)
        l4_frame[i, :] = tr1[:3, :3].reshape([1, 9], order='F')

        l5 = np.zeros([3, 3])
        l5[:, 0] = l5l.iloc[i, :]
        l5[:, 1] = l5m.iloc[i, :]
        l5[:, 2] = l5r.iloc[i, :]

        t = Cloud.rigid_body_transform(track_l5_ref, l5)
        tr2 = np.matmul(t, ar)
        l5_frame[i, :] = tr2[:3, :3].reshape([1, 9], order='F')

        r1 = Cloud.rigid_body_transform(tr1[:3, :3], tr0[:3, :3])
        r2 = Cloud.rigid_body_transform(tr1[:3, :3], tr2[:3, :3])

        l3l4 = Rotation.from_matrix(r1[:3, :3])
        l3l4_euler = l3l4.as_euler('xyz', True)
        l3l4_list.append(l3l4_euler)
        l4l5 = Rotation.from_matrix(r2[:3, :3])
        l4l5_euler = l4l5.as_euler('xyz', True)
        l4l5_list.append(l4l5_euler)
        pass
    p = np.atleast_2d(np.concatenate(l3l4_list)).reshape([len(l3l4_list), 3])
    q = np.atleast_2d(np.concatenate(l4l5_list)).reshape([len(l4l5_list), 3])
    trc.export('S30 L3L4L5 Flexion Hold 350N Compression.pineapple', 'E:/bones/sheep/labelleddata/')
    b2 = Yatsdo.load('E:/bones/sheep/labelleddata/S30 L3L4L5 Flexion Hold 350N Compression.pineapple')
    return b2

def S31tracking():
    end_frame = 25046
    trc = TRC.read('E:/bones/sheep/labelleddata/S31 L3L4L5 Flexion Hold 350N Compression - Labelled.trc')
    track_l3_ = np.load('E:/bones/sheep/labelleddata/S31_track_l3_ref.npz')
    track_l3_ref = np.asarray(track_l3_['a'])
    track_l3_.close()
    track_l4_ = np.load('E:/bones/sheep/labelleddata/S31_track_l4_ref.npz')
    track_l4_ref = np.asarray(track_l4_['a'])
    track_l4_.close()
    track_l5_ = np.load('E:/bones/sheep/labelleddata/S31_track_l5_ref.npz')
    track_l5_ref = np.asarray(track_l5_['a'])
    track_l5_.close()
    l3l = 1000 * trc.marker_set['L3Left']
    l3m = 1000 * trc.marker_set['L3Mid']
    l3r = 1000 * trc.marker_set['L3Right']
    l4l = 1000 * trc.marker_set['L4Left']
    l4m = 1000 * trc.marker_set['L4Mid']
    l4r = 1000 * trc.marker_set['L4Right']
    l5l = 1000 * trc.marker_set['L5Left']
    l5m = 1000 * trc.marker_set['L5Mid']
    l5r = 1000 * trc.marker_set['L5Right']
    ar = np.ones([4, 3])
    ar[:3, :3] = np.eye(3, 3)
    l3_frame = np.zeros([l3l.shape[0], 9])
    l4_frame = np.zeros([l4l.shape[0], 9])
    l5_frame = np.zeros([l5l.shape[0], 9])
    l3l4_list = []
    l4l5_list = []
    time_list = []
    for i in range(15092, end_frame):
        l3 = np.zeros([3, 3])
        l3[:, 0] = l3l.iloc[i, :]
        l3[:, 1] = l3m.iloc[i, :]
        l3[:, 2] = l3r.iloc[i, :]
        time_list.append(trc.data[i, 1])

        t = Cloud.rigid_body_transform(track_l3_ref, l3)
        tr0 = np.matmul(t, ar)
        l3_frame[i, :] = tr0[:3, :3].reshape([1, 9], order='F')

        l4 = np.zeros([3, 3])
        l4[:, 0] = l4l.iloc[i, :]
        l4[:, 1] = l4m.iloc[i, :]
        l4[:, 2] = l4r.iloc[i, :]

        t = Cloud.rigid_body_transform(track_l4_ref, l3)
        tr1 = np.matmul(t, ar)
        l4_frame[i, :] = tr1[:3, :3].reshape([1, 9], order='F')

        l5 = np.zeros([3, 3])
        l5[:, 0] = l5l.iloc[i, :]
        l5[:, 1] = l5m.iloc[i, :]
        l5[:, 2] = l5r.iloc[i, :]

        t = Cloud.rigid_body_transform(track_l5_ref, l5)
        tr2 = np.matmul(t, ar)
        l5_frame[i, :] = tr2[:3, :3].reshape([1, 9], order='F')

        r1 = Cloud.rigid_body_transform(tr1[:3, :3], tr0[:3, :3])
        r2 = Cloud.rigid_body_transform(tr1[:3, :3], tr2[:3, :3])

        l3l4 = Rotation.from_matrix(r1[:3, :3])
        l3l4_euler = l3l4.as_euler('xyz', True)
        l3l4_list.append(l3l4_euler)
        l4l5 = Rotation.from_matrix(r2[:3, :3])
        l4l5_euler = l4l5.as_euler('xyz', True)
        l4l5_list.append(l4l5_euler)
        pass
    p = np.atleast_2d(np.concatenate(l3l4_list)).reshape([len(l3l4_list), 3])
    q = np.atleast_2d(np.concatenate(l4l5_list)).reshape([len(l4l5_list), 3])
    trc.export('S31 L3L4L5 Flexion Hold 350N Compression.pineapple', 'E:/bones/sheep/labelleddata/')
    b2 = Yatsdo.load('E:/bones/sheep/labelleddata/S31 L3L4L5 Flexion Hold 350N Compression.pineapple')
    return b2


def S32tracking():
    end_frame = 25046
    trc = TRC.read('E:/bones/sheep/labelleddata/S32 L3L4L5 Flexion Hold 350N Compression - Labelled.trc')
    track_l3_ = np.load('E:/bones/sheep/labelleddata/S32_track_l3_ref.npz')
    track_l3_ref = np.asarray(track_l3_['a'])
    track_l3_.close()
    track_l4_ = np.load('E:/bones/sheep/labelleddata/S32_track_l4_ref.npz')
    track_l4_ref = np.asarray(track_l4_['a'])
    track_l4_.close()
    track_l5_ = np.load('E:/bones/sheep/labelleddata/S32_track_l5_ref.npz')
    track_l5_ref = np.asarray(track_l5_['a'])
    track_l5_.close()
    l3l = 1000 * trc.marker_set['L3Left']
    l3m = 1000 * trc.marker_set['L3Mid']
    l3r = 1000 * trc.marker_set['L3Right']
    l4l = 1000 * trc.marker_set['L4Left']
    l4m = 1000 * trc.marker_set['L4Mid']
    l4r = 1000 * trc.marker_set['L4Right']
    l5l = 1000 * trc.marker_set['L5Left']
    l5m = 1000 * trc.marker_set['L5Mid']
    l5r = 1000 * trc.marker_set['L5Right']
    ar = np.ones([4, 3])
    ar[:3, :3] = np.eye(3, 3)
    l3_frame = np.zeros([l3l.shape[0], 9])
    l4_frame = np.zeros([l4l.shape[0], 9])
    l5_frame = np.zeros([l5l.shape[0], 9])
    l3l4_list = []
    l4l5_list = []
    time_list = []
    for i in range(15092, end_frame):
        l3 = np.zeros([3, 3])
        l3[:, 0] = l3l.iloc[i, :]
        l3[:, 1] = l3m.iloc[i, :]
        l3[:, 2] = l3r.iloc[i, :]
        time_list.append(trc.data[i, 1])

        t = Cloud.rigid_body_transform(track_l3_ref, l3)
        tr0 = np.matmul(t, ar)
        l3_frame[i, :] = tr0[:3, :3].reshape([1, 9], order='F')

        l4 = np.zeros([3, 3])
        l4[:, 0] = l4l.iloc[i, :]
        l4[:, 1] = l4m.iloc[i, :]
        l4[:, 2] = l4r.iloc[i, :]

        t = Cloud.rigid_body_transform(track_l4_ref, l3)
        tr1 = np.matmul(t, ar)
        l4_frame[i, :] = tr1[:3, :3].reshape([1, 9], order='F')

        l5 = np.zeros([3, 3])
        l5[:, 0] = l5l.iloc[i, :]
        l5[:, 1] = l5m.iloc[i, :]
        l5[:, 2] = l5r.iloc[i, :]

        t = Cloud.rigid_body_transform(track_l5_ref, l5)
        tr2 = np.matmul(t, ar)
        l5_frame[i, :] = tr2[:3, :3].reshape([1, 9], order='F')

        r1 = Cloud.rigid_body_transform(tr1[:3, :3], tr0[:3, :3])
        r2 = Cloud.rigid_body_transform(tr1[:3, :3], tr2[:3, :3])

        l3l4 = Rotation.from_matrix(r1[:3, :3])
        l3l4_euler = l3l4.as_euler('xyz', True)
        l3l4_list.append(l3l4_euler)
        l4l5 = Rotation.from_matrix(r2[:3, :3])
        l4l5_euler = l4l5.as_euler('xyz', True)
        l4l5_list.append(l4l5_euler)
        pass
    p = np.atleast_2d(np.concatenate(l3l4_list)).reshape([len(l3l4_list), 3])
    q = np.atleast_2d(np.concatenate(l4l5_list)).reshape([len(l4l5_list), 3])
    trc.export('S32 L3L4L5 Flexion Hold 350N Compression.pineapple', 'E:/bones/sheep/labelleddata/')
    b2 = Yatsdo.load('E:/bones/sheep/labelleddata/S32 L3L4L5 Flexion Hold 350N Compression.pineapple')
    return b2


def S33tracking():
    end_frame = 25046
    trc = TRC.read('E:/bones/sheep/labelleddata/S33 L3L4L5 Flexion Hold 350N Compression - Labelled.trc')
    track_l3_ = np.load('E:/bones/sheep/labelleddata/S33_track_l3_ref.npz')
    track_l3_ref = np.asarray(track_l3_['a'])
    track_l3_.close()
    track_l4_ = np.load('E:/bones/sheep/labelleddata/S33_track_l4_ref.npz')
    track_l4_ref = np.asarray(track_l4_['a'])
    track_l4_.close()
    track_l5_ = np.load('E:/bones/sheep/labelleddata/S33_track_l5_ref.npz')
    track_l5_ref = np.asarray(track_l5_['a'])
    track_l5_.close()
    l3l = 1000 * trc.marker_set['L3Left']
    l3m = 1000 * trc.marker_set['L3Mid']
    l3r = 1000 * trc.marker_set['L3Right']
    l4l = 1000 * trc.marker_set['L4Left']
    l4m = 1000 * trc.marker_set['L4Mid']
    l4r = 1000 * trc.marker_set['L4Right']
    l5l = 1000 * trc.marker_set['L5Left']
    l5m = 1000 * trc.marker_set['L5Mid']
    l5r = 1000 * trc.marker_set['L5Right']
    ar = np.ones([4, 3])
    ar[:3, :3] = np.eye(3, 3)
    l3_frame = np.zeros([l3l.shape[0], 9])
    l4_frame = np.zeros([l4l.shape[0], 9])
    l5_frame = np.zeros([l5l.shape[0], 9])
    l3l4_list = []
    l4l5_list = []
    time_list = []
    for i in range(15092, end_frame):
        l3 = np.zeros([3, 3])
        l3[:, 0] = l3l.iloc[i, :]
        l3[:, 1] = l3m.iloc[i, :]
        l3[:, 2] = l3r.iloc[i, :]
        time_list.append(trc.data[i, 1])

        t = Cloud.rigid_body_transform(track_l3_ref, l3)
        tr0 = np.matmul(t, ar)
        l3_frame[i, :] = tr0[:3, :3].reshape([1, 9], order='F')

        l4 = np.zeros([3, 3])
        l4[:, 0] = l4l.iloc[i, :]
        l4[:, 1] = l4m.iloc[i, :]
        l4[:, 2] = l4r.iloc[i, :]

        t = Cloud.rigid_body_transform(track_l4_ref, l3)
        tr1 = np.matmul(t, ar)
        l4_frame[i, :] = tr1[:3, :3].reshape([1, 9], order='F')

        l5 = np.zeros([3, 3])
        l5[:, 0] = l5l.iloc[i, :]
        l5[:, 1] = l5m.iloc[i, :]
        l5[:, 2] = l5r.iloc[i, :]

        t = Cloud.rigid_body_transform(track_l5_ref, l5)
        tr2 = np.matmul(t, ar)
        l5_frame[i, :] = tr2[:3, :3].reshape([1, 9], order='F')

        r1 = Cloud.rigid_body_transform(tr1[:3, :3], tr0[:3, :3])
        r2 = Cloud.rigid_body_transform(tr1[:3, :3], tr2[:3, :3])

        l3l4 = Rotation.from_matrix(r1[:3, :3])
        l3l4_euler = l3l4.as_euler('xyz', True)
        l3l4_list.append(l3l4_euler)
        l4l5 = Rotation.from_matrix(r2[:3, :3])
        l4l5_euler = l4l5.as_euler('xyz', True)
        l4l5_list.append(l4l5_euler)
        pass
    p = np.atleast_2d(np.concatenate(l3l4_list)).reshape([len(l3l4_list), 3])
    q = np.atleast_2d(np.concatenate(l4l5_list)).reshape([len(l4l5_list), 3])
    trc.export('S33 L3L4L5 Flexion Hold 350N Compression.pineapple', 'E:/bones/sheep/labelleddata/')
    b2 = Yatsdo.load('E:/bones/sheep/labelleddata/S33 L3L4L5 Flexion Hold 350N Compression.pineapple')
    return b2


def processing(file):
    trc_data = TRC.create_from_c3d(file)
    trace = MarkerTracer(trc_data)
    trace.run()
    trace.cont()

def bk():
    info_sheet = pd.read_excel("E:/bones/sheep/Optitrack data overview.xlsx")
    row = info_sheet.iloc[2, :11].to_list()
    b = 1
    e = 1
    for r in range(0, len(row)):
        if "begin" in row[r].lower():
            row[r] = "{0}_{1}".format(row[r], b)
            b += 1
        if "end" in row[r].lower():
            row[r] = "{0}_{1}".format(row[r], e)
            e += 1
    clean = info_sheet.iloc[3:15, :11].to_numpy()
    info = pd.DataFrame(data=clean, columns=row)
    k = {s: {} for s in info['Sample']}
    for r in range(0, info.shape[0], 3):
        # k[info.iloc[r, 0]] = {info.iloc[r, 1]: {'a': {'begin': info.iloc[r, 3], 'end': info.iloc[r, 4]},
        #                                         'b': {'begin': info.iloc[r, 5], 'end': info.iloc[r, 6]},
        #                                         'c': {'begin': info.iloc[r, 7], 'end': info.iloc[r, 8]},
        #                                         'd': {'begin': info.iloc[r, 9], 'end': info.iloc[r, 10]}
        #                                         },
        #                       info.iloc[r+1, 1]: {'a': {'begin': info.iloc[r+1, 3], 'end': info.iloc[r+1, 4]},
        #                                           'b': {'begin': info.iloc[r + 1, 5], 'end': info.iloc[r + 1, 6]},
        #                                           'c': {'begin': info.iloc[r + 1, 7], 'end': info.iloc[r + 1, 7]},
        #                                           'd': {'begin': info.iloc[r + 1, 9], 'end': info.iloc[r + 1, 10]}
        #                                           },
        #                       info.iloc[r+2, 1]: {'a': {'begin': info.iloc[r+2, 3], 'end': info.iloc[r+2, 4]},
        #                                           'b': {'begin': info.iloc[r + 2, 5], 'end': info.iloc[r + 2, 6]},
        #                                           'c': {'begin': info.iloc[r + 2, 7], 'end': info.iloc[r + 2, 8]},
        #                                           'd': {'begin': info.iloc[r + 2, 9], 'end': info.iloc[r + 2, 10]},
        #                                           }}
        k[info.iloc[r, 0]] = {info.iloc[r, 1]: {'a': [info.iloc[r, 3], info.iloc[r, 4]],
                                                'b': [info.iloc[r, 5], info.iloc[r, 6]],
                                                'c': [info.iloc[r, 7], info.iloc[r, 8]],
                                                'd': [info.iloc[r, 9], info.iloc[r, 10]]
                                                },
                              info.iloc[r + 1, 1]: {'a': [info.iloc[r + 1, 3], info.iloc[r + 1, 4]],
                                                    'b': [info.iloc[r + 1, 5], info.iloc[r + 1, 6]],
                                                    'c': [info.iloc[r + 1, 7], info.iloc[r + 1, 8]],
                                                    'd': [info.iloc[r + 1, 9], info.iloc[r + 1, 10]]
                                                    },
                              info.iloc[r + 2, 1]: {'a': [info.iloc[r + 2, 3], info.iloc[r + 2, 4]],
                                                    'b': [info.iloc[r + 2, 5], info.iloc[r + 2, 6]],
                                                    'c': [info.iloc[r + 2, 7], info.iloc[r + 2, 8]],
                                                    'd': [info.iloc[r + 2, 9], info.iloc[r + 2, 10]],
                                                    }}
    # merger_land()

    # calibrate_tracking_markers()

    # trc = TRC.create_from_c3d('E:/bones/sheep/labelleddata/S29 L3L4L5 Flexion Hold 350N Compression - Labelled.c3d')
    # merger = MarkerMerger(trc)
    # new_trc = merger.run()
    # new_trc.write('E:/bones/sheep/labelleddata/S29 L3L4L5 Flexion Hold 350N Compression - Labelled.trc')


def grab_markers():
    root = "E:/bones/sheep/Optitrack data/"
    map_sheet = pd.read_excel("E:/bones/sheep/landmarkers.xlsx")
    markers = {t: {} for t in map_sheet['file'].to_list()}
    trials = {t: TRC.create_from_c3d(root+t) for t in markers}
    for r in range(0, map_sheet.shape[0]):
        row = map_sheet.iloc[r, :]
        t = trials[row['file']]
        markers[row['file']][row['marker']] = t.marker_set[row['c3d_marker']].iloc[row['frame'], :].to_list()
        pass
    JSONSUtl.write_json("E:/bones/sheep/landmarkers.json", markers)
    return markers


def info_sheet():
    infosheet = pd.read_excel("E:/bones/sheep/Optitrack data overview.xlsx")
    row = infosheet.iloc[2, :11].to_list()
    b = 1
    e = 1
    for r in range(0, len(row)):
        if "begin" in row[r].lower():
            row[r] = "{0}_{1}".format(row[r], b)
            b += 1
        if "end" in row[r].lower():
            row[r] = "{0}_{1}".format(row[r], e)
            e += 1
    clean = infosheet.iloc[3:15, :11].to_numpy()
    info = pd.DataFrame(data=clean, columns=row)
    k = {s: {} for s in info['Sample']}
    for r in range(0, info.shape[0], 3):
        k[info.iloc[r, 0]] = {info.iloc[r, 1]: {'a': [info.iloc[r, 3], info.iloc[r, 4]],
                                                'b': [info.iloc[r, 5], info.iloc[r, 6]],
                                                'c': [info.iloc[r, 7], info.iloc[r, 8]],
                                                'd': [info.iloc[r, 9], info.iloc[r, 10]]
                                                },
                              info.iloc[r + 1, 1]: {'a': [info.iloc[r + 1, 3], info.iloc[r + 1, 4]],
                                                    'b': [info.iloc[r + 1, 5], info.iloc[r + 1, 6]],
                                                    'c': [info.iloc[r + 1, 7], info.iloc[r + 1, 8]],
                                                    'd': [info.iloc[r + 1, 9], info.iloc[r + 1, 10]]
                                                    },
                              info.iloc[r + 2, 1]: {'a': [info.iloc[r + 2, 3], info.iloc[r + 2, 4]],
                                                    'b': [info.iloc[r + 2, 5], info.iloc[r + 2, 6]],
                                                    'c': [info.iloc[r + 2, 7], info.iloc[r + 2, 8]],
                                                    'd': [info.iloc[r + 2, 9], info.iloc[r + 2, 10]],
                                                    }}
        return info


class Landmarks(Enum):
    a = ('S1a', 'S2a', 'S3a', 'S4a')
    b = ('S1b', 'S2b', 'S3b', 'S4b')
    c = ('S1c', 'S2c', 'S3c', 'S4c')
    d = ('S1d', 'S2d', 'S3d', 'S4d')


def processing():
    # w = grab_markers()
    lm = JSONSUtl.load_json("E:/bones/sheep/landmarkers.json")
    info = info_sheet()
    ant_sty = pd.read_excel("E:/bones/sheep/ant_sty.xlsx")
    post_sty = pd.read_excel("E:/bones/sheep/post_sty.xlsx")

    asty = np.ones([4, 5])
    asty[:3, 0] = ant_sty.iloc[0, 1:] / 1000
    asty[:3, 1] = ant_sty.iloc[1, 1:] / 1000
    asty[:3, 2] = ant_sty.iloc[2, 1:] / 1000
    asty[:3, 3] = ant_sty.iloc[3, 1:] / 1000
    asty[:3, 4] = ant_sty.iloc[4, 1:] / 1000

    psty = np.ones([4, 5])
    psty[:3, 0] = post_sty.iloc[0, 1:] / 1000
    psty[:3, 1] = post_sty.iloc[1, 1:] / 1000
    psty[:3, 2] = post_sty.iloc[2, 1:] / 1000
    psty[:3, 3] = post_sty.iloc[3, 1:] / 1000
    psty[:3, 4] = post_sty.iloc[4, 1:] / 1000
    tlm = {}
    for t in lm:
        trial = lm[t]
        a = [trial[x] for x in Landmarks.a.value]
        b = [trial[x] for x in Landmarks.b.value]
        c = [trial[x] for x in Landmarks.c.value]
        d = [trial[x] for x in Landmarks.d.value]

        def ldmk(ax):
            alm = np.zeros([3, 4])
            alm[:3, 0] = ax[0]
            alm[:3, 1] = ax[1]
            alm[:3, 2] = ax[2]
            alm[:3, 3] = ax[3]
            trx = Cloud.rigid_body_transform(asty[:3, :4], alm)
            aty = np.matmul(trx, asty)
            return aty[:3, -1].tolist()

        tlm[t] = {'a': ldmk(a),
                  'b': ldmk(b),
                  'c': ldmk(c),
                  'd': ldmk(d),
                  }
    JSONSUtl.write_json("E:/bones/sheep/L3L4L5_landmarks.json", tlm)
    pass

    # processing(r"E:\bones\sheep\Optitrack data\S30 L3L4L5 Posterior landmarks.c3d")
    #
    # # S29_calibration(k['S29'])
    # # S29tracking()
    #
    # S30_calibration(k['S30'])
    # S30tracking()
    # S31_calibration(k['S31'])
    # S32_calibration(k['S32'])
    # S33_calibration(k['S33'])


def graph_results():
    file_path = "E:/bones/sheep/S29_L3_tracking.csv"
    m1 = pd.read_csv(file_path)
    m1_xyz = 1000 * m1[['x', 'y', 'z']].to_numpy()
    m1_xyz -= m1_xyz[0, :]
    # m1_xyz_b = np.zeros(m1_xyz.shape)
    # for i in range(0,3):
    #     m1_xyz_b[:, i] = Butterworth.butter_low_filter(m1_xyz[:, i], 20, 100, 4)
    plt.figure()
    plt.title(os.path.split(file_path)[1][:-4])
    plt.plot(m1_xyz)
    plt.legend(['x', 'y', 'z'])
    plt.xlabel("Frames")
    plt.ylabel("Displacement (mm)")
    plt.show()


if __name__ == '__main__':
    #graph_results()
    idx = "29"
    idx = '30'
    idx = '31'
    idx = '32'
    idx = '33'
    trc = TRC.read('E:/bones/sheep/labelleddata/S{0} L3L4L5 Flexion Hold 350N Compression - Labelled.trc'.format(idx))
    l3l = 1000 * trc.marker_set['L3Left']
    l3m = 1000 * trc.marker_set['L3Mid']
    l3r = 1000 * trc.marker_set['L3Right']

    st = 26078
    st = 23198
    st = 21068
    st = 23020
    st = 22000

    ed = 26469
    ed = 23563
    ed = 21575
    ed = 23509
    ed = 22363

    sample0 = l3l.to_numpy()[st:ed, :]
    sample1 = l3m.to_numpy()[st:ed, :]
    sample2 = l3r.to_numpy()[st:ed, :]
    avg = (sample0+sample1+sample2)/3.0
    # avg = sample0
    r = PCAModel.pca_rotation(avg)
    rx = Rotation.from_matrix(r.transformation)
    rd = rx.as_euler('xyz', degrees=True)
    for m in trc.marker_set:
        print(m)
        col = [c for c in trc.marker_set[m].columns]
        trc.marker_set[m] = pd.DataFrame(data=(np.matmul(r.transformation, (trc.marker_set[m].to_numpy()).T)).T,
                                         columns=col)

    trc.x_up_to_y_up()

    vb = trc.marker_set['L3Mid'].to_numpy()[ed, :]
    va = trc.marker_set['L5Mid'].to_numpy()[ed, :]
    v0 = vb - va
    nv0 = v0/ np.linalg.norm(v0)
    vd = trc.marker_set['L4Left'].to_numpy()[ed, :]
    vc = trc.marker_set['L4Right'].to_numpy()[ed, :]
    v1 = vd - vc
    nv1 = v1 / np.linalg.norm(v1)
    nv2 = np.cross(nv0, nv1)
    nv2 = nv2/ np.linalg.norm(nv2)
    nv3 = np.cross(nv2, nv1)
    nv3 = nv3/ np.linalg.norm(nv3)
    nx = np.eye(3)
    nx[:, 0] = nv3
    nx[:, 1] = nv2
    nx[:, 2] = nv1
    dx = np.linalg.det(nx)
    rd = Rotation.from_matrix(nx.T)
    print(rd.as_euler('xyz', degrees=True))
    angle = np.rad2deg(Trig.angle_between_2_vectors(va, vb))
    rx = Rotation.from_euler('xyz', [angle/2.0, 0, 0], degrees=True)
    print(angle/2.0)
    ry = rx.as_matrix()
    for m in trc.marker_set:
        print(m)
        col = [c for c in trc.marker_set[m].columns]
        trc.marker_set[m] = pd.DataFrame(data=(np.matmul(np.linalg.inv(nx), (trc.marker_set[m].to_numpy()).T)).T,
                                         columns=col)

    rx0 = Rotation.from_euler('xyz', [0, 90, 0], degrees=True)
    ry0 = rx0.as_matrix()
    rx1 = Rotation.from_euler('xyz', [-90, 0, 0], degrees=True)
    ry1 = rx1.as_matrix()
    ry = np.matmul(ry1, ry0)
    for m in trc.marker_set:
        print(m)
        col = [c for c in trc.marker_set[m].columns]
        trc.marker_set[m] = pd.DataFrame(data=(np.matmul(ry, (trc.marker_set[m].to_numpy()).T)).T,
                                         columns=col)

    file_path = "E:/bones/sheep/S{0}_L3_tracking.csv".format(idx)
    m1 = pd.read_csv(file_path)
    m1_xyz = m1[['x', 'y', 'z']].to_numpy()
    m1_xyz1 = np.matmul(r.transformation, m1_xyz.T)
    # r0 = Rotation.from_euler('xyz', [90, 0, 90], degrees=True)
    # t = (np.matmul(r0.as_matrix(), m1_xyz1)).T
    # m1.iloc[:, 4:] = t
    t = (trc.marker_set['L3Mid'].to_numpy() + trc.marker_set['L3Left'].to_numpy() + trc.marker_set[
        'L3Right'].to_numpy()) / 3.0
    m1.iloc[:, 4:] = t[:m1.shape[0], ]
    try:
        col = [c for c in trc.marker_set["5113"].columns]
        trc.marker_set["5113"].iloc[:, :] = t
    except KeyError:
        pass
    m1.to_csv("E:/bones/sheep/S{0}_L3_tracking_aligned.csv".format(idx), index=False)

    file_path = "E:/bones/sheep/S{0}_L4_tracking.csv".format(idx)
    m1 = pd.read_csv(file_path)
    m1_xyz = m1[['x', 'y', 'z']].to_numpy()
    m1_xyz1 = np.matmul(r.transformation, m1_xyz.T)
    # r0 = Rotation.from_euler('xyz', [-90, 0, 90], degrees=True)
    #t = (np.matmul(r0.as_matrix(), m1_xyz1)).T
    t = (trc.marker_set['L4Mid'].to_numpy() + trc.marker_set['L4Left'].to_numpy() + trc.marker_set['L4Right'].to_numpy())/3.0
    m1.iloc[:, 4:] = t[:m1.shape[0], ]
    try:
        col = [c for c in trc.marker_set["5114"].columns]
        trc.marker_set["5114"].iloc[:, :] = t
    except KeyError:
        pass
    m1.to_csv("E:/bones/sheep/S{0}_L4_tracking_aligned.csv".format(idx), index=False)

    file_path = "E:/bones/sheep/S{0}_L5_tracking.csv".format(idx)
    m1 = pd.read_csv(file_path)
    m1_xyz = m1[['x', 'y', 'z']].to_numpy()
    m1_xyz1 = np.matmul(r.transformation, m1_xyz.T)
    # r0 = Rotation.from_euler('xyz', [-90, 0, 90], degrees=True)
    # t = (np.matmul(r0.as_matrix(), m1_xyz1)).T
    # m1.iloc[:, 4:] = t

    t = (trc.marker_set['L5Mid'].to_numpy() + trc.marker_set['L5Left'].to_numpy() + trc.marker_set[
        'L5Right'].to_numpy()) / 3.0
    m1.iloc[:, 4:] = t[:m1.shape[0], ]
    try:
        col = [c for c in trc.marker_set["5115"].columns]
        trc.marker_set["5115"].iloc[:, :] = t
    except:
        pass
    m1.to_csv("E:/bones/sheep/S{0}_L5_tracking_aligned.csv".format(idx), index=False)

    print("write")
    trc.write('E:/bones/sheep/labelleddata/S{0} L3L4L5 Flexion Hold 350N Compression - aligned.trc'.format(idx))
    pass
