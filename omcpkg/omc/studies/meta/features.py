import copy
import os
import time
import socket

import numpy as np

from yatpkg.mlde.tags import MLKeys
from yatpkg.mlde.ml_util import MLOperations
from yatpkg.util.data import stamp, StorageIO, Yatsdo, Bapple, JSONSUtl
import matplotlib.pyplot as plt
import pandas as pd
from scipy.spatial.transform import Rotation

def read_imu():
    d, dt = stamp()
    print("Script Start\nTime: {0}".format(d))
    root = "M:/Mocap/Movella/"
    particip = [f for f in os.listdir(root)]
    for f in particip:
        print(f)
        for e in os.listdir("{0}{1}/".format(root, f)):
            files = [a for a in os.listdir("{0}{1}/{2}".format(root, f, e)) if a.endswith(".pkl")]
            if len(files) == 0:
                continue
            for a in files:
                filename = a[:-4]
                if not filename.endswith(".csv"):
                    filename+=".csv"
                else:
                    if os.path.exists("{0}{1}/{2}/{3}".format(root, f, e, filename)):
                        os.remove("{0}{1}/{2}/{3}".format(root, f, e, filename))
                    if os.path.exists("{0}{1}/{2}/{3}.csv".format(root, f, e, filename)):
                        os.remove("{0}{1}/{2}/{3}.csv".format(root, f, e, filename))
                print("{2}: out: {0}, in: {1}".format(filename, a, f))
                try:
                    data0 = pd.read_pickle("{0}{1}/{2}/{3}".format(root, f, e, a))
                    data0.iloc[:, 0] = data0.iloc[:, 0] - data0.iloc[0, 0]
                    data0.to_csv("{0}{1}/{2}/{3}".format(root, f, e, filename))
                except IndexError:
                    print("{2}: Error in file - out: {0}, in: {1}".format(filename, a, f))
                pass
    pass

def read_sto():
    d, dt = stamp()
    print("Script Start\nTime: {0}".format(d))
    s = StorageIO.load("M:/Mocap/P025/New Session/Gap_Fill/motion_files/Reactive VR 01.sto")
    y = Yatsdo.create_from_storage_io(s)
    ret_pd = y.to_panda_data_frame()

    d, _ = stamp()
    print("Script End\nTime: {0}".format(d))
    plt.figure()
    plt.scatter(y.x, -s.data['knee_angle_r'], marker='.')
    plt.plot(y.x, -ret_pd['knee_angle_r'], color='orange')
    plt.show()


def extract_raw_imu():
    d, dt = stamp()
    print("Script Start\nTime: {0}".format(d))
    root = "M:/Mocap/Movella_Re/"
    particip = [f for f in os.listdir(root)]
    g = np.transpose(np.atleast_2d([0, 0, 9.79936])) # Auckland
    for f in particip:
        print(f)
        for e in os.listdir("{0}{1}/".format(root, f)):
            ori_files = [a for a in os.listdir("{0}{1}/{2}".format(root, f, e)) if a.endswith("imu_ori.csv")]
            vec3_files = [a for a in os.listdir("{0}{1}/{2}".format(root, f, e)) if a.endswith("imu_vec3.csv") or a.endswith("imu_vec3_2.csv")]
            if len(vec3_files) != len(ori_files):
                continue
            for a in range(0, len(ori_files)):
                v_filename = vec3_files[a][:-4]
                paths = "{0}{1}/{2}/{3}".format(root, f, e, vec3_files[a])
                vd = pd.read_csv(paths)
                o_filename = ori_files[a][:-4]
                print("{0}: {1} - {2}, {3}".format(f, e, o_filename, v_filename))
                paths = "{0}{1}/{2}/{3}".format(root, f, e, ori_files[a])
                od = pd.read_csv(paths)
                m_acc = vd[["m_acceleration_X", "m_acceleration_Y", "m_acceleration_Z"]]
                m_vel = vd[["m_angularVelocity_X", "m_angularVelocity_Y", "m_angularVelocity_Z"]]
                m_acc_raw = np.zeros(m_acc.shape)
                for i in range(0, od.shape[0]):
                    q = od.iloc[i, 1:].to_list()
                    ro = Rotation.from_quat([q[1], q[2], q[3], q[0]])
                    gn = np.squeeze(np.matmul(ro.as_matrix(), g))
                    m_acc_raw[i, :] = m_acc.iloc[i, :] + gn
                    pass
                try:
                    imu_data = np.zeros([od.shape[0], 7])
                    imu_data[:, 0] = od.iloc[:, 0].to_list()
                    imu_data[:, 0] = imu_data[:, 0] - imu_data[0, 0]
                    imu_data[:, 1:4] = m_acc_raw
                    imu_data[:, 4:] = m_vel.to_numpy()
                    imu_data_pd = pd.DataFrame(data=imu_data, columns=["time", "m_acceleration_X", "m_acceleration_Y", "m_acceleration_Z", "m_angularVelocity_X", "m_angularVelocity_Y", "m_angularVelocity_Z"])
                    paths = "{0}{1}/{2}/{3}_raw.csv".format(root, f, e, v_filename)
                    imu_data_pd.to_csv(paths, index=False)
                except IndexError:
                    pass
                pass
    pass

def window_imu():
    root = "M:/Mocap/Movella_Re/"
    def element(p0):
        print(p0)
        return p0
    participants = [[p, root+element(p)+"/"] for p in os.listdir(root) if os.path.isdir(root+p)]
    tasks = {p[0]:{t: p[1]+t+"/" for t in os.listdir(p[1]) if 'figure' not in t.lower()} for p in participants}
    host_comp = socket.gethostname()
    for p in tasks:
        print(p)
        print('checking')
        os.listdir('M:/Mocap/')
        while os.path.exists('M:/Mocap/block.txt'):
            time.sleep(0.5)
            os.listdir('M:/Mocap/')
        print('blocking')
        with open('M:/Mocap/block.txt', 'w') as f:
            f.write('')
        print('loading dk')
        dk = JSONSUtl.load_json('M:/Mocap/run_log.json')
        if host_comp in dk['current']:
            if p not in dk['current'][host_comp]:
                if p in dk['taken']:
                    os.remove('M:/Mocap/block.txt')
                    continue
                else:
                    dk['taken'].append(p)
                    dk['current'][host_comp] = p
        else:
            if p in dk['taken']:
                os.remove('M:/Mocap/block.txt')
                continue
            else:
                dk['taken'].append(p)
                dk['current'][host_comp] = p
        print('update dk')
        JSONSUtl.write_json("M:/Mocap/run_log.json", dk)
        os.listdir('M:/Mocap/')
        print('unblock')
        time.sleep(0.5)
        os.remove('M:/Mocap/block.txt')

        for t in tasks[p]:
            files = ["{0}{1}".format(tasks[p][t], f) for f in os.listdir(tasks[p][t]) if '_raw.csv' in f]
            file_features = [f for f in os.listdir(tasks[p][t]) if '_features.pkl' in f]
            for f in files:
                fl = os.path.split(f)[1]
                flx = fl[:fl.index('_raw.csv')]
                if "{0}_features.pkl".format(flx) in file_features:
                    print("skip")
                    continue
                data = pd.read_csv(f)
                data_np = data.to_numpy()
                dt = np.nanmean(data_np[1:, 0] - data_np[:-1, 0])
                freq = 1/dt
                window = int(np.round(freq, 0))  # 1 sec window
                try:
                    y_obj = Yatsdo(data)
                except ValueError:
                    print("{0} - {1} value error".format(p, t))
                    continue
                timepoints = [[data_np[i, 0], data_np[i:i+60, 0]] for i in range(window, int(data.shape[0]))]
                def tsfresh_matrix(ix, w):
                    cols = copy.deepcopy(y_obj.column_labels)
                    cols.insert(0, 'id')
                    ret = np.zeros([w.shape[0], 8])
                    ret[:, 0] = int(ix+1)
                    ret[:, 1:] = w
                    return pd.DataFrame(data=ret, columns=cols)
                windows = [tsfresh_matrix(i, y_obj.get_samples(timepoints[i][1])) for i in range(0, len(timepoints))]
                raw_concat = pd.concat(windows, axis=0)
                window_map = pd.DataFrame(data=raw_concat.to_numpy(), columns=[c for c in raw_concat.columns])
                efx, param = MLOperations.extract_features_from_x(window_map,fc_parameters=MLKeys.CFCParameters,n_jobs=4)
                try:
                    MLOperations.export_features(efx, param,tasks[p][t], "{0}_features".format(flx))
                except OSError:
                    os.listdir('M:/Mocap/')
                    try:
                        MLOperations.export_features(efx, param, tasks[p][t], "{0}_features".format(flx))
                    except OSError:
                        print("{0} - {1} OS error, cannot pickle".format(p, t))
                        continue
                pass
            print(t)
    b = Bapple()
    # import imu data

    pass

def extract_features():
    pass

def pca_features():
    pass


if __name__ == '__main__':
    # read_imu()
    # extract_raw_imu()
    window_imu()
    pass
