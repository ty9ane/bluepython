from yatpkg.math.transformation import Quaternion
from yatpkg.util.data import TRC, StorageIO, StorageType
import os
import pandas as pd
import numpy as np

if __name__ == '__main__':
    # Read data
    root = "E:/meta/record/"
    trials = [t for t in os.listdir(root) if 'test' not in t.lower()]
    files_ori = [t for t in os.listdir(root + trials[50]) if 'ori' in t]
    files_v3 = [t for t in os.listdir(root + trials[50]) if 'vec3' in t]
    ret = None
    cols = ["time"]
    for f in range(0, len(files_ori)):

        d = pd.read_csv(root + trials[50] + "/" + files_ori[f])
        v = pd.read_csv(root + trials[50] + "/" + files_v3[f])

        cols.append(files_ori[f].split("_")[0] + "_m_position_X")
        cols.append(files_ori[f].split("_")[0] + "_m_position_Y")
        cols.append(files_ori[f].split("_")[0] + "_m_position_Z")
        ref = None
        if ret is None:
            ret = np.zeros([v.shape[0], len(files_ori)*3+1])
        for i in range(0, d.shape[0]):
            ret[i, 0] = v.iloc[i, 0]
            n = d.iloc[i, :].to_list()
            q = Quaternion(n[1:])
            m = q.to_mat()
            p = v[["m_position_X", "m_position_Y", "m_position_Z"]].iloc[i, :].to_list()
            tm = np.eye(4,4)
            tm[:3, :3] = m
            tm[:-1, 3] = p
            idxm = f*3+1
            ret[i, idxm:idxm+3] = p

    out = pd.DataFrame(data=ret, columns=cols)
    trc = TRC(out)
    trc.update()
    # s:TRC = StorageIO.load("F:/Wearable Sensing Project/Experiments/ABI SmartSplint/P08/TRC/P8IMUWalk03.trc", sto_type=StorageType.trc)
    out_file = root + trials[50] + "/marker_test.trc"
    trc.write(out_file)
    pass
    # q to matrx
    # first frame as ref
    # re calculate transform
    # apply transform

    pass