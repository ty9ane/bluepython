#  Copyright (c) 2003-2023 Movella Technologies B.V. or subsidiaries worldwide.
#  All rights reserved.
#  
#  Redistribution and use in source and binary forms, with or without modification,
#  are permitted provided that the following conditions are met:
#  
#  1.	Redistributions of source code must retain the above copyright notice,
#  	this list of conditions and the following disclaimer.
#  
#  2.	Redistributions in binary form must reproduce the above copyright notice,
#  	this list of conditions and the following disclaimer in the documentation
#  	and/or other materials provided with the distribution.
#  
#  3.	Neither the names of the copyright holders nor the names of their contributors
#  	may be used to endorse or promote products derived from this software without
#  	specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
#  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
#  THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
#  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
#  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
#  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
import time
from time import sleep

# import numpy as np
import pandas as pd

# from helpers import kbhit
import xme  # Import the xme python interface normally
from multiprocessing import Queue
from multiprocessing import Process
from multiprocessing import Pool
import sys
import os
import threading
import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt

# import xme.xme64 as xme           # Import the xme python interface for PyCharm, so its auto-completion works.
from PySide6.QtWidgets import (QApplication, QLabel, QWidget, QPushButton, QVBoxLayout, QLineEdit, QHBoxLayout,
                               QComboBox, QFileDialog, QMessageBox)

awindaChannel = 15
data_queue = Queue()
command = Queue()
imu_command = Queue()
feedback = Queue()
idx_ = "P06"

def sensors_list():

    sensors_r = [
        ['Pelvis', 0],
        ['T8', 4],
        ['Head', 6],
        ['RightShoulder', 7],
        ['RightUpperArm', 8],
        ['RightForeArm', 9],
        ['RightHand', 10],
        ['LeftShoulder', 11],
        ['LeftUpperArm', 12],
        ['LeftForeArm', 13],
        ['LeftHand', 14],
        ['RightUpperLeg', 15],
        ['RightLowerLeg', 16],
        ['RightFoot', 17],
        ['LeftUpperLeg', 19],
        ['LeftLowerLeg', 20],
        ['LeftFoot', 21]
    ]
    return sensors_r

def col_names():
    col_name = []

    for i in sensors_list():
        col_name.append(i[0] + 'angularAcceleration_X')
        col_name.append(i[0] + 'angularAcceleration_Y')
        col_name.append(i[0] + 'angularAcceleration_Z')
        col_name.append(i[0] + 'angularVelocity_X')
        col_name.append(i[0] + 'angularVelocity_Y')
        col_name.append(i[0] + 'angularVelocity_Z')
        col_name.append(i[0] + 'acceleration_X')
        col_name.append(i[0] + 'acceleration_Y')
        col_name.append(i[0] + 'acceleration_Z')
        col_name.append(i[0] + 'velocity_X')
        col_name.append(i[0] + 'velocity_Y')
        col_name.append(i[0] + 'velocity_Z')
        col_name.append(i[0] + 'position_X')
        col_name.append(i[0] + 'position_Y')
        col_name.append(i[0] + 'position_Z')
    return col_name

col_sen_name = col_names()

# Create the callbacks
class MyXmeCallbacks(xme.XmeCallback):
    def __init__(self):
        super(MyXmeCallbacks, self).__init__()
        self.calibrationAborted = False
        self.calibrationComplete = False
        self.calibrationProcessed = False
        self.counter = 0
    def onBufferOverflow(self, dev):
        print("WARNING! Data buffer overflow. Your PC can't keep up with the incoming data. Buy a better PC.")

    def onCalibrationAborted(self, dev):
        print("Calibration aborted")
        self.calibrationAborted = True

    def onCalibrationComplete(self, dev):
        print("Calibration completed")
        self.calibrationComplete = True

    def onCalibrationProcessed(self, dev):
        print("Calibration processed")
        self.calibrationProcessed = True

    def onHardwareDisconnected(self, dev):
        print("Hardware disconnected")

    def onHardwareError(self, dev):
        xmeStatus = xmeControl.status()  # important to get a copy!
        suitStatus = xmeStatus.suitStatus()
        hardwareStatus = suitStatus.m_hardwareStatus

        if suitStatus.m_masterDevice.m_deviceId.isAwindaX() and suitStatus.m_wirelessChannel != awindaChannel:
            print("Setting radio channel of", suitStatus.m_masterDevice.m_deviceId, "to", awindaChannel)
            dev.setRadioChannel(suitStatus.m_masterDevice.m_deviceId, awindaChannel)

        if hardwareStatus == xme.XHS_HardwareOk:
            print("The suit is configured correctly")

        elif hardwareStatus == xme.XHS_Error:
            print("An error occurred during initialization: %s" % suitStatus.m_hardwareStatusText)
            print("Device ID=%d" % suitStatus.m_lastProblematicDeviceId)

        elif hardwareStatus == xme.XHS_NothingFound:
            print("No master device was found")

        elif hardwareStatus == xme.XHS_MissingSensor:
            print("Master id: %08X" % suitStatus.m_masterDevice.m_deviceId)
            print("At least one usable Motion Tracker is missing")
            missingSensors = suitStatus.m_missingSensors
            print("%u missing locations:" % len(missingSensors))
            for missingSensor in missingSensors:
                print(" %d(%s)" % (missingSensor, xmeControl.segmentName(missingSensor)))

            # Check for duplicates
            duplicates = xme.XsIntArray()
            duplicatesIds = xme.XsDeviceIdArray()
            for index in range(len(suitStatus.m_sensors)):
                if suitStatus.m_sensors[index].m_validity == xme.XDV_Duplicate:
                    duplicates.push_back(suitStatus.m_sensors[index].m_segmentId)
                    duplicatesIds.push_back(suitStatus.m_sensors[index].m_deviceId)
            if len(duplicates):
                print("%d duplicate locations:" % len(duplicates))
                for index in range(len(duplicates)):
                    print(" [%s-%d(%s)]"
                          % (duplicatesIds[index], duplicates[index], xmeControl.segmentName(duplicates[index])))

        elif hardwareStatus == xme.XHS_ObrMode:
            print("The system is configured for On Body Recording mode, preventing it from being initialized")

        elif hardwareStatus == xme.XHS_Invalid:
            print("Some invalid hardware was detected or the status is uninitialized")

        else:
            print("The system is in unknown state")

    def onHardwareReady(self, dev):
        print("\nHardware is ready")
        xmeStatus = xmeControl.status()  # important to get a copy!
        sensors = xmeStatus.suitStatus().m_sensors
        print("Detected %u sensors\nLocation | DeviceId" % len(sensors))
        for sensor in sensors:
            print("%8d | 0x%x" % (sensor.m_segmentId, sensor.m_deviceId))

    def onLowBatteryLevel(self, dev):
        xmeStatus = xmeControl.status()  # important to get a copy!
        sensors = xmeStatus.suitStatus().m_sensors
        skipBatteryWarning = False
        for sensor in sensors:
            if ((sensor.m_batteryLevel == 0) or (sensor.m_batteryLevel == -1)):
                skipBatteryWarning = True
        if not (skipBatteryWarning):
            print("Attention! Low battery level!")

    def onPoseReady(self, dev):
        self.counter += 1
        if ((self.counter % 1) == 0):
            showPose(xme.XME_LAST_AVAILABLE_FRAME)

    def onRecordingStateChanged(self, dev, newState):
        if newState == xme.XRS_NotRecording:
            print("REC: Recording stopped")
        elif newState == xme.XRS_WaitingForStart:
            print("REC: Waiting for recording to start")
        elif newState == xme.XRS_Recording:
            print("REC: Recording started")
        elif newState == xme.XRS_Flushing:
            print("REC: Flushing data, receiving retransmissions")
        else:
            print("REC: Unknown state")

    def onHardwareWarning(self, dev, resultValue, additionalMessage):
        print("Hardware warning! Result value number %d, additional message: %s" % (resultValue, additionalMessage))


# Showcase functions:
def showPose(frameNumber):
    global data_queue
    pose = xmeControl.pose(frameNumber)
    sens = sensors_list()

    if pose.empty():
        print("No data available")
        return

    segmentStates = pose.m_segmentStates

    data_states = {i[0]: [int(i[1]), np.zeros([3, 5]), np.array([0,0,0,0])] for i in sens}
    for i in data_states:
        data_states[i][1][:, 0] = segmentStates[data_states[i][0]].m_angularAcceleration
        data_states[i][1][:, 1] = segmentStates[data_states[i][0]].m_angularVelocity
        data_states[i][1][:, 2] = segmentStates[data_states[i][0]].m_acceleration
        data_states[i][1][:, 3] = segmentStates[data_states[i][0]].m_velocity
        data_states[i][1][:, 4] = segmentStates[data_states[i][0]].m_position
        data_states[i][2] = np.array(segmentStates[data_states[i][0]].m_orientation)
    data_states['time'] = pose.m_relativeTime

    # Find the joint angle between the upper and the lower right leg (the right knee)
    jointAngles = np.array(xmeControl.jointAngles(pose))
    joints = xmeControl.joints()

    col = [j.name() for j in joints]
    p = pd.DataFrame(data=jointAngles.T, columns=col)
    data_states['joint_angles'] = p
    data_queue.put(data_states)
    # print(p)
    #print(data_queue.qsize())


# def process_data(trial_name):
#     print("trigger")
#     combine_joints(trial_name)
#     sensors = sensors_list()
#     combine_ori(sensors, trial_name)
#     combine_imu_vec3(sensors, trial_name)
#     print("combined")


# def combine_ori(sensors, trial_name):
#     print("combine frames IMU ori")
#     cols = ["time", "w", "i", "j", "k"]
#     work = [[cols, s, trial_name] for s in sensors]
#     p = Pool(6)
#     p.map(worker_ori, work)


# def worker_ori(wrk):
#     cols = wrk[0]
#     s = wrk[1]
#     trial_name = wrk[2]
#     sensor_files = [f for f in os.listdir("./record/" + trial_name) if s[0] in f and 'ori' in f]
#     m = np.zeros([len(sensor_files), len(cols)])
#     idx = 0
#     for f in sensor_files:
#         try:
#             nameo = "./record/" + trial_name + "/" + f
#             t = float(f.split("_")[-1].split(".")[0]) / 1000.0
#             m[idx, 0] = t
#             n = np.load(nameo)
#             m[idx, 1:] = n
#             idx += 1
#         except Exception:
#             pass
#     for f in sensor_files:
#         try:
#             nameo = "./record/" + trial_name + "/" + f
#             os.remove(nameo)
#         except Exception:
#             pass
#     ret = pd.DataFrame(data=m, columns=cols)
#     ret.to_csv("./record/" + trial_name + "/" + s[0] + "_imu_ori.csv", index=False)
#
#
# def combine_imu_vec3(sensors, trial_name):
#     print("combining frames IMU vec3")
#     cols_raw = ["m_angularAcceleration", "m_angularVelocity", "m_acceleration", "m_velocity", "m_position"]
#     cols = ['time']
#     for c in cols_raw:
#         cols.append(c + "_X")
#         cols.append(c + "_Y")
#         cols.append(c + "_Z")
#     work = [[cols, s, trial_name] for s in sensors]
#     p = Pool(6)
#     p.map(worker_ve3, work)
#
# def worker_ve3(wrk):
#     cols = wrk[0]
#     s = wrk[1]
#     trial_name = wrk[2]
#     sensor_files = [f for f in os.listdir("./record/" + trial_name) if s[0] in f and 'ori' not in f]
#     m = np.zeros([len(sensor_files), len(cols)])
#     idx = 0
#     for f in sensor_files:
#         t = float(f.split("_")[-1].split(".")[0]) / 1000.0
#         n = np.load("./record/" + trial_name + "/" + f)
#         j = n.reshape([1, n.shape[0] * n.shape[1]])
#         m[idx, 0] = t
#         m[idx, 1:] = j
#         idx += 1
#         os.remove("./record/" + trial_name + "/" + f)
#     ret = pd.DataFrame(data=m, columns=cols)
#     ret.to_csv("./record/" + trial_name + "/" + s[0] + "_imu_vec3.csv", index=False)

#
# def combine_joints(trial_name):
#     print("Saving joints")
#     joint_angle = [f for f in os.listdir("./record/" + trial_name) if 'joint' in f.lower()]
#     k = pd.read_csv("./record/" + trial_name + "/" + joint_angle[0])
#     b = ['time']
#     for c in k.columns:
#         b.append(c + "_X")
#         b.append(c + "_Y")
#         b.append(c + "_Z")
#     m = np.zeros([len(joint_angle), len(b)])
#     idx = 0
#     for i in joint_angle:
#         k = pd.read_csv("./record/" + trial_name + "/" + i)
#         t = float(i.split("_")[-1].split(".")[0]) / 1000.0
#         n: np.ndarray = k.to_numpy()
#         j = n.reshape([1, int(k.shape[0] * k.shape[1])])
#         m[idx, 0] = t
#         m[idx, 1:] = j
#         idx += 1
#         os.remove("./record/" + trial_name + "/" + i)
#         pass
#     joint = pd.DataFrame(data=m, columns=b)
#     joint.to_csv("./record/" + trial_name + "/joint_angles.csv", index=False)
#
#
# def write_out(out_q: Queue, trial_name):
#     if not os.path.exists("./record/"+trial_name):
#         os.makedirs("./record/"+trial_name)
#     print("./record/"+trial_name)
#
#     while out_q.qsize() > 0:
#         k = out_q.get()
#         t = k["time"]
#         for i in k:
#             print(i)
#             if 'time' in i:
#                 continue
#             elif 'joint_angles' in i:
#
#                 k[i].to_csv("./record/{0}/joint_angles_{1}.csv".format(trial_name, t), index=False)
#             else:
#                 f = "{0}_{1}_vec3_{2}".format(trial_name, i, t)
#                 j = "{0}_{1}_ori_{2}".format(trial_name, i, t)
#                 np.save("./record/{0}/{1}.npy".format(trial_name, f), k[i][1])
#                 np.save("./record/{0}/{1}.npy".format(trial_name, j), k[i][2])
#     pass


def write_out2(out_q: Queue, command: Queue, trial_name, idx_, save_dir_):
    if not os.path.exists(save_dir_+idx_+"/"+trial_name):
        os.makedirs(save_dir_+idx_+"/"+trial_name)
    else:
        print("Trial name exist")
    print("Saving >>"+ save_dir_+idx_+"/"+trial_name)
    rows = out_q.qsize() # assumes qsize is now fix
    joint = np.zeros([rows, 67])
    simu_v3 = {s[0]: np.zeros([rows, 16]) for s in sensors_list()}
    simu_ori = {s[0]: np.zeros([rows, 5]) for s in sensors_list()}
    joint_names = ['time', 'jL5S1_X', 'jL5S1_Y', 'jL5S1_Z', 'jL4L3_X', 'jL4L3_Y', 'jL4L3_Z', 'jL1T12_X', 'jL1T12_Y',
                   'jL1T12_Z', 'jT9T8_X', 'jT9T8_Y', 'jT9T8_Z', 'jT1C7_X', 'jT1C7_Y', 'jT1C7_Z', 'jC1Head_X',
                   'jC1Head_Y', 'jC1Head_Z', 'jRightT4Shoulder_X', 'jRightT4Shoulder_Y', 'jRightT4Shoulder_Z',
                   'jRightShoulder_X', 'jRightShoulder_Y', 'jRightShoulder_Z', 'jRightElbow_X', 'jRightElbow_Y',
                   'jRightElbow_Z', 'jRightWrist_X', 'jRightWrist_Y', 'jRightWrist_Z', 'jLeftT4Shoulder_X',
                   'jLeftT4Shoulder_Y', 'jLeftT4Shoulder_Z', 'jLeftShoulder_X', 'jLeftShoulder_Y', 'jLeftShoulder_Z',
                   'jLeftElbow_X', 'jLeftElbow_Y', 'jLeftElbow_Z', 'jLeftWrist_X', 'jLeftWrist_Y', 'jLeftWrist_Z',
                   'jRightHip_X', 'jRightHip_Y', 'jRightHip_Z', 'jRightKnee_X', 'jRightKnee_Y', 'jRightKnee_Z',
                   'jRightAnkle_X', 'jRightAnkle_Y', 'jRightAnkle_Z', 'jRightBallFoot_X', 'jRightBallFoot_Y',
                   'jRightBallFoot_Z', 'jLeftHip_X', 'jLeftHip_Y', 'jLeftHip_Z', 'jLeftKnee_X', 'jLeftKnee_Y',
                   'jLeftKnee_Z', 'jLeftAnkle_X', 'jLeftAnkle_Y', 'jLeftAnkle_Z', 'jLeftBallFoot_X', 'jLeftBallFoot_Y',
                   'jLeftBallFoot_Z']
    imu_v3_name = ['time', 'm_angularAcceleration_X', 'm_angularAcceleration_Y', 'm_angularAcceleration_Z',
                   'm_angularVelocity_X', 'm_angularVelocity_Y', 'm_angularVelocity_Z', 'm_acceleration_X',
                   'm_acceleration_Y', 'm_acceleration_Z', 'm_velocity_X', 'm_velocity_Y', 'm_velocity_Z',
                   'm_position_X', 'm_position_Y', 'm_position_Z']
    imu_ori_names = ['time', 'w', 'i', 'j', 'k']
    idx = 0
    st = time.time()
    while out_q.qsize() > 0:
        k = out_q.get()
        t = k["time"]/1000.0
        for i in k:
            if 'time' in i:
                continue
            elif 'joint_angles' in i:
                n = k[i].to_numpy()
                ns = n.reshape([1, n.shape[0]*n.shape[1]], order='F')
                joint[idx, 0] = t
                joint[idx, 1:] = ns
            else:
                x = simu_v3[i]
                x[idx, 0] = t
                x[idx, 1:] = k[i][1].reshape([1, 15], order='F')
                y = simu_ori[i]
                y[idx, 0] = t
                y[idx, 1:] = k[i][2]

        idx += 1
    ed = time.time()
    print("download buffer: {0}s".format(ed-st))
    jd = [[pd.DataFrame(data=joint, columns=joint_names), save_dir_+idx_+"/"+trial_name+"/joint_angles.csv"]]
    for o in simu_ori:
        jd.append([pd.DataFrame(data=simu_ori[o], columns=imu_ori_names),
                   save_dir_ + idx_ + "/" + trial_name + "/" + o + "_imu_ori.csv"])

    for o in simu_v3:
        jd.append([pd.DataFrame(data=simu_v3[o], columns=imu_v3_name),
                   save_dir_ + idx_ + "/" + trial_name + "/" + o + "_imu_vec3.csv"])

    st = time.time()
    p = Pool(6)
    p.map(worker_pd, jd)
    # outori = [[pd.DataFrame(data=simu_ori[o], columns=imu_ori_names), save_dir_+idx_+"/"+trial_name+"/"+o+"_imu_ori.csv"] for o in simu_ori]
    # p = Pool(6)
    # p.map(worker_pd, outori)
    # outv3 = [[pd.DataFrame(data=simu_v3[o], columns=imu_v3_name), save_dir_+idx_+"/"+trial_name+"/"+o+"_imu_vec3.csv"] for o in simu_v3]
    # p = Pool(6)
    # p.map(worker_pd, outv3)
    ed = time.time()
    print("save time: {0}s".format(ed - st))
    command.put("gui:Saved")
    print("Save Done!")


def worker_pd(w):
    d: pd.DataFrame = w[0]
    f = w[1]
    # d.to_csv(f, index=False)
    d.to_pickle(f+".pkl")


def scan_please(data_queue, commands):
    print("started listener")
    record = False
    current = 'test'
    p0x = "P00"
    out_q = Queue()
    k = None
    print_record = False
    while True:
        if commands.qsize() > 0:
            c = commands.get()
            if 'on' in c:
                info = c.split(':')
                current = info[1]
                p0x = info[2]
                if "!" in info[3]:
                    save_dir = info[3].replace("!", ":")
                else:
                    save_dir = info[3]
                print("current Trial: " + current)
                print("Save to: " + save_dir+p0x)
            elif 'start_trial' in c:
                record = True
                print_record = True
            elif 'end_trial' in c:
                record = False
                print("Stopping Recording")
                if k is not None:
                    while k.is_alive():
                        time.sleep(0.5)
                print("Recording Stopped")
                k = Process(target=write_out2, args=(out_q, commands, current, p0x, save_dir, ))
                k.start()
                #out_q = Queue()

            elif 'stop' in c:
                print("breaking")
                break
        if data_queue.qsize() > 0:
            q = data_queue.get()
            if record:
                if print_record:
                    print("Recording ... ")
                    print_record = False
                out_q.put(q)
                # if k is None or not k.is_alive():
                #     k = Process(target=write_out, args=(out_q, current,))
                #     k.start()
            # print(q['time'])
            # print(q['joint_angles'])

        else:
            sleep(0.5)
    print('printer stopped')

class GUI:
    def __init__(self, record_command, imu_command, feedback):
        self.record_command = record_command
        self.trial_name = None
        self.previous_trial = None
        self.height_opt = None
        self.foot_opt = None
        self.imu_commands = imu_command
        self.feedback: Queue = feedback
        self.scan:QPushButton = None
        self.stream = None
        self.body = None
        self.pose = None
        self.cali = None
        self.start = None
        self.time_label = None
        self.stop_time = False
        self.ended = False
        self.stop = None
        self.dot = None
        self.check_head_imu = None

    def listen(self):
        time.sleep(5)
        while True:
            if self.ended:
                break
            if self.feedback.qsize() > 0:
                while self.feedback.qsize():
                    c = self.feedback.get()
                    if 'scan' in c:
                        if 'False' in c:
                            self.scan.setChecked(False)
                            print("Scan: False")
            else:
                time.sleep(2)

    def update_time(self):
        sk = time.time()
        time.sleep(0.1)
        while not self.stop_time:
            if self.ended:
                break
            n = time.time()
            self.time_label.setText("Time: {0:.1f} s".format(n-sk))
            time.sleep(0.1)
            if np.remainder(int(n-sk), 2.0) == 0.0:
                if "Saving" not in self.dot.text():
                    self.dot.setText("<h2>"+'●'+"</h2>")
            else:
                if "Saving" not in self.dot.text():
                    self.dot.setText("<h2>" + '' + "</h2>")

        self.stop_time = False

    def set_person_height(self):
        ret = self.height_opt.text()
        if "---" in ret:
            ret = 1.75
        self.imu_commands.put('height:{0}'.format(ret))

    def set_person_foot(self):
        ret = self.foot_opt.text()
        if "---" in ret:
            ret = 0.25
        self.imu_commands.put('foot:{0}'.format(ret))

    def update_person(self):
        self.set_person_height()
        self.set_person_foot()
        print('Update person')

    def stop_trial(self):
        def reset_start_button():
            self.dot.setText("<h2>" + ' ... Saving!' + "</h2>")
            sleep(1)
            s0 = time.time()
            while True:
                self.dot.setText("<h2>" + ' ... Saving!' + "</h2>")
                c = self.record_command.get()
                if 'gui' in c and 'Saved' in c:
                    self.start.setChecked(False)
                    self.start.setEnabled(True)
                    self.dot.setText("<h2>" + '' + "</h2>")
                    break
                n = time.time()
                if (n - s0) > 20.0:
                    self.start.setChecked(False)
                    self.start.setEnabled(True)
                    self.dot.setText("<h2>" + '' + "</h2>")
                    break
                sleep(1)
            self.dot.setText("<h2>" + '' + "</h2>")
        if not self.start.isEnabled():
            self.dot.setText("<h2>" + ' ... Saving!' + "</h2>")
            self.record_command.put('end_trial')
            x = threading.Thread(target=reset_start_button, args=())
            x.start()
            print('end_trial')
            self.stop_time = True
            self.stop.setEnabled(False)

    def start_trial(self):
        if not self.stream.isChecked():
            self.stream.setChecked(True)
            self.streamer()

        trial_path = self.save_dir.text().strip()+self.pid_name.text().strip()+"/"+self.trial_name.text().strip()
        self.previous_trial = trial_path
        print(trial_path)
        if os.path.exists(trial_path):
            dlg = QMessageBox()
            dlg.setWindowTitle("Override Warning!")
            dlg.setText("The trial \"" + self.trial_name.text().strip() + "\" exists.\nDo you wish to continue?")
            dlg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            dlg.setIcon(QMessageBox.Warning)
            button = dlg.exec()

            if button == QMessageBox.Yes:
                print("Yes!")
            else:
                print("No!")
                return
        self.stop.setEnabled(True)
        dirt = self.save_dir.text().strip()
        if ":" in dirt:
            dirt = dirt.replace(":", "!")
        c = 'on:' + self.trial_name.text().strip()+":"+self.pid_name.text().strip()+":"+dirt
        print(c)
        self.record_command.put(c)
        time.sleep(0.1)
        self.record_command.put('start_trial')
        x = threading.Thread(target=self.update_time)
        x.start()
        self.start.setEnabled(False)
        print('start_trial')

    def scan_hardware(self):
        self.imu_commands.put('scan:{0}'.format(self.scan.isChecked()))

    def exit_handler(self):
        self.imu_commands.put('exit')
        self.ended = True

    def streamer(self):
        self.imu_commands.put('stream:{0}'.format(self.stream.isChecked()))

    def set_body(self):
        self.imu_commands.put('body:' + self.body.itemText(self.body.currentIndex()))
        print(self.body.itemText(self.body.currentIndex()))

    def set_pose(self):
        self.imu_commands.put('pose:' + self.pose.itemText(self.pose.currentIndex()))
        print(self.pose.itemText(self.pose.currentIndex()))

    def calibrate(self):
        self.imu_commands.put('calibrate')

    def set_save_dir(self):
        response = QFileDialog.getExistingDirectory(
            parent = None,
            caption='Open folder',
            dir=os.getcwd(),
        )
        if len(response.strip()) < 1:
            response = "./record"
        self.save_dir.setText("{0}/".format(response))

    def check(self):
        data0 = pd.read_pickle(self.previous_trial+"/Head_imu_vec3.csv.pkl")
        plt.figure()
        plt.plot(data0[["m_position_X", "m_position_Y", "m_position_Z"]])
        print(data0)
        plt.show()

    def open_window(self, feedback):
        print("started helper gui")
        self.feedback = feedback
        x = threading.Thread(target=self.listen)
        x.start()
        app = QApplication([])
        app.aboutToQuit.connect(self.exit_handler)
        window = QWidget()
        window.setWindowTitle("Minion")
        window.setGeometry(100, 100, 280, 80)
        main_box = QHBoxLayout()
        left = QWidget()
        box1 = QVBoxLayout()
        helloMsg = QLabel("<h1>Bello, Globo!</h1>", parent=window)
        box1.addWidget(helloMsg)
        box1.addSpacing(10)
        particip = QLabel("<h2>Save Folder</h2>", parent=window)
        box1.addWidget(particip)
        save_bar = QWidget()
        save_layout = QHBoxLayout()
        self.save_dir = QLineEdit("./record/")
        save_layout.addWidget(self.save_dir)
        save_dir_button = QPushButton("Open")
        save_dir_button.clicked.connect(self.set_save_dir)
        save_layout.addWidget(save_dir_button)
        save_bar.setLayout(save_layout)
        box1.addWidget(save_bar)
        particip = QLabel("<h2>Participant</h2>", parent=window)
        box1.addWidget(particip)
        self.pid_name = QLineEdit("P<XX>")
        box1.addWidget(self.pid_name)
        trial_bar = QWidget()
        thr = QHBoxLayout()
        trail = QLabel("<h2>Trial</h2>", parent=window)
        thr.addWidget(trail)
        self.dot = QLabel("<h1>"+""+"</h1>", parent=window)
        self.dot.setStyleSheet("color: red;")
        thr.addSpacing(5)
        thr.addWidget(self.dot)
        thr.addStretch(10)
        trial_bar.setLayout(thr)
        box1.addWidget(trial_bar)
        self.trial_name = QLineEdit("<Trail Name>")
        box1.addWidget(self.trial_name)
        self.start = QPushButton('Start')
        self.start.clicked.connect(self.start_trial)
        self.start.setCheckable(True)
        box1.addWidget(self.start)
        self.stop = QPushButton('Stop')
        self.stop.clicked.connect(self.stop_trial)
        self.stop.setEnabled(False)
        box1.addWidget(self.stop)
        self.time_label = QLabel("Time: 0.0 sec")
        box1.addWidget(self.time_label)
        box1.addSpacing(15)
        info_txt = "1)\tInput Individual's Height and Foot/Shoe length\n  \tthen press update.\n"
        info_txt += "2)\tPress Scan to scan for sensors\n"
        info_txt += "3)\tPress Calibrate to run the calibration\n"
        info_txt += "4)\tPress Data Stream to activate streaming. \n  \tThis need to activated before starting the trial\n"
        info = QLabel("<h2>How to use:</h2>")
        info_text = QLabel(info_txt)
        box1.addWidget(info)
        box1.addWidget(info_text)
        box1.addStretch(6)
        left.setLayout(box1)

        right = QWidget()
        right.setMinimumWidth(200)
        box2 = QVBoxLayout()
        indy = QLabel("<h2>Individual</h2>", parent=window)
        box2.addWidget(indy)
        height = QLabel("<h3>Height (m)</h3>", parent=window)
        self.height_opt = QLineEdit("---")
        foot_size = QLabel("<h3>Foot Size (m)</h3>", parent=window)
        self.foot_opt = QLineEdit("---")
        box2.addWidget(height)
        box2.addWidget(self.height_opt)
        box2.addWidget(foot_size)
        box2.addWidget(self.foot_opt)
        update = QPushButton("Update")
        update.clicked.connect(self.update_person)
        box2.addWidget(update)
        box2.addSpacing(30)
        s = QLabel("<h2>System</h2>", parent=window)
        box2.addWidget(s)
        coj = QLabel("<h4>Configuration</h4>", parent=window)
        box2.addWidget(coj)

        body_w = QWidget()
        bw = QHBoxLayout()
        bw.addWidget(QLabel('Body: '))
        self.body = QComboBox()
        self.body.setMinimumWidth(100)
        self.body.addItems(['FullBody', 'LowerBody'])
        self.body.currentIndexChanged.connect(self.set_body)
        bw.addWidget(self.body)
        body_w.setLayout(bw)
        self.imu_commands.put('body:' + self.body.itemText(self.body.currentIndex()))
        box2.addWidget(body_w)

        pose_w = QWidget()
        bw = QHBoxLayout()
        bw.addWidget(QLabel('Pose: '))
        self.pose = QComboBox()
        self.pose.setMinimumWidth(100)
        self.pose.addItems(['Npose', 'TPose', 'NPose_static', 'TPose_static'])
        self.pose.currentIndexChanged.connect(self.set_pose)
        self.imu_commands.put('pose:' + self.pose.itemText(self.pose.currentIndex()))
        bw.addWidget(self.pose)
        pose_w.setLayout(bw)
        box2.addWidget(pose_w)

        box2.addSpacing(5)

        con = QLabel("<h4>Control</h4>", parent=window)
        box2.addWidget(con)
        self.scan = QPushButton('Scan')
        self.scan.setCheckable(True)
        self.scan.clicked.connect(self.scan_hardware)
        box2.addWidget(self.scan)

        self.cali = QPushButton('Calibrate')
        self.cali.clicked.connect(self.calibrate)
        box2.addWidget(self.cali)

        self.stream = QPushButton('Data Stream')
        self.stream.setCheckable(True)
        self.stream.clicked.connect(self.streamer)
        box2.addWidget(self.stream)

        self.check_head_imu = QPushButton('Check IMU')
        self.check_head_imu.clicked.connect(self.check)
        box2.addWidget(self.check_head_imu)
        box2.addSpacing(5)
        box2.addStretch(10)
        right.setLayout(box2)

        main_box.addWidget(left)
        main_box.addWidget(right)
        window.setLayout(main_box)
        window.show()
        sys.exit(app.exec())


def showMenu(calibType, config):
    xmeStatus = xmeControl.status()
    print(
        "\n= Menu ================================= Status ===============================\n"
        "| b: Change configuration (before scan)| Configuration:          %-12s |\n"
        "| i: Toggle hardware scan mode         | Ready:                  %c            |\n"
        "| x: Disconnect Hardware               | Recording:              %c            |\n"
        "| t: Change calibration type           | Calibration Type:       %-12s |\n"
        "| w: Start calibration                 | Calibrating:            %c            |\n"
        "| n: Create new MVN file               | PostProcessing:         %c            |\n"
        "| r: Start recording                   | Hardware Status:        %3s          |\n"
        "| s: Stop recording                    | Frames Recorded:    %5d            |\n"
        "| f: Read frame from file              | Frames Processed:   %5d            |\n"
        "| o: Open MVN File                     |                                      |\n"
        "| a: Save MVN file                     |                                      |\n"
        "| c: Close MVN file                    |                                      |\n"
        "| p: Toggle real time pose mode        |                                      |\n"
        "| q: Quit                              |                                      |\n"
        "===============================================================================\n"
        "|                             Connected Devices                               |\n"
        "==============================================================================="
        % (config,
           ('Y' if xmeStatus.isConnected() else 'N'),
           ('Y' if xmeStatus.isRecording() else 'N'),
           calibType,
           ('Y' if xmeStatus.isCalibrating() else 'N'),
           ('Y' if xmeStatus.isProcessing() else 'N'),
           ("OK " if (xmeStatus.suitStatus().m_hardwareStatus == xme.XHS_HardwareOk)
            else ("ERR" if xmeStatus.isScanning() else "DIS")),
           (xmeStatus.frameCount()),
           (xmeStatus.framesProcessed())))

    masterStatus = xmeStatus.suitStatus().m_masterDevice
    print("| Master Devices Connected: %1d                                                 |"
          % (0 if (masterStatus.m_validity == xme.XDV_Empty) else 1))
    print("===============================================================================")

    if masterStatus.m_validity != xme.XDV_Empty:
        deviceRadioQuality = ""
        masterRadioQuality = masterStatus.m_radioQuality
        if masterRadioQuality >= 0 and masterRadioQuality <= 25:
            deviceRadioQuality = "BAD"
        elif masterRadioQuality >= 25 and masterRadioQuality <= 75:
            deviceRadioQuality = "MODERATE"
        elif masterRadioQuality > 75:
            deviceRadioQuality = "GOOD"
        elif masterRadioQuality == -1:
            deviceRadioQuality = "UNKNOWN"

        print(
            "| Dev. Id:      %08x | Conn. Quality :     %9s | Pow. Level:     %3u|"
            % (masterStatus.m_deviceId,
               deviceRadioQuality,
               masterStatus.m_batteryLevel))

        print("===============================================================================")


# Main function:
if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('mvnFile', default="testfile.mvn", type=str, help='The name of the MVN file to record',
                        nargs='?')
    parser.add_argument('awindaChannel', default=15, type=int, help='The Awinda radio channel to use [11-25]',
                        nargs='?')
    args = parser.parse_args()

    parser.print_help()  # we always print the usage in the example
    print("")

    mvnFileName = args.mvnFile
    awindaChannel = args.awindaChannel
    if awindaChannel < 11 or awindaChannel > 25:
        awindaChannel = 15

    xmeControl = None

    processes = []
    try:

        license = xme.XmeLicense()
        xmeControl = xme.XmeControl()
        version = xmeControl.version()
        print("XmeControl construction successful, XME version %s.%s.%s " % (
            version.major(), version.minor(), version.revision()))

        print("Using channel", awindaChannel, "for Awinda communication")
        print("Using file", mvnFileName, "for recording")

        calibType = "Npose"
        configuration = "FullBody"

        cb = MyXmeCallbacks()
        xmeControl.addCallbackHandler(cb)

        # kb = kbhit.KBHit()

        g = GUI(command, imu_command, feedback)
        showMenu(calibType, configuration)
        userQuit = False
        p = Process(target=g.open_window, args=(feedback,))
        p.start()
        processes.append(p)
        q = Process(target=scan_please, args=(data_queue, command,))
        q.start()
        processes.append(q)

        scanning_boo = False

        while userQuit != True:
            if scanning_boo:
                s = xmeControl.status()
                scanning_boo = s.isScanning()
                if not scanning_boo:
                    feedback.put("scan:False")
                    print("Hardware scan done")

            if imu_command.qsize() > 0:
                ic = imu_command.get()
                if 'exit' in ic:
                    userQuit = True
                    command.put('stop')
                    print("Stopping")

                if 'body' in ic:
                    configuration = ic.split(':')[1]
                    print(configuration)
                if 'pose' in ic:
                    calibType = ic.split(':')[1]
                    print(calibType)

                if 'stream' in ic:
                    b = ic.split(':')[1]
                    if 'True' in b:
                        xmeControl.setRealTimePoseMode(True)
                        print("Setting stream mode to: %s" % ("true"))
                    elif 'False' in b:
                        xmeControl.setRealTimePoseMode(False)
                        print("Setting stream mode to: %s" % ("false"))
                if 'calibrate' in ic:
                    cb.calibrationProcessed = False
                    cb.calibrationAborted = False
                    print("\nStarting 1-click calibration with", calibType)
                    print("STEP 1: Initialize calibration")
                    try:
                        xmeControl.initializeCalibration(calibType)
                    except Exception as e:
                        print("Error: %s" % e)
                        continue

                    times = xmeControl.calibrationRecordingTimesRemaining()
                    print("STEP 2: Start calibration")
                    try:
                        xmeControl.startCalibration()
                    except Exception as e:
                        print("Error: %s" % e)
                        continue

                    print("STEP 3: Wait for the recording to complete")
                    while len(times) > 0:
                        tRemaining = times[0] / 1000.0
                        print(tRemaining, "seconds remaining")
                        sleep(0.2)
                        times = xmeControl.calibrationRecordingTimesRemaining()

                    print("STEP 4: Wait for processing to complete")
                    while not cb.calibrationComplete and not cb.calibrationAborted:
                        sleep(0.01)

                    print("STEP 5: Calibration complete\n")

                if 'height' in ic:
                    value = float(ic.split(':')[1])
                    xmeControl.setBodyDimension("bodyHeight", value)
                if 'foot' in ic:
                    value = float(ic.split(':')[1])
                    xmeControl.setBodyDimension("footSize", value)
                if 'scan' in ic:
                    b = ic.split(':')[1]
                    if 'True' in b:
                        xmeControl.setScanMode(True)
                        scanning_boo = True
                        print("Setting scan mode to: %s" % ("true" if xmeControl.scanMode() else "false"))
                    elif 'False' in b:
                        xmeControl.setScanMode(False)
                        print("Setting scan mode to: %s" % ("true" if xmeControl.scanMode() else "false"))
                        print(False)
                        scanning_boo = False
                    try:
                        xmeControl.setConfiguration(configuration)

                        if xmeControl.scanMode():
                            xmeControl.configurationList()
                    except Exception as e:
                        print("Error: %s" % e)
                print(ic)
            sleep(1)


            # if kb.kbhit():
            #     userInput = kb.getch().lower()
            #     if userInput == 'a':
            #         print("Saving MVN file.")
            #         try:
            #             xmeControl.saveFile()
            #         except Exception as e:
            #             print("Error: %s" % e)
            #
            #     if userInput == 'b':
            #         if configuration == "FullBody":
            #             configuration = "LowerBody";
            #         elif configuration == "LowerBody":
            #             configuration = "UpperBody";
            #         else:
            #             configuration = "FullBody";
            #         showMenu(calibType, configuration)
            #
            #     elif userInput == 'c':
            #         print("Closing MVN file.")
            #         try:
            #             xmeControl.closeFile()
            #         except Exception as e:
            #             print("Error: %s" % e)
            #
            #     elif userInput == 'f':
            #         inputNumber = int(input("Enter frame number to read from file: "))
            #         print("Retrieving frame nr %u" % inputNumber)
            #         try:
            #             showPose(inputNumber)
            #         except Exception as e:
            #             print("Error: %s" % e)
            #
            #     elif userInput == 'i':
            #         try:
            #             xmeControl.setConfiguration(configuration)
            #             if xmeControl.scanMode():
            #                 xmeControl.setScanMode(False)
            #                 xmeControl.configurationList()
            #             else:
            #                 xmeControl.setScanMode(True)
            #             print("Setting scan mode to: %s" % ("true" if xmeControl.scanMode() else "false"))
            #         except Exception as e:
            #             print("Error: %s" % e)
            #
            #     elif userInput == 'p':
            #         try:
            #             if xmeControl.realTimePoseMode():
            #                 xmeControl.setRealTimePoseMode(False)
            #             else:
            #                 xmeControl.setRealTimePoseMode(True)
            #             print("Setting real time mode to: %s" % ("true" if xmeControl.realTimePoseMode() else "false"))
            #         except Exception as e:
            #             print("Error: %s" % e)
            #
            #     elif userInput == 'n':
            #         print("Creating new MVN file %s" % mvnFileName)
            #         try:
            #             xmeControl.createMvnFile(mvnFileName)
            #         except Exception as e:
            #             print("Error: %s" % e)
            #
            #     elif userInput == 'o':
            #         print("Opening MVN file %s" % mvnFileName)
            #         try:
            #             xmeControl.openMvnFile(mvnFileName)
            #         except Exception as e:
            #             print("Error: %s" % e)
            #
            #     elif userInput == 'q':
            #         command.put('stop')
            #         print("Quitting")
            #         userQuit = True
            #         break
            #     elif userInput == 'r':
            #         print("Starting recording")
            #         try:
            #             xmeControl.startRecording()
            #         except Exception as e:
            #             print("Error: %s" % e)
            #
            #     elif userInput == 's':
            #         print("Stopping recording")
            #         try:
            #             xmeControl.stopRecording()
            #         except Exception as e:
            #             print("Error: %s" % e)
            #
            #     elif userInput == 't':
            #         if calibType == "Npose":
            #             calibType = "TPose";
            #         elif calibType == "TPose":
            #             calibType = "NPose_static";
            #         elif calibType == "NPose_static":
            #             calibType = "TPose_static";
            #         else:
            #             calibType = "Npose";
            #         showMenu(calibType, configuration)
            #
            #     elif userInput == 'w':
            #         cb.calibrationProcessed = False
            #         cb.calibrationAborted = False
            #         print("\nStarting 1-click calibration with", calibType)
            #         print("STEP 1: Initialize calibration")
            #         try:
            #             xmeControl.initializeCalibration(calibType)
            #         except Exception as e:
            #             print("Error: %s" % e)
            #             break
            #
            #         times = xmeControl.calibrationRecordingTimesRemaining()
            #         print("STEP 2: Start calibration")
            #         try:
            #             xmeControl.startCalibration()
            #         except Exception as e:
            #             print("Error: %s" % e)
            #             break
            #
            #         print("STEP 3: Wait for the recording to complete")
            #         while len(times) > 0:
            #             tRemaining = times[0] / 1000.0;
            #             print(tRemaining, "seconds remaining");
            #             sleep(0.2)
            #             times = xmeControl.calibrationRecordingTimesRemaining()
            #
            #         print("STEP 4: Wait for processing to complete")
            #         while not cb.calibrationComplete and not cb.calibrationAborted:
            #             sleep(0.01)
            #
            #         print("STEP 5: Calibration complete\n");
            #
            #     elif userInput == 'x':
            #         print("Disconnecting hardware")
            #         try:
            #             if xmeControl.scanMode():
            #                 xmeControl.setScanMode(False)
            #             xmeControl.disableRadio()
            #             xmeControl.disconnectHardware()
            #         except Exception as e:
            #             print("Error: %s" % e)
            #
            #     else:
            #         showMenu(calibType, configuration)
            # else:
            #     sleep(1)

    except Exception as e:
        print("Error: %s" % e)

    finally:
        # Important, upon termination: remove callback handler and exit!
        print("setScanMode to false")
        xmeControl.setScanMode(False)
        print("turn off stream")
        xmeControl.setRealTimePoseMode(False)
        # print("Switch Off Hardware")
        # xmeControl.switchOffHardware()
        # print("Deactive Radio")
        # xmeControl.disableRadio()
        # # print("disconnect Hardware")
        # # xmeControl.disconnectHardware()
        print("removePyCallbackHandler")
        xmeControl.removePyCallbackHandler(cb)
        print("Switch Off Hardware")
        xmeControl.switchOffHardware()
    for o in processes:
        try:
            o.kill()
        except:
            pass
    # clean up
    xmeControl = None
    license = None
    print("xmeTerminate")
    xme.xmeTerminate()
