import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from yatpkg.math.transformation import Quaternion
from yatpkg.math.filters import Butterworth
from yatpkg.util.data import TRC, StorageIO, StorageType, Yatsdo, Mesh
from yatpkg.math.transformation import PCAModel
from scipy.signal import find_peaks
from scipy.spatial.transform import Rotation


def joint_angles():
    #global root, x, r, i, f
    # Read data
    root = "E:/meta/record/"
    trials = [t for t in os.listdir(root) if 'test' not in t.lower() if os.path.isdir(root + t)]
    x = np.zeros([6, len(trials)])
    idx = -1
    leg_height = [0, 0, -0.851]

    for t in trials:
        if os.path.isdir(root + t):
            idx += 1
            files_joint = [r for r in os.listdir(root + t) if 'joint' in r]
            col = ["time", "jLeftAnkle_Z", "jLeftKnee_Z", "jLeftHip_Z"]
            knee = ["time", "jLeftKnee_X", "jLeftKnee_Y", "jLeftKnee_Z"]
            hip = ["time", "jLeftHip_X", "jLeftHip_Y", "jLeftHip_Z"]
            ankle = ["time", "jLeftAnkle_X", "jLeftAnkle_Y", "jLeftAnkle_Z"]
            torso = ["time", 'jT1C7_X',	'jT1C7_Y','jT1C7_Z']

            for i in files_joint:
                f = root + t + "/" + i
                d = pd.read_csv(f)
                r = d[col]

                tx = d["time"].to_numpy()
                tx = tx - float(tx[0])
                kn = d[knee].to_numpy()
                ke = kn[:int(kn.shape[0]/2), :]
                p = PCAModel.pca_rotation(ke[:, 1:])
                k = np.matmul(p.transformation, kn[:, 1:].T)
                k = k.T

                h = d[hip]
                hp = h.to_numpy()
                hps = hp[:int(hp.shape[0] / 2), :]
                ph = PCAModel.pca_rotation(hps[:, 1:])
                hk = np.matmul(ph.transformation, hp[:, 1:].T)
                hk = hk.T
                leg = np.zeros([hk.shape[0], 3])
                leg_n = []
                m0 = np.abs(np.max(k[:, 0]))
                m1 = np.abs(np.min(k[:, 0]))
                hg = m0 / 4
                if m0 < m1:
                    k[:, 0] = -1 * k[:, 0]
                    hg = m1 / 4

                for kx in range(0, k.shape[0]):
                    rx = Rotation.from_euler('xyz', [k[kx, 0], 0, 0], degrees=True)
                    leg[kx, :] = rx.apply(leg_height)

                lg = Butterworth.butter_low_filter(leg[:, 2], 6, 60)
                lmax, _ = find_peaks(np.abs(lg), height=np.max(np.abs(lg))*0.05)

                for i in range(0, len(lmax)-1):
                    idx0 = lmax[i]
                    idy0 = lmax[i+1]
                    leg_n.append([tx[idx0], np.linalg.norm(leg[idx0, :] - leg[idy0, :])])
                leg_n = np.array(leg_n)

                a = d[ankle]

                mx = np.max(r.iloc[:, 1])
                mn = np.min(r.iloc[:, 1])
                rom = mx - mn

                pak, _ = find_peaks(k[:, 0], height=hg)
                kl = -k[:, 0]
                ml = np.min(kl)
                kl = kl - ml
                pak0, _ = find_peaks(kl, height=np.max(kl)*0.5, prominence=2.5)
                tims = np.mean(tx[pak[1:]] - tx[pak[0:-1]])

                lstride, _ = find_peaks(leg_n[:, 1], height=np.max(leg_n[:, 1]) * 0.1)

                tor = d[torso]
                fig = plt.figure()
                xt0 = tor.iloc[:, 1] - np.min(tor.iloc[:, 1])
                xt1 = tor.iloc[:, 2] - np.min(tor.iloc[:, 2])
                xt2 = tor.iloc[:, 3] - np.min(tor.iloc[:, 3])

                mxt = [np.max(tor.iloc[:, 1]), np.max(tor.iloc[:, 2]), np.max(tor.iloc[:, 3])]
                mnt = [np.min(tor.iloc[:, 1]), np.min(tor.iloc[:, 2]), np.min(tor.iloc[:, 3])]
                met = [np.mean(xt0), np.mean(xt1), np.mean(xt2)]
                sdt = [np.std(xt0), np.std(xt1), np.std(xt2)]
                br1 = np.arange(3)
                plt.title(t + " - Torso Angle")
                plt.bar(br1, met, yerr=sdt, width=0.25, align='edge', capsize=10)
                plt.xticks([r + 0.25 for r in range(3)], ["x", "y", "z"])
                plt.savefig(root + t + "_torso_angle.png")
                # plt.show()

                # fig = plt.figure()
                # plt.plot(tx, leg[:, 0])
                # plt.plot(tx, leg[:, 1])
                # plt.plot(tx, leg[:, 2])
                # plt.plot(tx[lmax], leg[lmax, 0])
                # plt.plot(leg_n[lstride, 0], leg_n[lstride, 1])
                # plt.plot(tx[pak], leg[pak, 1], marker='o')
                # plt.show()



                print("Trial: " + t)
                print("Max: {0:.2f}".format(mx))
                print("Min: {0:.2f}".format(mn))
                print("ROM: {0:.2f}".format(rom))
                print("Step Freq: {0:.2f}Hz".format(1 / np.median(tims)))
                print("Step length: {0:.2f}m".format(np.mean(leg[pak, 1])))
                print("Step speed: {0:.2f}m/s".format(np.mean(leg[pak, 1])/tims))

                x[0, idx] = mx
                x[1, idx] = mn
                x[2, idx] = rom
                x[3, idx] = 1 / np.mean(tims)
                x[4, idx] = np.mean(leg[pak, 1])
                x[5, idx] = np.mean(leg[pak, 1])/tims

                # fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, figsize=(10, 6))
                # ax1.plot(tx, a[ankle[1]], c='blue', label='x')
                # ax1.plot(tx, a[ankle[2]], c='lightsteelblue', label='y')
                # ax1.plot(tx, a[ankle[3]], c='darkblue', label='z')
                # ax1.yaxis.labelpad = 10
                # ax1.legend()
                # # ax2.plot(d[col[0]], d[col[2]], c='red')
                # ax2.plot(tx, k[:, 0], c='red', label='x')
                # ax2.plot(tx, k[:, 1], c='tomato', label='y')
                # ax2.plot(tx, k[:, 2], c='darkred', label='z')
                # ax2.plot(tx[pak], k[pak, 0], c='coral', marker='x')
                # ax2.plot(tx[pak0], k[pak0, 0], c='coral', marker='o')
                # ax2.yaxis.labelpad = 10
                # ax2.legend()
                # # ax2.plot(tx, d[knee].iloc[:, 1], c='orange')
                # # ax2.plot(tx, d[knee].iloc[:, 2], c='orange')
                # # ax2.plot(tx, d[knee].iloc[:, 3], c='orange')
                # ax3.plot(tx, hk[:, 0], c='green', label='x')
                # ax3.plot(tx, hk[:, 1], c='darkgreen', label='y')
                # ax3.plot(tx, hk[:, 2], c='limegreen', label='z')
                # # ax3.plot(tx, h[hip[1]], c='green', label='y')
                # # ax3.plot(tx, h[hip[2]], c='darkgreen', label='y')
                # # ax3.plot(tx, h[hip[3]], c='limegreen', label='z')
                # ax3.yaxis.labelpad = 10
                # ax3.legend()
                #
                # ax1.set_ylabel('Ankle Joint Angles')
                # ax2.set_ylabel('Knee Joint Angles')
                # ax3.set_ylabel('Hip Joint Angles')
                #
                # fig.align_ylabels()
                # fig.suptitle("Task: " + str(t))
                # ax3.set_xlabel('Time (s)')
                # plt.show()
                # plt.savefig(root + t + ".png")
        pass
    ret = pd.DataFrame(data=x, columns=trials, index=["Max (degrees)", "Min (degrees)", "ROM (degrees)", "Step Freq (Hz)", "Step length (m)", "Step speed (m/s)"])
    ret.to_csv(root + "/results.csv")

    pass

def retplotter():
    root = "E:/meta/record/"
    ret = pd.read_csv(root + "/results.csv", index_col=0)
    tasks = ["Combination", "Defined Path", "Free path", "Obstacle avoidance", "Reactive stop", "Stairs", "Straight path"]
    columns = [["Combination 3", "Combination AR 3", "Combination VR 3"],
               ["Defined path 2", "Defined path VR 3"],
               ["Free path 3", "Free path AR 3", "Free path VR 3"],
               ["Obstacle avoidance 3", "Obstacle avoidance AR 3", "Obstacle avoidance VR 3"],
               ["Reactive stop 3", "Reactive stop AR 3", "Reactive stop VR 3"],
               ["Stairs 3", "Stairs AR 3", "Stairs VR 3"],
               ["Straight path 3", "Straight path AR 3", "Straight path VR 3"]]
    txick = [ti for ti in ret.index]
    barWidth = 0.25
    br1 = np.arange(3)
    br2 = [x + barWidth for x in br1]
    br3 = [x + barWidth for x in br2]
    br = [br1, br2, br3]
    for c in range(0, len(tasks)):
        cols = columns[c]
        ps = ret[cols]
        idx = [i for i in ps.index]
        plt.figure()

        for i in range(0, len(txick)-3):
            plt.bar(br[i][:len(cols)], ps.iloc[3+i, :], width=0.25, align='edge')

        plt.legend(idx[3:])
        plt.xticks([r + barWidth for r in range(len(cols))], cols)
        plt.title(tasks[c])
        # plt.show()
        plt.savefig(root + tasks[c] + "_gait.png")
        plt.close()
        lab2 = ["Max", "Min", "ROM"]
        plt.figure()
        for i in range(0, len(txick) - 3):
            plt.bar(br[i][:len(cols)], ps.iloc[i, :], width=0.25, align='edge')

        plt.legend(idx[:3])
        plt.xticks([r + barWidth for r in range(len(cols))], cols)
        plt.title(tasks[c])
        # plt.show()
        plt.savefig(root + tasks[c] + "_joint.png")


def gait():
    root = "E:/meta/record/"
    trials = [t for t in os.listdir(root) if 'test' not in t.lower() if os.path.isdir(root + t)]
    x = np.zeros([3, len(trials)])
    idx = -1
    for t in trials:
        if os.path.isdir(root + t):
            idx += 1
            files_v3 = [r for r in os.listdir(root + t) if 'vec3' in r and 'leg' in r.lower()]
            for i in files_v3:
                f = root + t + "/" + i
                d = pd.read_csv(f)
                j = [c for c in d.columns if "m_acc" in c]
                j.insert(0, 'time')
                d0 = d[j]
                plt.figure()
                plt.title(t+" "+i)
                plt.plot(d0['time'], d0[j[1]])
                plt.plot(d0['time'], d0[j[2]])
                plt.plot(d0['time'], d0[j[3]])
                plt.show()
                pass
            pass
    pass


def markers():
    root = "C:/Users/tyeu008/Downloads/P001_1402/MMG/Test/T1/test_1402/"
    f = [f for f in os.listdir(root) if f.endswith(".trc") and 'test' not in f]
    for i in f:
        print(i)
        s: TRC = StorageIO.load(root+"/"+i, fill_data=True)
        d = s.to_panda()
        y = Yatsdo(d.iloc[:, 1:])
        x = y.x
        y0 = y.get_samples(x)
        s.data[:, 1:] = y0
        s.update()
        s.write(root+i[:i.rindex(".")]+"_test.trc")
        pass


def remesh():
    c = "E:/RTS_V2/ACL RTS/Analysis_P12-22/P13/geom/"
    d = "E:/RTS_V2/ACL RTS/Analysis_P12-22/P13/geom2/"
    c0 = [e for e in os.listdir(c)]
    if not os.path.exists(d):
        os.makedirs(d)
    for c1 in c0:
        Mesh.convert_vtp_2_vtp(c+c1, d+c1)



if __name__ == '__main__':
    joint_angles()
    # retplotter()
    # gait()
    # markers()
    # remesh()
