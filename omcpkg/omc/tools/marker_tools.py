from yatpkg.util.data import TRC
import numpy as np
import pandas as pd
from itertools import permutations
import pickle


class MarkerTracer:
    def __init__(self, data: TRC):
        self.original = data
        pass

    def find_markers_need_tracing(self, ignore="Ignore"):
        markers_need_tracing = []
        for m in self.original.marker_set:
            if ignore.lower() in m.lower():
                continue
            p = self.original.marker_set[m]
            f = np.sum((np.sum(np.isnan(p))).to_list())
            if f > 0:
                markers_need_tracing.append(m)
        return markers_need_tracing

    def run(self, ignore="Ignore"):
        markers_need_tracing = self.find_markers_need_tracing(ignore)
        first_frame = 530
        cluster = []
        for c in markers_need_tracing:
            p = self.original.marker_set[c]
            f = np.sum(np.isnan(p.iloc[first_frame, :]))
            if f == 0:
                cluster.append(c)
        cl = np.zeros([3, 4])
        cl[:, 0] = self.original.marker_set[cluster[0]].iloc[first_frame, :]
        cl[:, 1] = self.original.marker_set[cluster[1]].iloc[first_frame, :]
        cl[:, 2] = self.original.marker_set[cluster[2]].iloc[first_frame, :]
        cl[:, 3] = self.original.marker_set[cluster[3]].iloc[first_frame, :]
        perm = permutations(markers_need_tracing, 4)
        cluster_c = ['S1', 'S2', 'S3', 'S4']
        cluster_p = {'S1': np.zeros(self.original.marker_set[cluster[0]].shape),
                     'S2': np.zeros(self.original.marker_set[cluster[0]].shape),
                     'S3': np.zeros(self.original.marker_set[cluster[0]].shape),
                     'S4': np.zeros(self.original.marker_set[cluster[0]].shape)}
        current_frame = cl
        perms = [p for p in list(perm)]
        for i in range(first_frame, self.original.shape[0]):
            lowest = 10000
            current_perm = None
            markers = None
            for j in perms:
                nl = np.zeros([3, 4])
                nl[:, 0] = self.original.marker_set[j[0]].iloc[i, :]
                nl[:, 1] = self.original.marker_set[j[1]].iloc[i, :]
                nl[:, 2] = self.original.marker_set[j[2]].iloc[i, :]
                nl[:, 3] = self.original.marker_set[j[3]].iloc[i, :]
                ml = np.linalg.norm(nl-current_frame)
                if not np.isnan(ml):
                    print(j)
                    print(ml)
                    if lowest > ml:
                        current_perm = j
                        lowest = ml
                        markers = nl
            if markers is not None:
                for k in range(0, 4):
                    cluster_p[cluster_c[k]][i, :] = markers[:, k]
                current_frame = markers
            else:
                for k in range(0, 4):
                    cluster_p[cluster_c[k]][i, :] = current_frame[:, k]
        num_markers = len(self.original.marker_set.keys())
        indx = 1
        for s in cluster_p:
            self.original.marker_set[s] = pd.DataFrame(data=cluster_p[s], columns=["X{0}".format(num_markers+indx),
                                                                                   "Y{0}".format(num_markers+indx),
                                                                                   "Z{0}".format(num_markers+indx)])
            indx += 1
        self.original.update_from_markerset()
        dbfile = open("E:/bones/sheep/temp/S30.pkl", 'ab')
        pickle.dump(cluster_p, dbfile)
        dbfile.close()
        pass

    def cont(self):
        num_markers = len(self.original.marker_set.keys())
        indx = 0
        dbfile = open("E:/bones/sheep/temp/S30.pkl", 'rb')
        cluster_p = pickle.load(dbfile)
        for s in cluster_p:
            self.original.marker_set[s] = pd.DataFrame(data=cluster_p[s], columns=["X{0}".format(num_markers+indx),
                                                                                   "Y{0}".format(num_markers+indx),
                                                                                   "Z{0}".format(num_markers+indx)])
            indx += 1
        self.original.update_from_markerset()
        self.original.write("E:/bones/sheep/temp/S30.trc")
        pass


class MarkerMerger:
    def __init__(self, data: TRC):
        self.original = data
        self.marker_labels = []
        self.marker = None
        self.data = None
        pass

    def worker(self):
        pass

    def find_min_number_markers(self):
        m0 = [k for k in self.original.marker_names if 'ignore' not in k.lower()]
        m1 = []
        for m in m0:
            k = m.split('_')
            if k[-1] not in m1:
                m1.append(k[-1])
        return m1

    def run(self):
        rows = self.original.data.shape[0]
        self.marker = {m: pd.DataFrame(data=np.full([rows, 3], np.nan), columns=['X', 'Y', 'Z']) for m in self.find_min_number_markers()}
        self.marker_labels = ['Frame#', 'time']
        count = 1
        for m in self.marker:
            self.marker_labels.append(m + "_X{0}".format(count))
            self.marker_labels.append(m + "_Y{0}".format(count))
            self.marker_labels.append(m + "_Z{0}".format(count))
            self.marker[m].columns = ["X{0}".format(count), "Y{0}".format(count), "Z{0}".format(count)]
            count += 1
        num_markers = count - 1
        for m in self.original.marker_set:
            idx = m.split('_')[-1]
            if 'ignore' in idx.lower():
                continue
            tag0 = self.original.marker_set[m]
            num = tag0.to_numpy()
            tag1 = self.marker[idx]
            for t in range(0, rows):
                t0 = num[t, :]
                x = [1 if b else 0 for b in np.isnan(t0)]
                if np.sum(x) == 0:
                    tag1.iloc[t, :] = t0

            pass
        self.data = np.zeros([rows, len(self.marker_labels)])
        self.data[:, :2] = self.original.data[:, :2]
        x = 2
        for m in self.marker:
            self.data[:, x:x+3] = self.marker[m].to_numpy()
            x += 3
        t = TRC(data=self.data, col_names=self.marker_labels)
        t.update()
        t.headers['CameraRate'] = self.original.headers['CameraRate']
        t.headers['DataRate'] = self.original.headers['DataRate']
        t.headers['OrigDataRate'] = self.original.headers['OrigDataRate']
        t.headers['Units'] = self.original.headers['Units']
        return t


class AnteriorStylus:
    def __init__(self):
        self.unit = "mm"
        self.S4 = [42.656, -0.74445, 0]   # 1
        self.S3 = [0, -48.03, 0]  # 2
        self.S2 = [0, 27.764, 0]  # 3
        self.S1 = [-53.95, 0.941557, 0]  # 4
        self.tracker = np.zeros([3, 4])
        self.tracker[:, 0] = self.S1
        self.tracker[:, 1] = self.S2
        self.tracker[:, 2] = self.S3
        self.tracker[:, 3] = self.S4
        self.point = [0, 0, -163.5]

    def relabel(self, order):
        """

        :param order: List of ints
        :return:
        """
        k = [self.S1, self.S2, self.S3, self.S4]
        l = [k[i] for i in order]
        self.S1 = l[0]
        self.S2 = l[1]
        self.S3 = l[2]
        self.S4 = l[3]
        self.tracker = np.zeros([3, 4])
        self.tracker[:, 0] = self.S1
        self.tracker[:, 1] = self.S2
        self.tracker[:, 2] = self.S3
        self.tracker[:, 3] = self.S4
        pass


class PosteriorStylus:
    def __init__(self):
        self.unit = "mm"
        self.S2 = [0, 0, 0]
        self.S1 = [0, 28.59, 41.02]
        self.S4 = [0, 0, 88.0]
        self.S3 = [0, -44.32, 40.45]
        self.tracker = np.zeros([3, 4])
        self.tracker[:, 0] = self.S1
        self.tracker[:, 1] = self.S2
        self.tracker[:, 2] = self.S3
        self.tracker[:, 3] = self.S4
        self.point = [5.8, 0, 182]
