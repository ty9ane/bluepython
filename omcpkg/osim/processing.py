from yatpkg.util.opensim_tools import IK, ID
import tkinter as tk


def window_control():
    components = {}
    window = tk.Tk()
    components['window'] = window
    window.title('Opensim IK Helper - Setup file creator')
    window.geometry("480x600")
    window.config(bg="skyblue")
    top_frame = tk.Frame(window, width=460, height=20)
    top_frame.grid(row=0, column=0, padx=5, pady=5)
    tk.Label(top_frame, text="hello", width=10).grid(row=0, column=0, padx=5, pady=5)
    tk.Entry(top_frame, width=30, bg="light yellow").grid(row=0, column=1, padx=5, pady=5)
    tk.Button(top_frame, width=10).grid(row=0, column=3, padx=5, pady=5)
    bottom_frame = tk.Frame(window, width=460, height=10)
    bottom_frame.grid(row=1, column=0, padx=10, pady=5)
    window.mainloop()


if __name__ == '__main__':
    window_control()
    pass
