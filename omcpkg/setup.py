import setuptools

long_description = ""

setuptools.setup(
    name="mcpkg-ytedy",
    version="0.0.02",
    author="Ted Yeung",
    author_email="tyeu008@aucklanduni.ac.nz",
    description="OMC processing",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    install_requires=['pandas', 'scipy', 'numpy', 'yatpkg']
)
