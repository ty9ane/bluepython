
import numpy as np
from tools import ReadFilt, devision #, feature_extraction
import pandas as pd
import csv
import h5py
from sklearn import preprocessing
from numpy import savetxt
import tables



if __name__ == "__main__":
    num_subjects = 5 # declare the number of subjects which used to make the dataset
    # body mass and height of subjects
    m_02 = 96.5
    m_03 = 72
    m_04 = 66
    m_05 = 71.3
    m_06 = 62.5
    m_07 = 68
    G = 9.81 # the gravity acceleration

    h_02 = 1.953
    h_03 = 1.69
    h_04 = 1.685
    h_05 = 1.715
    h_06 = 1.715
    h_07 = 1.71

    # read data
    id_2 = np.array(ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB02/inverse_dynamics.sto", file_type="ID"))
    emg_2 = ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB02/walk_00deg01_EMG.mot", file_type="EMG", filter='N', cut=id_2.shape[0]*10) # D:/my_tsfresh_Dataset/AB02/walk_00deg01_EMG.mot
    ik_2 = ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB02/walk_00deg_IK_AB02.mot", file_type="IK") # D:/my_tsfresh_Dataset/AB02/walk_00deg_IK_AB02.mot

    id_6 = np.array(ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB06/inverse_dynamics.sto", file_type="ID"))
    emg_6 = ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB06/walk_00deg_AB06_EMG.mot", file_type="EMG", filter='N', cut=id_6.shape[0]*10)
    ik_6 = ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB06/AB06_walk_00deg_IK.mot", file_type="IK")

    id_4 = np.array(ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB04/inverse_dynamics.sto", file_type="ID"))
    emg_4 = ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB04/walk_00deg01_EMG.mot", file_type="EMG", filter='N', cut=id_4.shape[0]*10)
    ik_4 = ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB04/walk_00deg_IK_AB04.mot", file_type="IK")

    id_7 = np.array(ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB07/inverse_dynamics.sto", file_type="ID"))
    emg_7 = ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB07/walk_00deg01_EMG.mot", file_type="EMG", filter='N', cut=id_7.shape[0]*10)
    ik_7 = ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB07/walk_00deg_IK_AB07.mot", file_type="IK")

    id_5 = np.array(ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB05/inverse_dynamics.sto", file_type="ID"))
    emg_5 = ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB05/walk_00deg01_EMG.mot", file_type="EMG", filter='N', cut=id_5.shape[0]*10)
    ik_5 = ReadFilt.load_data("C:/Users/hzar638/OneDrive - The University of Auckland/dataset/my_tsfresh_Dataset/AB05/walk_00deg_IK_AB05.mot", file_type="IK")


    # separate the 1(m/s) speed part from entire trial. (the length of data defined manually. 7251 is half size and 9001 is the full size data of 1m/s speed)
    ID_conc = np.concatenate([id_2[5501:7251,]/(m_02*G*h_02), id_4[5501:7251,]/(m_04*G*h_04), id_5[5501:7251,]/(m_05*G*h_05), id_6[5501:7251,]/(m_06*G*h_06), id_7[5501:7251,]/(m_07*G*h_07)]) # 9001
    IK_conc = np.concatenate([ik_2[5501:7251,:], ik_4[5501:7251,:], ik_5[5501:7251,:], ik_6[5501:7251,:], ik_7[5501:7251,:]]) # 9001
    EMG_conc = np.concatenate([emg_2[55010:72510,:], emg_4[55010:72510,:], emg_5[55010:72510,:], emg_6[55010:72510,:], emg_7[55010:72510,:]]) # 90010


    all_train_input = np.empty((0,27))
    all_train_output = np.array([])
    all_test_input = np.empty((0,27))
    all_test_output = np.array([])

    ### this for loop needs to repeat based on the number of subjects
    for i in range(num_subjects):
        # filter the ID
        ID = ID_conc[i * 1750:(i + 1) * 1750]  # 3500
        IK = IK_conc[i * 1750:(i + 1) * 1750]  # 3500
        EMG = EMG_conc[i * 17500:(i + 1) * 17500]  # 35000

        # filter the ID results to make that smoother and remove the jerky fluctuations from the ID results
        ID = ReadFilt.filterID(ID)


        print('the size of ID is: ', ID.shape)
        print('the size of IK is: ', IK.shape)
        print('the size of EMG is: ', EMG.shape)

        # filter the EMG
        EMG_filt = ReadFilt.filteremg(EMG)

        # in the initial plan we suppose to do the normalization here but plan changed afterward however i am still using the same naming system as the initial plan
        EMG_norm = EMG_filt # EMG_norm = ReadFilt.normalize(EMG_filt)
        IK_norm = IK  #  IK_norm = ReadFilt.normalize(IK)

        # down sampling the EMG after filtering and normalizing
        EMG_down = EMG_norm[0::10,:]
        print('the size after down sampling is: ', EMG_down.shape)

        # calculate the angular velocity (deg/sec):
        vel_R = ReadFilt.vel_cal(IK[:, 0], fq=100)
        print("the vel_R size is: ", vel_R.shape)
        vel_L = ReadFilt.vel_cal(IK[:, 1], fq=100)
        print("the vel_L size is: ", (vel_L.shape))

        #  in the initial plan we suppose to do the normalization here but plan changed afterward however i am still using the same naming system as the initial plan
        vel_R_norm = vel_R  # ReadFilt.normalize(vel_R)
        vel_L_norm = vel_L  # ReadFilt.normalize(vel_L)
        print('the normalized velocity for Right is:', vel_L_norm.shape)

        # Generate the new time series based on the original inputs:
        # angle_R - angle_L
        angle_R_L = IK_norm[:,0] - IK_norm[:,1]
        print("the angle_R_L size is: ", angle_R_L.shape)

        vel_R_L = vel_R_norm - vel_L_norm
        print("the vel_R_L size is: ", vel_R_L.shape)

        # generate the EMG based time series
        data = EMG_down
        sol_tib_r = data[:,2]-data[:,3]
        gasmed_tib_r = data[:,0]-data[:,3]
        gaslat_tib_r = data[:,1]-data[:,3]

        sol_tib_l = data[:,6]-data[:,7]
        gasmed_tib_l = data[:,4]-data[:,7]
        gaslat_tib_l = data[:,5]-data[:,7]

        sol_r_l = data[:,2]-data[:,6]
        tib_r_l = data[:,3]-data[:,7]
        gaslat_r_l = data[:,1]-data[:,5]
        gasmed_r_l = data[:,0]-data[:,4]

        # add time and concatenate the ALL original and generated data
        time = np.zeros((IK.shape[0]))
        # add subject ID to the end of the data. the reason is that we will used that afterward to separate the data for each individual based on this number
        subj_ID = np.zeros((IK.shape[0]))
        for j in range(IK.shape[0]):  #     (i*int(IK.shape[0]), (i+1)*int(IK.shape[0])):
            time[j] = j+i*int(IK.shape[0])
            subj_ID[j] = i+1 # this is the line in which the subject ID is defined. this is used in next steps to separate the data for each subject
        time = np.array(time) # convert from list to array
        time = time.reshape(time.shape[0],1) # change the shape of array to what we want which is a vertical vector of data points

        subj_ID = np.array(subj_ID) # convert from list to array
        subj_ID = subj_ID.reshape(subj_ID.shape[0],1) # change the shape of array to what we want which is a vertical vector of data points

        x_seq = np.concatenate((time, IK_norm, vel_R_norm, vel_L_norm, angle_R_L[:,None], vel_R_L, EMG_down
                                , sol_tib_r[:,None], gasmed_tib_r[:,None], gaslat_tib_r[:,None], sol_tib_l[:,None]
                                , gasmed_tib_l[:,None], gaslat_tib_l[:,None], sol_r_l[:,None], tib_r_l[:,None]
                                , gaslat_r_l[:,None], gasmed_r_l[:,None], subj_ID), axis=1) # concatinate two arrays columnwise (side by side) / for row wise concatination the axis must be zero (axis=0)
        x_seq = np.array(x_seq)
        print("the size of created input time series is: ", x_seq.shape)
        print("the TYPE of created input time series is: ", type(x_seq))

        x_seq_mod = x_seq[1:,:] # removing the first row of input time series in which the angular vel is zero
        y_out = IK[1:,0] # removing the out put corresponding to the zero angular vel row. considering the right ankle angle as the desired output
        print("the size of input time series after removing zero angular_vel is: ", x_seq_mod.shape)
        print("the size of output right angle corresponding to the input time series after removing zero angular_vel is: ", y_out.shape)


        win_L = 100
        sliding = 10
        future = 100 # 30ms ahead means 0.03 second equal to 3 time steps in 100hz sampling rate
        X,Y = ReadFilt.window_multi_data( x_seq_mod, y_out, win_size=win_L, forward=sliding, future_steps=future, id=int(((int(y_out.shape[0])-win_L)/sliding)-(future-1))*i+1) # the id should get updated based on the number of windows for each subject #  , id=int(((int(y_out.shape[0])-100)/10)-100)*i+1

        all_train_input = np.concatenate((all_train_input, X))
        all_train_output = np.concatenate((all_train_output, Y))

    # scale and normalize the input time series as a cohort instead of normalizing the data of each subject individually
    x_scaled = preprocessing.scale(all_train_input[:, 2:-1]) # apply scaling to the features apart from the id and time which are the first two columns
    all_train_input_scaled = np.concatenate((np.reshape(all_train_input[:,0:2],(-1,2)), x_scaled, (np.reshape(all_train_input[:,26],(-1,1)))), axis=1) # add the id and time columns to the begining of the other scaled features
                                                                                                                                                        # (np.reshape(all_train_input[:,0:2],(-1,2)) ==> this is for reshaping the first two columns of data to a (n X 2) matrix and then concatenate that to the rest of the datapoint
    # print(x_scaled.mean(axis=0))
    # print(x_scaled.std(axis=0))

    # add header to the matrix of input time series and output. the output is still called torque
    label_train_input = ReadFilt.header_input(all_train_input_scaled)
    # label_test_input = ReadFilt.header_input(all_test_input)
    label_train_output = ReadFilt.header_output(all_train_output)
    # label_test_output = ReadFilt.header_output(all_test_output)

    print("the total windowed train input data without header size is: ", all_train_input_scaled.shape)
    print("the total windowed train output data without header size is: ", all_train_output.shape)

    # with open('C:/Users/hzar638/Desktop/data_sample/windowed_labeled_input.csv', 'w', newline='') as csvFile:
    #     writer = csv.writer(csvFile)
    #     writer.writerows(label_train_input)
    # csvFile.close()

    # save the generated data as input time series and corresponding output in a .h file.
    # first you need to convert the data from the numpy array to the pandas data format which is happening in this section.
    X_train = pd.DataFrame(data=(label_train_input[1:, :].astype(float)),  # values
                     index=None,    # 1st column as index
                     columns=label_train_input[0, :])

    Y_train = pd.DataFrame(data=(label_train_output[1:].transpose().astype(float)),  # values
                     index=None,    # 1st column as index
                     columns=label_train_output[0:1])

    print(X_train)

    # in this section the directory for the file selected and data saved as they declared in this section.
    file_dir = 'C:/Users/hzar638/Desktop/data_sample/paper_2/input_data.h5'
    X_train.to_hdf(file_dir, key='X_train', mode='w')
    Y_train.to_hdf(file_dir, key='Y_train', mode='a')

    # this section is just for read the created file and check the list of components in that
    with h5py.File(file_dir, 'r') as hdf:
        print(list(hdf.keys()))





