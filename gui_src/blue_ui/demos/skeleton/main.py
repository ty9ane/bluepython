import copy

from blue_ui.templates.basic_main import BasicMainWindow, LeftWidget, CentreVTKWidget
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout
from PyQt5.QtWidgets import QLabel, QSlider, QLabel, QCheckBox
from PyQt5.Qt import Qt
import vtk
import numpy as np
import pandas as pd
from yatpkg.util.data import Yatsdo
from enum import Enum
from scipy.spatial.transform import Rotation as rt


class OpensimJoint(Enum):
    pelvis_tilt = [-1, [-90, 90]]
    pelvis_list = [-2, [-90, 90]]
    pelvis_rotation = [-3, [-90, 90]]
    hip_flexion_r = [0, [-120, 120]]
    hip_adduction_r = [1,[-120, 120]]
    hip_rotation_r = [2, [-120, 120]]
    hip_flexion_l = [3, [-120, 120]]
    hip_adduction_l = [4, [-120, 120]]
    hip_rotation_l = [5, [-120, 120]]
    knee_angle_r = [6, [-120, 10]]
    knee_angle_l = [7, [-120, 10]]
    ankle_r = [8, [-90, 90]]
    ankle_l = [9, [-90, 90]]
    lumbar_extension = [10, [-90, 90]]
    lumbar_bending = [11, [-90, 90]]
    lumbar_rotation = [12, [-90, 90]]

    @staticmethod
    def match(name):
        for a in OpensimJoint:
            if a.name == name:
                return a
        return None


class OpensimModel(Enum):
    pelvis = [-1, np.asarray([0, 0, 0]), [OpensimJoint.pelvis_list, OpensimJoint.pelvis_tilt, OpensimJoint.pelvis_rotation]]
    hip_l = [0, np.asarray([-0.0707, -0.0661, -0.0835]), [OpensimJoint.hip_flexion_l, OpensimJoint.hip_adduction_l, OpensimJoint.hip_rotation_l]]
    hip_r = [1, np.asarray([-0.0707, -0.0661, 0.0835]), [OpensimJoint.hip_flexion_r, OpensimJoint.hip_adduction_r, OpensimJoint.hip_rotation_r]]
    ankle_l = [2, np.asarray([0.0, -0.43, 0.0]), [OpensimJoint.ankle_l]]
    ankle_r = [3, np.asarray([0.0, -0.43, 0.0]), [OpensimJoint.ankle_r]]
    foot_l = [4, np.asarray([-0.04877, -0.04195, -0.00792])]
    foot_r = [5, np.asarray([-0.04877, -0.04195, 0.00792])]
    toes_l = [6, np.asarray([0.1788, -0.002, -0.00108])]
    toes_r = [7, np.asarray([0.1788, -0.002, 0.00108])]
    lumbar = [8, np.asarray([-0.1007, 0.0815, 0]), [OpensimJoint.lumbar_bending, OpensimJoint.lumbar_extension, OpensimJoint.lumbar_rotation]]
    sc = [9, np.asarray([-0.063, 0.46, 0])]
    clavicle_l = [10, np.asarray([-0.0127205, 0.00849541, -0.025])]
    clavicle_r = [11, np.asarray([-0.0127205, 0.00849541, 0.025])]
    scapula_l = [12, np.asarray([-0.01433, 0.02007, -0.135535])]
    scapula_r = [13, np.asarray([-0.01433, 0.02007, 0.135535])]
    humerus_l = [14, np.asarray([-0.00955, -0.034, -0.009])]
    humerus_r = [15, np.asarray([-0.00955, -0.034, 0.009])]
    elbow_l = [16, np.asarray([0.0161, -0.2704, -0.0423])]
    elbow_r = [17, np.asarray([0.0161, -0.2704, 0.0423])]
    radius_l = [18, np.asarray([0.0004, -0.011503, -0.019999])]
    radius_r = [19, np.asarray([0.0004, -0.011503, 0.019999])]
    hand_l = [20, np.asarray([-0.01, -0.208100258785044, -0.06070687891017])]
    hand_r = [21, np.asarray([-0.01, -0.208100258785044, 0.06070687891017])]
    knee_l = [22, np.asarray([0, 0, 0]), [OpensimJoint.knee_angle_l]]
    knee_r = [23, np.asarray([0, 0, 0]), [OpensimJoint.knee_angle_r]]


class Actor:
    def __init__(self, filename, color=None, scale=1, translation= None):
        if color is None:
            color = [144 / 255, 207 / 255, 252 / 255]

        reader = vtk.vtkPLYReader()
        if filename.endswith('.stl'):
            reader = vtk.vtkSTLReader()
        reader.SetFileName(filename)
        reader.Update()
        self.polydata = reader.GetOutput()

        self.mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            # mapper.SetInput(reader.GetOutput())
            self.mapper.SetInput(self.polydata)
        else:
            self.mapper.SetInputData(self.polydata)
        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)
        self.actor.GetProperty().SetColor(color[0], color[1], color[2])

        self.actor.GetProperty().SetRepresentationToWireframe()
        self.actor.GetProperty().EdgeVisibilityOn()
        self.actor.GetProperty().SetEdgeColor(0, 0, 0)

        mesh_data_copy = vtk.vtkPolyData()
        mesh_data_copy.DeepCopy(self.polydata)
        vert = []
        for i in range(mesh_data_copy.GetNumberOfPoints()):
            vert.append(mesh_data_copy.GetPoint(i))
        if translation is None:
            translation = np.array([0, 0, 0])
        self.rotation_centre = scale*translation
        vert_np = scale*np.asarray(vert) + scale*translation
        vs = vtk.vtkPoints()
        vs.SetData(Skeleton.numpy_to_vtk(vert_np))
        self.actor.GetMapper().GetInput().SetPoints(vs)
        self.points = vert_np
        pass

    def apply_points(self, points):
        vs = vtk.vtkPoints()
        vs.SetData(Skeleton.numpy_to_vtk(points))
        self.actor.GetMapper().GetInput().SetPoints(vs)


class Bone:
    def __init__(self, bone: dict = None, scale=1):
        self.__bone__: dict[str, Actor] = {}
        self.bone_scale = scale
        self.bone_const = None
        self.parent = None
        self.child = None
        self.root = None
        self.current_angle = np.asarray([0, 0, 0])
        self.parent_rc = []
        self.parent_ori = []
        self.__rotation_centre__ = np.asarray([0, 0, 0])
        if bone is not None:
            for b in bone:
                self.__bone__[b] = bone[b]

    @property
    def rotation_centre(self):
        return self.__rotation_centre__

    @rotation_centre.setter
    def rotation_centre(self, x):
        self.__rotation_centre__ = np.asarray(x)

    def add_bone(self, key, bone):
        self.__bone__[key] = bone

    def __apply_transformation__(self, transform, rc=None):
        if rc is None:
            if len(self.parent_rc) == 0:
                self.parent_rc = [self.bone_scale * self.rotation_centre]
            rc = copy.deepcopy(self.parent_rc)
            rc.insert(1, self.bone_scale * self.bone_const.value[1])
            transform1 = [ori for ori in self.parent_ori]
            transform1.insert(0, transform)
            transform = transform1
        else:
            r = rt.from_euler('yzx', self.current_angle, degrees=True)
            self.parent_ori = copy.deepcopy(transform)
            self.parent_rc = copy.deepcopy(rc)
            transform.insert(0, r.as_matrix())
            rc.insert(0, self.bone_scale * self.bone_const.value[1])

        for d in self.__bone__:
            print(d)
            m0 = np.zeros([self.__bone__[d].points.shape[0] + 1, self.__bone__[d].points.shape[1]])
            m0[:-1, :] = self.__bone__[d].points
            m = m0 - self.bone_scale * self.rotation_centre
            for t in range(1, len(transform)):
                m = np.matmul(transform[t], m.T)
                m = m.T + rc[t]

            self.__bone__[d].apply_points(m[:-1, :])
        if self.child is not None:
            print("-----")
            if isinstance(self.child, list):
                for c in self.child:
                    c.apply_transformation(transform, rc)
            else:
                self.child.apply_transformation(transform, rc)
        self.root.update()
        pass

    def apply_transformation(self, transform, rc=None):
        self.__apply_transformation__(transform, rc)
        # if rc is None:
        #     rc = self.rotation_centre
        # for d in self.__bone__:
        #     print(d)
        #     m0 = np.zeros([self.__bone__[d].points.shape[0]+1, self.__bone__[d].points.shape[1]])
        #     m0[:-1, :] = self.__bone__[d].points
        #     m0[-1, :] = rc
        #     m = m0 - rc
        #     m1 = np.matmul(transform, m.T)
        #     m2 = m1.T + rc
        #     self.rotation_centre = m2[-1, :]
        #     self.__bone__[d].apply_points(m2[:-1, :])
        # if self.child is not None:
        #     print("-----")
        #     self.child.apply_transformation(transform, rc)
        # self.root.update()
        # print()

    def get_actor_all(self):
        return [self.__bone__[d] for d in self.__bone__]

    def get_actor(self, actor):
        return self.__bone__[actor]


class Skeleton(BasicMainWindow):
    def __init__(self):
        super().__init__()
        self.joint_labels = ['GH']
        bone_scale = 1000
        self.ratio = 0.2778

        self.pelvis = Skeleton.create_pelvis(bone_scale)
        self.create_femur(bone_scale)
        self.pelvis.child = [self.femur_l, self.femur_r]
        self.knee_left = KneeModel.knee_left_loc()
        self.knee_right = KneeModel.knee_right_loc()
        shanks = self.create_shank(bone_scale, {'left': self.knee_left, 'right': self.knee_left})
        self.shank_l = shanks['left']
        self.shank_l.parent = self.femur_l
        self.femur_l.child = self.shank_l
        self.shank_r = shanks['right']
        self.shank_r.parent = self.femur_r
        self.femur_r.child = self.shank_r
        foot = self.create_foot(bone_scale, {'left': self.knee_left, 'right': self.knee_left})
        self.foot_l = foot['left']
        self.foot_l.parent = self.shank_l
        self.shank_l.child = self.foot_l
        self.foot_r = foot['right']
        self.foot_r.parent = self.shank_r
        self.shank_r.child = self.foot_r
        self.torso = self.create_torso(bone_scale, OpensimModel.lumbar.value[1])
        self.pelvis.child.append(self.torso)
        upper_l = OpensimModel.sc.value[1] + OpensimModel.clavicle_l.value[1]
        upper_r = OpensimModel.sc.value[1] + OpensimModel.clavicle_r.value[1]
        self.clavicle_l = Clavicle(
            {"clavicle_l": Actor("default_meshes/l_clavicle.stl", scale=bone_scale, translation=upper_l)})
        self.clavicle_l.bone_scale = 1000
        self.clavicle_l.bone_const = OpensimModel.clavicle_l
        self.clavicle_r = Clavicle(
            {"clavicle_r": Actor("default_meshes/r_clavicle.stl", scale=bone_scale, translation=upper_r)})

        self.clavicle_r.bone_scale = 1000
        self.clavicle_r.bone_const = OpensimModel.clavicle_r

        self.torso.child = [self.clavicle_l, self.clavicle_r]

        upper_l = upper_l + OpensimModel.scapula_l.value[1]
        upper_r = upper_r + OpensimModel.scapula_r.value[1]

        self.scapula_l = Bone(
            {"scapula_l": Actor("default_meshes/l_scapula.stl", scale=bone_scale, translation=upper_l)})
        self.scapula_l.bone_scale = 1000
        self.scapula_l.bone_const = OpensimModel.scapula_l
        self.clavicle_l.child = self.scapula_l

        self.scapula_r = Bone(
            {"scapula_r": Actor("default_meshes/r_scapula.stl", scale=bone_scale, translation=upper_r)})
        self.scapula_r.bone_scale = 1000
        self.scapula_r.bone_const = OpensimModel.scapula_r
        self.clavicle_r.child = self.scapula_r

        upper_l = upper_l + OpensimModel.humerus_l.value[1]
        upper_r = upper_r + OpensimModel.humerus_r.value[1]

        self.humerus_l = Bone(
            {"humerus_l": Actor("default_meshes/l_humerus.stl", scale=bone_scale, translation=upper_l)})

        self.humerus_r = Bone(
            {"humerus_r": Actor("default_meshes/r_humerus.stl", scale=bone_scale, translation=upper_r)})

        upper_l = upper_l + OpensimModel.elbow_l.value[1]
        upper_r = upper_r + OpensimModel.elbow_r.value[1]

        self.ulna_l = Bone(
            {"ulna_l": Actor("default_meshes/l_ulna.stl", scale=bone_scale, translation=upper_l)})

        self.ulna_r = Bone(
            {"ulna_r": Actor("default_meshes/r_ulna.stl", scale=bone_scale, translation=upper_r)})

        upper_l = upper_l + OpensimModel.radius_l.value[1]
        upper_r = upper_r + OpensimModel.radius_r.value[1]

        self.radius_l = Bone(
            {"radius_l": Actor("default_meshes/l_radius.stl", scale=bone_scale, translation=upper_l)})

        self.radius_r = Bone(
            {"radius_r": Actor("default_meshes/r_radius.stl", scale=bone_scale, translation=upper_r)})

        upper_l = upper_l + OpensimModel.hand_l.value[1]
        upper_r = upper_r + OpensimModel.hand_r.value[1]

        self.hand_l = Bone(
            {"hand_l": Actor("default_meshes/l_hand_v3.stl", scale=bone_scale, translation=upper_l)})

        self.hand_r = Bone(
            {"hand_r": Actor("default_meshes/r_hand_v3.stl", scale=bone_scale, translation=upper_r)})

    def create_femur(self, bone_scale):
        self.femur_l = Femur(
            {"femur_l": Actor("default_meshes/femur_l.stl", scale=bone_scale, translation=OpensimModel.hip_l.value[1])})
        self.femur_l.parent = self.pelvis
        self.femur_l.bone_const = OpensimModel.hip_l
        self.femur_l.bone_scale = 1000
        self.femur_l.rotation_centre = OpensimModel.hip_l.value[1]
        self.femur_r = Femur(
            {"femur_r": Actor("default_meshes/femur_r.stl", scale=bone_scale, translation=OpensimModel.hip_r.value[1])})
        self.femur_r.parent = self.pelvis
        self.femur_r.bone_const = OpensimModel.hip_r
        self.femur_r.bone_scale = 1000
        self.femur_r.rotation_centre = OpensimModel.hip_r.value[1]

    @staticmethod
    def create_torso(scale, translate):
        b = Bone()
        b.add_bone("spine", Actor("default_meshes/hat_spine.stl", scale=scale, translation=translate))
        b.add_bone("skull", Actor("default_meshes/hat_skull.stl", scale=scale, translation=translate))
        b.add_bone("jaw", Actor("default_meshes/hat_jaw.stl", scale=scale, translation=translate))
        b.add_bone("ribs", Actor("default_meshes/hat_ribs.stl", scale=scale, translation=translate))
        b.bone_const = OpensimModel.lumbar
        return b

    @staticmethod
    def create_pelvis(scale):
        b = Pelvis()
        b.add_bone("pelvis_l", Actor("default_meshes/l_pelvis.stl", scale=scale))
        b.add_bone("pelvis_r", Actor("default_meshes/pelvis.stl", scale=scale))
        b.add_bone("sacrum", Actor("default_meshes/sacrum.stl", scale=scale))
        b.bone_const = OpensimModel.pelvis
        return b

    @staticmethod
    def create_shank(scale, info: dict[str: list[Yatsdo]]):
        left = OpensimModel.hip_l.value[1] + KneeModel.knee_loc(info['left'], 0)
        right = OpensimModel.hip_r.value[1] + KneeModel.knee_loc(info['right'], 0)
        b_l = KneeModel()
        b_l.rotation_centre = left
        b_l.parent_rc = [scale * OpensimModel.hip_l.value[1], np.asarray([0, 0, 0])]
        b_l.bone_scale = scale
        b_l.add_bone("tibia_l", Actor("default_meshes/tibia_l.stl", scale=scale, translation=left))
        b_l.add_bone("fibula_l", Actor("default_meshes/fibula_l.stl", scale=scale, translation=left))
        b_r = KneeModel(is_right=True)
        b_r.rotation_centre = right
        b_r.bone_scale = scale
        b_r.parent_rc = [scale * OpensimModel.hip_r.value[1], np.asarray([0, 0, 0])]
        b_r.add_bone("tibia_r", Actor("default_meshes/tibia_r.stl", scale=scale, translation=right))
        b_r.add_bone("fibula_r", Actor("default_meshes/fibula_r.stl", scale=scale, translation=right))
        return {'left': b_l, 'right': b_r}

    @staticmethod
    def create_foot(scale, info_knee: dict[str: list[Yatsdo]]):
        left = OpensimModel.hip_l.value[1] + KneeModel.knee_loc(info_knee['left'], 0) + OpensimModel.ankle_l.value[1]
        b_l = Ankle(scale=scale)
        b_l.bone_const = OpensimModel.ankle_l
        b_l.rotation_centre = left
        b_l.add_bone("talus_l", Actor("default_meshes/l_talus.stl", scale=scale, translation=left))
        left = OpensimModel.hip_l.value[1] + KneeModel.knee_loc(info_knee['left'], 0) + OpensimModel.ankle_l.value[
            1] + OpensimModel.foot_l.value[1]
        b_l.add_bone("foot_l", Actor("default_meshes/l_foot.stl", scale=scale, translation=left))
        left = OpensimModel.hip_l.value[1] + KneeModel.knee_loc(info_knee['left'], 0) + OpensimModel.ankle_l.value[
            1] + OpensimModel.foot_l.value[1] + OpensimModel.toes_l.value[1]
        b_l.add_bone("toes_l", Actor("default_meshes/l_bofoot.stl", scale=scale, translation=left))

        right = OpensimModel.hip_r.value[1] + KneeModel.knee_loc(info_knee['right'], 0) + OpensimModel.ankle_r.value[1]
        b_r = Ankle(scale=scale)
        b_r.bone_const = OpensimModel.ankle_r
        b_r.rotation_centre = right
        b_r.add_bone("talus_l", Actor("default_meshes/r_talus.stl", scale=scale, translation=right))
        right = OpensimModel.hip_r.value[1] + KneeModel.knee_loc(info_knee['right'], 0) + OpensimModel.ankle_r.value[
            1] + OpensimModel.foot_r.value[1]
        b_r.add_bone("foot_l", Actor("default_meshes/r_foot.stl", scale=scale, translation=right))
        right = OpensimModel.hip_r.value[1] + KneeModel.knee_loc(info_knee['right'], 0) + OpensimModel.ankle_r.value[
            1] + OpensimModel.foot_r.value[1] + OpensimModel.toes_r.value[1]
        b_r.add_bone("toes_l", Actor("default_meshes/r_bofoot.stl", scale=scale, translation=right))
        return {'left': b_l, 'right': b_r}

    def add_bone_actors(self):
        def add_bones(bone_list):
            for b in bone_list:
                self.central_widget.add_actor(b.actor, False)

        # root
        add_bones(self.pelvis.get_actor_all())
        self.pelvis.root = self.central_widget

        # lower
        add_bones(self.femur_l.get_actor_all())
        self.femur_l.root = self.central_widget
        add_bones(self.femur_r.get_actor_all())
        self.femur_r.root = self.central_widget
        add_bones(self.shank_l.get_actor_all())
        self.shank_l.root = self.central_widget
        add_bones(self.shank_r.get_actor_all())
        self.shank_r.root = self.central_widget
        add_bones(self.foot_l.get_actor_all())
        self.foot_l.root = self.central_widget
        add_bones(self.foot_r.get_actor_all())
        self.foot_r.root = self.central_widget

        # torso
        add_bones(self.torso.get_actor_all())
        self.torso.root = self.central_widget
        add_bones(self.clavicle_l.get_actor_all())
        self.clavicle_l.root = self.central_widget
        add_bones(self.clavicle_r.get_actor_all())
        self.clavicle_r.root = self.central_widget
        add_bones(self.scapula_l.get_actor_all())
        self.scapula_l.root = self.central_widget
        add_bones(self.scapula_r.get_actor_all())
        self.scapula_r.root = self.central_widget
        add_bones(self.humerus_l.get_actor_all())
        self.humerus_l.root = self.central_widget
        add_bones(self.humerus_r.get_actor_all())
        self.humerus_r.root = self.central_widget
        add_bones(self.ulna_l.get_actor_all())
        self.ulna_l.root = self.central_widget
        add_bones(self.ulna_r.get_actor_all())
        self.ulna_r.root = self.central_widget
        add_bones(self.radius_l.get_actor_all())
        self.radius_l.root = self.central_widget
        add_bones(self.radius_r.get_actor_all())
        self.radius_r.root = self.central_widget
        add_bones(self.hand_l.get_actor_all())
        self.hand_l.root = self.central_widget
        add_bones(self.hand_r.get_actor_all())
        self.hand_r.root = self.central_widget
        self.central_widget.vtk_widget.update()

    def setup(self):
        self.central_widget = CentreVTKWidget()
        self.central_widget.add_actor(self.axes)
        self.add_bone_actors()
        self.setCentralWidget(self.central_widget)
        self.left_widget = LeftWidget()
        self.left_widget.setMaximumWidth(int(self.ratio*self.geometry().width()))
        self.left_widget.title.setText("Joint Angles")
        j = JointUI('Pelvis', OpensimModel.pelvis.value[2], Joint(OpensimModel.pelvis.value[2], bone=self.pelvis))
        j.add_to_parent_layout(self.left_widget.left_side)
        j0 = JointUI('Hip Left', OpensimModel.hip_l.value[2], Joint(OpensimModel.hip_l.value[2], bone=self.femur_l))
        j0.add_to_parent_layout(self.left_widget.left_side)
        j1 = JointUI('Hip Right', OpensimModel.hip_r.value[2], Joint(OpensimModel.hip_r.value[2], bone=self.femur_r))
        j1.add_to_parent_layout(self.left_widget.left_side)
        j3 = JointUI('Knee Left', OpensimModel.knee_l.value[2], Joint(OpensimModel.knee_l.value[2], bone=self.shank_l))
        j3.add_to_parent_layout(self.left_widget.left_side)
        j4 = JointUI('Knee Right', OpensimModel.knee_r.value[2], Joint(OpensimModel.knee_r.value[2], bone=self.shank_r))
        j4.add_to_parent_layout(self.left_widget.left_side)
        j5 = JointUI('Ankle Left', OpensimModel.ankle_l.value[2], Joint(OpensimModel.ankle_l.value[2], bone=self.foot_l))
        j5.add_to_parent_layout(self.left_widget.left_side)
        j6 = JointUI('Ankle Right', OpensimModel.ankle_r.value[2],
                     Joint(OpensimModel.ankle_r.value[2], bone=self.foot_r))
        j6.add_to_parent_layout(self.left_widget.left_side)
        self.left_widget.setup()
        h_layout = QHBoxLayout()
        h_layout.addWidget(self.left_widget)
        h_layout.addWidget(self.central_widget.canvas_3d)

        self.central_widget.setLayout(h_layout)

    def resizeEvent(self, event):
        self.left_widget.setMaximumWidth(int(self.ratio*self.geometry().width()))


class Shoulder:
    def __init__(self):
        self.humerus = None
        self.scapula = None
        self.clavicle = None
        self.current_angle = [0.0, 0.0, 0.0]
        self.__humerus__ = [0.0, 0.0, 0.0]
        self.__scapula__ = [0.0, 0.0, 0.0]
        self.__clavicle__ = [0.0, 0.0, 0.0]

    def apply_transformation(self, transform, rc=None):
        if self.current_angle[2] < 30:
            self.__humerus__[2] = self.current_angle[2]
            pass
        else:
            angle = self.current_angle[2] - 30
            part = angle/3.0
            self.__humerus__[2] = 30 + 2 * part
            self.__scapula__[2] = part
            pass

        if self.current_angle[1] < 30:
            self.__humerus__[1] = self.current_angle[1]
            pass
        else:
            angle = self.current_angle[1] - 30
            part = angle/3.0
            self.__humerus__[1] = 30 + 2 * part
            self.__scapula__[2] = part
            pass
        pass


class Clavicle(Bone):
    def __init__(self, bone: dict = None, scale=1):
        super().__init__(bone, scale)
        self.parent_ori = [np.eye(3,3)]
        self.parent_rc = [self.bone_scale * np.asarray([0, 0, 0])]
        self.current_angle = np.eye(3,3)

    def apply_transformation(self, transform, rc=None):
        old_rotation_centre = self.rotation_centre
        root = np.atleast_2d(self.bone_const.value[1])
        boo = False
        if rc is None:
            self.current_angle = transform
            rc = copy.deepcopy(self.parent_rc)
            rc.insert(0, self.bone_scale * self.bone_const.value[1])
            transform1 = [ori for ori in self.parent_ori]
            transform1.insert(0, transform)
            transform = transform1
        else:
            boo = True
            r = rt.from_matrix(self.current_angle)
            root = np.matmul(transform[0], root.T)
            self.parent_ori = copy.deepcopy(transform)
            self.parent_rc = copy.deepcopy(rc)
            transform.insert(0, r.as_matrix())
            rc.insert(1, self.bone_scale * np.squeeze(root.T))
        for d in self.__bone__:
            print(d)
            m0 = np.zeros([self.__bone__[d].points.shape[0] + 1, self.__bone__[d].points.shape[1]])
            m0[:-1, :] = self.__bone__[d].points
            m = m0 - self.bone_scale*old_rotation_centre
            for t in range(1, len(transform)):
                m = np.matmul(transform[t], m.T)
                m = m.T + rc[t]

            self.__bone__[d].apply_points(m[:-1, :])

        # this is a hack to get order of transforms to be correct
        if boo:
            rc = copy.deepcopy(self.parent_rc)
            rc.insert(0, self.bone_scale * self.bone_const.value[1])
            transform1 = [ori for ori in self.parent_ori]
            transform1.insert(0, self.current_angle)
            transform = transform1
        if self.child is not None:
            print("-----")
            self.child.apply_transformation(transform, rc)
        self.root.update()


class Scapula(Bone):
    def __init__(self, bone: dict = None, scale=1):
        super().__init__(bone, scale)
        self.parent_ori = [np.eye(3, 3)]
        self.parent_rc = [self.bone_scale * np.asarray([0, 0, 0])]
        self.current_angle = np.eye(3, 3)

    def apply_transformation(self, transform, rc=None):
        old_rotation_centre = self.rotation_centre
        root = np.atleast_2d(self.bone_const.value[1])
        boo = False
        if rc is None:
            self.current_angle = transform
            rc = copy.deepcopy(self.parent_rc)
            rc.insert(0, self.bone_scale * self.bone_const.value[1])
            transform1 = [ori for ori in self.parent_ori]
            transform1.insert(0, transform)
            transform = transform1
        else:
            boo = True
            r = rt.from_matrix(self.current_angle)
            root = np.matmul(transform[0], root.T)
            self.parent_ori = copy.deepcopy(transform)
            self.parent_rc = copy.deepcopy(rc)
            transform.insert(0, r.as_matrix())
            rc.insert(1, self.bone_scale * np.squeeze(root.T))
        for d in self.__bone__:
            print(d)
            m0 = np.zeros([self.__bone__[d].points.shape[0] + 1, self.__bone__[d].points.shape[1]])
            m0[:-1, :] = self.__bone__[d].points
            m = m0 - self.bone_scale * old_rotation_centre
            for t in range(0, len(transform)):
                m = np.matmul(transform[t], m.T)
                m = m.T + rc[t]

            self.__bone__[d].apply_points(m[:-1, :])

        # this is a hack to get order of transforms to be correct
        if boo:
            rc = copy.deepcopy(self.parent_rc)
            rc.insert(0, self.bone_scale * self.bone_const.value[1])
            transform1 = [ori for ori in self.parent_ori]
            transform1.insert(0, self.current_angle)
            transform = transform1
        if self.child is not None:
            print("-----")
            self.child.apply_transformation(transform, rc)
        self.root.update()


class Femur(Bone):
    def __init__(self, bone: dict = None, scale=1):
        super().__init__(bone, scale)
        self.parent_ori = [np.eye(3,3)]
        self.parent_rc = [self.bone_scale * np.asarray([0, 0, 0])]
        self.current_angle = np.eye(3,3)

    def apply_transformation(self, transform, rc=None):
        old_rotation_centre = self.rotation_centre
        root = np.atleast_2d(self.bone_const.value[1])
        boo = False
        if rc is None:
            self.current_angle = transform
            rc = copy.deepcopy(self.parent_rc)
            rc.insert(0, self.bone_scale * self.bone_const.value[1])
            transform1 = [ori for ori in self.parent_ori]
            transform1.insert(0, transform)
            transform = transform1
        else:
            boo = True
            r = rt.from_matrix(self.current_angle)
            root = np.matmul(transform[0], root.T)
            self.parent_ori = copy.deepcopy(transform)
            self.parent_rc = copy.deepcopy(rc)
            transform.insert(0, r.as_matrix())
            rc.insert(1, self.bone_scale * np.squeeze(root.T))
        for d in self.__bone__:
            print(d)
            m0 = np.zeros([self.__bone__[d].points.shape[0] + 1, self.__bone__[d].points.shape[1]])
            m0[:-1, :] = self.__bone__[d].points
            m = m0 - self.bone_scale*old_rotation_centre
            for t in range(0, len(transform)):
                m = np.matmul(transform[t], m.T)
                m = m.T + rc[t]

            self.__bone__[d].apply_points(m[:-1, :])

        # this is a hack to get order of transforms to be correct
        if boo:
            rc = copy.deepcopy(self.parent_rc)
            rc.insert(0, self.bone_scale * self.bone_const.value[1])
            transform1 = [ori for ori in self.parent_ori]
            transform1.insert(0, self.current_angle)
            transform = transform1
        if self.child is not None:
            print("-----")
            self.child.apply_transformation(transform, rc)
        self.root.update()


class Pelvis(Bone):
    def __init__(self, bone: dict = None, scale=1):
        super().__init__(bone, scale)
        self.current_angle = 0
        self.bone_const: OpensimModel = None

    def apply_transformation(self, transform, rc=None):
        if rc is None:
            if len(self.parent_rc) == 0:
                self.parent_rc = [self.bone_scale * self.rotation_centre]
            rc = self.bone_scale * self.bone_const.value[1]

        for d in self.__bone__:
            print(d)
            m0 = np.zeros([self.__bone__[d].points.shape[0] + 1, self.__bone__[d].points.shape[1]])
            m0[:-1, :] = self.__bone__[d].points
            m = m0 - self.bone_scale * self.rotation_centre
            m = np.matmul(transform, m.T)
            m = m.T + rc

            self.__bone__[d].apply_points(m[:-1, :])
        if self.child is not None:
            print("-----")
            if isinstance(self.child, list):
                for c in self.child:
                    c.apply_transformation([transform], [rc])
            else:
                self.child.apply_transformation([transform], [rc])
        self.root.update()


class Ankle(Bone):
    def __init__(self, bone: dict = None, scale=1):
        super().__init__(bone, scale)
        self.current_angle = 0
        self.bone_const: OpensimModel = None

    def apply_transformation(self, transform, rc=None):
        if rc is None:
            if len(self.parent_rc) == 0:
                self.parent_rc = [self.bone_scale * self.rotation_centre]
            rc = copy.deepcopy(self.parent_rc)
            rc.insert(1, self.bone_scale*self.bone_const.value[1])
            r = rt.from_matrix(transform)
            angle = r.as_euler('yzx', True)
            self.current_angle = angle[2]
            r = rt.from_euler('xyz', [0, 0, angle[2]], degrees=True)
            transform1 = r.as_matrix()
            transform = [ori for ori in self.parent_ori]
            transform.insert(0, transform1)
        else:
            r = rt.from_euler('xyz', [0, 0, self.current_angle], degrees=True)
            self.parent_ori = copy.deepcopy(transform)
            self.parent_rc = copy.deepcopy(rc)
            transform.insert(0, r.as_matrix())
            rc.insert(1, self.bone_scale * self.bone_const.value[1])

        for d in self.__bone__:
            print(d)
            m0 = np.zeros([self.__bone__[d].points.shape[0]+1, self.__bone__[d].points.shape[1]])
            m0[:-1, :] = self.__bone__[d].points
            m = m0 - self.bone_scale * self.rotation_centre
            for t in range(0, len(transform)):
                m = np.matmul(transform[t], m.T)
                m = m.T + rc[t]

            self.__bone__[d].apply_points(m[:-1, :])
        if self.child is not None:
            print("-----")
            self.child.apply_transformation(transform, rc)
        self.root.update()


class KneeModel(Bone):
    def __init__(self, bone: dict = None, is_right=False):
        super().__init__(bone)
        self.knee_center = KneeModel.knee_left_loc()
        self.parent_rc = [np.asarray([0, 0, 0])]
        if is_right:
            self.knee_center = KneeModel.knee_right_loc()
        self.current_angle = 0
        self.parent_ori = [np.eye(3, 3)]
        self.bone_scale = 1

    def __apply_transformation__(self, transform, rc=None):
        cur = KneeModel.knee_loc(self.knee_center, self.current_angle)
        if rc is None:
            if len(self.parent_rc) == 0:
                self.parent_rc = [self.bone_scale * self.rotation_centre]
            rc = copy.deepcopy(self.parent_rc)
            rc.insert(0, self.bone_scale * KneeModel.knee_loc(self.knee_center, self.current_angle))
            r = rt.from_matrix(transform)
            angle = r.as_euler('yzx', True)
            self.current_angle = angle[2]
            r = rt.from_euler('xyz', [0, 0, angle[2]], degrees=True)
            transform1 = r.as_matrix()
            transform = [ori for ori in self.parent_ori]
            transform.insert(0, transform1)
        else:
            r = rt.from_euler('xyz', [0, 0, self.current_angle], degrees=True)
            self.parent_ori = copy.deepcopy(transform)
            self.parent_rc = copy.deepcopy(rc)
            transform.insert(0, r.as_matrix())
            rc.insert(0, self.bone_scale * KneeModel.knee_loc(self.knee_center, self.current_angle))
        print(rc)
        rot = np.sum(self.parent_rc, axis=0) + self.bone_scale * cur
        for d in self.__bone__:
            print(d)
            m0 = np.zeros([self.__bone__[d].points.shape[0] + 1, self.__bone__[d].points.shape[1]])
            m0[:-1, :] = self.__bone__[d].points
            m = m0 - rot
            for t in range(0, len(transform)):
                m = np.matmul(transform[t], m.T)
                m = m.T + rc[t]

            self.__bone__[d].apply_points(m[:-1, :])
        if self.child is not None:
            print("-----")
            self.child.apply_transformation(transform, rc)
        self.root.update()

    def apply_transformation(self, transform, rc=None):
        self.__apply_transformation__(transform, rc)
        # cur = KneeModel.knee_loc(self.knee_center, self.current_angle)
        # angle = [0, 0, self.current_angle]
        # if rc is not None:
        #     self.parent_ori = copy.deepcopy(transform)
        #     self.parent_rc = copy.deepcopy(rc[0])/self.bone_scale
        # else:
        #     r = rt.from_matrix(transform)
        #     angle = r.as_euler('yzx', True)
        #     self.current_angle = angle[2]
        # rc = self.bone_scale * (self.parent_rc + cur)   #self.rotation_centre
        # r = rt.from_euler('xyz', [0, 0, angle[2]], degrees=True)
        # po = KneeModel.knee_loc(self.knee_center, angle[2])
        # transform1 = r.as_matrix()
        # for d in self.__bone__:
        #     print(d)
        #     m0 = np.zeros([self.__bone__[d].points.shape[0] + 1, self.__bone__[d].points.shape[1]])
        #     m0[:-1, :] = self.__bone__[d].points
        #     m0[-1, :] = po
        #
        #     m = m0 - rc
        #     m = m.T
        #     m1 = np.matmul(transform1, m)
        #     m1 = m1.T
        #     m2 = m1 + self.bone_scale * po
        #
        #     m4 = np.matmul(self.parent_ori, m2.T)
        #     m5 = m4.T + self.bone_scale * self.parent_rc
        #     self.__bone__[d].apply_points(m5[:-1, :])
        #
        # if self.child is not None:
        #     rotataions = [transform1, self.parent_ori]
        #     translations = [self.bone_scale * po, self.bone_scale * self.parent_rc]
        #     self.child.apply_transformation(rotataions, translations)
        #
        # self.root.update()

    @staticmethod
    def knee_left_loc():
        dx = pd.DataFrame(
            {'u': [-2.0944, -1.74533, -1.39626, -1.0472, -0.698132, -0.349066, -0.174533, 0.197344, 0.337395, 0.490178,
                   1.52146, 2.0944],
             'v': [-0.0032, 0.00179, 0.00411, 0.0041, 0.00212, -0.001, -0.0031, -0.005227, -0.005435, -0.005574,
                   -0.005435, -0.00525]})
        x = Yatsdo(dx)
        dy = pd.DataFrame(
            {'u': [-2.0944, -1.22173, -0.523599, -0.349066, -0.174533, 0.159149, 2.0944],
             'v': [-0.4226, -0.4082, -0.399, -0.3976, -0.3966, -0.395264, -0.396]})
        y = Yatsdo(dy)
        return [x, y]

    @staticmethod
    def knee_right_loc():
        dx = pd.DataFrame(
            {'u': [-2.0944, -1.74533, -1.39626, -1.0472, -0.698132, -0.349066, -0.174533, 0.197344, 0.337395, 0.490178,
                   1.52146, 2.0944],
             'v': [-0.0032, 0.00179, 0.00411, 0.0041, 0.00212, -0.001, -0.0031, -0.005227, -0.005435, -0.005574,
                   -0.005435, -0.00525]})
        x = Yatsdo(dx)
        dy = pd.DataFrame(
            {'u': [-2.0944, -1.22173, -0.523599, -0.349066, -0.174533, 0.159149, 2.0944],
             'v': [-0.4226, -0.4082, -0.399, -0.3976, -0.3966, -0.395264, -0.396]})
        y = Yatsdo(dy)
        return [x, y]

    @staticmethod
    def knee_loc(d: list[Yatsdo], s):
        s = (s/180)*np.pi
        ret = np.asarray([0.0, 0.0, 0.0])
        idx = 0
        for yd in d:
            loc = yd.get_sample(s)
            ret[idx] = loc[0, 1]
            idx += 1
        return ret


class DOF:
    def __init__(self, dof: OpensimJoint, joint, steps=50):
        self.name = dof.name
        self.range = dof.value[1]
        self.label = QLabel('{0}: \t0°\t'.format(self.name))
        self.slider = QSlider(Qt.Horizontal)
        self.steps = steps
        self.joint = joint
        self.value = 0

        def update_gui_element(event):
            self.label.setText("{0}: \t{1}°\t".format(self.name, event))
            self.joint.update_model(OpensimJoint.match(self.name), event)

        self.slider.setMinimum(self.range[0])
        self.slider.setMaximum(self.range[1])
        self.slider.setValue(0)
        self.slider.valueChanged.connect(update_gui_element)


class JointUI:
    def __init__(self, name, dof, joint):
        self.name = name
        self.label = QLabel(self.name+':')
        self.dof = {d.name: DOF(d, joint) for d in dof}

    def add_to_parent_layout(self, layout):
        layout.addWidget(self.label)
        for d in self.dof:
            layout.addWidget(self.dof[d].label)
            layout.addWidget(self.dof[d].slider)


class Joint:
    def __init__(self, info, bone):
        self.info = info
        self.angles = [0, 0, 0]
        self.bone: Bone = bone
        pass

    def update_model(self, dof, angle):
        for i in range(0, len(self.info)):
            if self.info[i] == dof:
                self.angles[i] = angle
                break
        r = rt.from_euler('yzx', [self.angles[2], self.angles[1], self.angles[0]], degrees=True)
        test = r.as_matrix()
        self.bone.apply_transformation(test)
        pass


if __name__ == '__main__':
    app = QApplication([])
    window = Skeleton()
    window.initialise()
    app.exec_()
