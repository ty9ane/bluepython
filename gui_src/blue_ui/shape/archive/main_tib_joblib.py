import vtk
from vtkmodules.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from vtkmodules.util.numpy_support import numpy_to_vtk

from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QHBoxLayout, QVBoxLayout
from PyQt5.QtWidgets import QPushButton, QSlider, QLabel, QCheckBox, QToolBar, QAction, QStatusBar, QFileDialog, \
    QSizePolicy
from PyQt5.Qt import Qt, QSize
from PyQt5.QtGui import QIcon, QFont, QPalette, QColor

import pandas as pd
import numpy as np
import os
import copy
import joblib

from scipy.spatial.transform import Rotation as rt
from multiprocessing import Pool
from yatpkg.math.transformation import PCAModel

from typing import Union, Iterable
from datetime import datetime
from enum import Enum
import time
import json

"""
This is a test class for showing the PCs 1-3 for the shoulder (scapula, humerus)
It uses the *.pc.npz file generated from gias 3 and mean model to generate the views.
It will require maps of the landmarks and mesh parts for this script to work.

ICONS from KDr3w, deviantart and Ted

Version 0.0.1 Alpha
Authors(so far): Ted Yeung
"""


class Keywords(Enum):
    actor = [0, 'actor']
    polydata = [1, 'polydata']
    vertices = [2, 'vertices']
    idx = [3, 'idx']
    idm = [4, 'idm']


def milli():
    date = datetime.utcnow() - datetime(1970, 1, 1)
    seconds = (date.total_seconds())
    milliseconds = round(seconds * 1000)
    return milliseconds


class Mesh:
    def __init__(self, filename=None, boo=False, ignore=False):
        self.mesh_filename = filename
        self.reader = vtk.vtkSTLReader()
        if filename is not None and self.mesh_filename.endswith(".ply"):
            self.reader = vtk.vtkPLYReader()
        self.mesh = None
        self.points = None
        self.cog = None
        self.mc = None
        if self.mesh_filename is not None:
            self.load_as_vtk(self.mesh_filename)

    def load_as_vtk(self, filename):
        self.mesh_filename: str = filename
        if self.mesh_filename.endswith(".ply"):
            self.reader = vtk.vtkPLYReader()
        self.reader.SetFileName(self.mesh_filename)
        self.reader.Update()
        self.mesh = self.reader.GetOutput()
        polydata = self.mesh
        self.points = np.asarray([polydata.GetPoint(i) for i in range(polydata.GetNumberOfPoints())])
        self.cog = np.nanmean(self.points, axis=0)
        self.mc = self.cog
        pass


class Generators:

    @staticmethod
    def worker_finder(data):
        target = data[0]
        points = data[1]
        idx = data[2]
        time.sleep(0.01)
        print("worker: " + str(idx) + " working")
        found = -1
        ret = None
        lowest = 10000000000000
        closest = None
        for j in range(0, points.shape[0]):
            diff = np.linalg.norm(target - points[j, :])
            if lowest > diff:
                lowest = diff
                closest = [idx, j, lowest]
            if diff < 1e-5:
                ret = [idx, j, diff]
                found = j
                break
        if ret is None:
            found = closest[1]
        print("worker: " + str(idx) + " Done. J index: " + str(found) + "\n")
        if ret is not None:
            return ret
        return closest

    @staticmethod
    def finder(el, mean):
        with Pool(processes=10) as pool:
            arg_list = [[el.points[i, :], mean.points, i] for i in range(0, el.points.shape[0])]
            ret = pool.map(Generators.worker_finder, arg_list)
        d = pd.DataFrame(data=ret, columns=["idx", "idm", "errors"])
        return d

    @staticmethod
    def export_mapping(da_map, out_dir="C:\\Users\\tyeu008\\OneDrive\\Work\\Bones\\Shape\\Test\\maps\\"):
        for m in da_map:
            da_map[m].to_csv("{0}{1}_map_to_mean.csv".format(out_dir, m), index=False)

    @staticmethod
    def map_landmarks_for_humerus(wk, landmarks, mean_mesh_file):
        """
        ISB Definition of the GH Joint
        :return: dictionary of mesh name and vector x, y, z
        """
        mesh_map = {}
        el = Mesh()
        em = Mesh()
        mean = Mesh()
        glen = Mesh()
        el.load_as_vtk(wk + landmarks['EL'])
        em.load_as_vtk(wk + landmarks['EM'])
        glen.load_as_vtk(wk + landmarks['Humeral_Head'])
        mean.load_as_vtk(wk + mean_mesh_file)

        mesh_map["HumEL"] = Generators.finder(el, mean)
        mesh_map["HumEM"] = Generators.finder(em, mean)
        mesh_map["Humeral_Head"] = Generators.finder(glen, mean)
        return mesh_map

    @staticmethod
    def map_landmarks_for_scapula(wk, landmarks, mean_mesh_file):
        """
        ISB Definition of the GH Joint Requires stl or ply of landmarks segmented
        :return: dictionary of mesh name and vector x, y, z
        """
        mesh_map = {}
        aa = Mesh()
        ai = Mesh()
        ts = Mesh()
        mean = Mesh()
        glen = Mesh()
        aa.load_as_vtk(wk + landmarks['AA'])
        ai.load_as_vtk(wk + landmarks['AI'])
        ts.load_as_vtk(wk + landmarks['TS'])
        glen.load_as_vtk(wk + landmarks['Glenoid'])
        mean.load_as_vtk(wk + mean_mesh_file)

        mesh_map["ScapAA"] = Generators.finder(aa, mean)
        mesh_map["ScapAI"] = Generators.finder(ai, mean)
        mesh_map["ScapTS"] = Generators.finder(ts, mean)
        mesh_map["ScapGlen"] = Generators.finder(glen, mean)
        return mesh_map


class ShapeModel:
    def __init__(self, pc: str = None, mean_mesh=None, maps=None):
        if pc is not None and pc.endswith(".pc.npz"):
            s = np.load(pc, encoding='bytes', allow_pickle=True)
        elif pc is not None and pc.endswith("joblib"):
            n = joblib.load(pc)
            s = {
                'mean': n.mean_,
                'weights': n.explained_variance_,
                'modes': n.components_.T,
                'SD': None,
                'projectedWeights': None
            }
        else:
            s = np.load("../articulating_ssm_gui/maps/00_0000_HumerusScapula_Both.pc.npz", encoding='bytes', allow_pickle=True)
        self.mean = s['mean']
        self.weights = s['weights']  # PC weights are variance
        self.modes = s['modes']
        self.SD = s['SD']
        self.projectedWeights = s['projectedWeights']
        self.dimensions = 3
        if mean_mesh is None:
            self.mean_mesh = ModelData.mesh_load("../articulating_ssm_gui/meshes/00_0000_HumerusScapula_Both_mean.stl")
        else:
            self.mean_mesh = ModelData.mesh_load(mean_mesh, opac=0.7)
            self.inner_mesh = ModelData.mesh_load(mean_mesh, color=[155 / 255, 200 / 255, 100 / 255], opac=0.8)

        st = milli()
        self.mesh_color = [144 / 255, 207 / 255, 252 / 255]
        self.mean_mesh_data = {
            Keywords.actor: self.mean_mesh[Keywords.actor],
            Keywords.polydata: self.mean_mesh[Keywords.polydata],
            Keywords.vertices: np.asarray([self.mean_mesh[Keywords.polydata].GetPoint(i) for i in range(self.mean_mesh[Keywords.polydata].GetNumberOfPoints())])
        }
        self.inner_mesh_data = {
            Keywords.actor: self.inner_mesh[Keywords.actor],
            Keywords.polydata: self.inner_mesh[Keywords.polydata],
            Keywords.vertices: np.asarray([self.inner_mesh[Keywords.polydata].GetPoint(i) for i in
                                           range(self.inner_mesh[Keywords.polydata].GetNumberOfPoints())])
        }
        ssm = np.reshape(self.mean, (-1, self.dimensions))
        while self.mean_mesh_data[Keywords.vertices].shape[0] != ssm.shape[0]:
            self.dimensions += 1
            ssm = np.reshape(self.mean, (-1, self.dimensions))
        en = milli()
        print("time to for mapping pcs: {0} msec".format(en - st))
        print()

    def reconstruct_diff(self, pc, sd=0, part=None, debug=False, add_mean=False):
        if part is None:
            return None
        w = sd * np.sqrt(self.weights[pc])
        m = w * self.modes[:, pc]
        me = m
        if add_mean:
            me = m + self.mean
        mesh = np.reshape(me, [int(self.mean.shape[0] / 3), 3])
        export = mesh[part["idm"].to_list(), :]
        if debug:
            if not os.path.exists("./meshes/temp/"):
                os.makedirs("./meshes/temp/")
            ms = pd.DataFrame(data=export, columns=["x", "y", "z"])
            ms.to_csv("./default_meshes/temp/part_cloud.csv", index=False)

        return export

    def reconstruct_diff_all(self, sd, add_mean=False):
        sdo = np.array([0.0 for i in range(len(self.weights))])
        sdo[:len(sd)] = sd
        w = np.atleast_2d(sdo * np.sqrt(self.weights))
        m = np.dot(w, self.modes.T)
        me = m.T
        if add_mean:
            me = m + self.mean
        mesh = np.reshape(me, [int(self.mean.shape[0] / self.dimensions), self.dimensions])
        return mesh

    def create_part(self, color, force_build, mean_mesh, part_map):
        mesh = np.reshape(self.mean, [int(self.mean.shape[0] / 3), 3])
        part = mesh[part_map, :]
        new_shape = None
        if force_build or not os.path.exists(mean_mesh):
            new_shape = self.extract_parts(part_map)
            # Write the mesh to file "*.ply"
            w = vtk.vtkPLYWriter()
            w.SetInputData(new_shape[0])
            w.SetFileName(mean_mesh)
            w.Write()
        if force_build:
            polydata = new_shape[0]
        else:
            reader = vtk.vtkPLYReader()
            reader.SetFileName(mean_mesh)
            reader.Update()
            polydata = reader.GetOutput()
        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            # mapper.SetInput(reader.GetOutput())
            mapper.SetInput(polydata)
        else:
            mapper.SetInputData(polydata)
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor(color[0], color[1], color[2])
        return {Keywords.actor: actor, Keywords.polydata: polydata, Keywords.vertices: part}

    @staticmethod
    def clean_poly_data(polydata):

        # Create a vtkCleanPolyData filter
        clean_filter = vtk.vtkCleanPolyData()
        clean_filter.SetInputData(polydata)

        # Update the filter to remove duplicate points
        clean_filter.Update()

        # Get the cleaned polydata output
        cleaned_polydata = clean_filter.GetOutput()

        return cleaned_polydata

    def extract_parts(self, node_id_list, export_vert=True):
        mesh_data_copy = vtk.vtkPolyData()
        mesh_org = self.mean_mesh['polydata']
        mesh_data_copy.DeepCopy(mesh_org)
        cell_data = mesh_data_copy.GetPolys()
        vertices = None
        if export_vert:
            # Get the point data and cell data of the mesh
            points_l = []
            for i in range(len(node_id_list)):
                p = mesh_data_copy.GetPoint(node_id_list[i])
                points_l.append(p)
            vertices = np.array(points_l)

        for i in range(cell_data.GetNumberOfCells()):
            cell = mesh_data_copy.GetCell(i)
            p1 = cell.GetPointId(0)
            p2 = cell.GetPointId(1)
            p3 = cell.GetPointId(2)
            if not (p1 in node_id_list and p2 in node_id_list and p3 in node_id_list):
                mesh_data_copy.DeleteCell(i)
        mesh_data_copy.RemoveDeletedCells()

        return [mesh_data_copy, vertices]


class ModelData:

    def __init__(self):
        self.glenoid = pd.read_csv("../articulating_ssm_gui/maps/ScapGlen_map_to_mean.csv")
        self.AA = pd.read_csv("../articulating_ssm_gui/maps/ScapAA_map_to_mean.csv")
        self.AI = pd.read_csv("../articulating_ssm_gui/maps/ScapAI_map_to_mean.csv")
        self.TS = pd.read_csv("../articulating_ssm_gui/maps/ScapTS_map_to_mean.csv")
        self.humeral_head = pd.read_csv("../articulating_ssm_gui/maps/Humeral_Head_map_to_mean.csv")
        self.EL = pd.read_csv("../articulating_ssm_gui/maps/HumEL_map_to_mean.csv")
        self.EM = pd.read_csv("../articulating_ssm_gui/maps/HumEM_map_to_mean.csv")
        self.scapula = pd.read_csv("../articulating_ssm_gui/maps/Scapula_map_to_mean.csv")
        self.humerus = pd.read_csv("../articulating_ssm_gui/maps/Humerus_map_to_mean.csv")

        self.current_scapula = None
        self.current_humerus = None
        self.current_humerus_rc = None
        self.__current_mesh = None
        self.ref_ord = None

    @property
    def current_mesh(self):
        return self.__current_mesh

    @current_mesh.setter
    def current_mesh(self, x):
        if len(x.shape) == 1:
            self.__current_mesh = np.reshape(x, [int(x.shape[0] / 3), 3])
        else:
            self.__current_mesh = x

    @staticmethod
    def mesh_load(file_name, color=None, opac=1):
        if color is None:
            color = [0.5, 0.5, 1.0]
        reader = None
        if file_name.endswith(".stl"):
            reader = vtk.vtkSTLReader()
        elif file_name.endswith(".ply"):
            reader = vtk.vtkPLYReader()
        else:
            return None
        reader.SetFileName(file_name)
        reader.Update()
        polydata = reader.GetOutput()
        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            # mapper.SetInput(reader.GetOutput())
            mapper.SetInput(polydata)
        else:
            mapper.SetInputData(polydata)
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor(color[0], color[1], color[2])
        actor.GetProperty().SetOpacity(opac)
        return {Keywords.actor: actor, Keywords.polydata: polydata}

    def calc_translation(self):
        idm_scap = self.glenoid['idm'].to_list()
        rc_scap = ModelData.sphere_fit(self.current_scapula[idm_scap])
        idm_hum = self.humeral_head['idm'].to_list()
        rc_hum = ModelData.sphere_fit(self.current_humerus[idm_hum])
        return np.squeeze(rc_scap - rc_hum)

    def calc_rotation(self):
        hdata = self.humerus_corr()[0]
        sdata = self.scapula_corr()[0]
        m = ModelData.transformation_from_svd(hdata.T, sdata.T)
        return m

    def scapula_corr(self):
        """
        This is the ISB recommend coordinate system for the scapula

            Wu, G., van der Helm, F. C., Veeger, H. E., Makhsous, M., Van Roy, P., Anglin, C., Nagels, J.,
            Karduna, A. R., McQuade, K., Wang, X., Werner, F. W., Buchholz, B.,
            & International Society of Biomechanics (2005).
            ISB recommendation on definitions of joint coordinate systems of various joints for the reporting of human
            joint motion--Part II: shoulder, elbow, wrist and hand. Journal of biomechanics, 38(5), 981–992.
            https://doi.org/10.1016/j.jbiomech.2004.05.042

        :return: a matrix containing points for origin,x,y,z
        """

        aa_point = np.mean(self.current_scapula[self.AA['idm'].to_list()], axis=0)
        ai_point = np.mean(self.current_scapula[self.AI['idm'].to_list()], axis=0)
        ts_point = np.mean(self.current_scapula[self.TS['idm'].to_list()], axis=0)
        z_raw = aa_point - ts_point
        z = (1 / np.linalg.norm(z_raw)) * z_raw
        yx1_raw = ts_point - aa_point
        yx2_raw = ai_point - aa_point
        x_raw = np.cross(yx2_raw, yx1_raw)
        x = (1 / np.linalg.norm(x_raw)) * x_raw
        y = np.cross(z, x)
        ret = np.array([[0, 0, 0], 50 * x, 100 * y, 150 * z])
        without_magnitude = np.array([[0, 0, 0], x, y, z])
        out = pd.DataFrame(data=without_magnitude, columns=["x", "y", "z"])
        self.ref_ord = without_magnitude
        return [ret, out]

    def humerus_corr(self):
        """
        This is the ISB recommend coordinate system for the humerus.
        It uses points Lateral Epicondyle

            Wu, G., van der Helm, F. C., Veeger, H. E., Makhsous, M., Van Roy, P., Anglin, C., Nagels, J.,
            Karduna, A. R., McQuade, K., Wang, X., Werner, F. W., Buchholz, B.,
            & International Society of Biomechanics (2005).
            ISB recommendation on definitions of joint coordinate systems of various joints for the reporting of human
            joint motion--Part II: shoulder, elbow, wrist and hand. Journal of biomechanics, 38(5), 981–992.
            https://doi.org/10.1016/j.jbiomech.2004.05.042

        :return: a matrix containing points for origin,x,y,z
        """
        rc = np.squeeze(ModelData.sphere_fit(self.current_humerus[self.humeral_head['idm'].to_list()]))
        self.current_humerus_rc = rc
        el_point = np.nanmean(self.current_humerus[self.EL['idm'].to_list()], axis=0)
        em_point = np.nanmean(self.current_humerus[self.EM['idm'].to_list()], axis=0)
        mid_ep = 0.5 * (em_point + el_point)
        y_raw = rc - mid_ep
        y = (1 / np.linalg.norm(y_raw)) * y_raw
        X0 = em_point - rc
        X1 = el_point - rc
        x_raw = np.cross(X1, X0)
        x = (1 / np.linalg.norm(x_raw)) * x_raw
        z = np.cross(x, y)
        ret = np.array([[0, 0, 0], 50 * x, 100 * y, 150 * z])
        without_magnitude = np.array([[0, 0, 0], x, y, z])
        out = pd.DataFrame(data=without_magnitude, columns=["x", "y", "z"])
        return [ret, out]

    @staticmethod
    def vtkpoints_to_numpy(mesh):
        mesh_points = mesh[Keywords.polydata].GetPoints()
        mean_scap = np.zeros([mesh_points.GetNumberOfPoints(), 3])
        for i in range(0, mesh_points.GetNumberOfPoints()):
            s0 = mesh_points.GetPoint(i)
            mean_scap[i, :] = [s0[0], s0[1], s0[2]]
        return mean_scap

    @staticmethod
    def sphere_fit(points):
        """

        :param points: ndarray that is n x 3
        :return: center of the points
        """
        p_mean = np.nanmean(points, axis=0)
        n = points.shape[0]
        a = np.eye(3)
        for i in range(0, 3):
            a[i, 0] = np.nansum([(points[x, i] * (points[x, 0] - p_mean[0])) / n for x in range(0, n)])
            a[i, 1] = np.nansum([(points[x, i] * (points[x, 1] - p_mean[1])) / n for x in range(0, n)])
            a[i, 2] = np.nansum([(points[x, i] * (points[x, 2] - p_mean[2])) / n for x in range(0, n)])
        a: np.ndarray = 2 * a
        b: np.ndarray = np.atleast_2d([[0.0], [0.0], [0.0]])
        xc = np.array([points[x, 0] ** 2 for x in range(0, n)])
        yc = np.array([points[x, 1] ** 2 for x in range(0, n)])
        zc = np.array([points[x, 2] ** 2 for x in range(0, n)])
        sum_axis = xc + yc + zc
        xb = sum_axis * (np.transpose(points[:, 0]) - p_mean[0]) / n
        yb = sum_axis * (np.transpose(points[:, 1]) - p_mean[1]) / n
        zb = sum_axis * (np.transpose(points[:, 2]) - p_mean[2]) / n

        b[0, 0] = np.sum(xb)
        b[1, 0] = np.sum(yb)
        b[2, 0] = np.sum(zb)
        c = np.matmul(np.linalg.inv(np.matmul(a.transpose(), a)), np.matmul(a.transpose(), b))
        return np.squeeze(c)

    @staticmethod
    def transformation_from_svd(source: np.ndarray,
                                target: np.ndarray,
                                weights: Union[np.ndarray, Iterable] = None,
                                rowpoints: bool = False,
                                result_as_4x4: bool = True):
        """
        Compute the rigid body transformation (rotation and translation) to fit the
        source points to the target points, so that: target = R * source + t
        """
        # Transpose input array if points were given as rows.
        if rowpoints:
            src = source.T
            tar = target.T
        else:
            src = source
            tar = target

        # Check array shape. Given N points, it *must* have the shape 3 x N.
        assert src.shape[0] == 3 and tar.shape[0] == 3
        assert src.shape[1] == tar.shape[1]

        # Check weights.
        n_points = src.shape[1]
        if weights is None:
            w = np.identity(n_points)
        else:
            w = np.diag(weights)
            assert w.shape[0] == w.shape[1] == n_points

        # Compute weighted centroids of both point sets.
        src_mean = (np.matmul(src, w).sum(axis=1) / w.sum()).reshape([3, 1])
        tar_mean = (np.matmul(tar, w).sum(axis=1) / w.sum()).reshape([3, 1])

        # Compute "centralised" points.
        src_c = src - src_mean
        tar_c = tar - tar_mean

        # Compute covariance matrix C = X * W * Y.T
        cov = np.matmul(np.matmul(src_c, w), tar_c.T)

        # Compute singular value decomposition C = U * S * V.T (V.T == vt)
        u, s, vt = np.linalg.svd(cov)

        # Compute rotation matrix R = V * U.T
        r = np.matmul(vt.T, u.T)

        # Compute translation.
        t = tar_mean - np.matmul(r, src_mean)

        if result_as_4x4:
            return np.row_stack([np.column_stack([r, t]), (0, 0, 0, 1)])
        else:
            return r, t

    @staticmethod
    def pc_pre_process_n(scapula: np.ndarray, scapula_pc: np.ndarray, no_slices=20):
        gradent = (1 / no_slices) * (scapula_pc - scapula)
        slices = [t * gradent for t in range(0, no_slices + 1)]
        return slices


class PCWidget(QWidget):
    def __init__(self, root, idx=0):
        super().__init__()
        self.root = root
        self.name = 'PC {0}'.format(idx + 1)
        self.pcx = idx
        self.pc_scap = QLabel('{0}:\t 0.00'.format(self.name))
        self.sp_pc1_scap = QSlider(Qt.Horizontal)
        self.sp_pc1_scap.setMinimum(-self.root.step)
        self.sp_pc1_scap.setMaximum(self.root.step)
        self.sp_pc1_scap.setValue(0)

        def update_gui_element(event):
            self.pc_scap.setText("{0}:\t {1:0.2f}".format(self.name, event / (self.root.step / 2)))
            self.root.pcs[self.pcx] = event
            self.root.update_model()
            self.root.vtk_widget.update()

        self.sp_pc1_scap.valueChanged.connect(update_gui_element)

    def reset(self):
        self.sp_pc1_scap.setValue(0)

    def add_to_parent_layout(self, layout):
        layout.addWidget(self.pc_scap)
        layout.addWidget(self.sp_pc1_scap)


class VisualControlWidget(QWidget):

    def __init__(self, root, bones_dic={}):
        super().__init__()
        self.root = root
        self.widget_list = []
        self.left_side = QVBoxLayout()
        self.bone_map = bones_dic
        self.setup_check_box()
        self.reset_button = QPushButton(self)

        self.axes_l = QCheckBox('Show Global Axes')
        self.axes_l.setChecked(True)

        self.add_to_pane()

        def axe_vis():
            if self.axes_l.isChecked():
                self.root.axes.VisibilityOn()
            else:
                self.root.axes.VisibilityOff()
            self.root.vtk_widget.update()

        self.axes_l.stateChanged.connect(axe_vis)

        def reset_pressed():
            for x in self.bone_map:
                self.bone_map[x]["QCheckBox"].setChecked(True)
            self.axes_l.setChecked(True)

            print("Reset")

        self.reset_button.clicked.connect(reset_pressed)

    def add_to_pane(self):
        title = QLabel('GUI Elements:')
        space = QLabel(' ')
        bones = QLabel('Bones:')
        space2 = QLabel(' ')
        visual = QLabel('Visual Elements:')

        if len(self.widget_list) > 0:
            for c in self.widget_list:
                self.left_side.removeWidget(c)
            self.widget_list.clear()
            self.left_side.addStretch(-5)
            self.left_side.setSpacing(0)

        self.widget_list.append(title)
        self.left_side.addWidget(title)

        self.left_side.addWidget(space)
        self.widget_list.append(space)

        self.left_side.addWidget(bones)
        self.widget_list.append(bones)

        for d in self.bone_map:
            self.left_side.addWidget(self.bone_map[d]["QCheckBox"])
            self.widget_list.append(self.bone_map[d]["QCheckBox"])
        self.left_side.addWidget(space2)
        self.widget_list.append(space2)

        self.left_side.addWidget(visual)
        self.widget_list.append(visual)

        self.left_side.addWidget(self.axes_l)
        self.widget_list.append(self.axes_l)

        self.left_side.addStretch(5)
        self.left_side.setSpacing(2)

        spacer = QLabel(' ')
        self.left_side.addWidget(spacer)
        self.widget_list.append(spacer)

        self.reset_button.setText("Reset")
        self.left_side.addWidget(self.reset_button)
        self.widget_list.append(self.reset_button)
        self.setLayout(self.left_side)

    def setup_check_box(self):
        for d in self.bone_map:
            self.bone_map[d]["QCheckBox"] = QCheckBox(self.bone_map[d]["Name"])
            self.bone_map[d]["QCheckBox"].setChecked(True)

            def bone_vis():
                if self.bone_map[d]["QCheckBox"].isChecked():
                    self.root.mesh[Keywords.actor].VisibilityOn()
                else:
                    self.root.mesh[Keywords.actor].VisibilityOff()
                self.root.vtk_widget.update()

            self.bone_map[d]["QCheckBox"].stateChanged.connect(bone_vis)


class ControlWidget(QWidget):

    def __init__(self, root, num_pcs):
        super().__init__()
        self.root = root
        self.num_pcs = num_pcs
        title = QLabel('Principal Components:')
        gh = QLabel('GH Joint:')
        space = QLabel(' ')
        space01 = QLabel(' ')
        space02 = QLabel(' ')

        self.pcs_controls = [PCWidget(self.root, idx=i) for i in range(0, self.num_pcs)]

        x_hum = QLabel('Abduction/Adduction:\t 0°')
        sp_pc1_hum = QSlider(Qt.Horizontal)
        sp_pc1_hum.setMinimum(-60)
        sp_pc1_hum.setMaximum(21)
        sp_pc1_hum.setValue(0)

        z_hum = QLabel('Extension/Flexion:    \t 0°')
        sp_pc2_hum = QSlider(Qt.Horizontal)
        sp_pc2_hum.setMinimum(-21)
        sp_pc2_hum.setMaximum(60)
        sp_pc2_hum.setValue(0)

        y_hum = QLabel('External/ Internal Rotation\t 0°')
        sp_pc3_hum = QSlider(Qt.Horizontal)
        sp_pc3_hum.setMinimum(-30)
        sp_pc3_hum.setMaximum(30)
        sp_pc3_hum.setValue(0)

        self.left_side = QVBoxLayout()
        self.left_side.addWidget(title)
        self.left_side.addWidget(space)
        for pc in self.pcs_controls:
            pc.add_to_parent_layout(self.left_side)
        self.left_side.addWidget(space01)
        # self.left_side.addWidget(gh)
        # self.left_side.addWidget(x_hum)
        # self.left_side.addWidget(sp_pc1_hum)
        # self.left_side.addWidget(z_hum)
        # self.left_side.addWidget(sp_pc2_hum)
        # self.left_side.addWidget(y_hum)
        # self.left_side.addWidget(sp_pc3_hum)
        # self.left_side.addWidget(space02)

        self.left_side.addStretch(5)
        self.left_side.setSpacing(2)
        self.left_side.addWidget(QLabel(' '))
        self.reset_button = QPushButton(self)
        self.reset_button.setText("Reset")
        self.left_side.addWidget(self.reset_button)

        self.setLayout(self.left_side)

        def reset_pressed():
            # self.root.humerus[Keywords.actor].VisibilityOn()
            # self.root.scapula[Keywords.actor].VisibilityOn()
            # self.root.axes.VisibilityOn()
            # sp_pc1_hum.setValue(0)
            # sp_pc2_hum.setValue(0)
            # sp_pc3_hum.setValue(0)
            for p in self.pcs_controls:
                p.reset()
            print("Reset")

        self.reset_button.clicked.connect(reset_pressed)

        def value_change_ac1(event):
            angle = int((180 / 60) * event)
            self.root.humerus_angles[2] = angle
            x_hum.setText("Abduction/Adduction:\t {0}°".format(angle))
            self.root.update_model()
            self.root.vtk_widget.update()

        sp_pc1_hum.valueChanged.connect(value_change_ac1)

        def value_change_ac2(event):
            angle = int((180 / 60) * event)
            self.root.humerus_angles[1] = angle
            z_hum.setText("Extension/ Flexion:   \t {0}°".format(angle))
            self.root.update_model()
            self.root.vtk_widget.update()

        sp_pc2_hum.valueChanged.connect(value_change_ac2)

        def value_change_ac3(event):
            angle = int((180 / 60) * event)
            self.root.humerus_angles[0] = angle
            y_hum.setText("External/ Internal Rotation:\t {0}°".format(angle))
            self.root.update_model()
            self.root.vtk_widget.update()

        sp_pc3_hum.valueChanged.connect(value_change_ac3)


class MyMainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.mean_tib = None
        self.tib = None
        self.num_of_pc = 9
        self.mesh = None
        self.mean_mesh = None
        self.inner_mesh = None
        self._scale_factors = {"L1": [0.975, 0.6, 0.6],
                               "L2": [1.0, 1.6, 1.6],
                               "min_thickness": 1.0 # dicom resolution
                               }
        mean_mesh = "./meshes/shape_model_mean.ply"
        mean_mesh = "C:\\Users\\tyeu008\\Documents\\Repos\\bluepython\\gui_src\\blue_ui\\shape\\articulating_ssm_gui\\meshes\\mean_mesh.ply"
        #self.template = ShapeModel(pc="./maps/shape_model.pc.npz", mean_mesh=mean_mesh)
        self.template = ShapeModel(pc="C:\\Users\\tyeu008\\Documents\\Repos\\bluepython\\gui_src\\blue_ui\\shape\\articulating_ssm_gui\\maps\\pca_model.joblib", mean_mesh=mean_mesh)

        self.step = 100
        self.max_sd = 2
        self.model = ModelData()
        self.setGeometry(100, 100, 1000, 700)
        self.setWindowTitle("Shape Model Viewer")
        self.ratio = 200 / 720
        self.w = self.ratio * self.geometry().width()
        self.left_widget = ControlWidget(self, self.num_of_pc)
        self.visualwidget = VisualControlWidget(self, bones_dic={"tibiaR": {"Name": mean_mesh.split('/')[-1]}})

        self.left_widget.setMaximumWidth(int(self.w))
        self.visualwidget.setMaximumWidth(int(self.w))
        self.visualwidget.setVisible(False)
        self.selected = []
        self.balls = []
        self.control_pressed = False
        self.shift_pressed = False

        rgb = '63, 68, 69'

        toolbar = QToolBar("Menu toolbar")
        toolbar.setStyleSheet("background-color: rgb({0});".format(rgb))
        toolbar.setIconSize(QSize(32, 32))
        toolbar.setMovable(False)
        self.addToolBar(toolbar)

        self.control_button = QAction(QIcon("../articulating_ssm_gui/icons/control panel ON.ico"), "Control", self)
        self.control_button.setStatusTip("PCS - Control Panel")
        self.control_button.triggered.connect(self.control_button_action)
        self.control_button.setCheckable(True)
        self.control_button.setChecked(True)

        self.open_lib = QAction(QIcon("../articulating_ssm_gui/icons/Libraries.ico"), "Open", self)
        self.open_lib.setStatusTip("Open File")
        self.open_lib.triggered.connect(self.open_file)
        toolbar.addAction(self.open_lib)

        self.appearance_button = QAction(QIcon("../articulating_ssm_gui/icons/Appearance Off.ico"), "Visuals", self)
        self.appearance_button.setStatusTip("Visuals Setting")
        self.appearance_button.triggered.connect(self.appearance_button_action)
        self.appearance_button.setCheckable(True)

        self.info_button = QAction(QIcon("../articulating_ssm_gui/icons/info.png"), "Info", self)
        self.info_button.setStatusTip("Information")
        self.info_button.triggered.connect(self.OpenInfoDialog)

        toolbar.addAction(self.control_button)
        toolbar.addAction(self.appearance_button)

        # Create a spacer widget
        spacer = QWidget()
        spacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        toolbar.addWidget(spacer)

        self.selected_mesh_names = []
        self.selected_mesh = QLabel("\t")
        font = QFont()
        font.setPointSize(12)  # Set the desired font size
        self.selected_mesh.setFont(font)
        self.selected_mesh.setStyleSheet("color: white")

        # palette = QPalette()
        # palette.setColor(QPalette.WindowText, QColor(Qt.white))  # Set the desired text color
        # selected_mesh.setPalette(palette)

        toolbar.addWidget(self.selected_mesh)
        toolbar.addAction(self.info_button)

        status_bar = QStatusBar(self)
        status_bar.setStyleSheet("background-color: rgb({0}); color: white".format(rgb))
        self.setStatusBar(status_bar)

        self.pcs = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.humerus_angles = [0, 0, 0]  # y,z,x

        self.setup_meshes()

        # setup axes
        self.axes = vtk.vtkAxesActor()
        self.axes.SetTotalLength(100, 100, 100)
        self.axes.SetNormalizedTipLength(0.2, 0.2, 0.2)

        self.txt = vtk.vtkTextActor()
        self.txt.SetDisplayPosition(20, 20)
        self.__key_mods__ = []
        self.__other_keys__ = []
        txtprop = self.txt.GetTextProperty()
        txtprop.SetFontSize(18)
        txtprop.SetColor(0, 0, 0)

        # Create a renderer
        self.ren = vtk.vtkRenderer()
        self.ren.AddActor(self.mesh[Keywords.actor])
        self.ren.AddActor(self.inner_mesh[Keywords.actor])
        self.ren.AddActor(self.axes)
        self.ren.AddActor(self.txt)

        # ren.SetBackground(0.1, 0.1, 0.1)
        self.ren.SetBackground(0.85, 0.85, 0.95)

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        self.picker = vtk.vtkPropPicker()

        # Create a RenderWindowInteractor to permit manipulating the camera
        self.style_interactor = vtk.vtkInteractorStyleTrackballCamera()
        self.vtk_widget = QVTKRenderWindowInteractor(parent=self.central_widget)
        self.vtk_widget.SetInteractorStyle(self.style_interactor)
        # self.vtk_widget.AddObserver("RightButtonPressEvent", self.right_mouse_clicked)
        self.vtk_widget.AddObserver("LeftButtonPressEvent", self.left_mouse_clicked)
        self.vtk_widget.AddObserver("KeyPressEvent", self.keypressed)
        self.vtk_widget.AddObserver("KeyReleaseEvent", self.keyrelease)
        self.vtk_widget.Initialize()
        self.vtk_widget.Start()

        self.h_layout = QHBoxLayout()
        self.h_layout.addWidget(self.left_widget)
        self.h_layout.addWidget(self.visualwidget)
        self.h_layout.addWidget(self.vtk_widget)

        self.central_widget.setLayout(self.h_layout)

        self.show()

        # Connect the VTK render window to the PyQt widget
        ren_win = self.vtk_widget.GetRenderWindow()
        ren_win.AddRenderer(self.ren)
        self.vtk_widget.SetRenderWindow(ren_win)
        self.vtk_widget.update()

    def control_button_action(self, s):
        if s:
            self.left_widget.setVisible(True)
            self.control_button.setIcon(QIcon("../articulating_ssm_gui/icons/control panel ON.ico"))
            self.appearance_button.setChecked(False)
            self.appearance_button_action(False)
        else:
            self.control_button.setIcon(QIcon("../articulating_ssm_gui/icons/Control Panel Off.ico"))
            self.left_widget.setVisible(False)
        self.central_widget.update()
        print("click", s)

    def appearance_button_action(self, s):
        if s:
            self.visualwidget.setVisible(True)
            self.appearance_button.setIcon(QIcon("../articulating_ssm_gui/icons/Appearance.ico"))
            self.control_button.setChecked(False)
            self.control_button_action(False)
        else:
            self.appearance_button.setIcon(QIcon("../articulating_ssm_gui/icons/Appearance Off.ico"))
            self.visualwidget.setVisible(False)
        print("click", s)

    def OpenInfoDialog(self, s):
        print("open")

    def open_file(self, s):
        file_filter = 'Config (*.json);; All File (*.*)'
        self.open_lib.setIcon(QIcon("../articulating_ssm_gui/icons/Libraries On.ico"))
        response = QFileDialog.getOpenFileName(
            parent=self,
            caption='Select a Config file',
            directory=os.getcwd(),
            filter=file_filter,
            initialFilter='Config (*.json)'
        )
        print("open", response)
        file = response[0]
        if len(file) != 0:
            with open(file, 'r') as infile:
                data = json.load(infile)
                mean_mesh = data["mesh"]
                self.ren.RemoveActor(self.template.mean_mesh[Keywords.actor])
                if self.template.inner_mesh is not None:
                    self.ren.RemoveActor(self.template.inner_mesh[Keywords.actor])
                self.template = ShapeModel(pc=data["pc"], mean_mesh=mean_mesh)
                self.visualwidget.bone_map["tibiaR"]["Name"] =  mean_mesh.split('/')[-1]
                self.visualwidget.bone_map["tibiaR"]["QCheckBox"].setText(mean_mesh.split('/')[-1])
                # self.visualwidget.setup_check_box()
                # self.visualwidget.add_to_pane()
                self.setup_meshes()
                self.ren.AddActor(self.template.mean_mesh[Keywords.actor])
                self.update_model()
        self.open_lib.setIcon(QIcon("../articulating_ssm_gui/icons/Libraries.ico"))

    def set_wire_frame_on(self, bone):
        if bone == 0:
            self.scapula[Keywords.actor].GetProperty().SetRepresentationToWireframe()
            self.scapula[Keywords.actor].GetProperty().EdgeVisibilityOn()
            self.scapula[Keywords.actor].GetProperty().SetEdgeColor(0, 0, 0)
        elif bone == 1:
            self.humerus[Keywords.actor].GetProperty().SetRepresentationToWireframe()
            self.humerus[Keywords.actor].GetProperty().EdgeVisibilityOn()
            self.humerus[Keywords.actor].GetProperty().SetEdgeColor(0, 0, 0)

    def set_wire_frame_off(self, bone):
        if bone == 0:
            self.scapula[Keywords.actor].GetProperty().SetRepresentationToSurface()
            self.scapula[Keywords.actor].GetProperty().EdgeVisibilityOff()
            # self.scapula[Keywords.actor].GetProperty().SetEdgeColor(0, 0, 0)
        elif bone == 1:
            self.humerus[Keywords.actor].GetProperty().SetRepresentationToSurface()
            self.humerus[Keywords.actor].GetProperty().EdgeVisibilityOff()
            # self.humerus[Keywords.actor].GetProperty().SetEdgeColor(0, 0, 0)

    def keypressed(self, iren, event):
        key = iren.GetKeySym()
        print("Pressed " + key)

        if key == 'Control_L' or key == 'Control_R':
            self.control_pressed = True
            self.__key_mods__.append('Control')

        elif key == 'Shift_L' or key == 'Shift_R':
            self.shift_pressed = True
            self.__key_mods__.append("Shift")
        else:
            if key not in self.__other_keys__:
                self.__other_keys__.append(key)
                print(self.__other_keys__)
        str_list = ""
        if len(self.__key_mods__) > 0:
            for i in self.__key_mods__:
                str_list += i + " + "
            str_list = str_list[:-3]
        if len(self.__other_keys__) > 0:
            str_list += " + "
            for i in self.__other_keys__:
                str_list += i + " + "
            str_list = str_list[:-3]
        self.txt.SetInput(str_list)
        self.vtk_widget.update()

    def keyrelease(self, iren, event):
        key = iren.GetKeySym()
        print("Released " + key)

        if key == 'Control_L' or key == 'Control_R':
            self.control_pressed = False
            self.__key_mods__.remove("Control")

        elif key == 'Shift_L' or key == 'Shift_R':
            self.shift_pressed = False
            self.__key_mods__.remove("Shift")
        else:
            if key in self.__other_keys__:
                self.__other_keys__.remove(key)
        str_list = ""
        if len(self.__key_mods__) > 0:
            for i in self.__key_mods__:
                str_list += i + " + "
            str_list = str_list[:-3]
        if len(self.__other_keys__) > 0:
            str_list += " + "
            for i in self.__other_keys__:
                str_list += i + " + "
            str_list = str_list[:-3]
        self.txt.SetInput(str_list)
        self.vtk_widget.update()

    def right_mouse_clicked(self, iren, event):
        x, y = iren.GetEventPosition()
        print("Right button pressed at ({0}, {1})".format(x, y))
        self.picker.PickProp(x, y, self.ren)
        picked = self.picker.GetProp3D()
        scolor = self.template.mesh_color

        if picked:
            bone = self.picker.GetActor()
            bone.GetProperty().SetColor(50 / 255, 200 / 255, 128 / 255)
            if picked in self.balls:
                bone.GetProperty().SetColor(15 / 255, 200 / 255, 100 / 255)

            if picked in self.balls and self.control_pressed:
                self.ren.RemoveActor(picked)
                self.balls.remove(picked)
            elif self.mesh[Keywords.actor] == bone and bone not in self.selected:
                self.selected.append(bone)
                self.selected_mesh_names.append("mesh")
                if not self.shift_pressed:
                    self.selected_mesh_names = ["mesh"]
                    self.selected = [bone]
                    self.mesh[Keywords.actor].GetProperty().SetColor(scolor[0], scolor[1], scolor[2])
            the_word = ""
            for m in self.selected_mesh_names:
                the_word += m + " and "
            the_word = the_word[:-5] + "   "
            self.selected_mesh.setText(the_word)
        else:
            self.selected_mesh.setText("\t")
            self.selected_mesh_names = []
            self.selected = []
            self.mesh[Keywords.actor].GetProperty().SetColor(scolor[0], scolor[1], scolor[2])
        print(len(self.selected))

    def left_mouse_clicked(self, iren, event):
        x, y = iren.GetEventPosition()
        print("left button pressed at ({0}, {1})".format(x, y))
        self.picker.PickProp(x, y, self.ren)
        picked = self.picker.GetActor()
        if picked and picked in self.selected and self.control_pressed:
            picked3d = self.picker.GetPickPosition()
            source = vtk.vtkSphereSource()
            source.SetRadius(5)
            mapper = vtk.vtkPolyDataMapper()
            mapper.SetInputConnection(source.GetOutputPort())
            actor = vtk.vtkActor()
            actor.SetMapper(mapper)
            actor.GetProperty().SetColor(0, 100 / 255, 200 / 255)
            actor.SetPosition(picked3d)
            self.balls.append(actor)
            self.ren.AddActor(actor)

    def setup_meshes(self):
        st = milli()
        self.mesh = self.template.mean_mesh_data
        self.mean_mesh = ModelData.vtkpoints_to_numpy(self.mesh)
        self.model.current_mesh = self.template.mean
        self.inner_mesh = self.template.inner_mesh_data
        en1 = milli()
        print("time taken for setting up: {0} msec".format(en1 - st))
        print("Updating models in view")
        self.update_model()



    def update_model(self, update_orientation=True, update_humerus=True):
        """
        Warning: Currently optimised for thickness of the tibia

        This Method takes the values set by the PC sliders and calculate the new shape.
        Once the new shape is calculated the inner mesh is created by first calculating the projection direction using
        the shrink method then the thickness is applied.

        :param update_orientation: Not used leftover from when multiple bones are articulated
        :param update_humerus: Not used leftover from when multiple bones are displayed
        :return: N/A
        """
        st = time.time()
        mean_mesh = np.reshape(self.template.mean, [int(self.template.mean.shape[0] / self.template.dimensions), self.template.dimensions])
        cen = np.nanmean(mean_mesh, axis=0)
        mean_mesh = mean_mesh - cen
        sd_pc = np.asarray(self.pcs) / (self.step / self.max_sd)
        sl = self.template.reconstruct_diff_all(sd_pc)
        new_shape = mean_mesh + sl
        px0 = PCAModel.pca_rotation(new_shape[:, :3])

        self.model.current_mesh = copy.deepcopy(px0.transformed_data)
        vs = vtk.vtkPoints()
        vs.SetData(numpy_to_vtk(self.model.current_mesh))
        self.mesh[Keywords.actor].GetMapper().GetInput().SetPoints(vs)

        if self.template.dimensions == 4:
            scale = np.diag(self._scale_factors["L1"])
            px = np.matmul(scale, px0.transformed_data.T).T
            scale = np.diag(self._scale_factors["L2"])
            py = np.matmul(scale, px.T).T
            mx = np.max(px[:, 0])
            mn = np.min(px[:, 0])
            rg = mx - mn
            long_bone_seg = rg / 8.0
            grad = 1 / long_bone_seg
            start0 = mx - long_bone_seg
            start1 = mn + long_bone_seg
            grad0 = np.zeros(py.shape)
            for i in range(0, px.shape[0]):
                if px[i, 0] > (mx - long_bone_seg):
                    a = grad * (px[i, 0] - start0)
                    grad0[i, :] = [a, a, a]
                elif px[i, 0] < (mn - long_bone_seg):
                    b = 1-(grad * (px[i, 0] - start1))
                    grad0[i, :] = [b, b, b]
            px0 = grad0*py + (1-grad0)*px
            diff = self.model.current_mesh - px0
            d = np.atleast_2d(np.sqrt(np.sum(diff*diff, axis=1)))
            norms = ((1/d).T * diff)
            offset = np.atleast_2d(np.abs(new_shape[:, 3])).T*norms
            c = self.model.current_mesh - offset

            vs = vtk.vtkPoints()
            vs.SetData(numpy_to_vtk(c))
            self.inner_mesh[Keywords.actor].GetMapper().GetInput().SetPoints(vs)
        en = time.time()
        print(en - st)

    # def _update_model_v0_with_vtk_norm_(self, update_orientation=True, update_humerus=True):
    #         st = time.time()
    #         mean_mesh = np.reshape(self.template.mean, [int(self.template.mean.shape[0] / self.template.dimensions),
    #                                                     self.template.dimensions])
    #         cen = np.nanmean(mean_mesh, axis=0)
    #         mean_mesh = mean_mesh - cen
    #         sd_pc = np.asarray(self.pcs) / (self.step / self.max_sd)
    #         sl = self.template.reconstruct_diff_all(sd_pc)
    #         new_shape = mean_mesh + sl
    #         px0 = PCAModel.pca_rotation(new_shape[:, :3])
    #
    #         self.model.current_mesh = copy.deepcopy(px0.transformed_data)
    #         vs = vtk.vtkPoints()
    #         vs.SetData(numpy_to_vtk(self.model.current_mesh))
    #         self.mesh[Keywords.actor].GetMapper().GetInput().SetPoints(vs)
    #         if self.template.dimensions == 4:
    #             # normals = vtk.vtkPolyDataNormals()
    #             # normals.SetInputData(self.mesh[Keywords.actor].GetMapper().GetInput())
    #             # normals.ComputeCellNormalsOn()
    #             # normals.SetAutoOrientNormals(True)
    #             # normals.Update()
    #             # output = normals.GetOutput()
    #             # cellData = output.GetPointData()
    #             # _normals = cellData.GetNormals()
    #             # norms0 = np.array([_normals.GetTuple3(f) for f in range(_normals.GetNumberOfTuples()-3)])
    #
    #             scale = np.diag(self._scale_factors["L1"])
    #             px = np.matmul(scale, px0.transformed_data.T).T
    #             scale = np.diag(self._scale_factors["L2"])
    #             py = np.matmul(scale, px.T).T
    #             mx = np.max(px[:, 0])
    #             mn = np.min(px[:, 0])
    #             rg = mx - mn
    #             long_bone_seg = rg / 8.0
    #             grad = 1 / long_bone_seg
    #             start0 = mx - long_bone_seg
    #             start1 = mn + long_bone_seg
    #             grad0 = np.zeros(py.shape)
    #             for i in range(0, px.shape[0]):
    #                 if px[i, 0] > (mx - long_bone_seg):
    #                     a = grad * (px[i, 0] - start0)
    #                     grad0[i, :] = [a, a, a]
    #                 elif px[i, 0] < (mn - long_bone_seg):
    #                     b = 1 - (grad * (px[i, 0] - start1))
    #                     grad0[i, :] = [b, b, b]
    #             px0 = grad0 * py + (1 - grad0) * px
    #             diff = self.model.current_mesh - px0
    #             d = np.atleast_2d(np.sqrt(np.sum(diff * diff, axis=1)))
    #             norms = ((1 / d).T * diff)
    #             offset = np.atleast_2d(np.abs(new_shape[:, 3])).T * norms
    #             c = self.model.current_mesh - offset
    #
    #             vs = vtk.vtkPoints()
    #             vs.SetData(numpy_to_vtk(c))
    #             self.inner_mesh[Keywords.actor].GetMapper().GetInput().SetPoints(vs)
    #         en = time.time()
    #         print(en - st)
    #
    #     pass
    #
    # def _update_model_(self, update_orientation=True, update_humerus=True):
    #     r = rt.from_euler('yzx', self.humerus_angles, degrees=True)
    #     test = r.as_matrix()
    #     mean_mesh = np.reshape(self.template.mean, [int(self.template.mean.shape[0] / 3), 3])
    #     sd_pc = np.asarray(self.pcs) / (self.step / self.max_sd)
    #     sl = self.template.reconstruct_diff_all(sd_pc)
    #     new_shape = mean_mesh + sl
    #
    #     self.model.current_mesh = copy.deepcopy(new_shape)
    #     self.model.current_scapula = copy.deepcopy(new_shape)
    #     if update_humerus:
    #         self.model.current_humerus = copy.deepcopy(new_shape)
    #
    #     if update_orientation:
    #         m = self.model.scapula_corr()[1].iloc[1:, :].to_numpy()
    #         self.model.current_scapula = np.matmul(m, self.model.current_scapula.T).T
    #         aa_point = -1 * np.mean(self.model.current_scapula[self.model.AA['idm'].to_list()], axis=0)
    #         self.model.current_scapula = self.model.current_scapula + np.tile(aa_point,
    #                                                                           [self.model.current_scapula.shape[0], 1])
    #     vs = vtk.vtkPoints()
    #     vs.SetData(numpy_to_vtk(self.model.current_scapula))
    #     self.scapula[Keywords.actor].GetMapper().GetInput().SetPoints(vs)
    #
    #     if update_orientation:
    #         m = self.model.calc_rotation()
    #         temp = np.matmul(m[:3, :3], self.model.current_humerus.T)
    #         m2 = self.model.ref_ord[1:4, :]
    #         m2[0, :] = (1 / np.linalg.norm(m2[0, :])) * m2[0, :]
    #         m2[1, :] = (1 / np.linalg.norm(m2[1, :])) * m2[1, :]
    #         m2[2, :] = (1 / np.linalg.norm(m2[2, :])) * m2[2, :]
    #         temp = np.matmul(m2, temp)
    #         temp = np.matmul(test, temp)
    #         temp = np.matmul(m2.T, temp)
    #         self.model.current_humerus = temp.T
    #         t = self.model.calc_translation()
    #         t_ = np.tile(t, [self.model.current_humerus.shape[0], 1])
    #         self.model.current_humerus = self.model.current_humerus + t_
    #     if update_humerus:
    #         vh = vtk.vtkPoints()
    #         vh.SetData(numpy_to_vtk(self.model.current_humerus))
    #         self.humerus[Keywords.actor].GetMapper().GetInput().SetPoints(vh)

    def resizeEvent(self, event):
        self.left_widget.setMaximumWidth(int(self.geometry().width() * self.ratio))


if __name__ == '__main__':
    # n = joblib.load("C:\\Users\\tyeu008\\Documents\\Repos\\bluepython\\gui_src\\blue_ui\\shape\\articulating_ssm_gui\\maps\\pca_model.joblib")
    # n0 = n.mean_.reshape([int(n.mean_.size/4), 4])
    # weights = n.explained_variance_

    app = QApplication([])
    window = MyMainWindow()
    app.exec_()
