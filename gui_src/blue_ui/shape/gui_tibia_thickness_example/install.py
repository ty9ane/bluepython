import os

if __name__ == '__main__':
    dependencies = ['numpy', 'scipy', 'PyQt5', 'vtk', 'pandas', 'joblib', 'scikit-learn']
    for d in dependencies:
        os.system('python -m pip install ' + d)
