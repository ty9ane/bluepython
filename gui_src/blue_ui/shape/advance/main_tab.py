import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QWidget, QTabWidget, QVBoxLayout
from PyQt5.QtGui import QFont
from PyQt5.QtCore import pyqtSlot
from blue_ui.grid.components.ui_components_main import MainScene, MainView, MainMenuBar
from blue_ui.grid.components.ui_nodes import RectangleNodeUI
from blue_ui.shape.advance.nodes.io import ReaderNode


class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = 'Shape Model Scaling'
        self.left = 0
        self.top = 0
        self.width = 1280
        self.height = 720
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.main_widget = MyWidget(self)
        self.table_widget = MyTabWidget(self, self.main_widget)
        self.main_widget.add(self.table_widget)
        self.setCentralWidget(self.main_widget)

        self.show()


class MyWidget(QWidget):
    def __init__(self, parent):
        super(QWidget, self).__init__(parent)
        self.layout = QVBoxLayout()
        self.menu_bar = MainMenuBar()
        self.layout.setMenuBar(self.menu_bar)

        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

    def set_view(self, view):
        self.menu_bar.set_view(view)
        pass

    def add(self, widget):
        self.layout.addWidget(widget)


class Workflow:
    def __init__(self, ui_scene):
        self.nodes = {}
        self.ui_scene = ui_scene
        self.node_operation_order = []

    def add_node(self, node_name, node):
        """
        Will add node and append as next operation order
        :param node_name:
        :param node:
        :return:
        """
        try:
            a = self.nodes[node_name]
            if a is not None:
                self.nodes[node_name] = node
                return
        except KeyError:
            pass
        self.node_operation_order.append(node_name)
        if isinstance(node, RectangleNodeUI):
            self.ui_scene.addItem(node)
        else:
            x = node.ui
            self.ui_scene.addItem(x)
        self.nodes[node_name] = node


class Scaler(Workflow):
    def __init__(self, ui_scene):
        super().__init__(ui_scene)
        self.setup()

    def setup(self):
        reader_trc = ReaderNode(title="TRC Reader", filetype="trc")
        reader_trc.set_pos(-2000, -300)
        self.add_node(reader_trc.title, reader_trc)

        reader_model = ReaderNode(title="Model Reader", filetype="ply")
        reader_model.set_pos(-1800, -300)
        self.add_node(reader_model.title, reader_model)


class MyTabWidget(QWidget):

    def __init__(self, grand, parent):
        super(QWidget, self).__init__(grand)
        self.layout = QVBoxLayout(self)
        self.ui_scene = MainScene()
        self.view = MainView(self.ui_scene, self)
        self.p = parent
        self.p.set_view(self.view)
        self.workflow = Scaler(self.ui_scene)

        # Initialize tab screen
        self.tabs = QTabWidget()
        self.tab1 = QWidget()
        self.tabs.setFont(QFont('Calibri', 12))
        self.tab2 = QWidget()

        # Add tabs
        self.tabs.addTab(self.tab1, "Nodes")
        self.tabs.addTab(self.tab2, "3D View")

        # Create first tab
        self.tab1.layout = QVBoxLayout(self)
        self.pushButton1 = QPushButton("Run")
        self.tab1.layout.addWidget(self.view)
        self.tab1.layout.addWidget(self.pushButton1)

        self.tab1.setLayout(self.tab1.layout)

        # Add tabs to widget
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

    @pyqtSlot()
    def on_click(self):
        print("\n")
        for currentQTableWidgetItem in self.tableWidget.selectedItems():
            print(currentQTableWidgetItem.row(), currentQTableWidgetItem.column(), currentQTableWidgetItem.text())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())