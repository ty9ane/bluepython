from typing import Union, Iterable
from datetime import datetime
from enum import Enum
import time
import json


def milli():
    date = datetime.utcnow() - datetime(1970, 1, 1)
    seconds = (date.total_seconds())
    milliseconds = round(seconds * 1000)
    return milliseconds


class Keywords(Enum):
    actor = [0, 'actor']
    polydata = [1, 'polydata']
    vertices = [2, 'vertices']
    idx = [3, 'idx']
    idm = [4, 'idm']