import pandas as pd
import numpy as np
import vtk
from blue_ui.shape.advance.tools.util import Keywords
from typing import Union, Iterable


class Model:

    def __init__(self):
        self.__current_mesh = None
        self.ref_ord = None

    @property
    def current_mesh(self):
        return self.__current_mesh

    @current_mesh.setter
    def current_mesh(self, x):
        if len(x.shape) == 1:
            self.__current_mesh = np.reshape(x, [int(x.shape[0] / 3), 3])
        else:
            self.__current_mesh = x

    @staticmethod
    def mesh_load(file_name, color=None, opac=1):
        print("Reading mesh")
        if color is None:
            color = [0.5, 0.5, 1.0]
        reader_handle = vtk.vtkOBJReader
        if file_name.endswith(".stl"):
            reader_handle = vtk.vtkSTLReader
        elif file_name.endswith(".ply"):
            reader_handle = vtk.vtkPLYReader
        else:
            return None
        reader = reader_handle()
        reader.SetFileName(file_name)
        reader.Update()
        polydata = reader.GetOutput()
        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            # mapper.SetInput(reader.GetOutput())
            mapper.SetInput(polydata)
        else:
            mapper.SetInputData(polydata)
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor(color[0], color[1], color[2])
        actor.GetProperty().SetOpacity(opac)
        return {Keywords.actor: actor, Keywords.polydata: polydata}

    def calc_translation(self):
        return [0, 0, 0]

    def calc_rotation(self):
        return np.eye(3)

    @staticmethod
    def vtkpoints_to_numpy(mesh):
        mesh_points = mesh[Keywords.polydata].GetPoints()
        mean_ = np.zeros([mesh_points.GetNumberOfPoints(), 3])
        for i in range(0, mesh_points.GetNumberOfPoints()):
            s0 = mesh_points.GetPoint(i)
            mean_[i, :] = [s0[0], s0[1], s0[2]]
        return mean_

    @staticmethod
    def sphere_fit(points):
        """

        :param points: ndarray that is n x 3
        :return: center of the points
        """
        p_mean = np.nanmean(points, axis=0)
        n = points.shape[0]
        a = np.eye(3)
        for i in range(0, 3):
            a[i, 0] = np.nansum([(points[x, i] * (points[x, 0] - p_mean[0])) / n for x in range(0, n)])
            a[i, 1] = np.nansum([(points[x, i] * (points[x, 1] - p_mean[1])) / n for x in range(0, n)])
            a[i, 2] = np.nansum([(points[x, i] * (points[x, 2] - p_mean[2])) / n for x in range(0, n)])
        a: np.ndarray = 2 * a
        b: np.ndarray = np.atleast_2d([[0.0], [0.0], [0.0]])
        xc = np.array([points[x, 0] ** 2 for x in range(0, n)])
        yc = np.array([points[x, 1] ** 2 for x in range(0, n)])
        zc = np.array([points[x, 2] ** 2 for x in range(0, n)])
        sum_axis = xc + yc + zc
        xb = sum_axis * (np.transpose(points[:, 0]) - p_mean[0]) / n
        yb = sum_axis * (np.transpose(points[:, 1]) - p_mean[1]) / n
        zb = sum_axis * (np.transpose(points[:, 2]) - p_mean[2]) / n

        b[0, 0] = np.sum(xb)
        b[1, 0] = np.sum(yb)
        b[2, 0] = np.sum(zb)
        c = np.matmul(np.linalg.inv(np.matmul(a.transpose(), a)), np.matmul(a.transpose(), b))
        return np.squeeze(c)

    @staticmethod
    def transformation_from_svd(source: np.ndarray,
                                target: np.ndarray,
                                weights: Union[np.ndarray, Iterable] = None,
                                rowpoints: bool = False,
                                result_as_4x4: bool = True):
        """
        Compute the rigid body transformation (rotation and translation) to fit the
        source points to the target points, so that: target = R * source + t
        """
        # Transpose input array if points were given as rows.
        if rowpoints:
            src = source.T
            tar = target.T
        else:
            src = source
            tar = target

        # Check array shape. Given N points, it *must* have the shape 3 x N.
        assert src.shape[0] == 3 and tar.shape[0] == 3
        assert src.shape[1] == tar.shape[1]

        # Check weights.
        n_points = src.shape[1]
        if weights is None:
            w = np.identity(n_points)
        else:
            w = np.diag(weights)
            assert w.shape[0] == w.shape[1] == n_points

        # Compute weighted centroids of both point sets.
        src_mean = (np.matmul(src, w).sum(axis=1) / w.sum()).reshape([3, 1])
        tar_mean = (np.matmul(tar, w).sum(axis=1) / w.sum()).reshape([3, 1])

        # Compute "centralised" points.
        src_c = src - src_mean
        tar_c = tar - tar_mean

        # Compute covariance matrix C = X * W * Y.T
        cov = np.matmul(np.matmul(src_c, w), tar_c.T)

        # Compute singular value decomposition C = U * S * V.T (V.T == vt)
        u, s, vt = np.linalg.svd(cov)

        # Compute rotation matrix R = V * U.T
        r = np.matmul(vt.T, u.T)

        # Compute translation.
        t = tar_mean - np.matmul(r, src_mean)

        if result_as_4x4:
            return np.row_stack([np.column_stack([r, t]), (0, 0, 0, 1)])
        else:
            return r, t

    @staticmethod
    def pc_pre_process_n(scapula: np.ndarray, scapula_pc: np.ndarray, no_slices=20):
        gradent = (1 / no_slices) * (scapula_pc - scapula)
        slices = [t * gradent for t in range(0, no_slices + 1)]
        return slices


class Shoulder(Model):
    def __init__(self):
        super().__init__()
        self.current_scapula = None
        self.current_humerus = None
        self.current_humerus_rc = None
        self.glenoid = pd.read_csv("maps/ScapGlen_map_to_mean.csv")
        self.AA = pd.read_csv("maps/ScapAA_map_to_mean.csv")
        self.AI = pd.read_csv("maps/ScapAI_map_to_mean.csv")
        self.TS = pd.read_csv("maps/ScapTS_map_to_mean.csv")
        self.humeral_head = pd.read_csv("maps/Humeral_Head_map_to_mean.csv")
        self.EL = pd.read_csv("maps/HumEL_map_to_mean.csv")
        self.EM = pd.read_csv("maps/HumEM_map_to_mean.csv")
        self.scapula = pd.read_csv("maps/Scapula_map_to_mean.csv")
        self.humerus = pd.read_csv("maps/Humerus_map_to_mean.csv")

    def scapula_corr(self):
        """
        This is the ISB recommend coordinate system for the scapula

            Wu, G., van der Helm, F. C., Veeger, H. E., Makhsous, M., Van Roy, P., Anglin, C., Nagels, J.,
            Karduna, A. R., McQuade, K., Wang, X., Werner, F. W., Buchholz, B.,
            & International Society of Biomechanics (2005).
            ISB recommendation on definitions of joint coordinate systems of various joints for the reporting of human
            joint motion--Part II: shoulder, elbow, wrist and hand. Journal of biomechanics, 38(5), 981–992.
            https://doi.org/10.1016/j.jbiomech.2004.05.042

        :return: a matrix containing points for origin,x,y,z
        """

        aa_point = np.mean(self.current_scapula[self.AA['idm'].to_list()], axis=0)
        ai_point = np.mean(self.current_scapula[self.AI['idm'].to_list()], axis=0)
        ts_point = np.mean(self.current_scapula[self.TS['idm'].to_list()], axis=0)
        z_raw = aa_point - ts_point
        z = (1 / np.linalg.norm(z_raw)) * z_raw
        yx1_raw = ts_point - aa_point
        yx2_raw = ai_point - aa_point
        x_raw = np.cross(yx2_raw, yx1_raw)
        x = (1 / np.linalg.norm(x_raw)) * x_raw
        y = np.cross(z, x)
        ret = np.array([[0, 0, 0], 50 * x, 100 * y, 150 * z])
        without_magnitude = np.array([[0, 0, 0], x, y, z])
        out = pd.DataFrame(data=without_magnitude, columns=["x", "y", "z"])
        self.ref_ord = without_magnitude
        return [ret, out]

    def humerus_corr(self):
        """
        This is the ISB recommend coordinate system for the humerus.
        It uses points Lateral Epicondyle

            Wu, G., van der Helm, F. C., Veeger, H. E., Makhsous, M., Van Roy, P., Anglin, C., Nagels, J.,
            Karduna, A. R., McQuade, K., Wang, X., Werner, F. W., Buchholz, B.,
            & International Society of Biomechanics (2005).
            ISB recommendation on definitions of joint coordinate systems of various joints for the reporting of human
            joint motion--Part II: shoulder, elbow, wrist and hand. Journal of biomechanics, 38(5), 981–992.
            https://doi.org/10.1016/j.jbiomech.2004.05.042

        :return: a matrix containing points for origin,x,y,z
        """
        rc = np.squeeze(Model.sphere_fit(self.current_humerus[self.humeral_head['idm'].to_list()]))
        self.current_humerus_rc = rc
        el_point = np.nanmean(self.current_humerus[self.EL['idm'].to_list()], axis=0)
        em_point = np.nanmean(self.current_humerus[self.EM['idm'].to_list()], axis=0)
        mid_ep = 0.5 * (em_point + el_point)
        y_raw = rc - mid_ep
        y = (1 / np.linalg.norm(y_raw)) * y_raw
        X0 = em_point - rc
        X1 = el_point - rc
        x_raw = np.cross(X1, X0)
        x = (1 / np.linalg.norm(x_raw)) * x_raw
        z = np.cross(x, y)
        ret = np.array([[0, 0, 0], 50 * x, 100 * y, 150 * z])
        without_magnitude = np.array([[0, 0, 0], x, y, z])
        out = pd.DataFrame(data=without_magnitude, columns=["x", "y", "z"])
        return [ret, out]

    def calc_translation(self):
        idm_scap = self.glenoid['idm'].to_list()
        rc_scap = Model.sphere_fit(self.current_scapula[idm_scap])
        idm_hum = self.humeral_head['idm'].to_list()
        rc_hum = Model.sphere_fit(self.current_humerus[idm_hum])
        return np.squeeze(rc_scap - rc_hum)

    def calc_rotation(self):
        hdata = self.humerus_corr()[0]
        sdata = self.scapula_corr()[0]
        m = Model.transformation_from_svd(hdata.T, sdata.T)
        return m

