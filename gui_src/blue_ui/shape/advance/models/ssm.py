import vtk
import joblib
import pandas as pd
import numpy as np
import os

from blue_ui.shape.advance.models.mesh import Model
from blue_ui.shape.advance.tools.util import milli, Keywords


class ShapeModel:
    @staticmethod
    def color_convert(color=[0, 0, 0]):
        return [color[0]/255.0, color[1]/255, color[2]/255]

    def __init__(self, pc: str = None, mean_mesh=None, maps=None):
        if pc is not None and pc.endswith(".pc.npz"):
            s = np.load(pc, encoding='bytes', allow_pickle=True)
        elif pc is not None and pc.endswith("joblib"):
            n = joblib.load(pc)
            s = {
                'mean': n.mean_,
                'weights': n.explained_variance_,
                'modes': n.components_.T,
                'SD': None,
                'projectedWeights': None
            }
        else:
            s = np.load("maps/00_0000_HumerusScapula_Both.pc.npz", encoding='bytes', allow_pickle=True)
        self.mean = s['mean']
        self.weights = s['weights']  # PC weights are variance
        self.modes = s['modes']
        self.SD = s['SD']
        self.projectedWeights = s['projectedWeights']
        self.dimensions = 3
        if mean_mesh is None:
            self.mean_mesh = Model.mesh_load("meshes/00_0000_HumerusScapula_Both_mean.stl")
        else:
            # [0.5, 0.8, 1.0]
            self.mean_mesh = Model.mesh_load(mean_mesh, color=ShapeModel.color_convert([204, 199, 147]), opac=0.6)
            # self.inner_mesh = ModelData.mesh_load(mean_mesh, color=[155 / 255, 200 / 255, 100 / 255], opac=0.8)
            self.inner_mesh = Model.mesh_load(mean_mesh, color=ShapeModel.color_convert([150, 143, 96]), opac=0.5)

        st = milli()
        self.mesh_color = [144 / 255, 207 / 255, 252 / 255]
        self.mean_mesh_data = {
            Keywords.actor: self.mean_mesh[Keywords.actor],
            Keywords.polydata: self.mean_mesh[Keywords.polydata],
            Keywords.vertices: np.asarray([self.mean_mesh[Keywords.polydata].GetPoint(i) for i in range(self.mean_mesh[Keywords.polydata].GetNumberOfPoints())])
        }
        self.inner_mesh_data = {
            Keywords.actor: self.inner_mesh[Keywords.actor],
            Keywords.polydata: self.inner_mesh[Keywords.polydata],
            Keywords.vertices: np.asarray([self.inner_mesh[Keywords.polydata].GetPoint(i) for i in
                                           range(self.inner_mesh[Keywords.polydata].GetNumberOfPoints())])
        }
        ssm = np.reshape(self.mean, (-1, self.dimensions))
        while self.mean_mesh_data[Keywords.vertices].shape[0] != ssm.shape[0]:
            self.dimensions += 1
            ssm = np.reshape(self.mean, (-1, self.dimensions))
        en = milli()
        print("time to for mapping pcs: {0} msec".format(en - st))
        print()

    def reconstruct_diff(self, pc, sd=0, part=None, debug=False, add_mean=False):
        if part is None:
            return None
        w = sd * np.sqrt(self.weights[pc])
        m = w * self.modes[:, pc]
        me = m
        if add_mean:
            me = m + self.mean
        mesh = np.reshape(me, [int(self.mean.shape[0] / 3), 3])
        export = mesh[part["idm"].to_list(), :]
        if debug:
            if not os.path.exists("./meshes/temp/"):
                os.makedirs("./meshes/temp/")
            ms = pd.DataFrame(data=export, columns=["x", "y", "z"])
            ms.to_csv("./default_meshes/temp/part_cloud.csv", index=False)

        return export

    def reconstruct_diff_all(self, sd, add_mean=False):
        sdo = np.array([0.0 for i in range(len(self.weights))])
        sdo[:len(sd)] = sd
        w = np.atleast_2d(sdo * np.sqrt(self.weights))
        m = np.dot(w, self.modes.T)
        me = m.T
        if add_mean:
            me = m + self.mean
        mesh = np.reshape(me, [int(self.mean.shape[0] / self.dimensions), self.dimensions])
        return mesh

    def create_part(self, color, force_build, mean_mesh, part_map):
        mesh = np.reshape(self.mean, [int(self.mean.shape[0] / 3), 3])
        part = mesh[part_map, :]
        new_shape = None
        if force_build or not os.path.exists(mean_mesh):
            new_shape = self.extract_parts(part_map)
            # Write the mesh to file "*.ply"
            w = vtk.vtkPLYWriter()
            w.SetInputData(new_shape[0])
            w.SetFileName(mean_mesh)
            w.Write()
        if force_build:
            polydata = new_shape[0]
        else:
            reader = vtk.vtkPLYReader()
            reader.SetFileName(mean_mesh)
            reader.Update()
            polydata = reader.GetOutput()
        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            # mapper.SetInput(reader.GetOutput())
            mapper.SetInput(polydata)
        else:
            mapper.SetInputData(polydata)
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor(color[0], color[1], color[2])
        return {Keywords.actor: actor, Keywords.polydata: polydata, Keywords.vertices: part}

    @staticmethod
    def clean_poly_data(polydata):

        # Create a vtkCleanPolyData filter
        clean_filter = vtk.vtkCleanPolyData()
        clean_filter.SetInputData(polydata)

        # Update the filter to remove duplicate points
        clean_filter.Update()

        # Get the cleaned polydata output
        cleaned_polydata = clean_filter.GetOutput()

        return cleaned_polydata

    def extract_parts(self, node_id_list, export_vert=True):
        mesh_data_copy = vtk.vtkPolyData()
        mesh_org = self.mean_mesh['polydata']
        mesh_data_copy.DeepCopy(mesh_org)
        cell_data = mesh_data_copy.GetPolys()
        vertices = None
        if export_vert:
            # Get the point data and cell data of the mesh
            points_l = []
            for i in range(len(node_id_list)):
                p = mesh_data_copy.GetPoint(node_id_list[i])
                points_l.append(p)
            vertices = np.array(points_l)

        for i in range(cell_data.GetNumberOfCells()):
            cell = mesh_data_copy.GetCell(i)
            p1 = cell.GetPointId(0)
            p2 = cell.GetPointId(1)
            p3 = cell.GetPointId(2)
            if not (p1 in node_id_list and p2 in node_id_list and p3 in node_id_list):
                mesh_data_copy.DeleteCell(i)
        mesh_data_copy.RemoveDeletedCells()

        return [mesh_data_copy, vertices]