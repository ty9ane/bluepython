from blue_ui.grid.components.ui_nodes import RectangleNodeUI, Node
from blue_ui.shape.advance.models.mesh import Model
from yatpkg.util.data import StorageIO
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QLineEdit, QComboBox, QProgressBar, QFileDialog, QPushButton


class ContentWidget(QWidget):
    def __init__(self, node=None, content: list = [], parent=None):
        """

        :param parent:
        """
        super().__init__(parent=parent)
        self.wdg_label = None
        self.content = content
        self.layout = QVBoxLayout()
        self.node = node
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

    def add_content(self, widget):
        self.content.append(widget)
        if self.layout is not None:
            self.layout.addWidget(widget)

    def set_node(self, node):
        self.node = node

    def setup(self):
        pass


class ReaderNode(Node):
    def __init__(self, title="File Reader", filetype="csv"):
        """
        This is the node for reading data into MLDE.
        There are no inputs to this node. This will read the data into a Storage object.
        This object is the only output

        :param title: A name you want to give the node
        """
        self.node_name = title
        super().__init__(RectangleNodeUI(), title, [], [1])
        self.grNode.title = title
        self.grNode.content = ReaderContentWidget(node=self, filetype=filetype)
        self.grNode.setup_content()
        self.output_buffer = []
        self.grNode.setPos(-500, -100)
        self.filetype = filetype

    def run(self):
        print(self.node_name)
        if self.filetype == "csv" or self.filetype == "mot" or self.filetype == "trc":
            if len(self.output_buffer) < 3:
                self.output_buffer.append(StorageIO.load(self.content.filename))
            else:
                d = self.output_buffer.pop[0]
                self.output_buffer.append(StorageIO.load(self.content.filename))
        elif self.filetype == "ply" or self.filetype == "stl" or self.filetype == "obj":
            m = Model.mesh_load(self.content.filename)
            pass
        print(self.content.filename + " ... loaded")

    def receive_the_parcel(self, delivery: dict):
        pass

    def pass_the_parcel(self):
        parcel = self.output_buffer[-1]
        self.outputs_sockets[0].node.receive_the_parcel()


class ReaderContentWidget(ContentWidget):
    def __init__(self, node=None, filetype="csv", content: list = [], parent=None):
        """

        :param parent:
        """
        super().__init__(node=node, content=content, parent=parent)
        self.wdg_label = None
        self.filetype = filetype
        self.filename = "C:/foo.csv"
        self.display_file_text = QLineEdit(self.filename)
        self.progress = QProgressBar(self)
        self.progress.setStyleSheet("QProgressBar"
                                    "{"
                                    "background-color : #2f2f33;"
                                    "border : 1px"
                                    "}")
        self.setup(True)

    def button_pressed(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setNameFilter("{0} (*.{0})".format(self.filetype))
        self.filename = []
        if dlg.exec_():
            filenames = dlg.selectedFiles()
            if len(filenames) > 0:
                self.filename = filenames[0]
                self.display_file_text.setText(self.filename)
                self.node.run()
                c = 0
                while c < 100:
                    c += 0.001
                    self.progress.setValue(c)
                self.update()

    def setup(self, debug=False):

        if debug:
            button = QPushButton("&Open")
            button.clicked.connect(lambda:self.button_pressed())
            self.layout.addWidget(self.display_file_text)
            self.layout.addWidget(button)
            self.wdg_label = QLabel("Set input file: data will be stored in StorageIO object")
            self.wdg_label.setWordWrap(True)
            self.layout.addWidget(self.wdg_label)
            self.progress.setGeometry(200, 80, 250, 20)
            self.progress.setTextVisible(False)
            self.layout.addWidget(self.progress)