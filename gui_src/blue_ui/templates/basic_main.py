import vtk
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from vtk.util.numpy_support import numpy_to_vtk

from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QHBoxLayout, QVBoxLayout
from PyQt5.QtWidgets import QLabel


class LeftWidget(QWidget):

    def __init__(self):
        super().__init__()
        self.left_side = QVBoxLayout()
        self.title = QLabel('Something:')
        self.left_side.addWidget(self.title)
        self.setLayout(self.left_side)

    def add_control(self, c):
        self.left_side.addWidget(c)

    def setup(self):
        self.left_side.addStretch(5)
        self.left_side.setSpacing(2)


class CentreVTKWidget(QWidget):

    def __init__(self):
        super().__init__()
        # Create a RenderWindowInteractor to permit manipulating the camera
        self.style = vtk.vtkInteractorStyleTrackballCamera()
        self.vtk_widget = QVTKRenderWindowInteractor(parent=self)
        self.vtk_widget.SetInteractorStyle(self.style)
        self.vtk_widget.Initialize()
        self.vtk_widget.Start()

        # Create a renderer
        self.ren = vtk.vtkRenderer()
        self.ren.SetBackground(0.85, 0.85, 0.95)

        # Connect the VTK render window to the PyQt widget
        self.ren_win = self.vtk_widget.GetRenderWindow()
        self.ren_win.AddRenderer(self.ren)
        self.vtk_widget.SetRenderWindow(self.ren_win)
        self.vtk_widget.update()

    def update(self):
        super().update()
        self.vtk_widget.update()

    def add_actor(self, actor, do_update=True):
        self.ren.AddActor(actor)
        if do_update:
            self.vtk_widget.update()

    @property
    def canvas_3d(self):
        return self.vtk_widget


class BasicMainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        # setup axes
        self.axes = vtk.vtkAxesActor()
        self.axes.SetTotalLength(100, 100, 100)
        self.axes.SetNormalizedTipLength(0.2, 0.2, 0.2)

        self.central_widget = None
        self.left_widget = None

    def setup(self):
        self.central_widget = CentreVTKWidget()
        self.central_widget.add_actor(self.axes)
        self.setCentralWidget(self.central_widget)
        self.left_widget = LeftWidget()

        h_layout = QHBoxLayout()
        h_layout.addWidget(self.left_widget)
        h_layout.addWidget(self.central_widget.canvas_3d)

        self.central_widget.setLayout(h_layout)

    def initialise(self):
        self.setup()
        self.show()

    @staticmethod
    def numpy_to_vtk(vert_np):
        return numpy_to_vtk(vert_np)


if __name__ == '__main__':
    app = QApplication([])
    window = BasicMainWindow()
    window.initialise()
    app.exec_()
