from PyQt5.QtWidgets import QWidget, QGraphicsScene, QVBoxLayout, QGraphicsView, QGraphicsItem, QGraphicsTextItem
from PyQt5.QtWidgets import QGraphicsProxyWidget, QLabel, QTextEdit, QApplication, QAction, QMenu, QGraphicsPathItem
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QToolTip
from PyQt5.QtGui import QColor, QPen, QPainter, QMouseEvent, QFont, QBrush, QPainterPath, QIcon, QKeySequence, QCursor
from PyQt5.QtCore import QLine, Qt, QEvent, QRectF, QFile, QPointF

import numpy as np
from enum import Enum

from typing import List

"""
Node Editor Adapted from https://gitlab.com/pavel.krupala/pyqt-node-editor/
Original Author: pavel krupala
Modder: Ted Yeung
"""


class Tags(Enum):
    w = "Width"
    h = "height"
    edge = "edge"
    padding = "padding"
    title_h = "Title Height"
    title = "Title"
    size = "size"
    name = "name"
    qss = "qss"
    colour_bg = "colour background"
    colour_ol = "colour outline"
    colour_active = "colour active"
    input = "input"
    output = "output"
    pos = "pos"

    LEFT_TOP = 1
    LEFT_BOTTOM = 2
    RIGHT_TOP = 3
    RIGHT_BOTTOM = 4

    EDGE_TYPE_DIRECT = 1
    EDGE_TYPE_BEZIER = 2


class NodeDefaults(Enum):
    size = {Tags.w: 180, Tags.h: 238, Tags.edge: 10, Tags.title_h: 30, Tags.padding: 5}
    font = {Tags.name: "Verdana", Tags.size: 9}
    style = {Tags.qss: "NDContentWidget { background: transparent; }\n"
                       "NDContentWidget QLabel { color: #6d90ab;  }"}
    socket = {Tags.colour_bg: "#506670", Tags.colour_ol: "#1a313b", Tags.colour_active: "#36afe3",
              Tags.size: 6, Tags.edge: 1}

    def __getitem__(self, indices):
        return self.value[indices]


class QDMGraphicsEdge(QGraphicsPathItem):
    def __init__(self, edge, parent=None):
        super().__init__(parent)

        self.edge = edge

        self._color = QColor("#001000")
        self._color_selected = QColor("#00ff00")
        self._pen = QPen(self._color)
        self._pen_selected = QPen(self._color_selected)
        self._pen.setWidthF(5.0)
        self._pen_selected.setWidthF(7.5)

        self.setFlag(QGraphicsItem.ItemIsSelectable)

        self.setZValue(-1)
        self.posSource = [0, 0]
        self.posDestination = [200, 100]

    def setSource(self, x, y):
        self.posSource = [x, y]

    def setDestination(self, x, y):
        self.posDestination = [x, y]

    def paint(self, painter, QStyleOptionGraphicsItem, widget=None):
        self.updatePath()
        painter.setPen(self._pen if not self.isSelected() else self._pen_selected)
        painter.setBrush(Qt.NoBrush)
        painter.drawPath(self.path())

    def updatePath(self):
        """ Will handle drawing QPainterPath from Point A to B """
        raise NotImplemented("This method has to be overriden in a child class")


class QDMGraphicsEdgeDirect(QDMGraphicsEdge):
    def updatePath(self):
        path = QPainterPath(QPointF(self.posSource[0], self.posSource[1]))
        path.lineTo(self.posDestination[0], self.posDestination[1])
        self.setPath(path)


class QDMGraphicsEdgeBezier(QDMGraphicsEdge):
    def updatePath(self):
        s = self.posSource
        d = self.posDestination

        dist = (d[0] - s[0]) * 0.5
        if s[0] > d[0]: dist *= -1

        path = QPainterPath(QPointF(self.posSource[0], self.posSource[1]))
        path.cubicTo(s[0] + dist, s[1], d[0] - dist, d[1], self.posDestination[0], self.posDestination[1])
        self.setPath(path)


class Edge:
    DEBUG = False

    def __init__(self, scene, start_socket, end_socket, line_type=Tags.EDGE_TYPE_DIRECT):

        self.scene = scene
        print("Edge start")
        self.start_socket = start_socket
        if self.start_socket is not None or self.end_socket is not None:
            self.start_socket.setConnectedEdge(self)
        print("Edge end")
        self.end_socket = end_socket
        if self.start_socket is not None or self.end_socket is not None:
            self.end_socket.setConnectedEdge(self)
        self.line_type = line_type
        print("Edge graphics")
        if self.line_type == Tags.EDGE_TYPE_DIRECT:
            self.grEdge = QDMGraphicsEdgeDirect(self)
        elif self.line_type == Tags.EDGE_TYPE_BEZIER:
            self.grEdge = QDMGraphicsEdgeBezier(self)
        print(" Edge pos")
        if self.start_socket is not None and self.end_socket is not None:
            self.updatePositions()

        print("add to scene")
        self.scene.grScene.addItem(self.grEdge)

    def updatePositions(self, mouse=None):
        source_pos = np.asarray(self.start_socket.getSocketPosition())
        # print(self.start_socket.node.grNode.pos())
        # print(source_pos)
        # source_pos[0] += self.start_socket.node.grNode.pos().x()
        # source_pos[1] += self.start_socket.node.grNode.pos().y()
        self.grEdge.setSource(*source_pos)
        if self.end_socket is not None:
            end_pos = np.asarray(self.end_socket.getSocketPosition())
            # end_pos[0] += self.end_socket.node.grNode.pos().x()
            # end_pos[1] += self.end_socket.node.grNode.pos().y()
            self.grEdge.setDestination(*end_pos)
        else:
            self.grEdge.setDestination(*mouse)

        if Edge.DEBUG: print(" SS:", self.start_socket)
        if Edge.DEBUG: print(" ES:", self.end_socket)
        self.grEdge.update()

    def remove_from_sockets(self):
        if self.start_socket is not None:
            self.start_socket.edge = None
        if self.end_socket is not None:
            self.end_socket.edge = None
        self.end_socket = None
        self.start_socket = None

    def remove(self):
        self.remove_from_sockets()
        self.scene.grScene.removeItem(self.grEdge)
        self.grEdge = None
        self.scene.removeEdge(self)


class Spiderman(Edge):
    def __init__(self, scene, start_socket, end_socket, line_type=Tags.EDGE_TYPE_DIRECT):
        print("Spiderman spawn")
        self.scene = scene
        self.offsets = None
        self.start_socket = start_socket
        self.end_socket = end_socket
        self.line_type = line_type
        self.grEdge = None
        self.moving = False
        self.anchor = None
        self.end = None
        print("Spiderman - init Done")
        pass

    def set_offset(self, offsets):
        if offsets is not None:
            self.offsets = offsets

    def set_item_anchor(self, item):
        self.anchor = item

    def set_item_end(self, item):
        self.end = item

    def activate(self, mouse):
        if not self.moving:
            print("spider start to move")
            self.moving = True
            p = [mouse[0] - (self.offsets[0] / 2), mouse[1] - (self.offsets[1] / 2)]
            self.end_socket = p
            print("web shoot")
            if self.line_type == Tags.EDGE_TYPE_DIRECT:
                self.grEdge = QDMGraphicsEdgeDirect(self)
            elif self.line_type == Tags.EDGE_TYPE_BEZIER:
                self.grEdge = QDMGraphicsEdgeBezier(self)
            print(self.grEdge)
            print("attach to scene")
            self.scene.grScene.addItem(self.grEdge)
            self.grEdge.update()

    def deactivate(self):
        self.moving = False
        self.scene.grScene.removeItem(self.grEdge)

    def updatePositions(self, mouse=None):
        print("Swing")
        source_pos = self.start_socket
        p = [mouse[0] - (self.offsets[0] / 2), mouse[1] - (self.offsets[1] / 2)]
        self.end_socket = p
        self.grEdge.setSource(*source_pos)
        self.grEdge.setDestination(*self.end_socket)
        self.grEdge.update()

    def to_edge(self):
        print("Create Edge")
        print(self.anchor)
        print(self.end)
        edge2 = Edge(self.scene, self.anchor, self.end, line_type=self.line_type)
        self.deactivate()
        return


class Socket:
    DEBUG = False
    INPUT = 1
    OUTPUT = 2

    def __init__(self, node, io, s_type, index=0, position=Tags.LEFT_TOP):
        self.node = node
        self.io = io
        self.s_type = s_type
        self.index = index
        self.position = position
        self.is_active = False
        self.grSocket = NDGraphicsSocket(self, self.node.grNode)
        self.grSocket.setPos(*self.node.getSocketPosition(index, position))
        self.edge = []
        self.is_active_lock = False

    @property
    def radius(self):
        return self.grSocket.radius

    @property
    def status(self):
        return self.is_active

    @status.setter
    def status(self, x):
        if self.is_active_lock is True:
            self.is_active = True
        else:
            self.is_active = x
        if self.is_active:
            self.grSocket.active_profile()
        else:
            self.grSocket.inactive_profile()

    def getSocketPosition(self):
        if Socket.DEBUG: print("  GSP: ", self.index, self.position, "node:", self.node)
        res = self.node.getSocketPosition(self.index, self.position)
        #print(self.node.title)
        # print("soc pos " + str(self.grSocket.pos()))
        if Socket.DEBUG: print("  res", res)
        return res

    def setConnectedEdge(self, edge=None, is_active_lock=True):
        print("sock lock")
        self.is_active_lock = is_active_lock
        self.is_active = True
        print("sock active profile")
        self.grSocket.active_profile()
        print("sock append")
        self.edge.append(edge)
        print("sock return")

    def update(self):
        for e in self.edge:
            e.updatePositions()


class NDGraphicsSocket(QGraphicsItem):
    def __init__(self, sock, parent=None):
        super().__init__(parent)
        self.sock = sock
        self.radius = NodeDefaults.socket[Tags.size]
        self.outline_width = NodeDefaults.socket[Tags.edge]
        self._color_background = QColor(NodeDefaults.socket[Tags.colour_bg])
        self._color_outline = QColor(NodeDefaults.socket[Tags.colour_ol])

        self._pen = QPen(self._color_outline)
        self._pen.setWidthF(self.outline_width)
        self._brush = QBrush(self._color_background)

    def active_profile(self):
        self._color_background = QColor(NodeDefaults.socket[Tags.colour_active])
        self._brush = QBrush(self._color_background)
        self.update()

    def inactive_profile(self):
        if not self.sock.is_active_lock:
            self.sock.is_active = False
            self._color_background = QColor(NodeDefaults.socket[Tags.colour_bg])
            self._brush = QBrush(self._color_background)
            self.update()

    def paint(self, painter, QStyleOptionGraphicsItem, widget=None):
        # painting circle
        painter.setBrush(self._brush)
        painter.setPen(self._pen)
        painter.drawEllipse(-self.radius, -self.radius, 2 * self.radius, 2 * self.radius)

    def boundingRect(self):
        return QRectF(
            - self.radius - self.outline_width,
            - self.radius - self.outline_width,
            2 * (self.radius + self.outline_width),
            2 * (self.radius + self.outline_width),
        )


class CustomDialog(QDialog):

    def __init__(self, *args, **kwargs):
        super(CustomDialog, self).__init__(*args, **kwargs)

        self.setWindowTitle("Nodes")

        buttons = QDialogButtonBox.Ok | QDialogButtonBox.Cancel

        self.buttonBox = QDialogButtonBox(buttons)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.label = QLabel("hello")

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)


class NDMGraphicsView(QGraphicsView):
    def __init__(self, grScene, parent=None):
        super().__init__(parent)
        self.grScene: NDMGraphicsScene = grScene
        self.setup_view()
        self.setScene(self.grScene)

        self.zoom_in = 1.25
        self.zoom = 5
        self.zoom_step = 0.5
        self.zoom_range = [1, 10]
        self._createActions()
        self.mouse_pos = [0, 0]
        self.selected = []
        self.menu = None
        self.is_pressed = False
        self.initial_pressed_item = None
        self.temp_edge = None

    @property
    def scene(self):
        return self.grScene.scene

    def adjusted_mouse_pos(self, item=None):
        s = [self.size().width(), self.size().height()]
        if item is None:
            p = [self.mouse_pos[0] - (s[0] / 2), self.mouse_pos[1] - (s[1] / 2)]
        else:
            p = [item.pos().x() - (s[0] / 2), item.pos().y()- (s[1] / 2)]
        return p

    def find_socket(self):
        sock = None
        for n in self.grScene.scene.nodes:
            for s in n.sockets:
                for a in n.sockets[s]:
                    # print(s)
                    t = a.getSocketPosition()
                    s = [self.size().width(), self.size().height()]
                    p = [self.mouse_pos[0] - (s[0] / 2), self.mouse_pos[1] - (s[1] / 2)]
                    diff = np.linalg.norm(np.asarray(t) - np.asarray(p))
                    # print("socket: " + str(t))
                    # print("mouse: " + str(p))
                    # print("diff:" + str(diff))
                    if diff < a.radius:
                        # print("matched")
                        a.status = True
                        sock = a

        return sock

    def turn_off_all_socks(self):
        for n in self.grScene.scene.nodes:
            for s in n.sockets:
                for a in n.sockets[s]:
                    a.status = False

    def add_node(self):
        print("add node")
        node = Node(self.grScene.scene, "My Awesome Node 1", inputs=[1,2,3], outputs=[1])
        s = [self.size().width(), self.size().height()]
        p = [self.mouse_pos[0]-(s[0]/2), self.mouse_pos[1]-(s[1]/2)]
        node.set_pos(p)

    def delete_selected_node(self):
        print("delete")

    def _createActions(self):
        # File actions
        self.newAction = QAction(self)
        self.newAction.setText("&New")
        self.newAction.setIcon(QIcon(":file-new.svg"))
        self.openAction = QAction(QIcon(":file-open.svg"), "&Open...", self)
        self.addAction = QAction(self, triggered=self.add_node)
        self.addAction.setText("&Add")
        self.addAction.setIcon(QIcon(":file-new.svg"))
        self.addAction.setStatusTip("Add Node")
        self.addAction.setToolTip("Add Node")
        self.addAction.setShortcuts(QKeySequence.SelectAll)
        self.clear_action = QAction(self, triggered=self.turn_off_all_socks)
        self.clear_action.setText("&Clear Selected Sockets")
        self.clear_action.setIcon(QIcon(":file-new.svg"))
        self.clear_action.setStatusTip("Clear Selected Items")
        self.clear_action.setToolTip("Clear Selected Items")
        self.clear_action.setShortcuts(QKeySequence(str("Ctrl+d")))
        self.del_action = QAction(self, triggered=self.turn_off_all_socks)
        self.del_action.setText("&Delete Selected")
        self.del_action.setIcon(QIcon(":file-new.svg"))
        self.del_action.setStatusTip("Delete Selected Items")
        self.del_action.setToolTip("Delete Selected Items")
        self.saveAction = QAction(QIcon(":file-save.svg"), "&Save", self)
        self.exitAction = QAction("&Exit", self)

    def contextMenuEvent(self, event):
        # Creating a menu object with the central widget as parent
        self.menu = QMenu(self)
        # Populating the menu with actions
        self.menu.addAction(self.addAction)
        self.menu.addAction(self.clear_action)
        self.menu.addSeparator()
        self.menu.addAction(self.clear_action)
        self.menu.addSeparator()
        self.menu.addAction(self.newAction)
        self.menu.addAction(self.openAction)
        self.menu.addAction(self.saveAction)
        self.menu.setToolTipsVisible(True)
        self.menu.setShortcutEnabled(True)
        # self.menu.hovered.connect(self.handleMenuHovered)
        # Launching the menu
        self.menu.exec(event.globalPos())

    def setup_view(self):
        self.setRenderHints(QPainter.Antialiasing | QPainter.HighQualityAntialiasing | QPainter.TextAntialiasing | QPainter.SmoothPixmapTransform)

        self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)

        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)

    def mouseMoveEvent(self, event):
        self.mouse_pos = [event.localPos().x(), event.localPos().y()]
        if self.is_pressed:
            item = self.initial_pressed_item
            if item is not None:
                if isinstance(item, QGraphicsTextItem):
                    parent = item.parentItem()
                    if isinstance(parent, NDMGraphicsNode):
                        parent.node.update_edges()
                if isinstance(item, NDGraphicsSocket):
                    print("Spiderman - Activate")
                    self.temp_edge.activate(self.mouse_pos)
                    print("Spiderman - updatePositions")
                    self.temp_edge.updatePositions(self.mouse_pos)
                    pass
        self.grScene.update()
        return super().mouseMoveEvent(event)

    def mousePressEvent(self, event):
        if event.button() == Qt.MiddleButton:
            self.middleMouseButtonPress(event)
        elif event.button() == Qt.LeftButton:
            self.leftMouseButtonPress(event)
        elif event.button() == Qt.RightButton:
            self.rightMouseButtonPress(event)
        else:
            super().mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.MiddleButton:
            self.middleMouseButtonRelease(event)
        elif event.button() == Qt.LeftButton:
            self.leftMouseButtonRelease(event)
        elif event.button() == Qt.RightButton:
            self.rightMouseButtonRelease(event)
        else:
            super().mouseReleaseEvent(event)

    def middleMouseButtonPress(self, event):
        releaseEvent = QMouseEvent(QEvent.MouseButtonRelease, event.localPos(), event.screenPos(),
                                   Qt.LeftButton, Qt.NoButton, event.modifiers())
        super().mouseReleaseEvent(releaseEvent)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        fakeEvent = QMouseEvent(event.type(), event.localPos(), event.screenPos(),
                                Qt.LeftButton, event.buttons() | Qt.LeftButton, event.modifiers())
        super().mousePressEvent(fakeEvent)

    def middleMouseButtonRelease(self, event):
        fakeEvent = QMouseEvent(event.type(), event.localPos(), event.screenPos(),
                                Qt.LeftButton, event.buttons() & ~Qt.LeftButton, event.modifiers())
        super().mouseReleaseEvent(fakeEvent)
        self.setDragMode(QGraphicsView.NoDrag)

    def leftMouseButtonPress(self, event):
        print("left clicked")
        mg = event.globalPos()
        print(mg)
        self.mouse_pos = [event.localPos().x(), event.localPos().y()]
        print(self.mouse_pos)
        item = self.get_item_under_mouse(event)
        self.initial_pressed_item = item
        boo = False
        if item is not None:
            if isinstance(item, NDGraphicsSocket):
                item.active_profile()
                print(self.mouse_pos)
                self.selected.append(item)
                print("socket drag")
                s = [self.size().width(), self.size().height()]
                p = [self.mouse_pos[0]-(s[0]/2), self.mouse_pos[1]-(s[1]/2)]
                self.temp_edge = Spiderman(self.scene, p, self.mouse_pos, line_type=Tags.EDGE_TYPE_BEZIER)
                self.temp_edge.set_offset(s)
                self.temp_edge.set_item_anchor(item.sock)
                self.is_pressed = True
            elif isinstance(item, QGraphicsTextItem):
                self.is_pressed = True
                boo = True
            print(boo)
        if boo:
            return super().mousePressEvent(event)
        else:
            return

    def leftMouseButtonRelease(self, event):
        print("released Lefty")
        self.is_pressed = False
        self.initial_pressed_item = None
        try:
            item = self.selected.pop(0)
            if isinstance(item, NDGraphicsSocket):
                self.mouse_pos = [event.localPos().x(), event.localPos().y()]
                sock = self.find_socket()
                if sock is not None:
                    print(sock.io)
                    under_mouse = sock.grSocket
                    if item != under_mouse:
                        self.temp_edge.set_item_end(sock)
                        if item.sock.s_type == sock.s_type:
                            item.inactive_profile()
                            sock.grSocket.inactive_profile()
                            self.temp_edge.deactivate()
                        else:
                            self.temp_edge.to_edge()
                else:
                    item.inactive_profile()
                    self.temp_edge.deactivate()

        except IndexError:
            print("index error")
            pass
        return super().mouseReleaseEvent(event)

    def rightMouseButtonPress(self, event):
        return super().mousePressEvent(event)

    def rightMouseButtonRelease(self, event):
        self.mouse_pos = [event.localPos().x(), event.localPos().y()]
        print(self.mouse_pos)
        return super().mouseReleaseEvent(event)

    def get_item_under_mouse(self, event):
        pos = event.pos()
        obj = self.itemAt(pos)
        return obj

    def wheelEvent(self, event):
        zoom_out = 1 / self.zoom_in
        if event.angleDelta().y() > 0 or event.angleDelta().x() > 0:
            zoom_factor = self.zoom_in
            self.zoom += self.zoom_step
        else:
            zoom_factor = zoom_out
            self.zoom -= self.zoom_step

        if self.zoom_range[0] < self.zoom < self.zoom_range[1]:
            self.scale(zoom_factor, zoom_factor)
        elif self.zoom_range[0] > self.zoom:
            self.zoom = self.zoom_range[0]
        elif self.zoom > self.zoom_range[1]:
            self.zoom = self.zoom_range[1]


class NDMGraphicsNode(QGraphicsItem):
    def __init__(self, node, parent=None):
        super().__init__(parent)
        self.node = node
        self.content = self.node.content
        self._title_color = Qt.white
        self._title_font = QFont(NodeDefaults.font[Tags.name], NodeDefaults.font[Tags.size])

        self.width = NodeDefaults.size[Tags.w]
        self.height = NodeDefaults.size[Tags.h]
        self.edge_size = NodeDefaults.size[Tags.edge]
        self.title_height = NodeDefaults.size[Tags.title_h]
        self._padding = NodeDefaults.size[Tags.padding]

        self._pen_default = QPen(QColor("#7F000000"))
        self._pen_selected = QPen(QColor("#FFFFA637"))

        self._brush_title = QBrush(QColor("#FF313131"))
        self._brush_background = QBrush(QColor("#E3212121"))
        self.title_item: QGraphicsTextItem = None
        self.gr_content = None

        self.init_title()
        self.title = self.node.title
        
        self.setup_content()

        self.set_up_ui()
        # sockets - are inputs/output to the module/function

    @property
    def padding(self):
        return self._padding

    def boundingRect(self):
        return QRectF(
            0,
            0,
            2 * self.edge_size + self.width,
            2 * self.edge_size + self.height
        ).normalized()

    def set_up_ui(self):
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setFlag(QGraphicsItem.ItemIsMovable)

    def init_title(self):
        self.title_item = QGraphicsTextItem(parent=self)
        self.title_item.setParentItem(self)
        self.title_item.setDefaultTextColor(self._title_color)
        self.title_item.setFont(self._title_font)
        self.title_item.setPos(self._padding, 0)
        self.title_item.setTextWidth(
            self.width
            - 2 * self._padding
        )

    @property
    def title(self): return self._title

    @title.setter
    def title(self, value):
        self._title = value
        self.title_item.setPlainText(self._title)

    def paint(self, painter, QStyleOptionGraphicsItem, widget=None):
        # if self.isSelected():
        #     self.node.update_edges()
        # title
        path_title = QPainterPath()
        path_title.setFillRule(Qt.WindingFill)
        path_title.addRoundedRect(0,0, self.width, self.title_height, self.edge_size, self.edge_size)
        path_title.addRect(0, self.title_height - self.edge_size, self.edge_size, self.edge_size)
        path_title.addRect(self.width - self.edge_size, self.title_height - self.edge_size, self.edge_size, self.edge_size)
        painter.setPen(Qt.NoPen)
        painter.setBrush(self._brush_title)
        painter.drawPath(path_title.simplified())

        # # content
        # path_content = QPainterPath()
        # path_content.setFillRule(Qt.WindingFill)
        # path_content.addRoundedRect(0, self.title_height, self.width, self.height - self.title_height, self.edge_size, self.edge_size)
        # path_content.addRect(0, self.title_height, self.edge_size, self.edge_size)
        # path_content.addRect(self.width - self.edge_size, self.title_height, self.edge_size, self.edge_size)
        # painter.setPen(Qt.NoPen)
        # painter.setBrush(self._brush_background)
        # painter.drawPath(path_content.simplified())

        # outline
        path_outline = QPainterPath()
        path_outline.addRoundedRect(0, 0, self.width, self.height, self.edge_size, self.edge_size)
        painter.setPen(self._pen_default if not self.isSelected() else self._pen_selected)
        painter.setBrush(Qt.NoBrush)
        painter.drawPath(path_outline.simplified())

    def setup_content(self):
        self.gr_content = QGraphicsProxyWidget(self)
        self.content.setGeometry(self.edge_size, self.title_height + self.edge_size,
                                 self.width - 2 * self.edge_size, self.height - 2 * self.edge_size - self.title_height)
        self.gr_content.setWidget(self.content)
        pass


class NDContentWidget(QWidget):
    def __init__(self, content: list = [], parent=None):
        """

        :param parent:
        """
        super().__init__(parent=parent)
        self.wdg_label = None
        self.content = content
        self.layout = None
        self.setup(True)

    def add_content(self, widget):
        self.content.append(widget)
        if self.layout is not None:
            self.layout.addWidget(widget)

    def setup(self, debug=False):
        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.wdg_label = QLabel("Some Title")
        self.layout.addWidget(self.wdg_label)
        self.layout.addWidget(QTextEdit("foo"))


class Node:
    def __init__(self, scene, title="Undefined Node", inputs=[], outputs=[]):
        self.scene = scene

        self.title = title
        self.content = NDContentWidget()
        self.grNode = NDMGraphicsNode(self)

        self.scene.addNode(self)
        self.scene.grScene.addItem(self.grNode)

        self.socket_spacing = 22

        self.inputs_sockets = []
        self.outputs_sockets = []

        counter = 0
        for item in inputs:
            socket = Socket(node=self, io=item, s_type=Socket.INPUT, index=counter, position=Tags.LEFT_BOTTOM)
            counter += 1
            self.inputs_sockets.append(socket)

        counter = 0
        for item in outputs:
            socket = Socket(node=self, io=item, s_type=Socket.OUTPUT, index=counter, position=Tags.RIGHT_TOP)
            counter += 1
            self.outputs_sockets.append(socket)

        self.is_pressed = False

    @property
    def sockets(self):
        return {Tags.input: self.inputs_sockets, Tags.output: self.outputs_sockets}

    @property
    def pressed(self):
        return self.is_pressed

    @pressed.setter
    def pressed(self, boo):
        self.is_pressed = boo

    @property
    def pos(self):
        return self.grNode.pos()

    @property
    def pos_as_list(self):
        po = self.pos
        return[po.x(), po.y()]

    @property
    def pos_as_numpy(self):
        return np.asarray(self.pos_as_list)

    def set_pos(self, *point):
        if len(point) == 1:
            self.grNode.setPos(*point[0])
        else:
            self.grNode.setPos(*point)

    def getSocketPosition(self, index, position):
        x = 0 if (position in (Tags.LEFT_TOP, Tags.LEFT_BOTTOM)) else self.grNode.width

        if position in (Tags.LEFT_BOTTOM, Tags.RIGHT_BOTTOM):
            # start from bottom
            y = self.grNode.height - self.grNode.edge_size - self.grNode.padding - index * self.socket_spacing
        else:
            # start from top
            y = self.grNode.title_height + self.grNode.padding + self.grNode.edge_size + index * self.socket_spacing
        return [x+self.pos_as_list[0], y+self.pos_as_list[1]]

    def update_edges(self):
        for s in self.sockets:
            for e in self.sockets[s]:
                e.update()


class NDMGraphicsScene(QGraphicsScene):
    def __init__(self, scene, parent=None):
        super().__init__(parent)
        self.scene = scene
        # settings
        self.gridSize = 20
        self.gridSquares = 5

        self._color_background = QColor("#393939")
        self._color_light = QColor("#2f2f2f")
        self._color_dark = QColor("#292929")

        self._pen_light = QPen(self._color_light)
        self._pen_light.setWidth(1)
        self._pen_dark = QPen(self._color_dark)
        self._pen_dark.setWidth(2)

        self.scene_width, self.scene_height = 64000, 64000
        self.setSceneRect(-self.scene_width // 2, -self.scene_height // 2, self.scene_width, self.scene_height)

        self.setBackgroundBrush(self._color_background)

    def setGrScene(self, width, height):
        self.setSceneRect(-width // 2, -height // 2, width, height)

    def drawBackground(self, painter, rect):
        super().drawBackground(painter, rect)

        # here we create our grid
        left = int(np.floor(rect.left()))
        right = int(np.ceil(rect.right()))
        top = int(np.floor(rect.top()))
        bottom = int(np.ceil(rect.bottom()))

        first_left = left - (left % self.gridSize)
        first_top = top - (top % self.gridSize)

        # compute all lines to be drawn
        lines_light, lines_dark = [], []
        for x in range(first_left, right, self.gridSize):
            if (x % (self.gridSize * self.gridSquares)) != 0:
                lines_light.append(QLine(x, top, x, bottom))
            else:
                lines_dark.append(QLine(x, top, x, bottom))

        for y in range(first_top, bottom, self.gridSize):
            if (y % (self.gridSize * self.gridSquares)) != 0:
                lines_light.append(QLine(left, y, right, y))
            else:
                lines_dark.append(QLine(left, y, right, y))

        # draw the lines
        painter.setPen(self._pen_light)
        painter.drawLines(*lines_light)

        painter.setPen(self._pen_dark)
        painter.drawLines(*lines_dark)


class Scene:
    def __init__(self):
        self.nodes: List[Node] = []
        self.edges = []
        self.actor = None

        self.scene_width = 64000
        self.scene_height = 64000
        self.grScene = None
        self.set_up_scene()

    def add_actor(self, a):
        self.actor = a

    def set_up_scene(self):
        self.grScene = NDMGraphicsScene(self)
        self.grScene.setGrScene(self.scene_width, self.scene_height)

    def addNode(self, node):
        self.nodes.append(node)

    def addEdge(self, edge):
        self.edges.append(edge)

    def removeNode(self, node):
        self.nodes.remove(node)

    def removeEdge(self, edge):
        self.edges.remove(edge)


class MainWindow(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Hello World")
        self.layout = QVBoxLayout()
        self.scene = None
        self.view = None
        self.actor = None
        self.setup_ui()
        self.addNodes()

        QApplication.instance().setStyleSheet(NodeDefaults.style[Tags.qss])
        pass

    def addNodes(self):
        node1 = Node(self.scene, "My Awesome Node 1", inputs=[1, 2, 3], outputs=[1])
        node2 = Node(self.scene, "My Awesome Node 2", inputs=[1, 2, 3], outputs=[1])
        node3 = Node(self.scene, "My Awesome Node 3", inputs=[1, 2, 3], outputs=[1])
        node1.set_pos(-350, -250)
        node2.set_pos(-75, 0)
        node3.set_pos(200, -150)

        edge1 = Edge(self.scene, node1.outputs_sockets[0], node3.inputs_sockets[0])
        edge2 = Edge(self.scene, node1.outputs_sockets[0], node2.inputs_sockets[0], line_type=Tags.EDGE_TYPE_BEZIER)

    def setup_ui(self):
        print("MainWindow: setup_ui -> Start")
        self.setGeometry(200, 200, 1280, 720)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        # crate graphics scene
        self.scene = Scene()

        # create graphics view
        self.view = NDMGraphicsView(self.scene.grScene, self)
        self.layout.addWidget(self.view)
        print("MainWindow: setup_ui -> End")
        self.show()

    def loadStylesheet(self, filename):
        print('STYLE loading:', filename)
        file = QFile(filename)
        file.open(QFile.ReadOnly | QFile.Text)
        stylesheet = file.readAll()
        QApplication.instance().setStyleSheet(str(stylesheet, encoding='utf-8'))
