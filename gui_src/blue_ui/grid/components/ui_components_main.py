from PySide6.QtWidgets import *
from PySide6.QtGui import *
from PySide6.QtCore import *

import numpy as np

from blue_ui.grid.components.ui_nodes import RectangleNodeUI
from blue_ui.grid.components.misc import Tags, NodeDefaults


class MainMenuBar(QMenuBar):
    def __init__(self, parent=None, view=None, default_menus=True):
        super().__init__(parent)
        self.view = view
        if default_menus:
            self.default_menus()

    def set_view(self, view):
        self.view = view

    def default_menus(self):
        action_file = self.addMenu("File")
        action_file.addAction("New")
        action_file.addAction("Open")
        action_file.addAction("Save")
        action_file.addSeparator()
        action_file.addAction("Quit")
        action_edit = self.addMenu("Edit")
        action_edit.addAction("Preferences")
        action_view = self.addMenu("View")
        reset_zoom_action = action_view.addAction("&Reset Zoom")
        reset_zoom_action.triggered.connect(self.zoom_reset)
        action_run = self.addMenu("Program")
        action_run.addAction("Create Shape Model")
        action_run.addAction("Generate Marker Set")
        action_run.addAction("Run IK")
        self.addMenu("Help")

    def run(self):
        pass

    def zoom_reset(self):
        print("zoom")
        if self.view is not None:
            self.view.reset_zoom()


class MainScene(QGraphicsScene):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.gridSize = 20
        self.gridSquares = 5

        self._color_background = QColor("#393939")
        self._color_light = QColor("#2f2f2f")
        self._color_dark = QColor("#292929")

        self._pen_light = QPen(self._color_light)
        self._pen_light.setWidth(1)
        self._pen_dark = QPen(self._color_dark)
        self._pen_dark.setWidth(2)

        self.scene_width, self.scene_height = 64000, 64000
        self.setSceneRect(-self.scene_width // 2, -self.scene_height // 2, self.scene_width, self.scene_height)

        self.setBackgroundBrush(self._color_background)
        pass

    def drawBackground(self, painter, rect):
        super().drawBackground(painter, rect)

        # here we create our grid
        left = int(np.floor(rect.left()))
        right = int(np.ceil(rect.right()))
        top = int(np.floor(rect.top()))
        bottom = int(np.ceil(rect.bottom()))

        first_left = left - (left % self.gridSize)
        first_top = top - (top % self.gridSize)

        # compute all lines to be drawn
        lines_light, lines_dark = [], []
        for x in range(first_left, right, self.gridSize):
            if (x % (self.gridSize * self.gridSquares)) != 0:
                lines_light.append(QLine(x, top, x, bottom))
            else:
                lines_dark.append(QLine(x, top, x, bottom))

        for y in range(first_top, bottom, self.gridSize):
            if (y % (self.gridSize * self.gridSquares)) != 0:
                lines_light.append(QLine(left, y, right, y))
            else:
                lines_dark.append(QLine(left, y, right, y))

        # draw the lines
        painter.setPen(self._pen_light)
        painter.drawLines(lines_light)

        painter.setPen(self._pen_dark)
        painter.drawLines(lines_dark)


class MainViewMouseEventHandler:
    def __init__(self, parent):
        self.parent = parent

    def middle_mouse_button_press(self, event):
        release_event = QMouseEvent(QEvent.MouseButtonRelease, event.localPos(), event.screenPos(),
                                   Qt.LeftButton, Qt.NoButton, event.modifiers())
        self.parent.super_mouse_release(release_event)
        self.parent.setDragMode(QGraphicsView.ScrollHandDrag)

        fake_event = QMouseEvent(event.type(), event.localPos(), event.screenPos(),
                                Qt.LeftButton, event.buttons() | Qt.LeftButton, event.modifiers())
        self.parent.super_mouse_press(fake_event)

    def middle_mouse_button_release(self, event):
        fake_event = QMouseEvent(event.type(), event.localPos(), event.screenPos(),
                                Qt.LeftButton, event.buttons() & ~Qt.LeftButton, event.modifiers())
        self.parent.super_mouse_release(fake_event)
        self.parent.setDragMode(QGraphicsView.NoDrag)
        
    def left_mouse_button_press(self, event):
        print("left clicked")
        # mg = event.globalPos()
        mg = event.localPos()
        print([mg.x(), mg.y()])
        print(self.parent.mapFromScene(mg.x(), mg.y()))     # get point in the view not scene/windows
        self.parent.super_mouse_press(event)
        
    def left_mouse_button_release(self, event):
        print("released Lefty")
        mg = event.globalPos()
        print(mg)
        self.parent.super_mouse_release(event)

    def right_mouse_button_press(self, event):
        self.parent.super_mouse_press(event)

    def right_mouse_button_release(self, event):
        self.parent.super_mouse_release(event)
        pass

    def wheel_event(self, event):
        zoom_out = 1 / self.parent.zoom_in
        if event.angleDelta().y() > 0 or event.angleDelta().x() > 0:
            zoom_factor = self.parent.zoom_in
            self.parent.zoom += self.parent.zoom_step
        else:
            zoom_factor = zoom_out
            self.parent.zoom -= self.parent.zoom_step

        if self.parent.zoom_range[0] < self.parent.zoom < self.parent.zoom_range[1]:
            self.parent.scale(zoom_factor, zoom_factor)
        elif self.parent.zoom_range[0] > self.parent.zoom:
            self.parent.zoom = self.parent.zoom_range[0]
        elif self.parent.zoom > self.parent.zoom_range[1]:
            self.parent.zoom = self.parent.zoom_range[1]


class MainViewContextMenu:
    def __init__(self, parent):
        self.parent = parent
        self.newAction = QAction(parent)
        self.newAction.setText("&New")
        self.newAction.setIcon(QIcon(":file-new.svg"))
        self.openAction = QAction(QIcon(":file-open.svg"), "&Open...", parent)

        self.addAction = QAction(parent, triggered=self.add_node)
        self.addAction.setText("&Add")
        self.addAction.setIcon(QIcon(":file-new.svg"))
        self.addAction.setStatusTip("Add Node")
        self.addAction.setToolTip("Add Node")
        self.addAction.setShortcuts(QKeySequence.SelectAll)

        self.clear_action = QAction(parent, triggered=self.turn_off_all_socks)
        self.clear_action.setText("&Clear Selected Sockets")
        self.clear_action.setIcon(QIcon(":file-new.svg"))
        self.clear_action.setStatusTip("Clear Selected Items")
        self.clear_action.setToolTip("Clear Selected Items")
        self.clear_action.setShortcuts(QKeySequence(str("Ctrl+d")))

        self.del_action = QAction(parent, triggered=self.turn_off_all_socks)
        self.del_action.setText("&Delete Selected")
        self.del_action.setIcon(QIcon(":file-new.svg"))
        self.del_action.setStatusTip("Delete Selected Items")
        self.del_action.setToolTip("Delete Selected Items")
        self.saveAction = QAction(QIcon(":file-save.svg"), "&Save", parent)
        self.exitAction = QAction("&Exit", parent)
        self.menu = None
        self.mouse_me = [0, 0]

    def add_node(self):
        node1 = RectangleNodeUI()
        s = [self.parent.size().width(), self.parent.size().height()]
        p = QPoint(self.mouse_me.x() - (s[0] / 2), self.mouse_me.y() - (s[1] / 2))
        node1.setPos(p)
        self.parent.mnScene.addItem(node1)
        pass

    def delete_selected_node(self):
        print("delete")

    def turn_off_all_socks(self):
        pass

    def popup(self, event):
        self.mouse_me = event.pos()
        # Creating a menu object with the central widget as parent
        self.menu = QMenu(self.parent)
        # Populating the menu with actions
        self.menu.addAction(self.addAction)
        self.menu.addAction(self.clear_action)
        self.menu.addSeparator()
        self.menu.addAction(self.clear_action)
        self.menu.addSeparator()
        self.menu.addAction(self.newAction)
        self.menu.addAction(self.openAction)
        self.menu.addAction(self.saveAction)
        self.menu.setToolTipsVisible(True)
        self.menu.setShortcutEnabled(True)
        # self.menu.hovered.connect(self.handleMenuHovered)
        # Launching the menu
        self.menu.exec(event.globalPos())


class MainView(QGraphicsView):
    def __init__(self, main_scene, parent=None):
        super().__init__(parent)
        self.mnScene: MainScene = main_scene
        self.setup_view()
        self.setScene(self.mnScene)
        self.zoom_in = 1.25
        self.zoom = 5
        self.default_zoom = 5
        self.zoom_step = 0.5
        self.zoom_range = [1, 10]
        self.mouse = MainViewMouseEventHandler(self)
        self.context_menu = MainViewContextMenu(self)

    def setup_view(self):
        self.setRenderHints(
            QPainter.RenderHint.Antialiasing | QPainter.RenderHint.TextAntialiasing | QPainter.RenderHint.SmoothPixmapTransform)
        self.setViewportUpdateMode(QGraphicsView.ViewportUpdateMode.FullViewportUpdate)
        self.setCacheMode(QGraphicsView.CacheModeFlag.CacheBackground)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.setTransformationAnchor(QGraphicsView.ViewportAnchor.AnchorUnderMouse)

    def reset_zoom(self):
        t = self.transform()
        mx = np.asarray([[t.m11(), t.m12(), t.m13()],[t.m21(), t.m22(), t.m23()], [t.m31(), t.m32(), t.m33()]])
        self.zoom = self.default_zoom
        self.scale(1/mx[0, 0], 1/mx[1, 1])

    def contextMenuEvent(self, event):
        self.context_menu.popup(event)

    def super_mouse_press(self, press_event):
        return super().mousePressEvent(press_event)

    def super_mouse_release(self, press_event):
        return super().mouseReleaseEvent(press_event)

    def wheelEvent(self, event):
        self.mouse.wheel_event(event)
        print(self.zoom)

    def mousePressEvent(self, event):
        if event.button() == Qt.MiddleButton:
            self.mouse.middle_mouse_button_press(event)
        elif event.button() == Qt.LeftButton:
            self.mouse.left_mouse_button_press(event)
        elif event.button() == Qt.RightButton:
            self.mouse.right_mouse_button_press(event)
        else:
            super().mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.MiddleButton:
            self.mouse.middle_mouse_button_release(event)
        elif event.button() == Qt.LeftButton:
            self.mouse.left_mouse_button_release(event)
        elif event.button() == Qt.RightButton:
            self.mouse.right_mouse_button_release(event)
        else:
            super().mouseReleaseEvent(event)


class MainWindow(QWidget):
    def __init__(self, parent=None, setup_run=False):
        super().__init__(parent)
        self.setWindowTitle("World of Nodes")
        self.quick_access_layout = QVBoxLayout()
        self.quick_access_layout.setSpacing(0)
        self.quick_access_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.quick_access_layout)
        self.ui_scene = None
        self.view = None
        self.menu_bar = None
        self.nodes = {}
        if setup_run:
            self.setup_ui()
        self.anw = None
        QApplication.instance().setStyleSheet(NodeDefaults.style[Tags.qss])

    def setup_ui(self):
        print("MainWindow: setup_ui -> Start")

        self.setGeometry(200, 200, 1280, 720)
        self.ui_scene = MainScene()
        self.view = MainView(self.ui_scene, self)
        self.menu_bar = MainMenuBar(view=self.view)
        self.quick_access_layout.setMenuBar(self.menu_bar)
        self.quick_access_layout.addWidget(self.view)
        print("MainWindow: setup_ui -> End")

    def set_context_menu(self, menu):
        self.view.context_menu = menu

    def add_node(self, namme, node):
        try:
            a = self.nodes[namme]
            if a is not None:
                self.nodes[namme+"_"] = node
        except KeyError:
            self.nodes[namme] = node
        if isinstance(node, RectangleNodeUI):
            self.ui_scene.addItem(node)
        else:
            x = node.ui
            self.ui_scene.addItem(x)
