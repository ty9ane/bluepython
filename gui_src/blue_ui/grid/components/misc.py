from enum import Enum


class CoolColours(Enum):
    blue = "#2668FF"


class Tags(Enum):
    w = "Width"
    h = "height"
    edge = "edge"
    padding = "padding"
    title_h = "Title Height"
    title = "Title"
    size = "size"
    name = "name"
    qss = "qss"
    colour_bg = "colour background"
    colour_ol = "colour outline"
    colour_active = "colour active"
    input = "input"
    output = "output"
    pos = "pos"

    LEFT_TOP = 1
    LEFT_BOTTOM = 2
    RIGHT_TOP = 3
    RIGHT_BOTTOM = 4

    EDGE_TYPE_DIRECT = 1
    EDGE_TYPE_BEZIER = 2


class NodeDefaults(Enum):
    size = {Tags.w: 180, Tags.h: 238, Tags.edge: 10, Tags.title_h: 30, Tags.padding: 5}
    font = {Tags.name: "Verdana", Tags.size: 10}
    style = {Tags.qss: "ContentWidget { background: transparent; }\n"
                       "ContentWidget QLabel { color: #6d90ab;  }"}
    socket = {Tags.colour_bg: "#506670", Tags.colour_ol: "#1a313b", Tags.colour_active: "#36afe3",
              Tags.size: 6, Tags.edge: 1}

    def __getitem__(self, indices):
        return self.value[indices]