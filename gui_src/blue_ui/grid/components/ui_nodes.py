from PySide6.QtWidgets import *
from PySide6.QtGui import *
from PySide6.QtCore import *
import numpy as np
from blue_ui.grid.components.misc import Tags, NodeDefaults, CoolColours


class QDMGraphicsEdge(QGraphicsPathItem):
    def __init__(self, edge, parent=None):
        super().__init__(parent)

        self.edge = edge

        self._color = QColor("#001000")
        self._color_selected = QColor("#00ff00")
        self._pen = QPen(self._color)
        self._pen_selected = QPen(self._color_selected)
        self._pen.setWidthF(5.0)
        self._pen_selected.setWidthF(7.5)

        self.setFlag(QGraphicsItem.ItemIsSelectable)

        self.setZValue(-1)
        self.posSource = [0, 0]
        self.posDestination = [200, 100]

    def setSource(self, x, y):
        self.posSource = [x, y]

    def setDestination(self, x, y):
        self.posDestination = [x, y]

    def paint(self, painter, QStyleOptionGraphicsItem, widget=None):
        self.updatePath()
        painter.setPen(self._pen if not self.isSelected() else self._pen_selected)
        painter.setBrush(Qt.NoBrush)
        painter.drawPath(self.path())

    def updatePath(self):
        """ Will handle drawing QPainterPath from Point A to B """
        raise NotImplemented("This method has to be overriden in a child class")


class QDMGraphicsEdgeDirect(QDMGraphicsEdge):
    def updatePath(self):
        path = QPainterPath(QPointF(self.posSource[0], self.posSource[1]))
        path.lineTo(self.posDestination[0], self.posDestination[1])
        self.setPath(path)


class QDMGraphicsEdgeBezier(QDMGraphicsEdge):
    def updatePath(self):
        s = self.posSource
        d = self.posDestination

        dist = (d[0] - s[0]) * 0.5
        if s[0] > d[0]: dist *= -1

        path = QPainterPath(QPointF(self.posSource[0], self.posSource[1]))
        path.cubicTo(s[0] + dist, s[1], d[0] - dist, d[1], self.posDestination[0], self.posDestination[1])
        self.setPath(path)


class Edge:
    DEBUG = False

    def __init__(self, scene, start_socket, end_socket, line_type=Tags.EDGE_TYPE_DIRECT):

        self.scene = scene
        print("Edge start")
        self.start_socket = start_socket
        if self.start_socket is not None or self.end_socket is not None:
            self.start_socket.setConnectedEdge(self)
        print("Edge end")
        self.end_socket = end_socket
        if self.start_socket is not None or self.end_socket is not None:
            self.end_socket.setConnectedEdge(self)
        self.line_type = line_type
        print("Edge graphics")
        if self.line_type == Tags.EDGE_TYPE_DIRECT:
            self.grEdge = QDMGraphicsEdgeDirect(self)
        elif self.line_type == Tags.EDGE_TYPE_BEZIER:
            self.grEdge = QDMGraphicsEdgeBezier(self)
        print(" Edge pos")
        if self.start_socket is not None and self.end_socket is not None:
            self.updatePositions()

        print("add to scene")
        self.scene.addItem(self.grEdge)

    def updatePositions(self, mouse=None):
        source_pos = np.asarray(self.start_socket.getSocketPosition())
        # print(self.start_socket.node.grNode.pos())
        # print(source_pos)
        # source_pos[0] += self.start_socket.node.grNode.pos().x()
        # source_pos[1] += self.start_socket.node.grNode.pos().y()
        self.grEdge.setSource(*source_pos)
        if self.end_socket is not None:
            end_pos = np.asarray(self.end_socket.getSocketPosition())
            # end_pos[0] += self.end_socket.node.grNode.pos().x()
            # end_pos[1] += self.end_socket.node.grNode.pos().y()
            self.grEdge.setDestination(*end_pos)
        else:
            self.grEdge.setDestination(*mouse)

        if Edge.DEBUG: print(" SS:", self.start_socket)
        if Edge.DEBUG: print(" ES:", self.end_socket)
        self.grEdge.update()

    def remove_from_sockets(self):
        if self.start_socket is not None:
            self.start_socket.edge = None
        if self.end_socket is not None:
            self.end_socket.edge = None
        self.end_socket = None
        self.start_socket = None

    def remove(self):
        self.remove_from_sockets()
        self.scene.grScene.removeItem(self.grEdge)
        self.grEdge = None
        self.scene.removeEdge(self)


class Socket:
    DEBUG = False
    INPUT = 1
    OUTPUT = 2

    def __init__(self, node, io, s_type, index=0, position=Tags.LEFT_TOP):
        self.node = node
        self.io = io
        self.s_type = s_type
        self.index = index
        self.position = position
        self.is_active = False
        self.grSocket = SocketUI(self, self.node.ui)
        self.grSocket.setPos(*self.node.getSocketPosition(index, position))
        self.edge = []
        self.is_active_lock = False

    def getSocketPosition(self):
        if Socket.DEBUG: print("  GSP: ", self.index, self.position, "node:", self.node)
        res = self.node.getSocketPosition(self.index, self.position)
        #print(self.node.title)
        # print("soc pos " + str(self.grSocket.pos()))
        if Socket.DEBUG: print("  res", res)
        return res

    def setConnectedEdge(self, edge=None, is_active_lock=True):
        print("sock lock")
        self.is_active_lock = is_active_lock
        self.is_active = True
        print("sock active profile")
        self.grSocket.active_profile()
        print("sock append")
        self.edge.append(edge)
        print("sock return")

    def update(self):
        for e in self.edge:
            e.updatePositions()


class SocketUI(QGraphicsItem):
    def __init__(self, sock, parent=None):
        super().__init__(parent)
        self.sock = sock
        self.radius = NodeDefaults.socket[Tags.size]
        self.outline_width = NodeDefaults.socket[Tags.edge]
        self._color_background = QColor(NodeDefaults.socket[Tags.colour_bg])
        self._color_outline = QColor(NodeDefaults.socket[Tags.colour_ol])

        self._pen = QPen(self._color_outline)
        self._pen.setWidthF(self.outline_width)
        self._brush = QBrush(self._color_background)

    def active_profile(self):
        self._color_background = QColor(NodeDefaults.socket[Tags.colour_active])
        self._brush = QBrush(self._color_background)
        self.update()

    def inactive_profile(self):
        if not self.sock.is_active_lock:
            self.sock.is_active = False
            self._color_background = QColor(NodeDefaults.socket[Tags.colour_bg])
            self._brush = QBrush(self._color_background)
            self.update()

    def paint(self, painter, QStyleOptionGraphicsItem, widget=None):
        # painting circle
        painter.setBrush(self._brush)
        painter.setPen(self._pen)
        painter.drawEllipse(-self.radius, -self.radius, 2 * self.radius, 2 * self.radius)

    def boundingRect(self):
        return QRectF(
            - self.radius - self.outline_width,
            - self.radius - self.outline_width,
            2 * (self.radius + self.outline_width),
            2 * (self.radius + self.outline_width),
        )


class RectangleNodeUI(QGraphicsItem):
    def __init__(self, parent=None, w=None, h=None):
        super().__init__(parent)
        self._title_color = Qt.white
        self._title_font = QFont(NodeDefaults.font[Tags.name], NodeDefaults.font[Tags.size])
        self._title = ""
        self.width = NodeDefaults.size[Tags.w]
        self.height = NodeDefaults.size[Tags.h]
        if w is not None:
            self.width = w
        if h is not None:
            self.height = h
        self.edge_size = NodeDefaults.size[Tags.edge]
        self.title_height = NodeDefaults.size[Tags.title_h]
        self._padding = NodeDefaults.size[Tags.padding]

        self._pen_default = QPen(QColor("#7F000000"))
        self._pen_selected = QPen(QColor("#FFFFA637"))
        self.title_back_colour = QColor(CoolColours.blue.value)
        self._brush_title = QBrush(self.title_back_colour)
        self._brush_background = QBrush(QColor("#E3212121"))
        self.title_item: QGraphicsTextItem = None
        self.gr_content = None
        self.content = None

        self.init_title()
        self.title = "Hoi Wrâld"
        self.set_up_ui()

    @property
    def title_background_colour(self):
        return self.title_back_colour

    @title_background_colour.setter
    def title_background_colour(self, cool_colours=None):
        if cool_colours is None:
            return
        if isinstance(cool_colours, CoolColours):
            self.title_back_colour = QColor(cool_colours.value)
            self._brush_title = QBrush(self.title_back_colour)
        elif isinstance(cool_colours, str):
            self.title_back_colour = QColor(cool_colours)
            self._brush_title = QBrush(self.title_back_colour)

    @property
    def padding(self):
        return self._padding

    @property
    def title(self): return self._title

    @title.setter
    def title(self, value):
        self._title = value
        self.title_item.setPlainText(self._title)

    def boundingRect(self):
        return QRectF(
            0,
            0,
            2 * self.edge_size + self.width,
            2 * self.edge_size + self.height
        ).normalized()

    def set_up_ui(self):
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setFlag(QGraphicsItem.ItemIsMovable)

    def init_title(self):
        self.title_item = QGraphicsTextItem(parent=self)
        self.title_item.setParentItem(self)
        self.title_item.setDefaultTextColor(self._title_color)
        self.title_item.setFont(self._title_font)
        self.title_item.setPos(self._padding, self._padding/2)
        self.title_item.setTextWidth(
            self.width
            - 2 * self._padding
        )

    def setup_content(self):
        self.gr_content = QGraphicsProxyWidget(self)
        self.content.setGeometry(self.edge_size, self.title_height + self.edge_size,
                                 self.width - 2 * self.edge_size, self.height - 2 * self.edge_size - self.title_height)
        self.gr_content.setWidget(self.content)

    def paint(self, painter, QStyleOptionGraphicsItem, widget=None):
        # if self.isSelected():
        #     self.node.update_edges()
        # title
        path_title = QPainterPath()
        path_title.setFillRule(Qt.WindingFill)
        path_title.addRoundedRect(0,0, self.width, self.title_height, self.edge_size, self.edge_size)
        path_title.addRect(0, self.title_height - self.edge_size, self.edge_size, self.edge_size)
        path_title.addRect(self.width - self.edge_size, self.title_height - self.edge_size, self.edge_size, self.edge_size)
        painter.setPen(Qt.NoPen)
        painter.setBrush(self._brush_title)
        painter.drawPath(path_title.simplified())

        # content
        path_content = QPainterPath()
        path_content.setFillRule(Qt.WindingFill)
        path_content.addRoundedRect(0, self.title_height, self.width, self.height - self.title_height, self.edge_size, self.edge_size)
        path_content.addRect(0, self.title_height, self.edge_size, self.edge_size)
        path_content.addRect(self.width - self.edge_size, self.title_height, self.edge_size, self.edge_size)
        painter.setPen(Qt.NoPen)
        painter.setBrush(self._brush_background)
        painter.drawPath(path_content.simplified())

        # outline
        path_outline = QPainterPath()
        path_outline.addRoundedRect(0, 0, self.width, self.height, self.edge_size, self.edge_size)
        painter.setPen(self._pen_default if not self.isSelected() else self._pen_selected)
        painter.setBrush(Qt.NoBrush)
        painter.drawPath(path_outline.simplified())


class Node:
    def __init__(self, gr, title="Undefined Node", inputs=[], outputs=[]):
        self.title = title
        self.inputs_sockets = []
        self.outputs_sockets = []
        self.is_pressed: bool = False
        self.grNode = gr
        self.socket_spacing = 22
        counter = 0
        for item in inputs:
            socket = Socket(node=self, io=item, s_type=Socket.INPUT, index=counter, position=Tags.LEFT_BOTTOM)
            counter += 1
            self.inputs_sockets.append(socket)

        counter = 0
        for item in outputs:
            socket = Socket(node=self, io=item, s_type=Socket.OUTPUT, index=counter, position=Tags.RIGHT_TOP)
            counter += 1
            self.outputs_sockets.append(socket)

        self.is_pressed = False

    def set_pos(self, x, y):
        self.grNode.setPos(x, y)

    def getSocketPosition(self, index, position):
        x = 0 if (position in (Tags.LEFT_TOP, Tags.LEFT_BOTTOM)) else self.grNode.width

        if position in (Tags.LEFT_BOTTOM, Tags.RIGHT_BOTTOM):
            # start from bottom
            y = self.grNode.height - self.grNode.edge_size - self.grNode.padding - index * self.socket_spacing
        else:
            # start from top
            y = self.grNode.title_height + self.grNode.padding + self.grNode.edge_size + index * self.socket_spacing
        return [x + self.pos_as_list[0], y + self.pos_as_list[1]]

    def receive_the_parcel(self, delivery: dict):
        pass

    def pass_the_parcel(self):
        pass

    @property
    def content(self):
        return self.grNode.content

    @content.setter
    def content(self, c):
        self.grNode.content = c
        self.grNode.content.set_node(self)

    @property
    def ui(self):
        return self.grNode

    @property
    def pressed(self):
        return self.is_pressed

    @pressed.setter
    def pressed(self, boo):
        self.is_pressed = boo

    @property
    def pos(self):
        return self.grNode.pos()

    @property
    def pos_as_list(self):
        po = self.pos
        return [po.x(), po.y()]

    @property
    def pos_as_numpy(self):
        return np.asarray(self.pos_as_list)