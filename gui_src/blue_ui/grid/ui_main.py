import sys
from PySide6.QtWidgets import QApplication
from gui_src.blue_ui.grid.components.ui_components_main import MainWindow
from blue_ui.grid.components.ui_nodes import RectangleNodeUI

if __name__ == "__main__":
    app = QApplication(sys.argv)
    screen = app.screens()
    for s in screen:
        dpi = s.logicalDotsPerInch()
        print(dpi)
    w = MainWindow(setup_run=True)
    w.add_node("Default Node", RectangleNodeUI())
    w.show()
    sys.exit(app.exec())
