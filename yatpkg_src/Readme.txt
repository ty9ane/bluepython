***This package is a collection of tools that may or may not be useful***

To generate an install able wheel run:
python setup.py sdist bdist_wheel

Then to install:
python -m pip install <location of whl>\yatpkg_ytedy-<version number>-py3-none-any.whl
i.e.
python -m pip install C:\Users\tyeu008\Documents\Repos\bluepython\yatpkg_src\dist\yatpkg_ytedy-0.0.3-py3-none-any.whl

dependencies = ['pandas', 'scipy', 'numpy', 'scikit-learn', 'Pillow', 'vtk', 'h5py', 'tables', 'PyQt5', 'scikit-kinematics', 'tsfresh', 'numpy-stl', 'pymesh']
