import numpy as np
import pandas as pd


def read(filename):
    f = open(filename, "r")
    stream = "hoi, allo, moshi moshi?"
    data_str = []
    while len(stream) > 0:
        stream = f.readline()
        data_str.append(stream)
    node_count = 0
    for i in range(0, len(data_str)):
        if 'Node' in data_str[i]:
            node_count += 1
    data_mat = np.zeros([node_count, 3])
    j = 0
    for i in range(0, len(data_str)):
        if 'Node' in data_str[i]:
            data_mat[j, 0] = float(data_str[i + 1].strip())
            data_mat[j, 1] = float(data_str[i + 2].strip())
            data_mat[j, 2] = float(data_str[i + 3].strip())
            j += 1
    ret = pd.DataFrame(data=data_mat, columns=['x', 'y', 'z'])
    file_out = "{0}.csv".format(filename[:filename.rindex('.')])
    ret.to_csv(file_out, index=False)
    pass


if __name__ == '__main__':
    read('C:/Users/ty8on/Downloads/scapula/scapula.exnode')
    pass