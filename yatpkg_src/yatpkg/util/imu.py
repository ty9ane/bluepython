import pandas as pd

import yatpkg.util.mos as tools
from yatpkg.util.data import StorageIO, StorageType, resample, Yatsdo
from yatpkg.math.filters import process_data, Butterworth
import skinematics.imus as sk
import numpy as np
from yatpkg.math.transformation import Quaternion, PCAModel
import os
import matplotlib.pyplot as plt
import scipy.stats as stats
from enum import Enum
import scipy.stats as stats
from scipy.signal import find_peaks

from multiprocessing import Pool


class Direction(Enum):
    Left = 0,
    Right = 1


class Worker(object):
    def __init__(self, thread_id, name, cache):
        self.thread_id = thread_id
        self.name = name
        self.cache = cache

    def run(self):
        return 0


class WorkerMadgwick(Worker):
    # For the orientation determined by the madgwick filter
    # The imu should be in xyz rotation order i.e. a = euler, a[0] = yaw/z, a[1] = pitch/x, a[2] = roll/y,
    # but for the foot it is same xyz rotation order but a = euler, a[0] = yaw/z, a[1] = roll/y, a[2] = pitch/x,
    def __init__(self, thread_id, name, cache):
        super().__init__(thread_id, name, cache)

    def run(self):
        acc = self.cache['acc']
        gyr = self.cache['gyr']
        imu = self.cache['imu']
        d = []
        for i in range(0, acc.shape[0]):
            imu.Update(gyr[i, :], acc[i, :], np.array([0, 0, 0]))
            d.append(imu.Quaternion)
        return np.array(d)


class WorkerStatic(Worker):
    def __init__(self, thread_id, name, cache):
        super().__init__(thread_id, name, cache)

    def run(self):
        gyr = self.cache['gyr']
        step = self.cache['step']
        thres = self.cache['thres']
        stdv = [np.nanstd([np.linalg.norm(gyr[i, :]) for i in range(0, j)]) for j in range(200, gyr.shape[0], step)]
        frames = [int(i * step) + 200 for i in range(0, len(stdv) - 1) if stdv[i] < (thres * (np.pi / 180))]
        return frames[-1]


class WorkerRemoveYaw(Worker):
    def __init__(self, thread_id, name, cache):
        super().__init__(thread_id, name, cache)

    def run(self):
        ori = self.cache['ori']
        # Create helper Quaternion class
        imus_ori_q_class = [Quaternion(ori[i, :]) for i in range(0, ori.shape[0])]
        # to euler
        imus_ori_ypr = np.array(
            [imus_ori_q_class[i].to_euler('xyz') for i in range(0, ori.shape[0])])
        # remove yaw
        temp = [[0, imus_ori_ypr[i][1], imus_ori_ypr[i][2]] for i in range(0, imus_ori_ypr.shape[0])]

        # recreate helper Quaternion class
        imus_ori_tilt_q = [Quaternion.create_from_euler('xyz', temp[i]) for i in
                                range(0, imus_ori_ypr.shape[0])]
        displ = [imus_ori_tilt_q[i].to_array() for i in range(0, imus_ori_ypr.shape[0])]
        return {"q_class": imus_ori_tilt_q, "q_array": np.array(displ)}


class WorkerRemoveGravity(Worker):
    def __init__(self, thread_id, name, cache):
        super().__init__(thread_id, name, cache)

    def run(self):
        ori = self.cache['ori']
        accs = self.cache['acc']
        gyrs = self.cache['gyr']
        acc_rmg = np.zeros([accs.shape[0], 3])
        gyr_g = np.zeros([gyrs.shape[0], 3])
        accq0 = np.zeros([accs.shape[0], 3])
        for i in range(0, accs.shape[0]):
            acc = accs[i, :]
            gyr = gyrs[i, :]
            acca = Quaternion([0, acc[0], acc[1], acc[2]], is_rotation=False)
            gyra = Quaternion([0, gyr[0], gyr[1], gyr[2]], is_rotation=False)
            imu_inv = ori[i].inverse()
            acc_qrm = imu_inv.conjugate() * (acca * imu_inv)
            gyr_qrm = imu_inv.conjugate() * (gyra * imu_inv)
            accq = acc_qrm.to_array()
            gyrq = gyr_qrm.to_array()
            accq0[i, :] = accq[1:4]
            gyr_g[i, :] = gyrq[1:4]
            acc_rmg[i, :] = accq[1:4] - np.array([0, 0, 9.81])
        return {"acc": acc_rmg, "gyr": gyr_g}


def job_to_do(worker):
    return [worker.name, worker.run_single()]


class ScaleFactors(Enum):
    mm2m = 0.001
    m2mm = 1000
    deg2rad = np.pi/180
    rad2deg = 180/np.pi


class BasicIMUFilter:
    @staticmethod
    def madgwick_mag(imus, imus_resampled, imu_labels, imus_madgwick, m, t_rate):
        workers = []
        for imu in imus:
            acc = imus_resampled[imu][imu_labels[imu]['acc']].to_numpy()
            gyr = imus_resampled[imu][imu_labels[imu]['gyr']].to_numpy()
            gyr = np.matmul(np.diag([np.pi / 180, np.pi / 180, np.pi / 180]), gyr.transpose()).transpose()
            im = imus_madgwick[imu]
            job = {'acc': acc[:int(m / 2), :], 'gyr': gyr[:int(m / 2), :], 'imu': im}
            worker = WorkerMadgwick(1, imu, job)
            workers.append(worker)

        with Pool() as pool:
            result = pool.map(job_to_do, workers)
        return result

    @staticmethod
    def madgwick(imus, imus_resampled, imu_labels, imus_madgwick, m, t_rate):
        # Madgwick first pass
        workers = []
        for imu in imus:
            acc = imus_resampled[imu][imu_labels[imu]['acc']].to_numpy()
            gyr = imus_resampled[imu][imu_labels[imu]['gyr']].to_numpy()
            gyr = np.matmul(np.diag([np.pi / 180, np.pi / 180, np.pi / 180]), gyr.transpose()).transpose()
            im = imus_madgwick[imu]
            job = {'acc': acc[:int(m / 2), :], 'gyr': gyr[:int(m / 2), :], 'imu': im}
            worker = WorkerMadgwick(1, imu, job)
            workers.append(worker)

        with Pool() as pool:
            result = pool.map(job_to_do, workers)

        imus_ori = {}
        for i in range(0, len(result)):
            q = result[i][1][-1, :]
            imus_madgwick[result[i][0]] = sk.Madgwick(rate=t_rate, Beta=0.99, Quaternion=q)
            imus_ori[result[i][0]] = result[i][1]

        # Madgwick second pass
        workers = []
        for imu in imus:
            acc = imus_resampled[imu][imu_labels[imu]['acc']].to_numpy()
            gyr = imus_resampled[imu][imu_labels[imu]['gyr']].to_numpy()
            gyr = np.matmul(np.diag([np.pi / 180, np.pi / 180, np.pi / 180]), gyr.transpose()).transpose()
            im = imus_madgwick[imu]
            job = {'acc': acc[:int(m / 2), :], 'gyr': gyr[:int(m / 2), :], 'imu': im}
            worker = WorkerMadgwick(1, imu, job)
            workers.append(worker)

        with Pool() as pool:
            result = pool.map(job_to_do, workers)

        # Madgwick Final pass
        imus_ori = {}
        for i in range(0, len(result)):
            q = result[i][1][-1, :]
            imus_madgwick[result[i][0]] = sk.Madgwick(rate=t_rate, Beta=0.45, Quaternion=q)
            imus_ori[result[i][0]] = result[i][1]

        workers = []
        for imu in imus:
            acc = imus_resampled[imu][imu_labels[imu]['acc']].to_numpy()
            gyr = imus_resampled[imu][imu_labels[imu]['gyr']].to_numpy()
            gyr = np.matmul(np.diag([np.pi / 180, np.pi / 180, np.pi / 180]), gyr.transpose()).transpose()
            im = imus_madgwick[imu]
            job = {'acc': acc, 'gyr': gyr, 'imu': im}
            worker = WorkerMadgwick(1, imu, job)
            workers.append(worker)

        with Pool() as pool:
            result = pool.map(job_to_do, workers)
        return result

    @staticmethod
    def find_static(imus, imus_resampled, imu_labels):
        workers = []
        for imu in imus:
            gyr = imus_resampled[imu][imu_labels[imu]['gyr']].to_numpy()
            gyr = np.matmul(np.diag([np.pi / 180, np.pi / 180, np.pi / 180]), gyr.transpose()).transpose()
            job = {'gyr': gyr, 'step': 200}
            worker = WorkerStatic(1, imu, job)
            workers.append(worker)

        with Pool() as pool:
            result = pool.map(job_to_do, workers)
        print(result)
        static_poll = [result[i][1] for i in range(0, len(result))]
        m = stats.mode(static_poll)[0][0]
        return m

    @staticmethod
    def run(s, para):
        print("BasicIMUFilter")
        cs = para['linear_scale']
        rs = para['rotation_scale']
        s.find_dt()
        imus = s.separate(para['column_labels'], first_col=para['first_col'])
        imus_filtered = {}
        imus_resampled = {}
        # butterworth filter and downsample
        t_rate = para['target_rate']
        for imu in imus:
            imus_filtered[imu] = process_data(imus[imu], Butterworth.butter_low_pass,
                                              para=[para["butterworth_low_cutoff"], 1/s.info['dt']])
            imus_resampled[imu] = resample(imus_filtered[imu], t_rate)

        # Sort data to acc and gyro
        accl = para['acc_label']
        gyrl = para['gyr_label']
        imus_madgwick = {}
        imu_labels = {}
        for imu in imus:
            imus_madgwick[imu] = sk.Madgwick(rate=t_rate, Beta=0.99)
            temp = {}
            acc_ = []
            gyr_ = []
            for c in imus_resampled[imu].columns:
                for a in accl:
                    if c.lower().endswith(a.lower()):
                        acc_.append(c)
                for a in gyrl:
                     if c.lower().endswith(a.lower()):
                        gyr_.append(c)
            temp['acc'] = sorted(acc_)
            temp['gyr'] = sorted(gyr_)
            imu_labels[imu] = temp

        # find static
        print("Find Static")
        static_thres = para['static_threshold']
        m = BasicIMUFilter.find_static(imus, imus_resampled, imu_labels)

        print("Run Madgwick")
        result = BasicIMUFilter.madgwick(imus, imus_resampled, imu_labels, imus_madgwick, m, t_rate)

        # extract results
        imus_ori = {}
        workers = []
        for i in range(0, len(result)):
            imus_ori[result[i][0]] = result[i][1]
            job = {'ori': imus_ori[result[i][0]]}
            worker = WorkerRemoveYaw(1, result[i][0], job)
            workers.append(worker)
        print("Madgwick Done")

        pass
        print("Remove Yaw")
        # Remove yaw and recompose Quaternion()
        with Pool() as pool:
            result = pool.map(job_to_do, workers)

        imus_ori_q_removed = {}
        for i in range(0, len(result)):
            imus_ori_q_removed[result[i][0]] = result[i][1]
        print("Yaw Removed")

        # remove gravity from acceleration
        print("Remove gravity")

        workers = []
        for imu in imus:
            accs = imus_resampled[imu][imu_labels[imu]['acc']].to_numpy()
            accs = np.matmul(np.diag([cs.value, cs.value, cs.value]), accs.transpose()).transpose()
            gyr = imus_resampled[imu][imu_labels[imu]['gyr']].to_numpy()
            gyr = np.matmul(np.diag([rs.value, rs.value, rs.value]), gyr.transpose()).transpose()
            q = imus_ori_q_removed[imu]['q_class']
            job = {'ori': q, 'acc': accs, 'gyr': gyr}
            worker = WorkerRemoveGravity(1, imu, job)
            workers.append(worker)

        with Pool() as pool:
            result = pool.map(job_to_do, workers)

        imus_acc_rmg = {}
        imus_gyr_g = {}
        for i in range(0, len(result)):
            imus_acc_rmg[result[i][0]] = result[i][1]["acc"]
            imus_gyr_g[result[i][0]] = result[i][1]["gyr"]

        print("Gravity Removed")
        return {'acc_g_removed': imus_acc_rmg,
                'gyr_g_adjusted': imus_gyr_g,
                'imus': imus}


class BlueTridentTools(BasicIMUFilter):
    @staticmethod
    def quick_combine_and_filter(low_g, mag, target_rate=100, low_pass=[6, 6], remove_jitter=False, out_file=None):
        """
        This method combines the low_g and mag data into one data frame as well as downsampling the data
        to a target sampling rate.
        :param low_g:
        :param mag:
        :param target_rate:
        :param low_pass:
        :param remove_jitter:
        :param out_file:
        :return:
        """
        s0 = StorageIO.load(
            low_g,
            StorageType.csv)
        s1 = StorageIO.load(
            mag,
            StorageType.csv)
        rate = target_rate
        df0 = s0.to_yatsdo(remove_jitter)
        a = df0.filter(Butterworth.butter_low_pass, para=[low_pass[0], 1 / s0.info['dt']])
        df1 = s1.to_yatsdo(remove_jitter)
        b = df1.filter(Butterworth.butter_low_pass, para=[low_pass[1], 1 / s1.info['dt']])
        ys = Yatsdo.combine(a, b, rate)
        if out_file is not None:
            ys.to_csv(out_file)
        return ys

    @staticmethod
    def run(s, para):
        return

    @staticmethod
    def filter(data, cutoff, fs):
        boo = np.zeros(data.shape)
        boo[:, 0] = data.iloc[:, 0]
        for i in range(1, data.shape[1]):
            boo[:, i] = Butterworth.butter_low_filter(data.iloc[:, i], cutoff, fs)
        return pd.DataFrame(data=boo, columns=[c for c in data.columns])

    @staticmethod
    def extract_imu_set(imu_keys, s):
        cols = [c for c in s.columns]
        leg_set = []
        leg_all = [cols[0]]
        for im in imu_keys:
            shank = [cols[0]]
            for c in cols:
                if imu_keys[im] in c:
                    shank.append(c)
                    leg_all.append(c)
            leg_set.append(shank)
        return s[leg_all]

    @staticmethod
    def extract_initial_contact(data, legs):
        def find_col_names(n0, n1):
            return [c for c in data.columns if n0 in c.lower() or n1 in c.lower()]

        def extract_left_right_col_labels(cols):
            left = [c for c in cols[1:] if legs[Direction.Left] in c]
            right = [c for c in cols[1:] if legs[Direction.Right] in c]
            # assumes index 0 is time
            left.insert(0, cols[0])
            right.insert(0, cols[0])
            return left, right
        cols_gyro = find_col_names('gyro', 'time')
        tiid_gyro = data[cols_gyro[0]].to_numpy()
        left_gyro, right_gyro = extract_left_right_col_labels(cols_gyro)

        cols_acc = find_col_names('acc', 'time')
        tiid_acc = data[cols_acc[0]].to_numpy()
        left_acc, right_acc = extract_left_right_col_labels(cols_acc)

        # finds the axis with the most variation
        def convert_to_1D(data3d, signed=False, acc=True):
            col = [c for c in data3d.columns]
            mycol = [col[1], col[3]]

            mysubset = data3d[mycol]
            sd_left = np.nanstd(data3d[col[1:]], axis=0)
            if acc:
                sd_left = np.nanstd(mysubset, axis=0)
            ix = -1
            ix_current = 0

            for i in range(0, sd_left.shape[0]):
                if sd_left[i] > ix_current:
                    ix = i
                    ix_current = sd_left[i]
            directionless = np.zeros([data3d.shape[0], 2])
            directionless[:, 0] = tiid_gyro
            for i in range(0, data3d.shape[0]):
                sig = 1
                if acc:
                    if signed:
                        sig = np.sign(mysubset.iloc[i, ix])
                    directionless[i, 1] = mysubset.iloc[i, ix]
                else:
                    if signed:
                        sig = np.sign(data3d.iloc[i, ix+1])
                    directionless[i, 1] = data3d.iloc[i, ix+1]
                pass
            mx = np.max(directionless[:, 1])
            if mx < np.abs(np.min(directionless[:, 1])):
                directionless[:, 1] = -1*directionless[:, 1]
            return directionless

        left_shanks_gyro = data[left_gyro]
        left_shanks_acc = data[left_acc]

        left_directionless_gyro = convert_to_1D(left_shanks_gyro, signed=True, acc=False)
        left_directionless_gyro[:, 0] = tiid_gyro
        left_directionless_acc = convert_to_1D(left_shanks_acc, signed=True)
        left_directionless_acc[:, 0] = tiid_acc

        Left_initial_foot_contact, left_directionless_gyro_cutoff, peaks, peaks_acc2, peaks_acc3, peaksg = BlueTridentTools.initial_contact(
            left_directionless_acc, left_directionless_gyro)

        # plt.plot(left_directionless_gyro[:, 0], left_directionless_gyro[:, 1])
        # plt.plot(left_directionless_gyro_cutoff[:, 0], left_directionless_gyro_cutoff[:, 1])
        # plt.plot(left_directionless_gyro[peaksg, 0], left_directionless_gyro[peaksg, 1], "o")
        # plt.plot(left_directionless_acc[:, 0], 0.01 * left_directionless_acc[:, 1])
        # plt.plot(left_directionless_acc[peaks, 0], 0.01 * left_directionless_acc[peaks, 1], "o")
        # plt.plot(left_directionless_acc[peaks_acc2, 0], 0.01 * left_directionless_acc[peaks_acc2, 1], "o")
        # plt.plot(left_directionless_acc[peaks_acc3, 0], 0.01 * left_directionless_acc[peaks_acc3, 1], "o")
        # plt.plot(left_directionless_gyro[Left_initial_foot_contact, 0], left_directionless_gyro[Left_initial_foot_contact, 1], "o")
        # plt.show()

        right_shanks_gyro = data[right_gyro]
        right_shanks_acc = data[right_acc]

        right_directionless_gyro = convert_to_1D(right_shanks_gyro, signed=True, acc=False)
        right_directionless_gyro[:, 0] = tiid_gyro
        right_directionless_acc = convert_to_1D(right_shanks_acc, signed=True)
        right_directionless_acc[:, 0] = tiid_acc

        Right_initial_foot_contact, right_directionless_gyro_cutoff, peaks, peaks_acc2, peaks_acc3, peaksg = BlueTridentTools.initial_contact(
            right_directionless_acc, right_directionless_gyro)

        # plt.plot(right_directionless_gyro[:, 0], right_directionless_gyro[:, 1])
        # plt.plot(right_directionless_acc[:, 0], 0.01*right_directionless_acc[:, 1])
        # plt.plot(right_directionless_gyro[Right_initial_foot_contact, 0], right_directionless_gyro[Right_initial_foot_contact, 1], "o")
        # plt.show()
        return {Direction.Left: Left_initial_foot_contact, Direction.Right: Right_initial_foot_contact}

    @staticmethod
    def initial_contact(_directionless_acc, _directionless_gyro):
        hg = np.max(np.abs(_directionless_gyro))
        peaksg, _ = find_peaks(_directionless_gyro[:, 1], height=hg * 0.5)
        ha = np.max(np.abs(_directionless_acc))
        peaks, _ = find_peaks(_directionless_acc[:, 1], height=ha * 0.5)
        peaks_acc2, _ = find_peaks(_directionless_acc[:, 1], height=ha * 0.1)
        peaks_acc3 = [a for a in peaks_acc2 if a not in peaks]
        v = int(np.max(np.abs(peaksg[1:] - peaksg[:-1])))
        exclude_range_list = {}
        for p in range(0, len(peaksg)):
            exclude_height = 0.3 * _directionless_gyro[peaksg[p], 1]
            exclude_range = [0, 0]
            print(peaksg[p])
            boo_backwards = False
            boo_forward = False
            for pi in range(0, v):
                if _directionless_gyro[peaksg[p] - pi, 1] > exclude_height and not boo_backwards:
                    exclude_range[0] = peaksg[p] - pi
                else:
                    boo_backwards = True
            for pi in range(0, v):
                if _directionless_gyro[peaksg[p] + pi, 1] > exclude_height and not boo_forward:
                    exclude_range[1] = peaksg[p] + pi
                else:
                    boo_forward = True

            exclude_range_list[peaksg[p]] = exclude_range
            print("exclusion: " + str(exclude_height) + ", Range: " + str(exclude_range))
        _directionless_gyro_cutoff = np.copy(_directionless_gyro)
        for ex in exclude_range_list:
            ex_c = exclude_range_list[ex]
            print(ex_c)
            _directionless_gyro_cutoff[ex_c[0]:ex_c[1], 1] = 0.3 * _directionless_gyro[ex, 1]
        j_start = 0
        initial_foot_contact = []
        for i in peaksg:
            for j in range(j_start, len(peaks_acc3)):
                j_start = j
                if (peaks_acc3[j] - i) > 0:
                    break
            search_area = _directionless_gyro[i:peaks_acc3[j_start], 1]
            try:
                min_value = np.min(search_area)
                indexs = np.where(search_area == min_value)
                initial_foot_contact.append(i + int(indexs[0]))
            except ValueError:
                pass

        return initial_foot_contact, _directionless_gyro_cutoff, peaks, peaks_acc2, peaks_acc3, peaksg



