from yatpkg.math.transformation import PCAModel, NpTools
from yatpkg.util.data import VTKMeshUtl, milli
import joblib
import copy
import numpy as np
import pandas as pd
import os
import vtk


class Sampler:
    @staticmethod
    def sample(sample_size=-1):
        # Set the parameters for the normal distribution
        mean = 0  # Mean of the distribution
        std_dev = 1  # Standard deviation of the distribution

        # Generate a sample of random numbers following a normal distribution
        if sample_size < 0:
            sample_size = 100

        rng = np.random.default_rng()
        random_numbers = rng.normal(mean, std_dev, sample_size)

        # Scale and shift the numbers to be between -2 and +2
        scaled_numbers = 4 * (random_numbers - np.min(random_numbers)) / (
                    np.max(random_numbers) - np.min(random_numbers))-2
        return scaled_numbers

class ShapeModel:

    @property
    def inner(self):
        return "inner"

    @property
    def outer(self):
        return "outer"

    def __init__(self, pc: str = None, mean_mesh=None, mesh_name="Default"):
        """
        :param pc: file path to the *.pc.npz or *.joblib file
        :param mean_mesh:
        :param maps:
        """
        st = milli()
        if pc is not None and pc.endswith(".pc.npz"):
            s = np.load(pc, encoding='bytes', allow_pickle=True)
        elif pc is not None and pc.endswith("joblib"):
            n = joblib.load(pc)
            s = {
                'mean': n.mean_,
                'weights': n.explained_variance_,
                'modes': n.components_.T,
                'SD': None,
                'projectedWeights': None
            }
        else:
            s = np.load("maps/00_0000_HumerusScapula_Both.pc.npz", encoding='bytes', allow_pickle=True)
        self.mean = s['mean']
        self.weights = s['weights']  # PC weights are variance
        self.modes = s['modes']
        self.SD = s['SD']
        self.projectedWeights = s['projectedWeights']
        self.dimensions = 3
        self.mean_mesh = None
        if mean_mesh:
            self.mean_mesh = VTKMeshUtl.load(mean_mesh, clean_mesh=False)

        # initially with mean mesh
        self.mesh = {ShapeModel.inner: VTKMeshUtl.clone_poly(self.mean_mesh),
                     ShapeModel.outer: VTKMeshUtl.clone_poly(self.mean_mesh)}
        self.mesh_color = [144 / 255, 207 / 255, 252 / 255]
        self.mesh_name = mesh_name
        self._scale_factors = {"L1": [0.975, 0.6, 0.6],
                               "L2": [1.0, 1.6, 1.6],
                               "min_thickness": 1.0  # dicom resolution
                               }
        ssm = np.reshape(self.mean, (-1, self.dimensions))
        vertices = VTKMeshUtl.extract_points(self.mean_mesh)
        while vertices.shape[0] != ssm.shape[0]:
            self.dimensions += 1
            ssm = np.reshape(self.mean, (-1, self.dimensions))
        en = milli()
        print("time to for mapping pcs: {0} msec\n".format(en - st))

    def reconstruct_diff(self, pc, sd=0, part=None, debug=False, add_mean=False):
        if part is None:
            return None
        w = sd * np.sqrt(self.weights[pc])
        m = w * self.modes[:, pc]
        me = m
        if add_mean:
            me = m + self.mean
        mesh = np.reshape(me, [int(self.mean.shape[0] / 3), 3])
        export = mesh[part["idm"].to_list(), :]
        if debug:
            if not os.path.exists("./meshes/temp/"):
                os.makedirs("./meshes/temp/")
            ms = pd.DataFrame(data=export, columns=["x", "y", "z"])
            ms.to_csv("./default_meshes/temp/part_cloud.csv", index=False)

        return export

    def reconstruct_diff_all(self, sd, add_mean=False):
        sdo = np.array([0.0 for i in range(len(self.weights))])
        sdo[:len(sd)] = sd
        w = np.atleast_2d(sdo * np.sqrt(self.weights))
        m = np.dot(w, self.modes.T)
        me = m.T
        if add_mean:
            me = m + self.mean
        mesh = np.reshape(me, [int(self.mean.shape[0] / self.dimensions), self.dimensions])
        return mesh

    def sample_ssm(self, pick=30, pcs=7, out_dir="./samples/", filename="sample", transform=None):
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        if pcs > len(self.weights):
            print("Warning PC set to greater than the number in the model.\nSetting number pc to max: " + len(self.weights))
            pcs = len(self.weights)
        pc_list = [Sampler.sample(pick) for i in range(0, pcs)]
        sd_pc = []
        for s in range(0, pick):
            temp = []
            for t in range(0, len(pc_list)):
                temp.append(pc_list[t][s])
            sd_pc.append(temp)
        idx = 0
        for sd in sd_pc:
            print(sd)
            mesh = "{0}_{1}".format(filename, idx)
            self.generate_mesh_vtk(sd, write_out=True, outdir=out_dir, mesh_name=mesh, transform=transform)
            idx += 1

    def generate_mesh_vtk(self, sd_pc, update_orientation=True, write_out=False,
                      outdir="E:/mapclient/workflow/",
                      mesh_name=None,
                      transform=None):
        """
                Warning: Currently optimised for thickness of the tibia

                This Method takes the values set by the PC sliders and calculate the new shape.
                Once the new shape is calculated the inner mesh is created by first calculating the projection direction using
                the shrink method then the thickness is applied.

                :param sd_pc:
                :param update_orientation: Not used leftover from when multiple bones are articulated
                :param write_out: Not used leftover from when multiple bones are displayed
                :param outdir:
                :param mesh_name:
                :return: N/A
                """
        mean_mesh = np.reshape(self.mean,
                               [int(self.mean.shape[0] / self.dimensions), self.dimensions])
        cen = np.nanmean(mean_mesh, axis=0)
        temp = copy.deepcopy(mean_mesh[:, :3]) - cen[:3]
        mean_mesh[:, :3] = temp
        sl = self.reconstruct_diff_all(sd_pc)
        new_shape = mean_mesh + sl
        current_mesh = copy.deepcopy(new_shape)
        if update_orientation:
            px0 = PCAModel.pca_rotation(new_shape[:, :3])
            current_mesh = copy.deepcopy(px0.transformed_data)
        elif new_shape.shape[1] == 4:
            current_mesh = copy.deepcopy(new_shape[:, :2])
        if transform is not None:
            offset = NpTools.repeater(transform[:3, 3], current_mesh.shape[0], axis=1)
            current_mesh = (np.matmul(transform[:3, :3], current_mesh.T) + offset).T

        outer_clone = VTKMeshUtl.clone_poly(self.mesh[ShapeModel.outer])
        VTKMeshUtl.update_poly_w_points(current_mesh, outer_clone)

        if write_out:
            out_name = self.mesh_name
            if mesh_name is not None:
                out_name = mesh_name
            VTKMeshUtl.write("{0}{1}_outer.stl".format(outdir, out_name),
                             outer_clone)
            VTKMeshUtl.mesh2exf(mesh_filename="{0}{1}_outer.stl".format(outdir, out_name),
                                output_meshname="{0}{1}_outer.exf".format(outdir, out_name),
                                group_name="Cortical bone")
        if self.dimensions == 4:
            poly = outer_clone
            normals = vtk.vtkPolyDataNormals()
            normals.SetInputData(poly)
            normals.ComputeCellNormalsOn()
            normals.SetAutoOrientNormals(True)
            normals.Update()
            output = normals.GetOutput()
            cellData = output.GetPointData()
            _normals = cellData.GetNormals()
            norms0 = np.array([_normals.GetTuple3(f) for f in range(_normals.GetNumberOfTuples())])
            offset = np.atleast_2d(np.abs(new_shape[:, 3])).T * norms0[:new_shape.shape[0]]
            c = current_mesh - offset

            inner_clone = VTKMeshUtl.clone_poly(self.mesh[ShapeModel.inner])
            VTKMeshUtl.update_poly_w_points(c, inner_clone)
            if write_out:
                out_name = self.mesh_name
                if mesh_name is not None:
                    out_name = mesh_name
                VTKMeshUtl.write("{0}{1}_inner.stl".format(outdir, out_name),
                                 inner_clone)
                VTKMeshUtl.mesh2exf(mesh_filename="{0}{1}_inner.stl".format(outdir, out_name),
                                    output_meshname="{0}{1}_inner.exf".format(outdir, out_name),
                                    group_name="Cancellous bone")

    def generate_mesh(self, sd_pc, update_orientation=True, write_out=False,
                      outdir="E:/mapclient/workflow/",
                      mesh_name=None,
                      transform=None):
        """
                Warning: Currently optimised for thickness of the tibia

                This Method takes the values set by the PC sliders and calculate the new shape.
                Once the new shape is calculated the inner mesh is created by first calculating the projection direction using
                the shrink method then the thickness is applied.

                :param sd_pc:
                :param update_orientation: Not used leftover from when multiple bones are articulated
                :param write_out: Not used leftover from when multiple bones are displayed
                :param outdir:
                :param mesh_name:
                :return: N/A
                """
        mean_mesh = np.reshape(self.mean,
                               [int(self.mean.shape[0] / self.dimensions), self.dimensions])
        cen = np.nanmean(mean_mesh, axis=0)
        temp = copy.deepcopy(mean_mesh[:, :3]) - cen[:3]
        mean_mesh[:, :3] = temp
        sl = self.reconstruct_diff_all(sd_pc)
        new_shape = mean_mesh + sl
        current_mesh = copy.deepcopy(new_shape)
        if update_orientation:
            px0 = PCAModel.pca_rotation(new_shape[:, :3])
            current_mesh = copy.deepcopy(px0.transformed_data)
        elif new_shape.shape[1] == 4:
            current_mesh = copy.deepcopy(new_shape[:, :2])
        if transform is not None:
            offset = NpTools.repeater(transform[:3, 3], current_mesh.shape[0],axis=1)
            current_mesh = (np.matmul(transform[:3, :3], current_mesh.T) + offset).T

        outer_clone = VTKMeshUtl.clone_poly(self.mesh[ShapeModel.outer])
        VTKMeshUtl.update_poly_w_points(current_mesh, outer_clone)

        if write_out:
            out_name = self.mesh_name
            if mesh_name is not None:
                out_name = mesh_name
            VTKMeshUtl.write("{0}{1}_outer.stl".format(outdir, out_name),
                             outer_clone)
            VTKMeshUtl.mesh2exf(mesh_filename="{0}{1}_outer.stl".format(outdir, out_name),
                                output_meshname="{0}{1}_outer.exf".format(outdir, out_name),
                                group_name="Cortical bone")

        if self.dimensions == 4:
            scale = np.diag(self._scale_factors["L1"])
            px = np.matmul(scale, current_mesh.T).T
            scale = np.diag(self._scale_factors["L2"])
            py = np.matmul(scale, px.T).T
            mx = np.max(px[:, 0])
            mn = np.min(px[:, 0])
            rg = mx - mn
            long_bone_seg = rg / 8.0
            grad = 1 / long_bone_seg
            start0 = mx - long_bone_seg
            start1 = mn + long_bone_seg
            grad0 = np.zeros(py.shape)
            for i in range(0, px.shape[0]):
                if px[i, 0] > (mx - long_bone_seg):
                    a = grad * (px[i, 0] - start0)
                    grad0[i, :] = [a, a, a]
                elif px[i, 0] < (mn - long_bone_seg):
                    b = 1 - (grad * (px[i, 0] - start1))
                    grad0[i, :] = [b, b, b]
            px0 = grad0 * py + (1 - grad0) * px
            diff = current_mesh - px0
            d = np.atleast_2d(np.sqrt(np.sum(diff * diff, axis=1)))
            norms = ((1 / d).T * diff)
            offset = np.atleast_2d(np.abs(new_shape[:, 3])).T * norms
            c = current_mesh - offset

            inner_clone = VTKMeshUtl.clone_poly(self.mesh[ShapeModel.inner])
            VTKMeshUtl.update_poly_w_points(c, inner_clone)
            if write_out:
                out_name = self.mesh_name
                if mesh_name is not None:
                    out_name = mesh_name
                VTKMeshUtl.write("{0}{1}_inner.stl".format(outdir, out_name),
                                 inner_clone)
                VTKMeshUtl.mesh2exf(mesh_filename="{0}{1}_inner.stl".format(outdir, out_name),
                                    output_meshname="{0}{1}_inner.exf".format(outdir, out_name),
                                    group_name="Cancellous bone")
