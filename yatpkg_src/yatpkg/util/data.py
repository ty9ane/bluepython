import copy

from scipy import interpolate
from typing import Optional
from yatpkg.math.filters import Butterworth
import numpy as np
import pandas as pd
from enum import Enum
import os
import io
import yatpkg.util.c3d as c3d
import vtk
import math
from copy import deepcopy
from scipy.stats import mode
from scipy.spatial.transform import Rotation
from xml.dom import minidom
from datetime import datetime
import time
import json
from vtkmodules.util.numpy_support import numpy_to_vtk, vtk_to_numpy
from vtkmodules.vtkIOXML import vtkXMLPolyDataWriter
from multiprocessing import Pool
from tqdm import tqdm
from typing import Union
import zipfile
import struct



"""
TODOs:
> Add Documentation to code
"""

def milli():
    date = datetime.utcnow() - datetime(1970, 1, 1)
    seconds = (date.total_seconds())
    milliseconds = round(seconds * 1000)
    return milliseconds


class JSONSUtl:
    @staticmethod
    def write_json(pathto, dictm, indent=4, sort_keys=True):
        with open(pathto, 'w') as outfile:
            json.dump(dictm, outfile, sort_keys=sort_keys, indent=indent)

    @staticmethod
    def load_json(file):
        with open(file, 'r') as infile:
            data = json.load(infile)
        return data


class Yac3do:
    # Yet another c3d data object
    #
    def __init__(self, filename: str=None):
        self.filename = filename
        self.c3d_dict = StorageIO.readc3d_general(filename)
        self.__trc__ = None
        self.__trc__dirty__ = True
        pass

    @property
    def trc(self):
        if self.__trc__dirty__ or self.__trc__ is None:
            ret = self.c3d_dict['point_data'].copy()
            lab = ['time']
            for m in self.marker_labels:
                lab.append(m)
            markers = {m: [] for m in lab}
            for m in lab:
                if m == 'time':
                    markers[m] = [n*(1/self.c3d_dict['meta_data']['trial']['camera_rate']) for n in range(0, self.c3d_dict['meta_data']['trial']['end_frame'])]
                else:
                    columns = [n for n in ret.columns if m in n]
                    markers[m] = ret[columns]
                pass
            self.c3d_dict['markers'] = markers
            self.__trc__ = TRC.create_from_c3d_dict(self.c3d_dict, self.filename)
            self.__trc__dirty__ = False
        return self.__trc__

    @property
    def marker_labels(self):
        return self.c3d_dict['point_label']

    @property
    def markers(self):
        frames = [i+1 for i in range(0, self.c3d_dict['point_data'].shape[0])]
        t = [i*(1/self.c3d_dict['point_rate']) for i in range(0, self.c3d_dict['point_data'].shape[0])]
        ret = self.c3d_dict['point_data'].copy()
        ret.insert(0, 'frames', frames)
        ret.insert(1, 'time', t)
        return ret

    @property
    def analog(self):
        return self.c3d_dict['analog_data']


class Yatsdo:
    @staticmethod
    def create_from_storage_io(s, para=None):
        cols = [c for c in s.data.columns]
        ret = np.zeros(s.data.shape)
        t = np.array(s.data['time'].to_list())
        ret[:, 0] = t
        cut = 5
        fs = 1 / s.dt
        order = 4
        if para is not None:
            try:
                b = para['butter']
                cut = b['cut']
                fs = b['fs']
                order = b['order']
            except KeyError:
                pass
        for c in range(1, len(cols)):
            k = np.array(s.data[cols[c]].to_list())
            k_ret = Butterworth.butter_low_filter(k, cut, fs, order)
            ret[:, c] = k_ret
        ret_pd = pd.DataFrame(data=ret, columns=cols)
        return Yatsdo(ret_pd)

    def to_panda_data_frame(self):
        ret = pd.DataFrame(data=self.data, columns=self.column_labels)
        return ret
    # Yet another time series data object
    #
    # This object take a 2D np array and creates
    # functions for simple data manipulation
    # i.e. resampling data
    # Assume get_first column is time
    def __init__(self, data, col_names=[], fill_data=False, time_col=0):
        self.col_labels = col_names
        if isinstance(data, np.ndarray):
            self.data: np.ndarray = data
        elif isinstance(data, pd.DataFrame):
            self.data: np.ndarray = data.to_numpy()
            self.col_labels = [c for c in data.columns]
        else:
            self.data: np.ndarray = np.array(data)
        self.curve = {}
        self.dev_curve = {}
        self.x = self.data[:, time_col]
        if self.data.shape[0] > 3:
            for i in range(1, self.data.shape[1]):
                p = interpolate.InterpolatedUnivariateSpline(self.x, self.data[:, i])
                a = np.isnan(self.data[:, i])
                b = [b for b in range(0, a.shape[0]) if not a[b]]
                if len(b) < a.shape[0] and fill_data:
                    p = interpolate.InterpolatedUnivariateSpline(self.x[b], self.data[b, i])
                self.curve[i] = p
                self.dev_curve[i] = p.derivative()

        self.size = self.data.shape
        pass

    @staticmethod
    def load(filepath: str):
        """
        This method direct loads bapple into memory
        :param filepath:
        :param sep: path sep
        :param unzip: set to unzip or not default False
        :param del_temp:
        :return:
        """
        start = time.time()
        bar = tqdm(total=5, desc="Cooking", ascii=False,
                   ncols=120,
                   colour="#6e5b5b")
        block = np.load(filepath)
        bar.update(1)
        meta_data = json.load(io.BytesIO(block['meta_data.json']))
        bar.update(1)
        print("Loading apples and bananas")
        data_block = None
        a_block = None
        try:
            data_block = np.load(io.BytesIO(block['data.npz']))
        except KeyError:
            a_block = np.frombuffer(block['a.npz'])
            a_block = a_block.reshape(meta_data['pineapple']['shape'])
        bar.update(1)
        print("Preparing the pineapple")
        a_bapple = None
        if data_block is not None:
            a_bapple = pd.DataFrame(data=np.asarray(data_block['a']), columns=meta_data['pineapple'])

        if a_block is not None:
            a_bapple = pd.DataFrame(data=a_block, columns=meta_data['pineapple']['cols'])
        bar.update(1)
        bapple = Yatsdo(a_bapple)
        bar.update(1)
        bar.close()
        end = time.time()
        print("Order up: One Yatdos > Load completed in {0:0.2f}s".format(end - start))
        return bapple

    def export(self, filename: str, pathto: str, compresslevel=1):
        """
        Export bapple to a zip file
        Note: May fail if there is not enough space for the temp folder
        :param filename:
        :param pathto:
        :return:
        """

        if not pathto.endswith("/") or not pathto.endswith("\\"):
            pathto += "/"

        meta_data = {}

        if not os.path.exists(pathto+"temp/"):
            os.makedirs(pathto+"temp/")

        num_files = 0
        if self.data is not None:
            num_files += 1
        num_files += 1

        with zipfile.ZipFile(pathto + filename, mode="w", compression=zipfile.ZIP_DEFLATED, compresslevel=compresslevel) as archive:
            bar = tqdm(total=num_files, desc="Saving to {0} >".format(filename), ascii=False,
                       ncols=120,
                       colour="#6e5b5b")
            if self.data is not None:
                bar.desc = "Saving to {0} > Exporting Pineapple".format(filename)
                bar.refresh()
                meta_data['pineapple'] = {'cols': [c for c in self.column_labels], 'shape': self.data.shape}
                anp = self.data

                archive.writestr("a.npz", anp.tobytes())
                bar.update(1)

            bar.desc = "Saving to {0} > Exporting meta_data.json".format(filename)
            bar.refresh()
            json_string = json.dumps(meta_data)
            meta_data_bytes = json_string.encode('utf-8')
            archive.writestr("meta_data.json", meta_data_bytes)
            bar.update(1)
            bar.close()

    @property
    def shape(self):
        return self.data.shape

    @property
    def dt(self):
        """
        Get the mean sampling rate of the data
        :return: sampling rate (float)
        """
        a = 0.01    # default
        if self.x.shape[0] > 2:
            a = np.nanmean(self.x[1:] - self.x[:-1])
        return a

    # Given time points it will get the accompanying data
    def get_samples(self, time_points, assume_time_first_col=True, as_pandas=False):
        a = [time_points]
        # additional_col = 0
        # if not assume_time_first_col:
        #     additional_col = 1
        # cols = self.data.shape[1] + additional_col
        cols = self.data.shape[1]
        rows = len(time_points)
        ret = np.zeros([rows, cols])
        if not assume_time_first_col:
            ret[:, 0] = [i for i in range(1, rows+1)]
            ret[:, 1] = time_points
            for i in range(2, self.data.shape[1]):
                a.append(self.curve[i](time_points))
                ret[:, i] = self.curve[i](time_points)
        else:
            ret[:, 0] = time_points
            for i in range(1, self.data.shape[1]):
                a.append(self.curve[i](time_points))
                ret[:, i] = self.curve[i](time_points)

        # return np.squeeze(np.array(a)).transpose()

        if as_pandas:
            if len(self.column_labels) > 0:
                col = self.column_labels
                return pd.DataFrame(data=ret, columns=col)
            else:
                return pd.DataFrame(data=ret)

        return ret

    def get_sample(self, time_point, assume_time_first_col=True):
        additional_col = 0
        if not assume_time_first_col:
            additional_col = 1
        cols = self.data.shape[1] + additional_col
        ret = np.zeros([1, cols])
        ret[:, 0] = time_point
        for i in range(1, self.data.shape[1]):
            ret[:, i] = self.curve[i](time_point)
        return ret[0]

    # Given time points it will get the accompanying data
    def get_samples_dev(self, time_points):
        a = [time_points]
        for i in range(1, self.data.shape[1]):
            a.append(self.dev_curve[i](time_points))
        return np.squeeze(np.array(a)).transpose()

    def update(self):
        self.x = self.data[:, 0]
        for i in range(1, self.data.shape[1]):
            p = interpolate.InterpolatedUnivariateSpline(self.x, self.data[:, i])
            self.curve[i] = p

    def update_Spline(self):
        for i in range(1, self.data.shape[1]):
            p = interpolate.InterpolatedUnivariateSpline(self.x, self.data[:, i])
            self.curve[i] = p

    def filter(self, func, para):
        to_filter = self.data
        for i in range(1, to_filter.shape[1]):
            d = to_filter[:, i]
            to_filter[:, i] = func(np.squeeze(d), para)
        return Yatsdo(pd.DataFrame(data=to_filter, columns=self.column_labels))

    def remove_jitter(self):
        rate = 1 / np.nanmean(self.x[1:] - self.x[0:-1])
        period = self.x[-1] - self.x[0]
        frames = period / (1 / rate)
        new_frame = self.get_samples([self.x[0] + (1 / rate) * i for i in range(0, int(math.ceil(frames)))])
        return Yatsdo(pd.DataFrame(data=new_frame, columns=self.column_labels))

    @staticmethod
    def combine(a, b, rate=None):
        if rate is None:
            rate_a = 1 / np.nanmean(a.x[1:] - a.x[0:-1])
            rate_b = 1 / np.nanmean(b.x[1:] - b.x[0:-1])
            rate = rate_a
            if rate < rate_b:
                rate = rate_b
        period = a.x[-1] - a.x[0]
        frames = period / (1 / rate)
        dfR = a.get_samples([a.x[0] + (1 / rate) * i for i in range(0, int(math.ceil(frames)))])
        period = b.x[-1] - b.x[0]
        frames = period / (1 / rate)
        dfR1 = b.get_samples([b.x[0] + (1 / rate) * i for i in range(0, int(math.ceil(frames)))])
        lab = [c for c in a.column_labels]
        for c in range(1, len(b.column_labels)):
            lab.append(b.column_labels[c])
        return Yatsdo.merge(dfR, dfR1, lab)

    @staticmethod
    def merge(a, b, cols_ab, ignore_time=True):
        """
        This method merges two nd_arrays together and returns a Yatsdo
        :param a: nd_array
        :param b: nd_array
        :param cols_ab: list
        :param ignore_time: boolean
        :return: Yatsdo
        """
        rows = a.shape[0]
        if b.shape[0] < rows:
            rows = b.shape[0]
        cols = a.shape[1]+b.shape[1]-1
        nova = np.zeros([rows, cols])
        if ignore_time:
            nova[:rows, 0:a.shape[1]] = a[:rows, :]
            nova[:rows, a.shape[1]:cols] = b[:rows, 1:]
        y = Yatsdo(nova)
        y.column_labels = cols_ab
        return y

    @property
    def column_labels(self):
        return self.col_labels

    @column_labels.setter
    def column_labels(self, x):
        self.col_labels = x

    def to_csv(self, file_path):
        d = pd.DataFrame(data=self.data, columns=self.column_labels)
        d.to_csv(file_path, index=False)


class MocapFlags(Enum):
    DataRate = 'DataRate'
    CameraRate = 'CameraRate'
    NumFrames = 'NumFrames'
    NumMarkers = 'NumMarkers'
    Units = 'Units'
    OrigDataRate = 'OrigDataRate'
    OrigDataStartFrame = 'OrigDataStartFrame'
    OrigNumFrames = 'OrigNumFrames'
    mm = 'mm'
    m = 'm'

    @staticmethod
    def unit(unit:str):
        if MocapFlags.mm.value == unit:
            return MocapFlags.mm
        else:
            return MocapFlags.m

    @staticmethod
    def defaults_to_list():
        return [k for k in MocapFlags if isinstance(k.value, str) if k.value != 'mm' or k.value != 'm']


class TRC(Yatsdo):

    @staticmethod
    def read(filename, delimiter="\t", headers=True, fill_data=False):
        """
        This method reads in a trc file.
        Note - This should be moved to the TRC object
        :param filename:
        :param delimiter:
        :param headers:
        :param fill_data:
        :return:
        """
        ret = []
        lengths = []
        stream = "hoi"
        read_successful = False
        try:
            f = open(filename, "r")
            while len(stream) > 0:
                stream = f.readline()
                elements = stream.strip().split(delimiter)
                ret.append(elements)
                lengths.append(len(elements))
            read_successful = True
        except OSError:
            print('cannot open', filename)
        finally:
            f.close()
        meta_info = {ret[1][i]: ret[2][i] for i in range(0, len(ret[1]))}
        headings = [c for c in ret[3] if len(c) > 1]
        d = {}
        i = 0
        markers_col = [r for r in ret[4] if len(r) > 1]
        for c in headings[2:]:
            d[c] = {"col": markers_col[i:i + 3]}
            i += 3
        data_headings = ["Frame#", "Time"]
        for dh in d:
            dhl = [dh + "_" + h for h in d[dh]["col"]]
            for j in dhl:
                data_headings.append(j)
        st = 5
        start_data = ret[st]
        while len(start_data) < 2:
            st += 1
            start_data = ret[st]
        ed = st
        for k in range(st, len(ret)):
            if len(ret[k]) < 2:
                break
            ed += 1
        data = []
        cols = len(data_headings)

        for n in ret[st:ed]:
            dd = []
            for m in n:
                try:
                    dd.append(float(m))
                except ValueError:
                    dd.append(np.NaN)
            while len(dd) < cols:
                dd.append(np.NaN)
            data.append(dd)
        rows = len(data)
        np_data = np.zeros([rows, cols])
        for i in range(0, np_data.shape[0]):
            dx = data[i]
            if len(dx) == cols:
                np_data[i, :] = dx
        trc = TRC(np_data, data_headings, filename, fill_data=fill_data)
        trc.headers = meta_info
        trc.raw = ret
        trc.marker_names = headings[2:]
        trc.st = st
        k = 0
        try:
            for m in trc.marker_names:
                trc.marker_set[m] = pd.DataFrame(trc.data[:, 2 + k:5 + k], columns=d[m]['col'])
                k += 3
            if fill_data:
                trc.fill_gaps()
        except AssertionError:
            print("error creating {0} marker".format(m))
            cols = d[m]['col']
            error_data = trc.data[:, 2 + k:5 + k]
            pass
        # trc.write("G:/Shared drives/Rocco Hip Replacement Study/01/Pre-op/Sit00a.trc")
        return trc

    def get_samples_as_trc(self, time_points):
        subset = self.get_samples(time_points, assume_time_first_col=False)
        trc = copy.deepcopy(self)
        trc.data = subset
        trc.x = subset[:, 1]
        trc.update()
        return trc

    def get_data_between_timepoints(self, starttime, endtime, freq):
        period = endtime - starttime
        dt = 1.0/(1.0*freq)
        num_frames = period / dt
        timepoints = [t * dt + starttime for t in range(0, int(num_frames))]
        subset = self.get_samples_as_trc(timepoints)
        return subset

    def __init__(self, data, col_names=[], filename="", fill_data=False):
        super().__init__(data, col_names, fill_data, time_col=1)
        self.marker_names = []
        self.marker_set = {}
        self.raw = None
        self.st = 0
        self.first_line = "PathFileType\t3\t(X/Y/Z)\t{0}".format(filename)
        self.headers_labels = [MocapFlags.DataRate,
                               MocapFlags.CameraRate,
                               MocapFlags.NumFrames,
                               MocapFlags.NumMarkers,
                               MocapFlags.Units,
                               MocapFlags.OrigDataRate,
                               MocapFlags.OrigDataStartFrame,
                               MocapFlags.OrigNumFrames]
        self.headers = {h: -1.0 for h in self.headers_labels}
        if data is not None:
            self.x = self.data[:, 1]

    def to_panda(self):
        return pd.DataFrame(data=self.data, columns=self.column_labels)

    @property
    def dt(self):
        """
        Get the mean sampling rate of the data
        :return: sampling rate (float)
        """
        a = 0.01  # default
        if self.x.shape[0] > 2:
            a = np.nanmean(self.x[1:] - self.x[:-1])
        return a

    def fill_gaps(self):
        n = self.get_samples(self.x)
        self.data[:, 2:] = n[:, 2:]
        self.update()
        pass


    @staticmethod
    def create_from_c3d_dict(c3d_dic, filename, fill_data=False):
        header = c3d_dic["mocap"]["header"]
        trc_header = {k.value: -1 for k in MocapFlags.defaults_to_list()}
        trc_header[MocapFlags.DataRate.value] = header.frame_rate
        trc_header[MocapFlags.CameraRate.value] = header.frame_rate
        trc_header[MocapFlags.NumFrames.value] = header.last_frame
        trc_header[MocapFlags.Units.value] = MocapFlags.unit(c3d_dic['mocap']['units']).value

        trc_header[MocapFlags.OrigDataStartFrame.value] = header.first_frame
        trc_header[MocapFlags.OrigDataRate.value] = header.frame_rate
        trc_header[MocapFlags.OrigNumFrames.value] = header.last_frame
        markers = c3d_dic["markers"]
        markers_np = {n: np.array(markers[n]) for n in markers}
        marker_labels = [m for m in markers_np]
        trc_header[MocapFlags.NumMarkers.value] = len(markers) - 1
        frames_block = np.zeros([header.last_frame, (3 * len(marker_labels)) + 1])
        col_names = ['Frame#', 'time']
        inx = 1
        frames_block[:, 0] = [int(n + 1) for n in range(0, header.last_frame)]
        times = markers_np[marker_labels[0]]
        indx = 0
        if np.nanmin(markers_np[marker_labels[0]]) < 0:
            for t in range(0, times.shape[0]):
                indx = t
                if times[t] >= 0.0:
                    break
            times = times[indx:]
        frames_block[:, 1] = times
        markers_set = {}
        for m in range(1, len(marker_labels)):
            lb = marker_labels[m]
            m_np = markers_np[lb]
            s = (m * 3 + 1) - 2
            e = (m * 3 + 4) - 2
            frames_block[:, s: e] = m_np[indx:, :]
            col_names.append(marker_labels[m] + '_X{0}'.format(inx))
            col_names.append(marker_labels[m] + '_Y{0}'.format(inx))
            col_names.append(marker_labels[m] + '_Z{0}'.format(inx))
            markers_set[marker_labels[m]] = pd.DataFrame(data=m_np, columns=['X{0}'.format(inx), 'Y{0}'.format(inx),
                                                                             'Z{0}'.format(inx)])
            inx += 1

        trc = TRC(frames_block, col_names=col_names, filename=filename[:filename.rindex(".")] + ".trc", fill_data=fill_data)
        trc.headers = trc_header
        trc.marker_set = markers_set
        trc.marker_names = marker_labels[1:]
        return trc

    @staticmethod
    def create_from_c3d(data, fill_data=False):
        c3d_data = StorageIO.simple_readc3d(data)
        return TRC.create_from_c3d_dict(c3d_data, data)
        # header = c3d_data["mocap"]["header"]
        # trc_header = {k.value: -1 for k in MocapFlags.defaults_to_list()}
        # trc_header[MocapFlags.DataRate.value] = header.frame_rate
        # trc_header[MocapFlags.CameraRate.value] = header.frame_rate
        # trc_header[MocapFlags.NumFrames.value] = header.last_frame
        # trc_header[MocapFlags.Units.value] = MocapFlags.unit(c3d_data['mocap']['units']).value
        #
        # trc_header[MocapFlags.OrigDataStartFrame.value] = header.first_frame
        # trc_header[MocapFlags.OrigDataRate.value] = header.frame_rate
        # trc_header[MocapFlags.OrigNumFrames.value] = header.last_frame
        # markers = c3d_data["markers"]
        # markers_np = {n: np.array(markers[n]) for n in markers}
        # marker_labels = [m for m in markers_np]
        # trc_header[MocapFlags.NumMarkers.value] = len(markers)-1
        # frames_block = np.zeros([header.last_frame, (3 * len(marker_labels))+1])
        # col_names = ['Frame#', 'time']
        # inx = 1
        # frames_block[:, 0] = [int(n+1) for n in range(0, header.last_frame)]
        # frames_block[:, 1] = markers_np[marker_labels[0]]
        # markers_set = {}
        # for m in range(1, len(marker_labels)):
        #     lb = marker_labels[m]
        #     m_np = markers_np[lb]
        #     s = (m * 3 + 1) - 2
        #     e = (m * 3 + 4) - 2
        #     frames_block[:, s: e] = m_np
        #     col_names.append(marker_labels[m] + '_X{0}'.format(inx))
        #     col_names.append(marker_labels[m] + '_Y{0}'.format(inx))
        #     col_names.append(marker_labels[m] + '_Z{0}'.format(inx))
        #     markers_set[marker_labels[m]] = pd.DataFrame(data=m_np, columns=['X{0}'.format(inx), 'Y{0}'.format(inx), 'Z{0}'.format(inx)])
        #     inx += 1
        #
        # trc = TRC(frames_block, col_names=col_names, filename=data[:data.rindex(".")]+".trc", fill_data=fill_data)
        # trc.headers = trc_header
        # trc.marker_set = markers_set
        # trc.marker_names = marker_labels[1:]
        # return trc

    def z_up_to_y_up(self):
        offset = 2
        n = 0
        for m in self.marker_set:
            k = self.marker_set[m]
            j = k.to_numpy()
            r = Rotation.from_euler('xyz', [-90, -90, 0], degrees=True)
            y = np.matmul(r.as_matrix(), j.T)
            start = offset + n
            end = start + 3
            self.data[:, start: end] = y.T
            col = [c for c in k.columns]
            p = pd.DataFrame(data=y.T, columns=col)
            self.marker_set[m] = p
            n += 3
        self.update()

    def x_up_to_y_up(self):
        offset = 2
        n = 0
        for m in self.marker_set:
            k = self.marker_set[m]
            j = k.to_numpy()
            r = Rotation.from_euler('xyz', [-90, 0, 90], degrees=True)
            y = np.matmul(r.as_matrix(), j.T)
            r0 = Rotation.from_euler('xyz', [0, 180, 0], degrees=True)
            y = np.matmul(r0.as_matrix(), y)
            start = offset + n
            end = start + 3
            self.data[:, start: end] = y.T
            col = [c for c in k.columns]
            p = pd.DataFrame(data=y.T, columns=col)
            self.marker_set[m] = p
            n += 3
        self.update()

    def update_from_markerset(self):
        offset = 2
        n = 0
        ms = []
        a = 0
        num_markers = len(self.marker_set.keys())
        temp = np.zeros([self.data.shape[0], 3*num_markers+2])
        temp[:, :self.data.shape[1]] = self.data
        for m in self.marker_set:
            k = self.marker_set[m]
            j = k.to_numpy()
            start = offset + n
            end = start + 3
            temp[:, start: end] = j
            col = ["X{0}".format(a+1), "Y{0}".format(a+1), "Z{0}".format(a+1)]
            p = pd.DataFrame(data=j, columns=col)
            self.marker_set[m] = p
            n += 3
            a += 1
            ms.append(m)
        self.data = temp
        self.headers[MocapFlags.Units.value] = "m"
        self.headers[MocapFlags.NumFrames.value] = 1
        self.headers[MocapFlags.OrigNumFrames.value] = 1
        self.headers[MocapFlags.NumMarkers.value] = len(ms)

        col = [c for c in self.col_labels if ('frame' in c.lower() or 'time' in c.lower()) or c[:c.rindex('_')] in ms]
        self.col_labels = col
        self.marker_names = ms
        self.update()

    def update(self, units=None):
        if len([k for k in self.marker_set.keys()]) > 0:
            backup = deepcopy(self.data)
            marker_temp = {}
            self.x = self.data[:, 1]
            trc_header = {k.value: -1 for k in MocapFlags.defaults_to_list()}
            dt = 0.01
            if not np.isnan(self.dt):
                dt = self.dt
            trc_header[MocapFlags.DataRate.value] = int(1/dt)
            trc_header[MocapFlags.CameraRate.value] = int(1/dt)
            trc_header[MocapFlags.NumFrames.value] = self.data.shape[0]
            if units is None:
                trc_header[MocapFlags.Units.value] = self.headers['Units']
            else:
                trc_header[MocapFlags.Units.value] = units

            trc_header[MocapFlags.OrigDataStartFrame.value] = 1
            trc_header[MocapFlags.OrigDataRate.value] = int(1/dt)
            trc_header[MocapFlags.OrigNumFrames.value] = self.data.shape[0]
            trc_header[MocapFlags.NumMarkers.value] = len(self.marker_names)
            self.headers = trc_header
            for m in self.marker_set:
                for j in range(0, len(self.column_labels)):
                    label = self.column_labels[j]
                    if m in label and "x" in label.lower():
                        st = j
                        en = j + 3
                        mx = deepcopy(self.data[:, st:en])
                        marker_temp[m] = pd.DataFrame(data=mx, columns=self.marker_set[m].columns)
                        #self.marker_set[m].iloc[:, :] = self.data[:, st:en]
                        break

            self.marker_set = marker_temp
            self.data = backup
            pass
        else:
            def split_cols(k):
                idxs = k.split('_')
                if len(idxs) <= 2:
                    idx0 = idxs[0]
                    return idx0
                else:
                    idx0 = ""
                    for j in range(0, len(idxs) - 1):
                        idx0 += "{0}_".format(idxs[j])
                    idx0 = idx0[:-1]
                    return idx0
            col = [c for c in self.column_labels if 'time' not in c]
            kcol = []
            for k in col:
                idx = split_cols(k)
                if idx is not None:
                    kcol.append(idx)

            markers = {kcol[c]: pd.DataFrame(data=np.zeros([self.data.shape[0], 3]), columns=["X", "Y", "Z"]) for c in range(1, len(kcol)) if 'time' not in kcol[c].lower()}
            marker_names = [c for c in markers.keys()]
            if self.data[0, 0] > 1:
                self.data[:, 0] = self.data[:, 0] - self.data[0, 0] +1
            for m in range(0, len(marker_names)):
                markers[marker_names[m]].columns = ['X{0}'.format(m+1), 'Y{0}'.format(m+1), 'Z{0}'.format(m+1)]
            trc_header = {k.value: -1 for k in MocapFlags.defaults_to_list()}
            trc_header[MocapFlags.DataRate.value] = int(1/self.dt)
            trc_header[MocapFlags.CameraRate.value] = int(1/self.dt)
            trc_header[MocapFlags.NumFrames.value] = self.data.shape[0]
            trc_header[MocapFlags.Units.value] = 'm'

            trc_header[MocapFlags.OrigDataStartFrame.value] = 1
            trc_header[MocapFlags.OrigDataRate.value] = int(1/self.dt)
            trc_header[MocapFlags.OrigNumFrames.value] = self.data.shape[0]
            trc_header[MocapFlags.NumMarkers.value] = len(marker_names)
            self.headers = trc_header
            self.marker_names = marker_names
            for c in range(2, len(self.column_labels), 3):
                marker = split_cols(self.column_labels[c])
                markers[marker].iloc[:, :] = self.data[:, c:c+3]
            self.marker_set = markers
            temp = np.zeros([self.data.shape[0], self.data.shape[1]])
            temp[:, 0] = [i + 1 for i in range(0, self.data.shape[0])]
            temp[:, 1:] = self.data[:, 1:]
            self.data = temp
            self.x = self.data[:, 1]
            pass
        if self.data.shape[0] > 3:
            self.update_Spline()

    def write(self, filename):
        lines = [self.first_line + "\n"]
        line = ""
        for s in self.headers_labels:
            line += s.value+"\t"
        lines.append(line.strip()+"\n")
        line = ""
        for s in self.headers_labels:
            line += "{0}\t".format(self.headers[s.value])
        lines.append(line.strip() + "\n")
        c0 = "Frame#\tTime\t"
        c1 = "\t\t"
        for c in self.marker_names:
            c0 += c + "\t\t\t"
            d = [k for k in self.marker_set[c].columns]
            c1 += "{0}\t{1}\t{2}\t".format(d[0], d[1], d[2])
        lines.append(c0.strip() + "\t\t\n")
        lines.append("\t\t" + c1.strip() + "\n")
        for si in range(0, self.data.shape[0]):
            line = str(int(self.data[si, 0])) + '\t' + str(self.data[si, 1])
            for m in self.marker_names:
                marker = self.marker_set[m]
                x = marker.iloc[si, 0]
                y = marker.iloc[si, 1]
                z = marker.iloc[si, 2]

                xo = "{0:.5f}".format(x)
                yo = "{0:.5f}".format(y)
                zo = "{0:.5f}".format(z)
                if np.isnan(x):
                    xo = ''
                if np.isnan(y):
                    yo = ''
                if np.isnan(z):
                    zo = ''
                line += '\t' + str(xo) + '\t' + str(yo) + '\t' + str(zo)
            line += '\n'
            lines.append(line)
        with open(filename, 'w') as writer:
            writer.writelines(lines)
        pass


class Yadict(object):
    # Yet another dictionary
    # provides core set and get dict
    # and get keys in a list
    def __init__(self, d: dict):
        self.d = d

    def __getitem__(self, item):
        return self.d[item]

    def __setitem__(self, item, value):
        self.d[item] = value

    def get_keys(self):
        return [k for k in self.d]


class OSIMStorage(Yatsdo):
    """
    Coordinates
    version=1
    nRows=4269
    nColumns=40
    inDegrees=yes

    Units are S.I. units (second, meters, Newtons, ...)
    If the header above contains a line with 'inDegrees', this indicates whether rotational values are in degrees (yes) or radians (no).

    endheader
    """
    def __init__(self, data, col_names=None, fill_data=False, filename="", header=None, ext=".sto"):
        super().__init__(data, col_names, fill_data)
        self.filename = filename
        self.header = header
        self.ext = ext

    @staticmethod
    def yes_no_to_true_false(wo):
        if 'yes' in wo.lower():
            return True
        elif 'no' in wo.lower():
            return False
        return None

    @staticmethod
    def true_false_to_yes_no(wo):
        if wo:
            return 'yes'
        elif not wo:
            return 'no'
        return None

    @staticmethod
    def read(filename):
        if not os.path.exists(filename):
            return None
        s = StorageIO.load(filename, StorageType.mot)

        k = s.info["header"]
        header = {"type": k[0].strip(),
                  "version": int(k[1].split('=')[1].strip()),
                  "nRows": int(k[2].split('=')[1].strip()),
                  "nColumns": int(k[3].split('=')[1].strip()),
                  "inDegrees": OSIMStorage.yes_no_to_true_false(k[4].split('=')[1].strip()),
                  "note": "{0}{1}".format(k[6], k[7]),
                  "end": k[9]
                  }
        ret = OSIMStorage(s.data, filename=filename, header=header)
        return ret

    def update(self):
        super().update()
        self.header['nRows'] = self.data.shape[0]
        self.header['nColumns'] = self.data.shape[1]
        pass

    def header2string(self, h):
        if h not in ['type', 'note', 'end']:
            if h not in ['inDegrees']:
                return "{0}={1}\n".format(h, self.header[h])
            else:
                return "{0}={1}\n".format(h, OSIMStorage.true_false_to_yes_no(self.header[h]))
        else:
            return "{0}{1}\n".format('', self.header[h])

    def write(self, filename):
        lines = [self.header2string(h) for h in self.header]
        cols = ""
        for c in self.col_labels:
            cols += c
            cols += "\t"
        lines.append(cols.strip() + "\n")
        for i in range(0, self.data.shape[0]):
            ret = ""
            for j in range(0, self.data.shape[1]):
                ret += "{0:.8f}\t".format(self.data[i,j])
            ret.strip()
            ret += '\n'
            lines.append(ret)
            pass
        # f = open(filename + ".csv", "r")
        # count = 0
        # stream = "Hello world"
        # while len(stream) > 0:
        #     stream = f.readline()
        #     if count == 0:
        #         count += 1
        #         continue
        #     else:
        #         count += 1
        #         lines.append(stream)
        # f.close()

        with open(filename, 'w') as writer:
            writer.writelines(lines)
        pass


class StorageType(Enum):
    unknown = [-1, 'unknown']
    nexus = [0, 'csv']
    mot = [1, 'mot']
    C3D = [3, 'c3d']
    csv = [4, 'csv']
    captureU = [5, 'csv']
    trc = [6, 'trc']
    sto = [7, 'sto']

    @staticmethod
    def check_for_known(ext):
        for st in StorageType:
            s = st.value
            if s[1] in ext.lower():
                return st
        return StorageType.unknown


class StorageIO(object):
    def __init__(self, store: pd.DataFrame = None, buffer_size: int = 10):
        # A storage object that helps read (mot or csv with known formats) and write csv
        self.buffer = []
        self.buffer_size = buffer_size
        if store is not None:
            self.buffer.append(store)
        self.info = {}

    def write_mot(self, filename):
        lines = []
        for h in self.info['header']:
            lines.append(h.strip()+"\n")
        cols = ""
        for c in self.data.columns:
            cols += c
            cols += "\t"
        lines.append(cols.strip()+"\n")
        self.data.to_csv(filename+".csv", sep="\t", index=False)
        f = open(filename+".csv", "r")
        count = 0
        stream = "Hello world"
        while len(stream) > 0:
            stream = f.readline()
            if count == 0:
                count += 1
                continue
            else:
                count += 1
                lines.append(stream)
        f.close()

        with open(filename, 'w') as writer:
            writer.writelines(lines)
        pass

    @property
    def data(self):
        # returns last loaded
        return deepcopy(self.buffer[-1])

    @data.setter
    def data(self, x):
        if len(self.buffer) >= 10:
            self.buffer.pop(0)
        self.buffer.append(x)

    @staticmethod
    def scan_nexus_for_device_data(filename):
        """
        This method currently only tested on outputs from VICON Nexus 2.12
        :param filename: CSV file output from VICON Nexus 2.12
        :return:
        """
        f = open(filename, "r")
        buffer = []
        start_id = -100
        end_id = 0
        ids = 0
        stream = "hoi, allo, moshi moshi?"
        boo = True
        boo1 = True
        meta_ = {'Device Sampling Rate': 0, 'Devices Columns': [], 'Device Unit': {}}
        device_boo = False
        unit_boo = True
        data_buffer = []
        data_frame = 0
        device_end = False
        other_data = []
        while len(stream) > 0:
            stream = f.readline()
            if unit_boo:
                buffer.append(stream)
            if device_boo:
                meta_['Device Sampling Rate'] = float(stream.strip())
                device_boo = False

            if stream.find("Device") > -1 and not device_boo:
                device_boo = True

            if stream.find("Frame") > -1 and boo:
                device_labels = buffer[len(buffer)-2].split(',')
                data_labels = buffer[len(buffer)-1].split(',')
                current_device = ""
                for d in range(0, len(device_labels)):
                    if len(device_labels[d]) > 0:
                        current_device = device_labels[d] + " - "
                        if '\n' in current_device:
                            break
                    meta_['Devices Columns'].append(current_device+data_labels[d])

                start_id = ids + 1
                boo = False
            if ids == start_id:
                dunits = buffer[-1].split(',')
                meta_['Device Unit'] = {meta_['Devices Columns'][dc]: dunits[dc] for dc in range(0, len(meta_['Devices Columns']))}
                unit_boo = False
                pass
            cleaned = stream.strip()
            if ids >= start_id + 1 and not unit_boo:
                elements = cleaned.split(',')
                try:
                    if int(elements[0]) >= data_frame:
                        data_frame = int(elements[0])
                        data_buffer.append(np.asfarray(cleaned.split(',')))
                except ValueError:
                    device_end = True
            if device_end:
                other_data.append(cleaned)
            if len(cleaned) == 0 and boo1 and not boo:
                boo1 = False
                end_id = ids - 1
            ids += 1
        f.close()

        if ids > 0:
            temp = [n for n in data_buffer if n.shape[0] == len(meta_['Devices Columns'])]
            data_np = np.zeros([len(temp), len(meta_['Devices Columns'])])
            for n in range(0, len(temp)):
                data_np[n, :] = temp[n]
            data_df = pd.DataFrame(data=data_np, columns=meta_['Devices Columns'])
            return [i for i in range(0, start_id)], [start_id, end_id], meta_, data_df
        else:
            return [], [], meta_, None

    @staticmethod
    def trc_reader(filename, delimiter="\t", headers=True, fill_data=False):
        """
        This method reads in a trc file.
        :param filename: string path to file
        :param delimiter: how to parse the components, default is tab spaces
        :param headers: Save Header in Object, default is True
        :param fill_data: fill missing frame data, if you have gaps in your trc file used this otherwise trc file will not load
        :return: TRC (Yatdos) object containing the information in the TRC file
        """
        return TRC.read(filename, delimiter, headers, fill_data)

    @staticmethod
    def simple_read(filename, delimit=False, delimiter=","):
        stream = "hoi"
        buffer = []
        try:
            f = open(filename, "r")
            while len(stream) > 0:
                stream = f.readline()
                if delimit:
                    elements = stream.strip().split(delimiter)
                    buffer.append(elements)
                else:
                    buffer.append(stream)
        except OSError:
            print('cannot open', filename)
        finally:
            f.close()
        return buffer

    @staticmethod
    def simple_write(data, filename, delimit=False, delimiter=","):
        try:
            lines = data
            if delimit:
                lines = []
                for b in data:
                    line = ""
                    for i in range(0, len(b)):
                        line += b[i]
                        if i < len(b):
                            line += delimiter
                    line += "\n"
                    lines.append(line)
            with open(filename, 'w') as writer:
                writer.writelines(lines)

        except OSError:
            print('cannot open', filename)

    @staticmethod
    def failsafe_omega(filename, delimiter=",", headers=True):
        """
        If this doesn't work then the file is not a text file
        :param filename:
        :param delimiter:
        :param headers:
        :return:
        """
        ret = []
        lengths = []
        stream = "hoi"
        read_successful = False
        try:
            f = open(filename, "r")
            while len(stream) > 0:
                stream = f.readline()
                elements = stream.strip().split(delimiter)
                ret.append(elements)
                lengths.append(len(elements))
            read_successful = True
        except OSError:
            print('cannot open', filename)
        finally:
            f.close()
        if read_successful:
            col = mode(lengths)[0][0]
            bounty = [b for b in range(0, len(lengths)) if lengths[b] != col]
            ret2 = [ret[i] for i in range(0, len(ret)) if i not in bounty]
            if headers:
                header = ret2.pop(0)
            try:
                data = [[float(i[j]) if j > 0 else int(i[j]) for j in range(0, col)] for i in ret2]
                return pd.DataFrame(data=data, columns=header)
            except ValueError:
                print("Why")
        return None

    @staticmethod
    def mot_header(filename):
        f = open(filename, "r")
        stream = "hoi"
        index = 0
        while len(stream) > 0:
            stream = f.readline()
            if stream.find("endheader") > -1:
                break
            index += 1
        f.close()
        if index > 0:
            return [i for i in range(0, index + 1)]
        else:
            return []

    @staticmethod
    def mot_header_getter(filename):
        f = open(filename, "r")
        stream = "hoi"
        index = 0
        ret = []
        while len(stream) > 0:
            stream = f.readline()
            ret.append(stream)
            if stream.find("endheader") > -1:
                break
            index += 1
        f.close()
        return ret

    @staticmethod
    def file_extension_check(path):
        filename, file_extension = os.path.splitext(path)
        return StorageType.check_for_known(file_extension)

    @staticmethod
    def readc3d_general(filename):
        with open(filename, 'rb') as c3dfile:
            reader = c3d.Reader(c3dfile)
            first_frame = reader.first_frame()
            units = reader.groups.get('POINT').get('UNITS').bytes.decode("utf-8")
            subject_ = reader.groups.get('SUBJECTS')
            subject = {'name': subject_.params['NAMES'].bytes.decode("utf-8").strip(),
                       'marker_sets': subject_.params['MARKER_SETS'].bytes.decode("utf-8").strip(),
                       'is_static': int.from_bytes(subject_.params['IS_STATIC'].bytes, 'little')}
            trial_ = reader.groups.get('TRIAL')
            t0 = int.from_bytes(trial_.params['ACTUAL_START_FIELD'].bytes, 'little')
            t1 = int.from_bytes(trial_.params['ACTUAL_END_FIELD'].bytes, 'little')
            trial = {'camera_rate': struct.unpack('f', trial_.params['CAMERA_RATE'].bytes)[0],
                     'start_frame': t0,
                     'end_frame': t1}
            num_frames = reader.last_frame()-first_frame
            frames = []
            analog_labels = [a.strip() for a in reader.analog_labels]
            point_labels = [a.strip() for a in reader.point_labels]
            rate_diff = int(reader.analog_rate/reader.point_rate)
            ret = {'analog_channels_label': analog_labels,
                   'num_analog_channels': len(analog_labels),
                   'num_frames': num_frames,
                   'first_frame': first_frame,
                   'num_analog_frames': int((num_frames + 1)*rate_diff),
                   'rate-diff(A2M)': rate_diff,
                   'point_label': point_labels,
                   'point_label_expanded': 0,
                   'num_points': len(reader.point_labels),
                   'point_rate': reader.point_rate,
                   'analog_rate': reader.analog_rate,
                   'point_unit': units,
                   'meta_data': {
                       'subject': subject,
                       'trial': trial},
                   'mocap': {"rates": reader.point_rate, "units": units, "header": reader.header}
                   }

            flatten = lambda t: [item for sublist in t for item in sublist]
            expanded_labels = []
            if len(ret['point_label']) > 0:
                expanded_labels = [[label+'_X', label+'_Y', label+'_Z'] for label in point_labels]
                expanded_labels = flatten(expanded_labels)

            if ret['num_analog_channels'] > 0:
                analog_data = np.zeros([(first_frame+num_frames)*int(rate_diff), ret['num_analog_channels'] + 2])
            else:
                analog_data = None
            if ret['num_points'] > 0:
                point_data = np.zeros([num_frames+first_frame, ret['num_points']*3])
            else:
                point_data = None

            for i, points, analog in reader.read_frames():
                if analog_data is not None:
                    try:
                        analog_data[(i-1)*rate_diff: ((i-1)*rate_diff)+rate_diff, 2:] = analog.transpose()
                    except ValueError:
                        analog_data[(i - 1) * rate_diff: ((i - 1) * rate_diff) + rate_diff, 2:] = analog
                        analog_data[(i - 1) * rate_diff: ((i - 1) * rate_diff) + rate_diff, 1] = [j for j in range(0, rate_diff)]
                        analog_data[(i - 1) * rate_diff: ((i - 1) * rate_diff) + rate_diff, 0] = i
                if point_data is not None:
                    xyz = points[:, :3]
                    point_data[(i-1), :] = xyz.reshape([1, ret['num_points']*3])
                frames.append({"id": i, "points": points, "analog": analog})
            df = pd.DataFrame(data=point_data, columns=expanded_labels)
            cols_analog = ["mocap-frame", "sub-frame"]
            for c in ret['analog_channels_label']:
                cols_analog.append(c)
            df_analog = pd.DataFrame(data=analog_data, columns=cols_analog)
            ret['point_data'] = df
            ret['analog_data'] = df_analog
            ret['data'] = frames
        return ret

    @staticmethod
    def simple_readc3d(filename, exportas_text=False):
        with open(filename, 'rb') as c3dfile:
            reader = c3d.Reader(c3dfile)
            units = reader.groups.get('POINT').get('UNITS').bytes.decode("utf-8")
            ret = ""
            markers = {'time': []}
            count = 0
            marker_labels = ['time']
            for pl in reader.point_labels:
                lb = pl.strip()
                if len(lb) == 0:
                    lb = 'M{:03d}'.format(count)
                    count += 1
                marker_labels.append(lb)
                markers[lb] = []
            analogs = []
            for i, points, analog in reader.read_frames():
                if isinstance(analog, np.ndarray):
                    if len(analog.shape) > 1:
                        analog_temp = pd.DataFrame(data=analog, columns=reader.analog_labels)
                        # analogs.append(analog[:, 0])
                        # This contains more than 1 frame as there is usually 5 frames per 1 frame of point data if
                        # data OMC is recorded at 200 Hz and analog data recorded at 1000 Hz
                        analogs.append(analog_temp)
                markers[marker_labels[0]].append((i-1)*(1/reader.point_rate))
                for j in range(1, len(marker_labels)):
                    errors = points[j-1, 3:]
                    p = points[j-1, 0:3]
                    for e in errors:
                        if e == -1:
                            p = np.asarray([np.NaN, np.NaN, np.NaN])
                            break
                    markers[marker_labels[j]].append(p)
                if exportas_text:
                    ret += 'frame {}: point {}, analog {}'.format(
                        i, points, analog)
            box = {'markers': markers, 'mocap': {"rates": reader.point_rate, "units": units, "header": reader.header}, 'text': ret, "analog": analogs}
        return box

    @staticmethod
    def load(filename, sto_type: StorageType = None, fill_data=False):
        if sto_type is None:
            sto_type = StorageType.check_for_known(filename)
        sto = StorageIO()
        skip_rows = []
        delimiter = ' '
        data = None
        is_trc = False
        if sto_type == StorageType.nexus:
            skip_rows, st_en, meta_data, data = StorageIO.scan_nexus_for_device_data(filename)
            delimiter = ','
            sto.info["devices"] = meta_data
        elif sto_type == StorageType.mot or sto_type == StorageType.sto:
            skip_rows = StorageIO.mot_header(filename)
            sto.info["header"] = StorageIO.mot_header_getter(filename)
            delimiter = '\t'
        elif sto_type == StorageType.csv or sto_type == StorageType.captureU:
            delimiter = ','
        elif sto_type == StorageType.trc:
            delimiter = '\t'
            is_trc = True
        trc = None
        try:
            if sto_type == StorageType.nexus:
                p = data
                time_list = [i*1/meta_data['Device Sampling Rate'] for i in range(0, p.shape[0])]
                p.insert(2, 'time', time_list)
            if sto_type == StorageType.trc:
                trc = StorageIO.trc_reader(filename, delimiter=delimiter, fill_data=fill_data)
                p = trc.to_panda()
            if sto_type == StorageType.mot or sto_type == StorageType.csv or sto_type == StorageType.captureU:
                p = pd.read_csv(filename, delimiter=delimiter, skiprows=skip_rows)

        except pd.errors.ParserError:
            p = StorageIO.failsafe_omega(filename, delimiter=delimiter)
            if p is None:
                return None
        except IOError:
            p = StorageIO.failsafe_omega(filename, delimiter=delimiter)
            if p is None:
                return None
        col = p.columns
        drops = [c for c in col if 'Unnamed' in c]
        q = p.drop(columns=drops)
        sto.buffer.append(q)
        drops = [c for c in col if 'unix_timestamp_microsec' in c]
        if len(drops) > 0:
            r = p.drop(columns=drops)
            sto.buffer.append(r)
        try:
            sto.find_dt(sto_type)
        except TypeError:
            sto.info['dt'] = 0.01
        if not is_trc:
            return sto
        else:
            return trc

    def to_csv(self, path):
        self.data.to_csv(path, index=False)

    def to_yatsdo(self, remove_jitter=True):
        y = Yatsdo(self.data)
        if remove_jitter:
            return y.remove_jitter()
        return y

    def separate(self, parts, first_col=None):
        if len(parts) == 0:
            return {"imu": self.data}
        if len(parts) == 1:
            return {parts[0]: self.data}
        data_headings = self.data.columns
        separated_part = {}
        for b in parts:
            part = []
            if first_col is not None:
                part = [first_col]
            for c in data_headings:
                if b in c.lower():
                    part.append(c)
            separated_part[b] = self.data[part]
        return separated_part

    def find_dt(self, st):
        # a helper function for a special case where get_first column is time
        t = 0
        if st == StorageType.trc:
            t = 1
        df = self.data.to_numpy()
        df0 = df[:-1, t]
        df1 = df[1:, t]
        dt = -1
        if df.shape[0] > 1:
            dt = np.round(np.nanmean(df1 - df0), 5)
            self.info['dt'] = dt
        return dt

    @staticmethod
    def load_json(file):
        return JSONSUtl.load_json(file)

    @staticmethod
    def write_json(pathto, dictm):
        return JSONSUtl.write_json(pathto, dictm)

    @property
    def dt(self):
        try:
            return self.info['dt']
        except KeyError:
            self.find_dt(StorageType.mot)


class BasicVoxelInfo:
    # This is data holder
    def __init__(self, slice_thickness=0.625, image_size=[512, 512]):
        self.slice_thickness = slice_thickness
        self.image_size = image_size
        self.padding = 10
        self.marker_size = 4


class MYXML:
    # This is a simple xml reader/ writer
    def __init__(self, filename):
        self.tree = minidom.parse(filename)

    def set_value(self, tag, value):
        a = self.tree.getElementsByTagName(tag)
        print("Before: {0}".format(a[0].childNodes[0].data))
        a[0].childNodes[0].data = value
        print("After: {0}".format(a[0].childNodes[0].data))
        pass

    def add_node(self, parent, new_node, value):
        x = self.tree.getElementsByTagName(parent)
        a = self.tree.createElement(new_node)
        b = self.tree.createTextNode(value)
        a.appendChild(b)
        x[0].appendChild(a)

    def add_value(self, node, value):
        x = self.tree.getElementsByTagName(node)
        b = self.tree.createTextNode(value)
        x[0].appendChild(b)

    def write(self, filename, pretty=False):
        myfile = open(filename, "w")
        if pretty:
            dom_str = self.tree.toprettyxml()
            dom_str_element = [f+"\n" for f in dom_str.split("\n") if len(f.strip()) > 0]
            dom_str_new = ""
            dom_str_new = dom_str_new.join(dom_str_element)
            myfile.write(dom_str_new)
        else:
            myfile.write(self.tree.toxml())
        myfile.close()


class VTKMeshUtl(Enum):
    ply = [".ply", vtk.vtkPLYReader, vtk.vtkPLYWriter]
    stl = [".stl", vtk.vtkSTLReader, vtk.vtkSTLWriter]
    obj = [".obj", vtk.vtkOBJReader, vtk.vtkOBJWriter]
    vtp = [".vtp", vtk.vtkXMLPolyDataReader, vtk.vtkXMLPolyDataWriter]
    none = [None, None, None]
    unknown = ["unknown", None, None]

    @staticmethod
    def color_convert(color):
        return [color[0] / 255.0, color[1] / 255, color[2] / 255]

    @staticmethod
    def smooth_mesh(polydata, iter=15, factor=0.2):
        # Create a vtkSmoothPolyDataFilter
        smooth_filter = vtk.vtkSmoothPolyDataFilter()
        smooth_filter.SetInputData(polydata)
        smooth_filter.SetNumberOfIterations(iter)  # adjust this value to increase smoothing
        smooth_filter.SetRelaxationFactor(factor)  # adjust this value to increase smoothing
        smooth_filter.FeatureEdgeSmoothingOff()
        smooth_filter.BoundarySmoothingOff()
        smooth_filter.Update()
        return smooth_filter.GetOutput()

    @staticmethod
    def merge_meshes(polys):
        appendFilter = vtk.vtkAppendPolyData()
        for p in polys:
            appendFilter.AddInputData(p)
        # Merge the meshes
        appendFilter.Update()
        return appendFilter.GetOutput()

    @staticmethod
    def point_cloud_to_ply(file_name, cols=None, transform=None):
        if cols is None:
            cols = ['X', 'Y', 'Z']
        # Load the point cloud data from a CSV file
        df = pd.read_csv(file_name)
        if transform is not None and isinstance(transform, np.ndarray):
            n = df.to_numpy()
            np_points = np.ones([4, n.shape[0]])
            np_points[:3, :] = n.T
            transformed = np.matmul(transform, np_points)
            df = pd.DataFrame(data=transformed[:3, :].T, columns=cols)

        # Create a vtkPoints object and insert the points into it
        points = vtk.vtkPoints()
        for index, row in df.iterrows():
            points.InsertNextPoint(row[cols[0]], row[cols[1]], row[cols[2]])

        # Create a vtkPolyData object and add the points to it
        polydata = vtk.vtkPolyData()
        polydata.SetPoints(points)

        # Write the polydata to a .ply file
        plyWriter = vtk.vtkPLYWriter()
        plyWriter.SetFileName('{0}.ply'.format(file_name[:file_name.rindex('.')]))
        plyWriter.SetInputData(polydata)
        plyWriter.Write()

    @staticmethod
    def clone_poly(poly):
        p = vtk.vtkPolyData()
        p.DeepCopy(poly)
        return p

    @staticmethod
    def is_valid(filename):
        k = VTKMeshUtl.get_type(filename)
        if VTKMeshUtl.none == k or VTKMeshUtl.unknown:
            return False
        return True

    # @staticmethod
    # def test_load_mesh_as_actor():
    #     vertices = VTKMeshUtl.extract_points(self.mean_mesh)
    #     vertices0 = np.asarray([self.mean_mesh.GetPoint(i) for i in range(self.mean_mesh.GetNumberOfPoints())])

    @staticmethod
    def get_type(filename):
        if filename is None:
            return VTKMeshUtl.none
        if filename.endswith(".ply"):
            return VTKMeshUtl.ply
        if filename.endswith(".stl"):
            return VTKMeshUtl.stl
        if filename.endswith(".obj"):
            return VTKMeshUtl.obj
        if filename.endswith(".vtp"):
            return VTKMeshUtl.vtp
        return VTKMeshUtl.unknown

    @staticmethod
    def is_none(mesh_type):
        if VTKMeshUtl.none == mesh_type:
            return True
        else:
            return False

    def reader(self):
        return self.value[1]()

    def writer(self):
        return self.value[2]()

    def label(self):
        return self.value[0]

    @staticmethod
    def volume(polydata):
        mass_props = vtk.vtkMassProperties()
        mass_props.SetInputData(polydata)
        mass_props.Update()
        return mass_props.GetVolume()

    @staticmethod
    def center_of_mass(polydata):
        center_of_mass = vtk.vtkCenterOfMass()
        center_of_mass.SetInputData(polydata)
        center_of_mass.Update()
        return center_of_mass.GetCenter()

    @staticmethod
    def cog(polydata):
        return VTKMeshUtl.center_of_mass(polydata)

    @staticmethod
    def get_polydata_f_actor(actor):
        return actor.GetMapper().GetInput()

    @staticmethod
    def mesh2exf(mesh_filename: str, output_meshname: str, region_name: str = "", group_name: str = "external",
                 print_info=True):
        if print_info:
            print("mesh2exf: {0} -> {1}".format(mesh_filename, output_meshname))
        """
        This script reads in a mesh file and export as exf data point set.
        Supported mesh type is stl, ply and obj
        :param mesh_filename: full path to the mesh file
        :param output_meshname: full path to the save location/ name of the file
        :param region_name: name of the region (optional)
        :param group_name: name of the group (optional), default is external
        :return: -
        """
        header = "EX Version: 2\n"
        header += "Region: /{0}\n".format(region_name)
        header += "!#nodeset datapoints\n"
        header += "Shape. Dimension=0\n"
        header += "#Fields=1\n"
        header += "1) data_coordinates, coordinate, rectangular cartesian, real, #Components=3\n"
        header += " x. #Values=1 (value)\n"
        header += " y. #Values=1 (value)\n"
        header += " z. #Values=1 (value)\n"
        nodes = ""
        footer = " Group name: {0}\n".format(group_name)
        footer += "!#nodeset datapoints\n"
        footer += "Shape. Dimension=0\n"
        footer += "#Fields=0\n"
        node_list = ""

        # Reading mesh data
        if mesh_filename.endswith(".obj"):
            reader = vtk.vtkOBJReader()
        elif mesh_filename.endswith(".ply"):
            reader = vtk.vtkPLYReader()
        elif mesh_filename.endswith(".stl"):
            reader = vtk.vtkSTLReader()
        else:
            print("Unknown file format")
            return

        reader.SetFileName(mesh_filename)
        reader.Update()

        # Extracting points data
        polydata = reader.GetOutput()
        points = np.asarray([polydata.GetPoint(i) for i in range(polydata.GetNumberOfPoints())])
        for i in range(0, points.shape[0]):
            node = "Node: {0}\n".format(i + 1)
            idx = str(i + 1)
            max_idx = str(points.shape[0])
            n = idx.rjust(len(max_idx), ' ')
            node_list += " Node: {0}\n".format(n)
            p = points[i, :]
            for j in range(0, 3):
                node += " {: .15e}\n".format(p[j])
            nodes += node
            pass

        # Writing out to exf file
        if len(output_meshname) > 0 and output_meshname.endswith(".exf"):
            try:
                with open(output_meshname, 'w') as f:
                    f.write(header)
                    f.write(nodes)
                    f.write(footer)
                    f.write(node_list)
            except IOError:
                print("error saving the exf file")
                pass
        else:
            print("Invalid output filename or path")

    @staticmethod
    def slice_mesh_to_images(polydata, list_of_locations: list, pixel_spacing, slice_thickness=1.5,
                             image_size=(512, 512)):
        bar = tqdm(range(len(list_of_locations)),
                   desc="Making masks",
                   ascii=False, ncols=100, colour="#6e5b5b")
        ret = []
        for p in list_of_locations:
            blackImage = vtk.vtkImageData()
            blackImage.SetSpacing(pixel_spacing[0], pixel_spacing[1], slice_thickness)
            blackImage.SetDimensions(image_size[0], image_size[1], 1)
            blackImage.SetOrigin(p[1][0], p[1][1], p[1][2])
            blackImage.AllocateScalars(vtk.VTK_UNSIGNED_CHAR, 1)
            inval = 255
            outval = 0
            count = blackImage.GetNumberOfPoints()
            for i in range(0, count):
                blackImage.GetPointData().GetScalars().SetTuple1(i, inval)
            pol2stenc = vtk.vtkPolyDataToImageStencil()
            pol2stenc.SetInputData(polydata)
            pol2stenc.SetOutputOrigin(p[1][0], p[1][1], p[1][2])
            pol2stenc.SetOutputSpacing(pixel_spacing[0], pixel_spacing[1], 1.5)
            pol2stenc.SetOutputWholeExtent(blackImage.GetExtent())
            pol2stenc.Update()

            imgstenc = vtk.vtkImageStencil()
            imgstenc.SetInputData(blackImage)
            imgstenc.SetStencilConnection(pol2stenc.GetOutputPort())
            imgstenc.ReverseStencilOff()
            imgstenc.SetBackgroundValue(outval)
            imgstenc.Update()

            flip = vtk.vtkImageFlip()
            flip.SetInputConnection(imgstenc.GetOutputPort())
            flip.SetFilteredAxis(1)
            flip.FlipAboutOriginOn()
            flip.ReleaseDataFlagOn()
            flip.Update()
            ret.append(flip.GetOutput())
            bar.update(1)
        bar.close()
        return ret

    @staticmethod
    def sub_mesh(node_id_list, polydata, debug = False, try_speed_up=2):
        mesh_data_copy = vtk.vtkPolyData()
        mesh_data_copy.DeepCopy(polydata)
        cell_data = mesh_data_copy.GetPolys()
        # Get the point data and cell data of the mesh
        if debug:
            # currently slow for large meshes
            points_l = []
            if try_speed_up == 1:
                points_l = [mesh_data_copy.GetPoint(node_id_list[i]) for i in range(len(node_id_list))]
            else:
                for i in range(len(node_id_list)):
                    p = mesh_data_copy.GetPoint(node_id_list[i])
                    points_l.append(p)

        if try_speed_up == 1:
            def steps(i):
                cell = mesh_data_copy.GetCell(i)
                p1 = cell.GetPointId(0)
                p2 = cell.GetPointId(1)
                p3 = cell.GetPointId(2)
                if not (p1 in node_id_list and p2 in node_id_list and p3 in node_id_list):
                    mesh_data_copy.DeleteCell(i)
                return 1
            temp = [steps(i) for i in range(cell_data.GetNumberOfCells())]

        else:
            for i in range(cell_data.GetNumberOfCells()):
                cell = mesh_data_copy.GetCell(i)
                p1 = cell.GetPointId(0)
                p2 = cell.GetPointId(1)
                p3 = cell.GetPointId(2)
                if not (p1 in node_id_list and p2 in node_id_list and p3 in node_id_list):
                    mesh_data_copy.DeleteCell(i)
        mesh_data_copy.RemoveDeletedCells()
        return VTKMeshUtl.clean(mesh_data_copy)

    @staticmethod
    def closest_point(tar_point, polydata, idx=-1):
        octree = vtk.vtkOctreePointLocator()
        octree.SetDataSet(polydata)
        octree.BuildLocator()
        result = octree.FindClosestPoint(tar_point)
        point = polydata.GetPoint(result)
        diff = np.linalg.norm(point - tar_point)
        return [idx, result, diff]

    @staticmethod
    def closest_point_set(point_set, polydata):
        arg_list = [[point_set[i, :], polydata, i] for i in range(0, point_set.shape[0])]
        ret = [VTKMeshUtl.closest_point(w[0], w[1], w[2]) for w in arg_list]
        return pd.DataFrame(data=ret, columns=["idx", "idm", "errors"])

    @staticmethod
    def apply_icp(__source__, icp):
        icp_transform_filter = vtk.vtkTransformPolyDataFilter()
        icp_transform_filter.SetInputData(__source__)
        icp_transform_filter.SetTransform(icp)
        icp_transform_filter.Update()
        return icp_transform_filter.GetOutput()

    @staticmethod
    def icp(__source__, __target__, num_iter=1000):
        icp = vtk.vtkIterativeClosestPointTransform()
        icp.SetSource(__source__)
        icp.SetTarget(__target__)
        icp.GetLandmarkTransform().SetModeToRigidBody()
        icp.SetMeanDistanceModeToRMS()
        icp.SetMaximumMeanDistance(1e-6)
        # icp.DebugOn()
        icp.SetMaximumNumberOfIterations(num_iter)
        icp.StartByMatchingCentroidsOn()
        icp.Modified()
        icp.Update()

        return icp

    @staticmethod
    def mirror(_polydata_: vtk.vtkPolyData):
        m1 = np.eye(4)
        m1[1, 1] = -1
        m2 = np.eye(4)
        m2[0, 0] = -1
        m2[1, 1] = -1
        m3 = np.matmul(m2, m1)
        vtk_points: np.ndarray = VTKMeshUtl.extract_points(_polydata_)
        points = np.ones([4, vtk_points.shape[0]])
        points[:3, :] = vtk_points.T
        new_points = np.matmul(m3, points)[:3, :].T
        return VTKMeshUtl.update_poly_w_points(new_points, _polydata_)

    @staticmethod
    def draw_line_between_two_points(orig, p1, colour=None, line_width=5):
        if colour is None:
            colors = vtk.vtkNamedColors()
            v = colors.GetColor3d('Tomato')
            colour = [v.GetRed(), v.GetGreen(), v.GetBlue()]

        points = vtk.vtkPoints()
        points.InsertNextPoint(orig)
        points.InsertNextPoint(p1)

        polyLine = vtk.vtkPolyLine()
        polyLine.GetPointIds().SetNumberOfIds(2)
        for i in range(0, 2):
            polyLine.GetPointIds().SetId(i, i)

        # Create a cell array to store the lines in and add the lines to it
        cells = vtk.vtkCellArray()
        cells.InsertNextCell(polyLine)

        # Create a polydata to store everything in
        polyData = vtk.vtkPolyData()

        # Add the points to the dataset
        polyData.SetPoints(points)

        # Add the lines to the dataset
        polyData.SetLines(cells)

        # Setup actor and mapper
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputData(polyData)

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor(colour)
        actor.GetProperty().SetLineWidth(line_width)
        return actor

    @staticmethod
    def clip_and_fill(poly, origin, norm=None, radius=1, cut_type='plane'):
        cut_obj = None
        if cut_type == 'plane' and norm is not None:
            plane = vtk.vtkPlane()
            plane.SetOrigin(origin[0], origin[1], origin[2])
            plane.SetNormal(norm[0], norm[1], norm[2])
            cut_obj = plane
        elif cut_type == 'sphere':
            sphere = vtk.vtkSphere()
            sphere.SetCenter(origin[0], origin[1], origin[2])
            sphere.SetRadius(radius)
            pass
        clipper = vtk.vtkClipPolyData()
        clipper.SetClipFunction(cut_obj)
        clipper.SetInputConnection(poly)
        clipper.Update()

        # Use vtkFillHolesFilter to fill the hole created by clipping
        fillHoles = vtk.vtkFillHolesFilter()
        fillHoles.SetInputConnection(clipper.GetOutputPort())
        fillHoles.SetHoleSize(1000.0)  # Adjust the hole size limit as needed
        fillHoles.Update()

        filledMapper = vtk.vtkPolyDataMapper()
        filledMapper.SetInputConnection(fillHoles.GetOutputPort())
        return filledMapper


    @staticmethod
    def cal_normals(poly):
        normals = vtk.vtkPolyDataNormals()
        normals.SetInputData(poly)
        normals.ComputeCellNormalsOn()
        normals.SetAutoOrientNormals(True)
        normals.Update()
        return normals

    @staticmethod
    def normals_as_numpy(normals: vtk.vtkPolyDataNormals):
        output = normals.GetOutput()
        cell_data = output.GetPointData()
        _normals = cell_data.GetNormals()
        return np.array([_normals.GetTuple3(f) for f in range(_normals.GetNumberOfTuples())])

    @staticmethod
    def extract_points(_data_: Union[vtk.vtkPolyData, vtk.vtkActor]):
        if isinstance(_data_, vtk.vtkPolyData):
            return vtk_to_numpy(_data_.GetPoints().GetData())
        return vtk_to_numpy(_data_.GetMapper().GetInput().GetPoints().GetData())


    @staticmethod
    def make_sphere(loc, size=3.0):
        sphereSource = vtk.vtkSphereSource()
        sphereSource.SetCenter(loc[0], loc[1], loc[2])
        sphereSource.SetRadius(size)
        # Make the surface smooth.
        sphereSource.SetPhiResolution(100)
        sphereSource.SetThetaResolution(100)

        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(sphereSource.GetOutputPort())

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        colors = vtk.vtkNamedColors()
        actor.GetProperty().SetColor(colors.GetColor3d("Cornsilk"))
        return actor, sphereSource

    @staticmethod
    def update_poly_w_points(_points_: np.ndarray, _polydata_: vtk.vtkPolyData):
        _vh_ = vtk.vtkPoints()
        _vh_.SetData(numpy_to_vtk(_points_))
        _polydata_.SetPoints(_vh_)
        return _polydata_

    @staticmethod
    def extract_triangles(_polydata_: vtk.vtkPolyData):
        # assumes that faces are triangular
        poly_data = _polydata_.GetPolys().GetData()
        face_indices = vtk_to_numpy(poly_data)

        face_indices = face_indices.reshape((-1, 4))
        _nFaces = face_indices.shape[0]
        _triangles = face_indices[:, 1:].copy()
        return {"n_faces": _nFaces, "triangles": _triangles}

    @staticmethod
    def clean(polydata):
        clean_poly = vtk.vtkCleanPolyData()
        clean_poly.SetInputData(polydata)
        clean_poly.Update()
        return clean_poly.GetOutput()

    @staticmethod
    def write(filename, polydata):
        """

        :param filename: path
        :param polydata: polydata
        :return:
        """
        try:
            v = VTKMeshUtl.get_type(filename)
            cleaned = VTKMeshUtl.clean(polydata)
            w = v.writer()
            w.SetInputData(cleaned)
            w.SetFileName(filename)
            w.Write()
        except:
            print("Cannot output "+filename)

    def read(self, mesh_filename):
        if os.path.exists(mesh_filename):
            read = self.reader()
            read.SetFileName(mesh_filename)
            read.Update()
            return read.GetOutput()
        return None

    @staticmethod
    def load(mesh_filename, clean_mesh=True):
        """
        Load mesh as polydata
        :param mesh_filename: filepath of the mesh
        :param clean_mesh: remove unreferenced nodes
        :return: vtk.vtkPolyData
        """
        if os.path.exists(mesh_filename):
            mesh_type = VTKMeshUtl.get_type(mesh_filename)
            poly = mesh_type.read(mesh_filename)
            if clean_mesh and poly is not None:
                return VTKMeshUtl.clean(poly)
            else:
                return poly
        else:
            print("File not found: {0} does not exist!".format(mesh_filename))
            return None


class Mesh:
    def __init__(self, filename=None, ignore=False):
        self.mesh_filename = filename
        self.actor = None
        self.mesh_tool = VTKMeshUtl.get_type(filename)
        self.reader = self.mesh_tool.reader()
        self.current_reader = self.mesh_tool.label()

        self.mesh = None
        self.__points__ = None
        self.cog = None
        self.mc = None
        self.volume = None
        self.ignore_check = ignore
        if self.mesh_filename is not None:
            self.load_as_vtk(self.mesh_filename)

    @property
    def points(self):
        return self.__points__

    @points.setter
    def points(self, p):
        if isinstance(p, vtk.vtkPolyData):
            self.__points__ = VTKMeshUtl.extract_points(p)
        elif isinstance(p, np.ndarray):
            self.__points__ = p
        else:
            self.__points__ = None

    @staticmethod
    def convert_vtp_2_stl(vtp_file, stl_out):
        reader = vtk.vtkXMLPolyDataReader()
        reader.SetFileName(vtp_file)
        reader.Update()

        stl_writer = vtk.vtkSTLWriter()
        stl_writer.SetFileName(stl_out)
        stl_writer.SetInputConnection(reader.GetOutputPort())
        stl_writer.Write()

    @staticmethod
    def convert_vtp_2_vtp(vtp_file, stl_out):
        reader = vtk.vtkXMLPolyDataReader()
        reader.SetFileName(vtp_file)
        reader.Update()

        vtp_writer = vtkXMLPolyDataWriter()
        vtp_writer.SetFileName(stl_out)
        vtp_writer.SetDataModeToAscii()
        vtp_writer.SetInputConnection(reader.GetOutputPort())
        vtp_writer.Write()

    @staticmethod
    def convert(in_file, out_file):
        in_poly = VTKMeshUtl.load(in_file)
        VTKMeshUtl.write(out_file, in_poly)

    @staticmethod
    def convert_batch(working_dir: str, to_ext: str = ".ply"):
        if os.path.exists(working_dir):
            if not working_dir.endswith("/") or working_dir.endswith("\\"):
                working_dir += "/"
            wr_list = [f for f in os.listdir(working_dir) if VTKMeshUtl.is_valid(working_dir+f)]
            for w in wr_list:
                en = w.rindex(".")
                Mesh.convert(working_dir + w, working_dir + w[:en] + to_ext)

    @staticmethod
    def convert_vtp_2_stl_batch(wr):
        wr_list = os.listdir(wr)
        for w in wr_list:
            en = w.rindex(".")
            print(w[:en])
            Mesh.convert_vtp_2_stl(wr + w[:en] + '.vtp', wr + w[:en] + '.stl')

    def load_as_vtk(self, filename):
        self.mesh_filename: str = filename
        self.mesh_tool = VTKMeshUtl.get_type(self.mesh_filename)
        self.reader = self.mesh_tool.reader()
        self.mesh = VTKMeshUtl.load(self.mesh_filename)
        self.cog = VTKMeshUtl.cog(self.mesh)
        self.volume = VTKMeshUtl.volume(self.mesh)
        self.mc = self.cog
        self.points = VTKMeshUtl.extract_points(self.mesh)
        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            # mapper.SetInput(reader.GetOutput())
            mapper.SetInput(self.mesh)
        else:
            mapper.SetInputData(self.mesh)
        self.actor = vtk.vtkActor()
        self.actor.SetMapper(mapper)

    @property
    def center_of_gravity(self):
        return self.cog

    @property
    def mesh_center(self):
        return self.mc

    def apply_transform(self, m):
        axis = 0
        if self.points.shape[0] == 3:
            axis = 1
        dummy = np.ones([4, self.points.shape[axis]])

        if self.points.shape[0] == 3:
            dummy[:3, :] = self.points
        else:
            dummy[:3, :] = self.points.T

        self.points = (np.matmul(m, dummy))[:3, :].T
        self.update()

    def update(self):
        VTKMeshUtl.update_poly_w_points(self.mesh)
        self.cog = VTKMeshUtl.cog(self.mesh)
        self.volume = VTKMeshUtl.volume(self.mesh)
        self.mc = self.cog

    def center_mesh(self):
        trf = np.eye(4)
        trf[0:3, 3] = -self.cog
        self.apply_transform(trf)

    def translate_mesh(self, trl):
        trf = np.eye(4)
        trf[0:3, 3] = trl
        self.apply_transform(trf)

    def primary_axes(self):
        # primary X, secondary y, tertiary Z
        centered_data, cm = Stat.center_data(self.points)
        c = Stat.covariance_matrix(centered_data)
        e1, v1 = Stat.eig_rh(c)
        return v1, cm

    def rotate_points(self, r):
        # assumes r is 3x3
        new_points = np.matmul(r, self.points.transpose())
        return new_points.transpose()

    def principle_alignment(self, cogmcpstv=False):
        # center mesh to cloud centre
        v0, t0 = self.primary_axes()
        trf = np.eye(4)
        trf[0:3, 0:3] = v0.transpose()
        trf[:3, 3] = t0
        self.apply_transform(trf)
        diff = self.cog - self.mc
        if cogmcpstv and diff[0] < 0:
            # assumes cog and mesh centre is different
            r = Rotation.from_euler("xyz", [0, 0, np.pi])
            trf = np.eye(4)
            trf[0:3, 0:3] = r
            self.apply_transform(trf)
        pass

    def write_mesh(self, filename):
        VTKMeshUtl.write(filename, self.mesh)


class XROMMUtil:
    @staticmethod
    def save_maya_cam(output_filename, image_size: np.ndarray, k: np.ndarray, r: np.ndarray, t: np.ndarray, dist=None):
        image = 'image size\n{0:d}, {1:d}\n\n'.format(image_size[0], image_size[0])
        camera = 'camera matrix\n{0:f},{0:f},{0:f}\n{0:f},{0:f},{0:f}\n{0:f},{0:f},{0:f}\n\n'.format(
            k[0, 0], k[0, 1], k[0, 2], k[1, 0], k[1, 1], k[1, 2], k[2, 0], k[2, 1], k[2, 2]
        )
        rotation = 'rotation\n{0:f},{0:f},{0:f}\n{0:f},{0:f},{0:f}\n{0:f},{0:f},{0:f}\n\n'.format(
            r[0, 0], r[0, 1], r[0, 2], r[1, 0], r[1, 1], r[1, 2], r[2, 0], r[2, 1], r[2, 2]
        )
        translation = 'translation\n{0:f}\n{0:f}\n{0:f}\n\n'.format(
            t[0], t[1], t[2]
        )
        if dist is not None:
            print("Not Implemented!")
        try:
            file1 = open(output_filename, "w")  # write mode
            file1.write(image+camera+rotation+translation)
            file1.close()
        except IOError:
            print("Write Error!")


class IMU(Yatsdo):
    '''
    This is IMU data storage object
    '''

    window_frame = 0
    window_time = 1

    def __init__(self, data: [pd.DataFrame, np.ndarray], col_names: list = None, acc: list = None, gyro: list = None,
                 mag:list = None, ori=None):
        if col_names is None:
            col_names = []
        super().__init__(data, col_names)
        self.acc_id: Optional[list] = acc
        self.gyr_id: Optional[list] = gyro
        self.mag_id: Optional[list] = mag
        self.ori = ori
        self.ori_filtered = True


    @property
    def acc_gyr_data(self):
        acc_gyr_id = []
        for i in self.acc_id:
            acc_gyr_id.append(i)
        for j in self.gyr_id:
            acc_gyr_id.append(j)
        if isinstance(self.data, pd.DataFrame):
            return self.data[self.column_labels[acc_gyr_id]]
        elif isinstance(self.data, np.ndarray):
            ret = pd.DataFrame(data=self.data, columns=self.column_labels)
            get_ = [self.column_labels[c] for c in acc_gyr_id]
            get_.insert(0, "time")
            return ret[get_]

    @property
    def acc(self):
        if isinstance(self.data, pd.DataFrame):
            return self.data[self.column_labels[self.acc_id]]
        elif isinstance(self.data, np.ndarray):
            ret = pd.DataFrame(data=self.data, columns=self.column_labels)
            get_ = [self.column_labels[c] for c in self.acc_id]
            get_.insert(0, "time")
            return ret[get_]

    @property
    def gyr(self):
        if isinstance(self.data, pd.DataFrame):
            return self.data[self.column_labels[self.gyr_id]]
        elif isinstance(self.data, np.ndarray):
            ret = pd.DataFrame(data=self.data, columns=self.column_labels)
            get_ = [self.column_labels[c] for c in self.gyr_id]
            get_.insert(0, "time")
            return ret[get_]

    @property
    def mag(self):
        if isinstance(self.data, pd.DataFrame):
            return self.data[self.column_labels[self.mag_id]]
        elif isinstance(self.data, np.ndarray):
            ret = pd.DataFrame(data=self.data, columns=self.column_labels)
            get_ = [self.column_labels[c] for c in self.mag_id]
            get_.insert(0, "time")
            return ret[get_]

    @acc.setter
    def acc(self, data):
        raise NotImplementedError

    @gyr.setter
    def gyr(self, data):
        raise NotImplementedError

    @mag.setter
    def mag(self, data):
        raise NotImplementedError

    @staticmethod
    def create(s: StorageIO, key='TS'):
        rate = s.info['devices']['Device Sampling Rate']
        time = [i*(1/rate) for i in range(0, s.data.shape[0])]
        imu_labels = [c for c in s.data.columns if ('TS' in c and 'acc' in c) or ('TS' in c and 'gyr' in c) or ('TS' in c and 'mag' in c)]
        imu_id = []
        for c in imu_labels:
            elem = c.split('-')[1]
            idx = (key+'-'+elem).strip()
            if idx not in imu_id:
                imu_id.append(idx)
        imu_labels.insert(0, "time")
        imu_labels_dict = {"a": imu_labels}
        if len(imu_id) > 1:
            imu_labels_dict = {c: [d for d in imu_labels if c in d or 'time' in d] for c in imu_id}
        ret = {}

        def set_imu(imu_ls, s0, time0):
            temp = s0[imu_ls[1:]]
            data = np.zeros([temp.shape[0], temp.shape[1] + 1])
            data[:, 0] = time0
            data[:, 1:] = temp
            imu = IMU(data, imu_ls)
            imu.acc_id = [c for c in range(0, len(imu_labels)) if 'acc' in imu_labels[c].lower()]
            imu.gyr_id = [c for c in range(0, len(imu_labels)) if 'gyr' in imu_labels[c].lower()]
            imu.mag_id = [c for c in range(0, len(imu_labels)) if 'mag' in imu_labels[c].lower()]
            return imu

        for k in imu_labels_dict:
            ret[k] = set_imu(imu_labels_dict[k], s.data[imu_labels_dict[k][1:]], time)
        return ret

    def butterworth_filter(self, cut_off, sampling_rate):
        for d in range(1, self.acc.size[1]):
            data = self.acc.data[:, d]
            filtered = Butterworth.butter_low_filter(data, cut_off, sampling_rate, order=4)
            self.acc.data[:, d] = filtered
        self.acc.update()

        for d in range(1, self.gyro.size[1]):
            data = self.gyro.data[:, d]
            filtered = Butterworth.butter_low_filter(data, cut_off, sampling_rate, order=4)
            self.gyro.data[:, d] = filtered
        self.gyro.update()

        if self.mag is not None:
            for d in range(1, self.mag.size[1]):
                data = self.mag.data[:, d]
                filtered = Butterworth.butter_low_filter(data, cut_off, sampling_rate, order=4)
                self.mag.data[:, d] = filtered
            self.mag.update()

        if self.ori is not None and not self.ori_filtered:
            for d in range(1, self.ori.size[1]):
                data = self.mag.ori[:, d]
                filtered = Butterworth.butter_low_filter(data, cut_off, sampling_rate, order=4)
                self.ori.data[:, d] = filtered
            self.ori.update()

    def export(self, filename, boo=[True, True, False, True]):
        imu = [self.acc, self.gyr, self.mag, self.ori]
        ret = np.zeros([self.size[0], 1+(np.sum(boo)*3)])
        columns = ['time']
        boos = ['acc', 'gyr', 'mag', 'ori']
        ret[:, 0] = self.acc.x
        col = 1
        for i in range(0, len(boos)):
            if boo[i]:
                columns.append(boos[i]+'_x')
                columns.append(boos[i]+'_y')
                columns.append(boos[i]+'_z')
                ret[:, col: col+3] = imu[i].data[:, 1:]
                col = col + 3

        p = pd.DataFrame(data=ret, columns=columns)
        p.to_csv(filename, index=False)


class Stat(object):
    @staticmethod
    def center_data(x: np.ndarray):
        # calculate mean and center data to mean
        u = np.atleast_2d(np.sum(x, axis=0) / x.shape[0])
        u = u.transpose()
        um = np.repeat(u.transpose(), x.shape[0], axis=0)
        return x - um, np.squeeze(u)

    @staticmethod
    def covariance_matrix(x: np.ndarray):
        return (1 / x.shape[0]) * np.dot((np.transpose(x)), x)

    @staticmethod
    def eig_rh(x):
        v, p = np.linalg.eig(x)
        k = np.sort(v)[::-1]  # descending order
        j = [np.where(v == i)[0][0] for i in k]
        p0 = p[:, j]
        det_ = np.round(np.linalg.det(p0), 10)
        if det_ == -1.0:
            new_z = np.cross(p0[:, 0], p0[:, 1])
            rh = np.zeros([3, 3])
            rh[:, 0] = p0[:, 0]
            rh[:, 1] = p0[:, 1]
            rh[:, 2] = new_z
            p0 = rh
        return k, p0


class MinionKey(Enum):
    butterworth = 0
    cutoff = 1
    sampling_freq = 2
    order = 3
    apple = 4
    banana = 5
    bapple = 6
    by_time = 7
    by_index = 8
    chop_type = 8.1
    all = 10
    columns = 11
    size = 12
    data = 13
    start = 14
    increment = 15
    dt = 16
    combined = 17
    id = 18

    @staticmethod
    def get_by_name(name):
        for v in MinionKey:
            if v.name == name:
                return v


class Bapple:
    def __init__(self, x: Yatsdo = None, y: Yatsdo = None):
        """
        Version 2 of the Bapple (simplified):
        This Bapple just holds the data, export and import using npz instead of csv
        Hopefully this is faster
        :param x: Measurement/ Input data (i.e. IMU) (Type: pandas Dataframe)
        :param y: Measurement/ Input data (i.e. Target) (Type: pandas Dataframe)
        """
        self.__apples__ = x
        self.__bananas__ = y
        self.__filename__ = ""
        self.__filepath__ = ""

    @property
    def apples(self):
        """
        This creates a property of the class call apples, which allow you to access the apples (x)
        :return: apples
        """
        return self.__apples__

    @apples.setter
    def apples(self, a, para=None):
        if isinstance(a, np.ndarray) and para is not None:
            self.__apples__ = Yatsdo(a, para[MinionKey.columns])
        elif isinstance(a, pd.DataFrame) or isinstance(a, np.ndarray):
            self.__apples__ = Yatsdo(a)

    @property
    def bananas(self):
        """
        This creates a property of the class call bananas, which allow you to access the bananas (y)
        :return: banana
        """
        return self.__bananas__

    @bananas.setter
    def bananas(self, b, para=None):
        if isinstance(b, np.ndarray) and para is not None:
            self.__bananas__ = Yatsdo(b, para[MinionKey.columns])
        elif isinstance(b, pd.DataFrame) or isinstance(b, np.ndarray):
            self.__bananas__ = Yatsdo(b)

    @staticmethod
    def load(filepath: str):
        """
        This method direct loads bapple into memory
        :param filepath:
        :return:
        """
        start = time.time()
        block = np.load(filepath)
        meta_data = json.load(io.BytesIO(block['meta_data.json']))
        print("Loading apples and bananas")
        data_block = None
        a_block = None
        b_block = None
        try:
            data_block = np.load(io.BytesIO(block['data.npz']))
        except KeyError:
            a_block = np.frombuffer(block['a.npz'])
            a_block = a_block.reshape(meta_data['apple']['shape'])
            b_block = np.frombuffer(block['b.npz'])
            b_block = b_block.reshape(meta_data['banana']['shape'])

        print("Preparing the Bapple")
        a_bapple = None
        b_bapple = None

        if data_block is not None:
            a_bapple = pd.DataFrame(data=np.asarray(data_block['a']), columns=meta_data['apple'])
            b_bapple = pd.DataFrame(data=np.asarray(data_block['b']), columns=meta_data['banana'])
        if a_block is not None:
            a_bapple = pd.DataFrame(data=a_block, columns=meta_data['apple']['cols'])
        if b_block is not None:
            b_bapple = pd.DataFrame(data=b_block, columns=meta_data['banana']['cols'])

        bar = tqdm(total=3, desc="Cooking", ascii=False,
                   ncols=120,
                   colour="#6e5b5b")
        bar.update(2)
        bapple = Bapple(x=Yatsdo(a_bapple), y=Yatsdo(b_bapple))
        bar.update(1)
        bar.close()
        end = time.time()
        print("Order up: One Bapple > Load completed in {0:0.2f}s".format(end - start))
        return bapple

    def export(self, filename: str, pathto: str, compresslevel=1):
        """
        Export bapple to a zip file
        Note: May fail if there is not enough space for the temp folder
        :param filename:
        :param pathto:
        :return:
        """
        start = time.time()
        if not pathto.endswith("/") or not pathto.endswith("\\"):
            pathto += "/"

        meta_data = {}

        if not os.path.exists(pathto+"temp/"):
            os.makedirs(pathto+"temp/")

        num_files = 0
        if self.apples is not None:
            num_files += 1
        if self.bananas is not None:
            num_files += 1
        num_files += 1

        with zipfile.ZipFile(pathto + filename, mode="w", compression=zipfile.ZIP_DEFLATED, compresslevel=compresslevel) as archive:
            bar = tqdm(total=num_files, desc="Saving to {0} >".format(filename), ascii=False,
                       ncols=120,
                       colour="#6e5b5b")
            if self.apples is not None and self.bananas is not None:
                bar.desc = "Saving to {0} > Exporting Apples and Bananas".format(filename)
                bar.refresh()
                meta_data['apple'] = {'cols': [c for c in self.apples.column_labels], 'shape': self.apples.data.shape}
                meta_data['banana'] = {'cols': [c for c in self.bananas.column_labels], 'shape': self.bananas.data.shape}
                anp = self.apples.data
                bnp = self.bananas.data

                archive.writestr("a.npz", anp.tobytes())
                bar.update(1)
                archive.writestr("b.npz", bnp.tobytes())
                bar.update(1)

            bar.desc = "Saving to {0} > Exporting meta_data.json".format(filename)
            bar.refresh()
            json_string = json.dumps(meta_data)
            meta_data_bytes = json_string.encode('utf-8')
            archive.writestr("meta_data.json", meta_data_bytes)
            bar.update(1)
            bar.close()
        end = time.time()
        print("Order up: One Bapple > Saved completed in {0:0.2f}s".format(end - start))
        pass


def stamp():
    now = datetime.now()
    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S.%f")
    return dt_string, now


def date_time_convert(date_str):
    return datetime.strptime(date_str, "%d/%m/%Y %H:%M:%S")


def resample(data, target_freq):
    yoda = data
    # type check
    if isinstance(data, pd.DataFrame) or isinstance(data, np.ndarray):
        yoda = Yatsdo(data)
    elif not isinstance(data, Yatsdo):
        return None
    start = yoda.x[0]
    end = yoda.x[-1]
    cs = np.round((end - start) / (1.0 / target_freq), 5) + 1
    tp = [np.round(start + c * (1 / target_freq), 5) for c in range(0, int(cs))]
    d = yoda.get_samples(tp)
    if isinstance(data, pd.DataFrame):
        d = pd.DataFrame(data=d, columns=data.columns)
    elif isinstance(data, Yatsdo):
        return yoda
    return d

