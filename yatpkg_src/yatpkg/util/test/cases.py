import numpy as np
import pandas as pd
import os
from yatpkg.util.data import VTKMeshUtl
from yatpkg.math.transformation import Cloud


if __name__ == '__main__':
    folder = "E:/Document/Ju_cortical_thickness/"
    files = [folder + f for f in os.listdir(folder) if f.endswith(".csv")]
    transformation = np.array([[0.852505326271, 0.508775949478, 0.119923293591, -347.455566406250],
                               [0.507691025734, -0.860527038574, 0.041747033596, -198.764663696289],
                               [0.124437108636, 0.025294482708, -0.991905093193, -867.112487792969],
                               [0.000000000000, 0.000000000000, 0.000000000000, 1.000000000000]])

    for f in files:
        df = pd.read_csv(f)
        n0 = df.to_numpy()
        np_points = np.ones([4, n0.shape[0]])

        np_points[:3, :] = n0.T
        transformed = np.matmul(transformation, np_points)
        scale = np.array([[0.852505326271, 0.0, 0.0, 0],
                          [0.0, 0.860527038574, 0.0, 0],
                          [0.0, 0.0, 0.991905093193, 0],
                          [0.000000000000, 0.000000000000, 0.000000000000, 1.000000000000]])
        transformed = np.matmul(scale, transformed)
        n1 = transformed[:3, :].T
        r = Cloud.rigid_body_transform(n0.T, n1.T)
        pass
        # VTKMeshUtl.point_cloud_to_ply(f, transform=transformation)
    # root = "V:/Working Dir/dicoms/"
    # age = [d for d in os.listdir(root) if os.path.isdir(root+d+"/")]
    # part = {d: [root+d+"/"+ f +"/" for f in os.listdir(root+d) if os.path.isdir(root+d+"/"+f)] for d in age}
    # for p in part:
    #     for f in part[p]:
    #         if os.path.exists(f+"/"+"5_deidentified.nii.gz"):
    #             os.remove(f + "/"+"5_deidentified.nii.gz")
    pass