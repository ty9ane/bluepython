import numpy as np
import pandas as pd

from yatpkg.util.model import ShapeModel
import matplotlib.pyplot as plt


def example_norm_dist():
    x = np.random.normal(0, 1, 10000000)

    plt.hist(x, 1000)
    plt.show()


if __name__ in '__main__':

    mean_mesh = "E:/Repo/bluepython/gui_src/blue_ui/shape/articulating_ssm_gui/meshes/mean_mesh.ply"
    model = ShapeModel(pc="E:/Repo/bluepython/gui_src/blue_ui/shape/articulating_ssm_gui/maps/pca_model.joblib",
                       mean_mesh=mean_mesh)
    transform = pd.read_csv("E:/Repo/bluepython/gui_src/blue_ui/shape/gui_tibia_thickness_example/maps/transform_tibia.csv", header=None)
    # scale = np.diag([1.01, 1.01, 1.01, 1])
    # transform = np.matmul(scale, transform)
    # model.sample_ssm(filename="tibia", transform=transform.to_numpy())
    pass
