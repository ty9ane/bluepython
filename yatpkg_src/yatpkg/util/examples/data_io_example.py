from yatpkg.util.data import Mesh, StorageIO, StorageType, MYXML, IMU
import os


def read_IMU_example():
    global s, k
    s = StorageIO.load("C:/Users/ty8on/Studies/RTS/device_data/P12/box dop single 01.csv")
    imu = IMU.create(s)
    for k in imu:
        acc = imu[k].acc_gyr_data
        pass


def read_nexus_file_example():
    wa = StorageIO.load("G:\\Shared drives\\SFTI Project\\Backup\\SFTIWearable\\Test\\S17\\Session\\walk01.csv",
                        StorageType.nexus)
    wb = StorageIO.load("G:\\Shared drives\\SFTI Project\\Backup\\SFTIWearable\\Test\\S17\\Session\\walk02.csv",
                        StorageType.nexus)


def batch_convert_example():
    Mesh.convert_vtp_2_stl_batch("C:/OpenSim 4.2/Geometry/")
    Mesh.convert_vtp_2_stl_batch(
        "Z:/Shared drives/Rocco Hip Replacement Study/IMU Gait Rice Uni/IMU Thor Gait Analysis/opensim_model/geom/")
    lab = ["P01", "P02", "P03", "P04", "P05"]
    for i in lab:
        f = "C:\\Users\\tyeu008\\MapclientWorkingDir\\" + i + "\\osim_model\\geom\\"
        Mesh.convert_vtp_2_stl_batch(f)


def force_plate_data_example():
    global s, w, f, a
    s = StorageIO.load("G:/Shared drives/Rocco Hip Replacement Study/01/opensim/pre-op/force_plate/Sit00.mot",
                       StorageType.mot)
    s.write_mot("G:/Shared drives/Rocco Hip Replacement Study/01/opensim/pre-op/force_plate/Sit00abc.mot")
    m = MYXML("G:/Shared drives/Rocco Hip Replacement Study/01/opensim/xml/sitpre.xml")
    m.set_value("results_directory", "abc")
    m.write("G:/Shared drives/Rocco Hip Replacement Study/01/opensim/xml/sitpre_test.xml")
    print()
    vicon_config = []
    w = ""
    se = ""
    s = ""
    s0 = {}
    for cf in vicon_config:
        f = w + s + "/" + se + "/" + cf
        m = MYXML(f)
        dc = s0[s]
        a = m.tree.getElementsByTagName("ParamDefinitionList")
        for b in a:
            if b.attributes['name'].value == "DeviceOutputComponent::Analog EMG::Voltage":
                c = b.getElementsByTagName("ParamList")
                for d in c:
                    p = d.getElementsByTagName("Param")
                    for n in p:
                        if n.attributes['name'].value == "DeviceOutputComponentName":
                            print(n.attributes['value'].value)
                            n.attributes['value'].value = dc[n.attributes['value'].value]
                            print(n.attributes['value'].value)
                            break
                pass
        m.write(f)


def emg_and_force_plate_from_c3d_example():
    global a, f
    a = StorageIO.readc3d_general("X:\\Rocco\\06\\8Weeks\\Fast.c3d")
    analog = a['analog_data']
    emg_col = [c for c in analog.columns if 'emg' in c.lower()]
    emg_col.insert(0, 'sub-frame')
    emg_col.insert(0, 'mocap-frame')
    force_plate_col = ['mocap-frame', 'sub-frame',
                       'Force.Fx1', 'Force.Fy1', 'Force.Fz1', 'Moment.Mx1', 'Moment.My1', 'Moment.Mz1',
                       'Force.Fx2', 'Force.Fy2', 'Force.Fz2', 'Moment.Mx2', 'Moment.My2', 'Moment.Mz2',
                       'Force.Fx3', 'Force.Fy3', 'Force.Fz3', 'Moment.Mx3', 'Moment.My3', 'Moment.Mz3']
    emg = analog[emg_col]
    force_plate = analog[force_plate_col]
    folders_in = [
        "X:\\Rocco\\06\\4 weeks\\",
        "X:\\Rocco\\06\\8Weeks\\",
        "X:\\Rocco\\06\\Pre\\"
    ]
    folders_out = [
        "X:\\Rocco\\ForcePlate\\06\\4Wk\\",
        "X:\\Rocco\\ForcePlate\\06\\8Wk\\",
        "X:\\Rocco\\ForcePlate\\06\\PreWk\\"
    ]
    folders_in = [
        "X:\\Rocco\\01\\Post-op_1_4wk\\",
        "X:\\Rocco\\01\\Post-op_2_8wk\\",
        "X:\\Rocco\\01\\Pre-op\\"
    ]
    folders_out = [
        "X:\\Rocco\\ForcePlate\\01\\4Wk\\",
        "X:\\Rocco\\ForcePlate\\01\\8Wk\\",
        "X:\\Rocco\\ForcePlate\\01\\PreWk\\"
    ]
    folders_in = [
        "X:\\Rocco\\02\\4_weeks\\",
        "X:\\Rocco\\02\\8 weeks\\",
        "X:\\Rocco\\02\\Pre-op\\"
    ]
    folders_out = [
        "X:\\Rocco\\ForcePlate\\02\\4Wk\\",
        "X:\\Rocco\\ForcePlate\\02\\8Wk\\",
        "X:\\Rocco\\ForcePlate\\02\\PreWk\\"
    ]
    force_plate_col = ['mocap-frame', 'sub-frame',
                       'Force.Fx1', 'Force.Fy1', 'Force.Fz1', 'Moment.Mx1', 'Moment.My1', 'Moment.Mz1',
                       'Force.Fx2', 'Force.Fy2', 'Force.Fz2', 'Moment.Mx2', 'Moment.My2', 'Moment.Mz2',
                       'Force.Fx3', 'Force.Fy3', 'Force.Fz3', 'Moment.Mx3', 'Moment.My3', 'Moment.Mz3']
    folders_in = [
        "X:\\Rocco\\04\\Post-4wk\\",
        "X:\\Rocco\\04\\Post-8wk\\",
        "X:\\Rocco\\04\\Pre-op\\"
    ]
    folders_out = [
        "X:\\Rocco\\ForcePlate\\04\\4Wk\\",
        "X:\\Rocco\\ForcePlate\\04\\8Wk\\",
        "X:\\Rocco\\ForcePlate\\04\\PreWk\\"
    ]
    folders_in = [
        "X:\\Rocco\\05\\4WkPostOp\\",
        "X:\\Rocco\\05\\8WkPostOp\\",
        "X:\\Rocco\\05\\PreOp\\"
    ]
    folders_out = [
        "X:\\Rocco\\ForcePlate\\05\\4Wk\\",
        "X:\\Rocco\\ForcePlate\\05\\8Wk\\",
        "X:\\Rocco\\ForcePlate\\05\\PreWk\\"
    ]
    force_plate_col = ['mocap-frame', 'sub-frame',
                       'Force.Fx1', 'Force.Fy1', 'Force.Fz1', 'Moment.Mx1', 'Moment.My1', 'Moment.Mz1',
                       'Force.Fx2', 'Force.Fy2', 'Force.Fz2', 'Moment.Mx2', 'Moment.My2', 'Moment.Mz2',
                       ]
    for fold in range(0, len(folders_in)):
        files = [f for f in os.listdir(folders_in[fold]) if f.endswith('.c3d')]
        if not os.path.exists(folders_out[fold]):
            os.makedirs(folders_out[fold])
        for f in files:
            try:
                a = StorageIO.readc3d_general(folders_in[fold] + f)
                analog = a['analog_data']
                force_plate = analog[force_plate_col]
                force_plate.to_csv(folders_out[fold] + f + '.csv', index=False)
            except AttributeError:
                pass
    print()


def mesh_alignment_using_PC_example():
    test_mesh = Mesh("F:\\Work\\Mimics\\set\\Humerus\\16_5631_l_humerus.stl", ignore=False)
    test_mesh.principle_alignment()
    test_mesh.write_mesh("F:\\Work\\Mimics\\set\\Humerus\\16_5631_l_humerus_test.stl")


if __name__ == '__main__':
    read_IMU_example()
    read_nexus_file_example()
    batch_convert_example()
    force_plate_data_example()
    emg_and_force_plate_from_c3d_example()
    mesh_alignment_using_PC_example()
