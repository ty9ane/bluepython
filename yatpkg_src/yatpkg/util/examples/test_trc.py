from yatpkg.util.data import StorageIO, TRC
from scipy.spatial.transform import Rotation as R
import numpy as np
import os


if __name__ == "__main__":
    # folder where the trc files are
    # folder = "M:/Mocap/P001/Session01/"
    # # get trc files
    # trcs = [t for t in os.listdir(folder) if t.endswith('.trc')]
    # t = "Straight normal 1.trc"
    # trc: TRC = StorageIO.trc_reader(folder + t, delimiter="\t", fill_data=True)
    # r = R.from_euler('xyz', [90, 0, 90], degrees=True)
    # d = np.zeros([trc.data.shape[0], trc.data.shape[1]-2])
    # # apply transformations
    # for i in range(0, d.shape[1], 3):
    #     j = 2+i
    #     marker = trc.data[:, j:j+3].T
    #     new_marker = np.matmul(r.as_matrix(), marker)
    #     d[:, i:i+3] = new_marker.T
    # trc.data[:, 2:] = d
    # trc.update()
    # trc.write(folder + "{0}_filled.trc".format(t[:t.rindex('.')]))
    # trc: TRC = StorageIO.trc_reader("C:/Users/tyeu008/Downloads/" + "walking_speed.trc", delimiter="\t", fill_data=False)
    trc: TRC = TRC.read("C:/Users/tyeu008/Downloads/" + "walking_speed.trc", delimiter="\t", fill_data=False)
    # this is just a test time frame. You will need to replace the time points with the one you need.
    # period = 1.722 - 0.517
    # num_frames = period/(1/1000)
    # timepoints = [t*(1/1000) + 0.517 for t in range(0, int(num_frames))]
    # subset = trc.get_samples_as_trc(timepoints)
    subset = trc.get_data_between_timepoints(1.724, 2.975, 1000)

    subset.write("C:/Users/tyeu008/Downloads/walking_speed_sub.trc")
    pass
    # for t in trcs:
    #     try:
    #         # load in trcs
    #         trc: TRC = StorageIO.trc_reader(folder+t, delimiter="\t")
    #         # transform the marker data z up
    #         r = R.from_euler('xyz', [90, 0, 90], degrees=True)
    #         d = np.zeros([trc.data.shape[0], trc.data.shape[1]-2])
    #         # apply transformations
    #         for i in range(0, d.shape[1], 3):
    #             j = 2+i
    #             marker = trc.data[:, j:j+3].T
    #             new_marker = np.matmul(r.as_matrix(), marker)
    #             d[:, i:i+3] = new_marker.T
    #         trc.data[:, 2:] = d
    #         trc.update()
    #         # export new data
    #         trc.write(folder+"{0}_z_up.trc".format(t[:t.rindex('.')]))
    #     except IndexError:
    #         continue
