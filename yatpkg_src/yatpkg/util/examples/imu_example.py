from yatpkg.util.imu import BlueTridentTools, Direction
import pandas as pd
import matplotlib.pyplot as plt


if __name__ == '__main__':
    # for testing
    # drive_letter = tools.find_drive_letter("Samsung_T5")
    # s0 = StorageIO.load(
    #     drive_letter + "/Github/simulathor/validation_data_part_2/treadmill/P08/IMU/Walk/ProcessedIMUWalk03.mot",
    #     StorageType.mot)
    # paras = {'linear_scale': ScaleFactors.mm2m,
    #          'rotation_scale': ScaleFactors.deg2rad,
    #          'column_labels': ['foot', 'shank', 'thigh', 'pelvis'],
    #          'first_col': 'time',
    #          'target_rate': 200,
    #          'butterworth_low_cutoff': 6}
    # BasicIMUFilter.run(s0, paras)
    # s0 = pd.read_excel("F:/WorkingDir/IMUWalk_test.xlsx")
    # cols_all = [c for c in s0.columns]
    # cols = [c for c in s0.columns if c == "time" or "ts" in c.lower()]
    # cols_index = [c for c in range(0, len(cols_all)) if cols_all[c] == "time" or "ts" in cols_all[c].lower()]
    # col_new = []
    # for i in range(0, len(cols_index)):
    #     b = cols[i]
    #     a0 = s0.iloc[0, cols_index[i]]
    #     col_new.append(b + "_" + a0)
    #     if b != 'time':
    #         a1 = s0.iloc[0, cols_index[i]+1]
    #         a2 = s0.iloc[0, cols_index[i]+2]
    #         col_new.append(b + "_" + a1)
    #         col_new.append(b + "_" + a2)
    # data = s0.iloc[2:, :].to_numpy()
    # data_reformat = pd.DataFrame(data=data, columns=col_new)
    # data_reformat.to_csv("F:/WorkingDir/IMUWalk_test.csv", index=False)

    ############################################################
    # Step

    s = pd.read_csv("F:/WorkingDir/IMUWalk_test.csv")
    big = pd.read_csv("C:\\Users\\tyeu008\\Documents\\Knee_replacment study\\P045\\TM\\TM Take Home 3 June 2021-1_TS-00637_2021-06-15-09-51-36_lowg.csv")
    plt.plot(big["time_s"], big["gz_deg/s"])
    plt.show()
    leg = {Direction.Left: "2028", Direction.Right: "1497"}

    s_shank = BlueTridentTools.extract_imu_set(leg, s)
    s_shank2 = BlueTridentTools.filter(s_shank, 10, 1000)
    heel_strike = BlueTridentTools.extract_initial_contact(s_shank2, leg)
    pass