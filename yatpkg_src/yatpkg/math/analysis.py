import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from yatpkg.util.data import StorageIO, StorageType, Yatsdo, TRC
from scipy.signal import find_peaks

from yatpkg.math.filters import Butterworth


class Gait:
    force_plate_col = ['mocap-frame', 'sub-frame',
                       'Force.Fx1', 'Force.Fy1', 'Force.Fz1', 'Moment.Mx1', 'Moment.My1', 'Moment.Mz1',
                       'Force.Fx2', 'Force.Fy2', 'Force.Fz2', 'Moment.Mx2', 'Moment.My2', 'Moment.Mz2',
                       'Force.Fx3', 'Force.Fy3', 'Force.Fz3', 'Moment.Mx3', 'Moment.My3', 'Moment.Mz3',
                       ]

    def __init__(self, data_file):
        self.buffer = None
        self.y = None
        if data_file.endswith('.mot') or data_file.endswith('.sto'):
            self.buffer = StorageIO.load(data_file, sto_type=StorageType.mot)
            self.y = Yatsdo(self.buffer.data)
        elif data_file.endswith('.c3d'):
            marker = TRC.create_from_c3d(data_file)
            a = StorageIO.readc3d_general(data_file)
            analog = a['analog_data']
            self.buffer = {"data": marker, "analog": analog}
            self.y = self.buffer['analog']

    def extract_cycle_based_on_knee(self, ky=["time", "knee_angle_r"], t_start=10.0, offset=8, do_plot=False):
        time_x = self.buffer.data[ky[0]]
        # butterworth filter, using 5Hz
        new_trial_filtered = Butterworth.butter_low_filter(self.buffer.data[ky[1]].to_numpy(), 5, 200, order=3)
        # combine
        combined_data = np.zeros([new_trial_filtered.shape[0], 2])
        combined_data[:, 0] = time_x
        combined_data[:, 1] = new_trial_filtered
        combined_data_df = pd.DataFrame(data=combined_data, columns=ky)
        # create new feature set
        knee_angle = -1 * new_trial_filtered
        # time = new_trial.iloc[:, 1]
        max_value = max(knee_angle)
        # find max so we know what max value is
        peak_index = find_peaks(knee_angle, height=0.8 * max_value)[0]  # can set height = value (80% of max height)

        # heel strike/ toe off determination
        # flip and use function to determine when peaks are, highest should correlate to heel strike & toe off
        knee_angle_2 = new_trial_filtered
        max_value2 = max(knee_angle_2)
        min_value2 = min(knee_angle_2)
        shifted = knee_angle_2 - min_value2
        peak_index_2 = find_peaks(shifted, height=0.8 * max_value2)[0]

        if do_plot:
            # code below plots each of the trials with peaks highlighted to double check all is ok
            # peak swing faze plot to check
            fig, ax = plt.subplots()
            ax.plot(time_x, knee_angle)
            ax.plot(time_x.iloc[peak_index], knee_angle[peak_index], 'rx')
            plt.title('Inverse Peak Plot')
            plt.show()

            # heel strike & toe off plot to check
            # second plot to check
            fig, ax2 = plt.subplots()
            ax2.plot(time_x, shifted)
            ax2.plot(time_x.iloc[peak_index_2], shifted[peak_index_2], 'rx')
            plt.title('Normal peak plot but shifted up to positive')
            plt.show()

        knee = []
        all_joint = []
        for i in range(0, len(peak_index_2) - offset, 2):
            start = time_x[peak_index_2[i]]
            if time_x[peak_index_2[i]] < t_start:
                continue

            end = time_x[peak_index_2[i + 2]]
            dt = (end - start) / 101.0
            ts = [start + s * dt for s in range(0, 101)]
            xy = self.y.get_samples(ts)
            idx = self.y.column_labels.index(ky[1])
            knee.append(xy[:, idx])
            all_joint.append(pd.DataFrame(data=xy, columns=self.y.column_labels))

        if do_plot:
            fig, ax2 = plt.subplots()
            gc = [s for s in range(0, 101)]
            for v in knee:
                ax2.plot(gc, v)
            plt.title('heel to heel ' + ky[1])
            plt.show()

        gc = [s for s in range(0, 101)]
        k = np.nanmean(knee, axis=0)
        kstd = np.nanstd(knee, axis=0)
        k0 = k - 2.0 * kstd
        k1 = k + 2.0 * kstd
        fig, ax2 = plt.subplots()
        ax2.plot(gc, k)
        ax2.plot(gc, k0)
        ax2.plot(gc, k1)
        plt.title('heel to heel ' + ky[1])
        plt.xlabel('Percentage')
        plt.ylabel('Knee Angle')
        plt.show()
        return all_joint

    def extract_cycle_based_on_force(self, ky=["time", "F"], t_start=15, do_plot=True):
        pass

if __name__ == "__main__":
    # g = Gait("F:/WorkingDir/TreadmillML/target/raw/P08-WalkIKResults.mot")
    # cycles = g.extract_cycle_based_on_knee(t_start=15, do_plot=True)

    g = Gait("X:/SFTIWearable/Test/S02/Session3/walk01.c3d")
    force_plate = g.y[Gait.force_plate_col]
    max_np = np.max(np.abs(force_plate[['Force.Fx1', 'Force.Fy1', 'Force.Fz1', 'Moment.Mx1', 'Moment.My1', 'Moment.Mz1']]), axis=0)
    bleep = np.zeros([force_plate.shape[0], 3])
    first_plate = force_plate[['Force.Fx1', 'Force.Fy1', 'Force.Fz1']]
    for i in range(0, bleep.shape[0]):
        for j in range(0, bleep.shape[1]):
            if np.abs(force_plate[i, j]) > 0.1:
                bleep[i, j] = 1
    cycles = g.extract_cycle_based_on_force(t_start=15, do_plot=True)
    pass
