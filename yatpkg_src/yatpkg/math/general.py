import numpy as np


class DataError:
    @ staticmethod
    def rms(x: np.ndarray, y: np.ndarray):
        y = np.atleast_2d(y)
        diff = (x - y)
        ds_mean = np.nanmean(diff ** 2)
        ds_std = np.nanstd(diff)
        rms = np.sqrt(ds_mean)
        return rms, ds_std

    @staticmethod
    def mae(x: np.ndarray, y: np.ndarray):
        y = np.atleast_2d(y.to_numpy())
        diff = np.abs(x - y)
        ds_mean = np.nanmean(diff)
        return ds_mean


class Decomposition:
    @staticmethod
    def polar_decomposition(a: np.ndarray):
        t = np.zeros([3, 1])
        u = np.eye(3)
        r = np.eye(3)
        if a.shape[0] == 4 and a.shape[1] == 4:
            f = a[0:3, 0:3]
            t = a[0:3, 4]
        elif a.shape[0] == 3 and a.shape[1] == 3:
            f = a
        c = np.matmul(f.transpose(), f)
        if np.linalg.cond(c) > 1e-12:
            u = np.linalg.sqrtm(c)
            r = np.matmul(f, np.linalg.inv(u))
        return r, u, t