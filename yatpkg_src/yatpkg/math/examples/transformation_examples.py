from yatpkg.util.data import StorageIO
from yatpkg.math.transformation import Cloud, Quaternion
import yatpkg.math.examples
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.transform.rotation import Rotation as rt
import pandas as pd


if __name__ == '__main__':
    # Quick Tests (uncomment to test)
    test_markerset_to_image_stack = False
    test_quaternion = False
    test_transformation = True

    # data = StorageIO.readc3d_general("../../resources/c3d_test/Latency_0_Correction_01.c3d")
    # data = StorageIO.readc3d_general("../../resources/markerset_to_image_stack_test/input/XSHD00001_crossCal.c3d")
    # plt.figure()
    # plt.plot(data['analog_data']['EMGs.1'])
    # plt.show()

    pass
    if test_markerset_to_image_stack:
        Cloud.markerset_to_image_stack(
            c3d_file="../../resources/markerset_to_image_stack_test/input/XSHD00001_crossCal.c3d",
            out_dir="../../resources/markerset_to_image_stack_test/output/",
            filename="XSHD00001_crossCal_v01")

    if test_quaternion:
        q = Quaternion(np.array([[1], [0.5], [0.2], [0]]))
        print(q)
        q = Quaternion(np.array([[1, 0.5, 0.2, 0]]))
        print(q)
        q = Quaternion(np.array([1, 0.5, 0.2, 0]))
        print(q)
        q = Quaternion([1, 0.5, 0.2, 0])
        print(q)
        q = Quaternion()
        print(q)
        q = Quaternion([])
        print(q)
        q = Quaternion(np.array([]))
        print(q)
        q = Quaternion(np.array([0, 1, 2, 3, 4, 5]))
        print(q)

        q0 = Quaternion.create_from_euler('zyx', [90, 90, 45], True)
        print(q)
        q1 = Quaternion.null()
        print(q)

    if test_transformation:
        print("Test SVD extracted transform")
        m = [[0.8151962, -0.5093911, 0.2756374],
             [0.5572944, 0.8194665, -0.1337818],
             [-0.1577284, 0.2626695, 0.9519067]]
        r: rt = rt.from_matrix(m)
        m0 = r.as_matrix()
        # ensure pure rotation
        check = np.linalg.det(m0)
        e = np.eye(4)
        e[0:3, 0:3] = m0
        e[0:3, 3] = [1, 10, 100]

        # where the data came from
        t = StorageIO.load("X:\\SFTIWearable\\Test\\S16\\New Session\\re_walk01.trc")
        d = t.data
        dc = [c for c in d.columns if "LASIS" in c or "RASIS" in c or "LPSIS" in c or "RPSIS" in c]
        dc.insert(0, 'Time')

        d_pelvis = d[dc]
        # n_pelviso = np.reshape(d_pelvis.to_numpy()[17, 1:], [4, 3])
        nx = d_pelvis.to_numpy()
        r = Cloud.sphere_fit(nx[:, 4:7])
        n = np.cross

        n_pelvis = np.array([[-132.08162, 880.33862, -4458.0352],
                             [-263.81378, 879.20624, -4655.4858],
                             [-67.274658, 937.98987, -4676.7939],
                             [-37.100178, 937.26154, -4637.7295]])
        n_pelvis_T = n_pelvis.transpose()
        np.cross()
        n_pelvis_T_ones = np.ones([4, 4])
        n_pelvis_T_ones[0:3, :] = n_pelvis_T
        x_pelvis_T_ones = np.matmul(e, n_pelvis_T_ones)
        m1 = Cloud.transformation_from_svd(n_pelvis_T_ones[0:3, :], x_pelvis_T_ones[0:3, :])
        test = np.matmul(e, np.linalg.inv(m1))
        eye = np.max(np.abs(test - np.eye(4, 4)))
        if eye < 1e-10:
            print("Passed Check")
        else:
            print("Failed Check")
        print()
    pass
