import numpy as np
import joblib
from yatpkg.util.data import Mesh
from yatpkg.math.transformation import Cloud
from enum import Enum
from copy import deepcopy
import vtk
import pandas as pd
from vtkmodules.util.numpy_support import numpy_to_vtk
import os


class Keywords(Enum):
    actor = [0, 'actor']
    polydata = [1, 'polydata']
    vertices = [2, 'vertices']
    idx = [3, 'idx']
    idm = [4, 'idm']
    obj = [5, 'obj']
    ply = [6, 'ply']
    stl = [7, 'stl']


class StatisticalShapeModel:
    def __init__(self, pc: str = None, mean_mesh=None):
        """

        :param pc: pickled (numpy or joblib) PCA data
        :param mean_mesh: (Optional) Mean triangulated mesh (Supported types ply, stl, obj)
        :param maps:
        """
        # init vars
        self.mean = None
        self.weights = None  # PC weights are variance
        self.modes = None
        self.SD = None
        self.projectedWeights = None
        self.dimensions = 3
        self.sample_size = -1

        if pc is not None and pc.endswith(".pc.npz"):
            x = np.load(pc, encoding='bytes', allow_pickle=True)
            self.unpack(x)
        elif pc is not None and pc.endswith("joblib"):
            n = joblib.load(pc)
            x = {
                'mean': n.mean_,
                'weights': n.explained_variance_,
                'modes': n.components_.T,
                'SD': None,
                'projectedWeights': None
            }
            self.unpack(x)
        else:
            pass

        if mean_mesh is not None:
            self.mean_mesh = Mesh(filename=mean_mesh)
            self.inner_mesh = Mesh(filename=mean_mesh)

        if self.mean is not None:
            ssm = np.reshape(self.mean, (-1, self.dimensions))
            while self.mean_mesh.points.shape[0] != ssm.shape[0]:
                self.dimensions += 1
                ssm = np.reshape(self.mean, (-1, self.dimensions))

    def unpack(self, s):
        self.mean = s['mean']
        self.weights = s['weights']  # PC weights are variance
        self.modes = s['modes']
        self.SD = s['SD']
        self.projectedWeights = s['projectedWeights']

    def reconstruct_from_pc(self, sd, add_mean=False):
        sdo = np.array([0.0 for i in range(len(self.weights))])
        sdo[:len(sd)] = sd
        w = np.atleast_2d(sdo * np.sqrt(self.weights))
        m = np.dot(w, self.modes.T)
        me = m.T
        if add_mean:
            me = m + self.mean
        mesh = np.reshape(me, [int(self.mean.shape[0] / self.dimensions), self.dimensions])
        return mesh

    def sample(self, sample_size=-1):
        # Set the parameters for the normal distribution
        mean = 0  # Mean of the distribution
        std_dev = 1  # Standard deviation of the distribution

        # Generate a sample of random numbers following a normal distribution
        if sample_size < 0:
            sample_size = 100
        if self.sample_size < 0:
            self.sample_size = sample_size
        rng = np.random.default_rng()
        random_numbers = rng.normal(mean, std_dev, sample_size)

        # Scale and shift the numbers to be between -2 and +2
        scaled_numbers = 4 * (random_numbers - np.min(random_numbers)) / (
                    np.max(random_numbers) - np.min(random_numbers))-2
        return scaled_numbers

    def sample_model(self, pick=30, pcs=9):
        if pcs > len(self.weights):
            print("Warning PC set to greater than the number in the model.\nSetting number pc to max: {0}".format(len(self.weights)))
            pcs = len(self.weights)
        pc_list = [self.sample(pick) for i in range(0, pcs)]
        sd_pc = []
        for s in range(0, pick):
            temp = []
            for t in range(0, len(pc_list)):
                temp.append(pc_list[t][s])
            sd_pc.append(temp)
        idx = 0
        sampled_model = {}
        for sd in sd_pc:
            print(sd)
            sd_in = np.zeros([1, len(self.weights)])
            sd_in[0, :len(sd)] = sd
            sl = self.reconstruct_from_pc(np.array(sd_in))
            mean_mesh = np.reshape(self.mean, [int(self.mean.shape[0] / self.dimensions), self.dimensions])
            new_shape = mean_mesh + sl
            sampled_model[idx] = deepcopy(new_shape)
            idx += 1
        return sampled_model

    @staticmethod
    def export_as_mesh(current_mesh, idx, actor, outdir="./samples/", filename="sample", mesh_type=Keywords.ply):
        vs = vtk.vtkPoints()
        vs.SetData(numpy_to_vtk(current_mesh))
        actor.GetMapper().GetInput().SetPoints(vs)
        w = vtk.vtkPLYWriter()
        if mesh_type == Keywords.stl:
            w = vtk.vtkSTLWriter()
        elif mesh_type == Keywords.obj:
            w = vtk.vtkOBJWriter()
        w.SetInputData(actor.GetMapper().GetInput())
        w.SetFileName("{0}{2}_{1}.{3}".format(outdir, idx, filename, Keywords.ply.value[1]))
        w.Write()


class GHSSM(StatisticalShapeModel):
    def __init__(self, pc: str = None, mean_mesh=None, maps=None):
        super().__init__(pc, mean_mesh)
        try:
            self.scapula_map = maps['scapula']
        except TypeError:
            self.scapula_map = pd.read_csv("maps/Scapula_map_to_mean.csv")
        except KeyError:
            self.scapula_map = pd.read_csv("maps/Scapula_map_to_mean.csv")

        try:
            self.humerus_map = maps['humerus']
        except TypeError:
            self.humerus_map = pd.read_csv("maps/Humerus_map_to_mean.csv")

        self.scapula_color = [144 / 255, 207 / 255, 252 / 255]
        self.humerus_color = [144 / 255, 207 / 255, 252 / 255]
        self.scapula_data = self.create_part(self.scapula_color, False,
                                             "./meshes/mean_scapula.ply", self.scapula_map["idm"].tolist())
        self.humerus_data = self.create_part(self.humerus_color, False,
                                             "./meshes/mean_humerus.ply", self.humerus_map["idm"].tolist())
        self.glenoid = None
        self.humeral_head = None
        self.current_scapula = None
        self.current_humerus = None
        self.current_humerus_rc = None
        self.AA = None
        self.AI = None
        self.TS = None
        self.ref_ord = None
        self.EL = None
        self.EM = None

    def create_part(self, color, force_build, mean_mesh, part_map):
        """
        This method assumes the means is point only and a combinations of parts
        :param color:
        :param force_build:
        :param mean_mesh:
        :param part_map:
        :return:
        """
        mesh = np.reshape(self.mean, [int(self.mean.shape[0] / 3), 3])
        part = mesh[part_map, :]
        new_shape = None
        if force_build or not os.path.exists(mean_mesh):
            new_shape = self.extract_parts(part_map)
            # Write the mesh to file "*.ply"
            w = vtk.vtkPLYWriter()
            w.SetInputData(new_shape[0])
            w.SetFileName(mean_mesh)
            w.Write()
        if force_build:
            polydata = new_shape[0]
        else:
            reader = vtk.vtkPLYReader()
            reader.SetFileName(mean_mesh)
            reader.Update()
            polydata = reader.GetOutput()
        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            # mapper.SetInput(reader.GetOutput())
            mapper.SetInput(polydata)
        else:
            mapper.SetInputData(polydata)
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor(color[0], color[1], color[2])
        return {Keywords.actor: actor, Keywords.polydata: polydata, Keywords.vertices: part}

    def calc_translation(self):
        idm_scap = self.glenoid['idm'].to_list()
        rc_scap = Cloud.sphere_fit(self.current_scapula[idm_scap])
        idm_hum = self.humeral_head['idm'].to_list()
        rc_hum = Cloud.sphere_fit(self.current_humerus[idm_hum])
        return np.squeeze(rc_scap - rc_hum)

    def calc_rotation(self):
        hdata = self.humerus_corr()[0]
        sdata = self.scapula_corr()[0]
        m = Cloud.transformation_from_svd(hdata.T, sdata.T)
        return m

    def scapula_corr(self):
        """
        This is the ISB recommend coordinate system for the scapula

            Wu, G., van der Helm, F. C., Veeger, H. E., Makhsous, M., Van Roy, P., Anglin, C., Nagels, J.,
            Karduna, A. R., McQuade, K., Wang, X., Werner, F. W., Buchholz, B.,
            & International Society of Biomechanics (2005).
            ISB recommendation on definitions of joint coordinate systems of various joints for the reporting of human
            joint motion--Part II: shoulder, elbow, wrist and hand. Journal of biomechanics, 38(5), 981–992.
            https://doi.org/10.1016/j.jbiomech.2004.05.042

        :return: a matrix containing points for origin,x,y,z
        """

        aa_point = np.mean(self.current_scapula[self.AA['idm'].to_list()], axis=0)
        ai_point = np.mean(self.current_scapula[self.AI['idm'].to_list()], axis=0)
        ts_point = np.mean(self.current_scapula[self.TS['idm'].to_list()], axis=0)
        z_raw = aa_point - ts_point
        z = (1 / np.linalg.norm(z_raw)) * z_raw
        yx1_raw = ts_point - aa_point
        yx2_raw = ai_point - aa_point
        x_raw = np.cross(yx2_raw, yx1_raw)
        x = (1 / np.linalg.norm(x_raw)) * x_raw
        y = np.cross(z, x)
        ret = np.array([[0, 0, 0], 50 * x, 100 * y, 150 * z])
        without_magnitude = np.array([[0, 0, 0], x, y, z])
        out = pd.DataFrame(data=without_magnitude, columns=["x", "y", "z"])
        self.ref_ord = without_magnitude
        return [ret, out]

    def humerus_corr(self):
        """
        This is the ISB recommend coordinate system for the humerus.
        It uses points Lateral Epicondyle

            Wu, G., van der Helm, F. C., Veeger, H. E., Makhsous, M., Van Roy, P., Anglin, C., Nagels, J.,
            Karduna, A. R., McQuade, K., Wang, X., Werner, F. W., Buchholz, B.,
            & International Society of Biomechanics (2005).
            ISB recommendation on definitions of joint coordinate systems of various joints for the reporting of human
            joint motion--Part II: shoulder, elbow, wrist and hand. Journal of biomechanics, 38(5), 981–992.
            https://doi.org/10.1016/j.jbiomech.2004.05.042

        :return: a matrix containing points for origin,x,y,z
        """
        rc = np.squeeze(Cloud.sphere_fit(self.current_humerus[self.humeral_head['idm'].to_list()]))
        self.current_humerus_rc = rc
        el_point = np.nanmean(self.current_humerus[self.EL['idm'].to_list()], axis=0)
        em_point = np.nanmean(self.current_humerus[self.EM['idm'].to_list()], axis=0)
        mid_ep = 0.5 * (em_point + el_point)
        y_raw = rc - mid_ep
        y = (1 / np.linalg.norm(y_raw)) * y_raw
        X0 = em_point - rc
        X1 = el_point - rc
        x_raw = np.cross(X1, X0)
        x = (1 / np.linalg.norm(x_raw)) * x_raw
        z = np.cross(x, y)
        ret = np.array([[0, 0, 0], 50 * x, 100 * y, 150 * z])
        without_magnitude = np.array([[0, 0, 0], x, y, z])
        out = pd.DataFrame(data=without_magnitude, columns=["x", "y", "z"])
        return [ret, out]
