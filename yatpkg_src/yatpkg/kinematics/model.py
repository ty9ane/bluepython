from yatpkg.util.data import MYXML
from yatpkg.util.opensim_tools import OsimModel


class Model(OsimModel):
    def __init__(self, filepath):
        super().__init__(filepath)

    @staticmethod
    def build():
        pass


class Markers:
    def __init__(self):
        self.map = {}

    @staticmethod
    def load(filename):
        x = MYXML(filename)
        x0 = x.tree.getElementsByTagName('Marker')
        for n in x0:
            print(n.getAttribute("name"))
            body = n.getElementsByTagName("socket_parent_frame")[0]
            location = n.getElementsByTagName("location")[0]
            print(body.childNodes[0].data)
            print(location.childNodes[0].data)
        pass


class Segment:
    def __init__(self, seg_name):
        self.name = seg_name
        self.markers = {}
        self.origin = 0.0
        self.angles = {"x": 0.0, "y": 0.0, "z": 0.0}


class WholeBody(OsimModel):

    def __init__(self):
        super().__init__()
        self.head = Segment("head")
        self.torso = Segment("torso")
        self.left_humerus = Segment("left_humerus")
        self.right_humerus = Segment("right_humerus")
        self.left_forearm = Segment("left_forearm")
        self.right_forearm = Segment("right_forearm")
        self.left_thigh = Segment("left_thigh")
        self.right_thigh = Segment("right_thigh")
        self.left_shank = Segment("left_shank")
        self.right_shank = Segment("right_shank")
        self.left_foot = Segment("left_foot")
        self.right_foot = Segment("right_foot")
