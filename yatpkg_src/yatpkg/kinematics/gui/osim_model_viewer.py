from PySide6.QtWidgets import (QMainWindow, QApplication, QMenuBar, QWidget, QVBoxLayout, QHBoxLayout, QTabWidget,
                             QTabBar, QPushButton, QScrollArea, QGroupBox, QRadioButton, QTextEdit, QLabel, QTreeWidget,
                             QTreeWidgetItem, QCheckBox, QLineEdit, QFileDialog, QProgressBar, QMessageBox)
from PySide6.QtGui import QIcon, QColor, QFont
from PySide6.QtCore import QSize, Signal, Qt, QPoint

import sys
import os
from datetime import datetime
import time

from yatpkg.util.data import TRC, StorageIO, StorageType
from yatpkg.util.opensim_tools import IK, OsimModel
import opensim as om
from multiprocessing import Pool
from threading import Thread
from yatpkg.kinematics.gui.defaults.viewer import World, WorldView
from yatpkg.kinematics.gui.defaults.tools import BasicIO
import numpy as np


class Home(QWidget):
    def __init__(self, parent, console_ui):
        super().__init__(parent)
        self.setStyleSheet(BasicIO.read_as_block("./defaults/home.qss"))
        self.progress = QProgressBar()
        self.progress.setRange(0, 100)
        self.progress.setObjectName("progress_bar")
        self.progress.setTextVisible(False)

        self.layout = QVBoxLayout()
        self.terminal = QLabel('Terminal', self)
        self.terminal.setObjectName("terminal_label")
        self.runner = QPushButton("\t\tRun", self)
        self.runner.setIcon(QIcon("./icons/play_green.png"))
        self.runner.clicked.connect(self.run)
        self.runner.setCheckable(True)
        self.col1_widget = QWidget()
        self.col1 = QVBoxLayout()
        self.col1_widget.setLayout(self.col1)
        self.area2 = QScrollArea()
        self.area2.setObjectName("bottom_scroll")
        self.input_text = QTextEdit()
        self.input_text.setText(console_ui.header)
        self.area2.setFixedHeight(250)
        self.area2.setWidgetResizable(True)
        self.area2.setWidget(self.input_text)
        self.col1.addWidget(self.terminal)

        self.col1.addWidget(self.area2)
        self.console:C3DConsole = console_ui
        self.layout.addWidget(self.console)
        self.layout.addStretch(20)
        self.layout.addWidget(self.progress)
        self.layout.addWidget(self.col1_widget)
        self.layout.addWidget(self.runner)
        self.setLayout(self.layout)

    def update_log(self, log_txt):
        self.input_text.append("> {0}".format(log_txt))
        self.input_text.update()
        time.sleep(0.01)    # this sleep allows for the GUI to catchup
        vmax = self.input_text.verticalScrollBar().maximum()
        self.input_text.verticalScrollBar().setValue(vmax)
        self.input_text.update()

    def set_console(self):
        self.console.parent_widget = self
        self.console.setParent(self)

    def update_process(self, v):
        self.progress.setValue(int(v))
        if v >= 99:
            self.runner.setEnabled(True)
            self.runner.setChecked(False)
            self.runner.setIcon(QIcon("./icons/play_green.png"))
            self.runner.update()
        self.progress.update()

    def run(self):
        self.runner.setIcon(QIcon("./icons/play.png"))
        self.runner.setEnabled(False)
        self.runner.setChecked(True)
        self.progress.setTextVisible(True)
        self.progress.setValue(0)
        self.update()
        self.console.run()


class InfoWidget(QWidget):
    @property
    def OPEN_FILE(self):
        return 0

    @property
    def OPEN_FOLDER(self):
        return 1

    @property
    def SAVE_FILE(self):
        return 2

    @property
    def SAVE_FOLDER(self):
        return 3

    def __init__(self, root, info_label: str = 'New Mesh:',  mode: property = 0, file_filter: str = "All File (*.*)",
                 init_filter: str = ""):
        super().__init__()
        self.root: C3DConsole = root
        self.the_label = QLabel(info_label)
        self.text_box = QLineEdit()
        self.text_box.setMinimumHeight(25)
        self.button = QPushButton('Open', self)
        self.button.setCheckable(True)
        self.button.setIcon(QIcon("icons/folder-open.png"))
        self.button.setMinimumHeight(25)

        self.delete_button = QPushButton('', self)
        self.delete_button.setIcon(QIcon("icons/trash.png"))
        self.delete_button.setMinimumHeight(25)

        self.setting_button = QPushButton('', self)
        self.setting_button.setIcon(QIcon("icons/slider.png"))
        self.setting_button.setMinimumHeight(25)

        self.checker = QCheckBox('')
        self.vert = QVBoxLayout()
        self.qwid = QWidget()

        self.hor = QHBoxLayout()
        self.hor.addWidget(self.text_box)
        self.hor.addWidget(self.button)
        self.hor.addWidget(self.setting_button)
        self.hor.addWidget(self.delete_button)
        self.qwid.setLayout(self.hor)
        self.vert.addWidget(self.the_label)
        self.vert.addWidget(self.qwid)

        self.setLayout(self.vert)
        self.button.clicked.connect(self.on_click)
        self.delete_button.clicked.connect(self.reset)
        self.file_path = ""
        self.mode = mode
        self.file_filter = file_filter
        self.init_filter = init_filter

    def on_click(self):
        response = ""
        self.button.setChecked(True)
        if self.mode == InfoWidget.OPEN_FILE:
            response = QFileDialog.getOpenFileName(
                parent=self,
                caption='Open C3D file',
                dir=os.getcwd(),
                filter=self.file_filter,
                selectedFilter=self.init_filter
            )
            self.file_path = response[0]
        elif self.mode == InfoWidget.SAVE_FILE:
            response = QFileDialog.getSaveFileName(
                parent=self,
                caption='Save trc file',
                dir=os.getcwd(),
                filter=self.file_filter,
                selectedFilter=self.init_filter
            )
            self.file_path = response[0]
        elif self.mode == InfoWidget.OPEN_FOLDER or self.mode == InfoWidget.SAVE_FOLDER:
            response = QFileDialog.getExistingDirectory(
                parent=self,
                caption='Open folder',
                dir=os.getcwd(),
            )
            self.file_path = response
        if self.mode == InfoWidget.OPEN_FOLDER or self.mode == InfoWidget.SAVE_FOLDER:
            self.file_path = response
        self.text_box.setText(self.file_path)
        print("open", self.file_path)
        if len(self.file_path) > 0:
            if self.mode == InfoWidget.OPEN_FILE:
                self.root.update_log("Set input file: {0}".format(self.file_path))
            elif self.mode == InfoWidget.OPEN_FOLDER:
                self.root.update_log("Set input folder: {0}".format(self.file_path))
            elif self.mode == InfoWidget.SAVE_FILE:
                self.root.update_log("Set output file: {0}".format(self.file_path))
            elif self.mode == InfoWidget.SAVE_FOLDER:
                self.root.update_log("Set output folder: {0}".format(self.file_path))
        self.button.setChecked(False)
        pass

    def reset(self):
        self.file_path = ''
        self.text_box.setText(self.file_path)
        self.checker.setChecked(False)


class ScalerConsole(QWidget):
    @property
    def header(self):
        return "Scaler Batcher\n\nThis app will help you batch the Scaling.\n"

    def __init__(self, parent):
        super().__init__(parent)
        self.parent_widget = parent
        self.layout = QHBoxLayout()
        self.setObjectName("ScaleConsole")
        self.sty = BasicIO.read_as_block("./defaults/console.qss")
        self.area1 = QScrollArea()
        self.area1.setObjectName("top_scroll")
        self.area1.setFixedHeight(460)
        self.area1.setWidgetResizable(True)
        self.mesh_group = QGroupBox()
        self.label_options = QLabel("Options")
        self.label_options.setObjectName("label_options")

        self.left_side1 = QVBoxLayout()
        self.left_side1.addWidget(self.label_options)
        file_filter = 'Opensim Model (*.osim);; All File (*.*)'
        self.model_options = InfoWidget(self, "Model (osim):", InfoWidget.OPEN_FILE, file_filter, 'Opensim Model (*.osim)')
        self.left_side1.addWidget(self.model_options)
        file_filter = 'Scaler Setup (*.xml);; All File (*.*)'
        self.xml_options = InfoWidget(self, "Scaler Setup (xml):", InfoWidget.OPEN_FILE, file_filter, 'Scaler Setup (*.xml)')
        self.left_side1.addWidget(self.xml_options)
        file_filter = 'TRC (*.trc);; C3D (*.c3d);; All File (*.*)'
        self.input_options = InfoWidget(self, "Trial (*.trc):", InfoWidget.OPEN_FILE, file_filter, 'TRC (*.trc)')
        self.left_side1.addWidget(self.input_options)
        file_filter = 'Opensim Storage (*.sto);; All File (*.*)'
        self.output_options = InfoWidget(self, "Output:", InfoWidget.SAVE_FILE, file_filter,'Opensim Storage (*.sto)')
        self.left_side1.addWidget(self.output_options)
        self.left_side1.addStretch(10)
        self.mesh_group.setLayout(self.left_side1)
        self.area1.setWidget(self.mesh_group)

        self.radio0 = QRadioButton("Single File", self)
        self.radio0.toggled.connect(self.radio0_clicked)
        self.radio0.click()
        self.radio1 = QRadioButton("Folder", self)
        self.radio1.toggled.connect(self.radio1_clicked)
        self.col2_widget = QWidget()

        self.col2 = QVBoxLayout()
        self.col2.addWidget(QLabel(""))
        self.col2.addWidget(self.radio0)
        self.col2.addWidget(self.radio1)
        self.col2.addWidget(QLabel(""))
        self.col2.addStretch(2)
        self.col2.addStretch(6)
        self.col2_widget.setLayout(self.col2)

        self.col1 = QVBoxLayout()
        self.col1_widget = QWidget()
        self.col1.addWidget(self.area1)
        self.col1.addStretch(5)
        self.col1_widget.setLayout(self.col1)

        self.layout.addWidget(self.col1_widget)
        self.layout.addWidget(self.col2_widget)
        self.setLayout(self.layout)
        self.setStyleSheet(self.sty)

    def radio0_clicked(self):
        if self.radio0.isChecked():
            self.input_options.mode = InfoWidget.OPEN_FILE
            self.input_options.the_label.setText("Trial (*.trc):")
            self.output_options.mode = InfoWidget.SAVE_FILE

    def radio1_clicked(self):
        if self.radio1.isChecked():
            self.input_options.mode = InfoWidget.OPEN_FOLDER
            self.input_options.the_label.setText("Trials (Folder):")
            self.output_options.mode = InfoWidget.SAVE_FOLDER

    def update_log(self, txt):
        if isinstance(txt, str):
            self.parent_widget.update_log(txt)
        else:
            for v in txt:
                self.parent_widget.update_log(v)
                time.sleep(0.1)

    def update_process(self, v):
        self.parent_widget.update_process(v)

    @staticmethod
    def processing(data):
        osm = data[0]
        setup = data[1]
        in_file = data[2]
        out_file = data[3]
        ret = ["In: {0} \n   Out: {1}".format(in_file, out_file)]
        try:
            ret.append("Loading trc file.")
            model = OsimModel(osm)
            markerset = model.markerset
            trc = StorageIO.trc_reader(in_file)
            pass
            # IK.run()

        except FileNotFoundError:
            ret.append("File not found.")
            pass
        return ret

    @staticmethod
    def worker(obj, task):
        p = Pool(5)
        start = time.time()
        for i, res in enumerate(p.imap(IKConsole.processing, task), 1):
            try:
                obj.update_process(i / float(len(task)) * 100.0)
                obj.update_log(res)
            except:
                pass
            time.sleep(0.1)
        try:
            end = time.time()
            obj.update_log("Export Finished in {0}s.\n".format(end - start))
        except:
            pass

    def run(self):
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        self.update_log("Running exporter: {0}".format(dt_string))

        if len(self.input_options.file_path) > 0:

            try:
                self.update_log("Parsing ... ")
                task = [[self.model_options.file_path, self.xml_options.file_path, self.input_options.file_path, self.output_options.file_path]]
                if (self.input_options.mode == InfoWidget.OPEN_FOLDER and os.path.isdir(
                        self.input_options.file_path)) and (
                        self.output_options.mode == InfoWidget.SAVE_FOLDER and os.path.isdir(
                        self.output_options.file_path)):
                    self.update_log("Input is folder ... running batch.")
                    trc_files = [c for c in os.listdir(self.input_options.file_path) if c.endswith(".trc")]
                    if len(trc_files) == 0:
                        c3d_files = [c for c in os.listdir(self.input_options.file_path) if c.endswith(".c3d")]
                    sto_files = ["{0}.trc".format(c[:c.rindex(".")]) for c in c3d_files]
                    task = [[self.input_options.file_path + "/" + c3d_files[i],
                             self.output_options.file_path + "/" + trc_files[i]] for i in range(0, len(c3d_files))]
                else:
                    self.update_log("Input is File ... running")
                new_thread = Thread(target=IKConsole.worker, args=(self, task,), daemon=True)
                new_thread.start()
            except FileNotFoundError:
                pass

        else:
            self.update_log("No file or folder selected.\n")
            self.update_process(100)
        pass


class IKConsole(QWidget):
    @property
    def header(self):
        return "IK Batcher\n\nThis app will help you batch the IK.\n"

    def __init__(self, parent):
        super().__init__(parent)
        self.parent_widget = parent
        self.layout = QHBoxLayout()
        self.setObjectName("IKConsole")
        self.sty = BasicIO.read_as_block("./defaults/console.qss")
        self.area1 = QScrollArea()
        self.area1.setObjectName("top_scroll")
        self.area1.setFixedHeight(460)
        self.area1.setWidgetResizable(True)
        self.mesh_group = QGroupBox()
        self.label_options = QLabel("Options")
        self.label_options.setObjectName("label_options")

        self.left_side1 = QVBoxLayout()
        self.left_side1.addWidget(self.label_options)
        file_filter = 'Opensim Model (*.osim);; All File (*.*)'
        self.model_options = InfoWidget(self, "Model (osim):", InfoWidget.OPEN_FILE, file_filter, 'Opensim Model (*.osim)')
        self.left_side1.addWidget(self.model_options)
        file_filter = 'IK Setup (*.xml);; All File (*.*)'
        self.xml_options = InfoWidget(self, "IK Setup (xml):", InfoWidget.OPEN_FILE, file_filter, 'IK Setup (*.xml)')
        self.left_side1.addWidget(self.xml_options)
        file_filter = 'TRC (*.trc);; C3D (*.c3d);; All File (*.*)'
        self.input_options = InfoWidget(self, "Trial (*.trc):", InfoWidget.OPEN_FILE, file_filter, 'TRC (*.trc)')
        self.left_side1.addWidget(self.input_options)
        file_filter = 'Opensim Storage (*.sto);; All File (*.*)'
        self.output_options = InfoWidget(self, "Output:", InfoWidget.SAVE_FILE, file_filter,'Opensim Storage (*.sto)')
        self.left_side1.addWidget(self.output_options)
        self.left_side1.addStretch(10)
        self.mesh_group.setLayout(self.left_side1)
        self.area1.setWidget(self.mesh_group)

        self.radio0 = QRadioButton("Single File", self)
        self.radio0.toggled.connect(self.radio0_clicked)
        self.radio0.click()
        self.radio1 = QRadioButton("Folder", self)
        self.radio1.toggled.connect(self.radio1_clicked)
        self.col2_widget = QWidget()

        self.col2 = QVBoxLayout()
        self.col2.addWidget(QLabel(""))
        self.col2.addWidget(self.radio0)
        self.col2.addWidget(self.radio1)
        self.col2.addWidget(QLabel(""))
        self.col2.addStretch(2)
        self.col2.addStretch(6)
        self.col2_widget.setLayout(self.col2)

        self.col1 = QVBoxLayout()
        self.col1_widget = QWidget()
        self.col1.addWidget(self.area1)
        self.col1.addStretch(5)
        self.col1_widget.setLayout(self.col1)

        self.layout.addWidget(self.col1_widget)
        self.layout.addWidget(self.col2_widget)
        self.setLayout(self.layout)
        self.setStyleSheet(self.sty)

    def radio0_clicked(self):
        if self.radio0.isChecked():
            self.input_options.mode = InfoWidget.OPEN_FILE
            self.input_options.the_label.setText("Trial (*.trc):")
            self.output_options.mode = InfoWidget.SAVE_FILE

    def radio1_clicked(self):
        if self.radio1.isChecked():
            self.input_options.mode = InfoWidget.OPEN_FOLDER
            self.input_options.the_label.setText("Trials (folder):")
            self.output_options.mode = InfoWidget.SAVE_FOLDER

    def update_log(self, txt):
        if isinstance(txt, str):
            self.parent_widget.update_log(txt)
        else:
            for v in txt:
                self.parent_widget.update_log(v)
                time.sleep(0.1)

    def update_process(self, v):
        self.parent_widget.update_process(v)

    @staticmethod
    def processing(data):
        osm = data[0]
        setup = data[1]
        in_file = data[2]
        out_file = data[3]
        ret = ["In: {0} \n   Out: {1}".format(in_file, out_file)]
        try:
            if data[4] == InfoWidget.OPEN_FILE:
                ret.append("Loading trc file.")
                model = OsimModel(osm)
                markerset = model.markerset
                trc = StorageIO.trc_reader(in_file)
                savenamep = in_file.split('/')
                pathme = in_file[:in_file.rindex('/')]
                if len(savenamep) <= 1:
                    savenamep = in_file.split('\\')
                    pathme = in_file[:in_file.rindex('\\')]
                savename = savenamep[-1][:savenamep[-1].rindex('.')]
                IK.write_ik_setup(trc, setup, model, out_file, pathme+'/'+savename)
                IK.run(pathme+'/'+savename)

        except FileNotFoundError:
            ret.append("File not found.")
            pass
        return ret

    @staticmethod
    def worker(obj, task):
        p = Pool(5)
        start = time.time()
        for i, res in enumerate(p.imap(IKConsole.processing, task), 1):
            try:
                obj.update_process(i / float(len(task)) * 100.0)
                obj.update_log(res)
            except:
                pass
            time.sleep(0.1)
        try:
            end = time.time()
            obj.update_log("Export Finished in {0}s.\n".format(end - start))
        except:
            pass

    def run(self):
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        self.update_log("Running IK Helper: {0}".format(dt_string))

        if len(self.input_options.file_path) > 0:

            try:
                self.update_log("Parsing ... ")
                task = [[self.model_options.file_path, self.xml_options.file_path, self.input_options.file_path, self.output_options.file_path]]
                if (self.input_options.mode == InfoWidget.OPEN_FOLDER and os.path.isdir(
                        self.input_options.file_path)) and (
                        self.output_options.mode == InfoWidget.SAVE_FOLDER and os.path.isdir(
                        self.output_options.file_path)):
                    self.update_log("Input is folder ... running batch.")
                    mocap_files = [c for c in os.listdir(self.input_options.file_path) if c.endswith(".trc")]
                    if len(mocap_files) == 0:
                        mocap_files = [c for c in os.listdir(self.input_options.file_path) if c.endswith(".c3d")]
                    sto_files = ["{0}.trc".format(c[:c.rindex(".")]) for c in mocap_files]
                    task = [[self.model_options.file_path,
                             self.xml_options.file_path,
                             self.input_options.file_path + "/" + mocap_files[i],
                             self.output_options.file_path + "/" + sto_files[i],
                             self.input_options.mode
                             ] for i in range(0, len(mocap_files))]
                else:
                    self.update_log("Input is File ... running")
                new_thread = Thread(target=IKConsole.worker, args=(self, task,), daemon=True)
                new_thread.start()
            except FileNotFoundError:
                pass

        else:
            self.update_log("No file or folder selected.\n")
            self.update_process(100)
        pass


class C3DConsole(QWidget):
    @property
    def header(self):
        return "C3D to TRC Exporter\n\nThis app will help you extract marker information from the C3D file.\n"

    def __init__(self, parent):
        super().__init__(parent)
        self.parent_widget = parent
        self.layout = QHBoxLayout()
        self.setObjectName("C3DConsole")
        self.sty = BasicIO.read_as_block("./defaults/console.qss")
        self.area1 = QScrollArea()
        self.area1.setObjectName("top_scroll")
        self.area1.setFixedHeight(300)
        self.area1.setWidgetResizable(True)
        self.mesh_group = QGroupBox()
        self.label_options = QLabel("Options")
        self.label_options.setObjectName("label_options")

        self.left_side1 = QVBoxLayout()
        self.left_side1.addWidget(self.label_options)
        file_filter = 'C3D (*.c3d);; All File (*.*)'
        self.input_options = InfoWidget(self, "Input:", InfoWidget.OPEN_FILE, file_filter, 'C3D (*.c3d)')
        self.left_side1.addWidget(self.input_options)
        file_filter = 'TRC (*.trc);; All File (*.*)'
        self.output_options = InfoWidget(self, "Output:", InfoWidget.SAVE_FILE, file_filter, 'TRC (*.trc)')
        self.left_side1.addWidget(self.output_options)
        self.left_side1.addStretch(10)
        self.mesh_group.setLayout(self.left_side1)
        self.area1.setWidget(self.mesh_group)

        self.radio0 = QRadioButton("Single File", self)
        self.radio0.toggled.connect(self.radio0_clicked)
        self.radio0.click()
        self.radio1 = QRadioButton("Folder", self)
        self.radio1.toggled.connect(self.radio1_clicked)
        self.col2_widget = QWidget()

        self.col2 = QVBoxLayout()
        self.col2.addWidget(QLabel(""))
        self.col2.addWidget(self.radio0)
        self.col2.addWidget(self.radio1)
        self.col2.addWidget(QLabel(""))
        self.col2.addStretch(2)
        self.col2.addStretch(6)
        self.col2_widget.setLayout(self.col2)

        self.col1 = QVBoxLayout()
        self.col1_widget = QWidget()
        self.col1.addWidget(self.area1)
        self.col1.addStretch(5)
        self.col1_widget.setLayout(self.col1)

        self.layout.addWidget(self.col1_widget)
        self.layout.addWidget(self.col2_widget)
        self.setLayout(self.layout)
        self.setStyleSheet(self.sty)

    def radio0_clicked(self):
        if self.radio0.isChecked():
            self.input_options.mode = InfoWidget.OPEN_FILE
            self.output_options.mode = InfoWidget.SAVE_FILE

    def radio1_clicked(self):
        if self.radio1.isChecked():
            self.input_options.mode = InfoWidget.OPEN_FOLDER
            self.output_options.mode = InfoWidget.SAVE_FOLDER

    def update_log(self, txt):
        if isinstance(txt, str):
            self.parent_widget.update_log(txt)
        else:
            for v in txt:
                self.parent_widget.update_log(v)
                time.sleep(0.1)

    def update_process(self, v):
        self.parent_widget.update_process(v)

    @staticmethod
    def processing(data):
        in_file = data[0]
        out_file = data[1]
        ret = ["In: {0} \n   Out: {1}".format(in_file, out_file)]
        try:
            ret.append("Loading c3d file.")
            trc = TRC.create_from_c3d(in_file)
            ret.append("Loaded marker data.")
            try:
                ret.append("Writing data to trc.")
                trc.write(out_file)
            except PermissionError:
                ret.append("File permission error.")
                pass
        except FileNotFoundError:
            ret.append("File not found.")
            pass
        return ret

    @staticmethod
    def worker(obj, task):
        p = Pool(5)
        start = time.time()
        for i, res in enumerate(p.imap(C3DConsole.processing, task), 1):
            try:
                obj.update_process(i / float(len(task)) * 100.0)
                obj.update_log(res)
            except:
                pass
            time.sleep(0.1)
        try:
            end = time.time()
            obj.update_log("Export Finished in {0}s.\n".format(end - start))
        except:
            pass

    def run(self):
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        self.update_log("Running exporter: {0}".format(dt_string))

        if len(self.input_options.file_path) > 0:

            try:
                self.update_log("Parsing ... ")
                task = [[self.input_options.file_path, self.output_options.file_path]]
                if (self.input_options.mode == InfoWidget.OPEN_FOLDER and os.path.isdir(
                        self.input_options.file_path)) and (
                        self.output_options.mode == InfoWidget.SAVE_FOLDER and os.path.isdir(
                        self.output_options.file_path)):
                    self.update_log("Input is folder ... running batch.")
                    c3d_files = [c for c in os.listdir(self.input_options.file_path) if c.endswith(".c3d")]
                    trc_files = ["{0}.trc".format(c[:c.rindex(".")]) for c in c3d_files]
                    task = [[self.input_options.file_path + "/" + c3d_files[i],
                             self.output_options.file_path + "/" + trc_files[i]] for i in range(0, len(c3d_files))]
                else:
                    self.update_log("Input is File ... running")
                new_thread = Thread(target=C3DConsole.worker, args=(self, task,), daemon=True)
                new_thread.start()
            except FileNotFoundError:
                pass

        else:
            self.update_log("No file or folder selected.\n")
            self.update_process(100)
        pass


class MainWidget(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.par = parent
        self.layout = QVBoxLayout()
        self.menu_bar = MainMenuBar(parent=self)
        self.setStyleSheet(self.menu_bar.sty)
        self.layout.setMenuBar(self.menu_bar)
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        p = self.palette()
        p.setColor(self.backgroundRole(), QColor("#ededeb"))
        self.setPalette(p)
        self.no_3d = [0, 0]
        self.with_3d = [100, 100]

    def setno_3dwidth(self):
        self.par.width = self.no_3d[0]
        self.par.height = self.no_3d[1]
        self.par.resize(QSize(self.no_3d[0], self.no_3d[1]))

    def setwith_3dwidth(self):
        self.par.width = self.with_3d[0]
        self.par.height = self.with_3d[1]
        self.par.resize(QSize(self.with_3d[0], self.with_3d[1]))

    def set_view(self, view):
        self.menu_bar.set_view(view)
        pass

    def add(self, widget):
        self.layout.addWidget(widget)

    def set_save_action(self, handle):
        self.menu_bar.add_save_action(handle)

    def set_open_action(self, handle):
        self.menu_bar.add_open_action(handle)


class MainMenuBar(QMenuBar):
    debug = False

    def mousePressEvent(self, event):
        super().mousePressEvent(event)
        self.par.par.qw.on_focus()

    def __init__(self, parent=None, view=None, default_menus=True, splash=None):
        super().__init__(parent)
        self.par: MainWidget = parent
        self.view = view
        self.save_action = None
        self.open_file = None
        self.splash = splash

        self.view3d = False
        if default_menus:
            self.default_menus()

        self.sty = BasicIO.read_as_block("./defaults/menu.qss")

    def set_view(self, view):
        self.view = view

    def add_save_action(self, save):
        self.save_action.triggered.connect(save)

    def add_open_action(self, open_func):
        self.open_file.triggered.connect(open_func)

    def default_menus(self):
        action_file = self.addMenu("File")
        new_file = action_file.addAction("New")
        new_file.setIcon(QIcon('./icons/add-document.png'))
        self.open_file = action_file.addAction("Open")
        self.open_file.setIcon(QIcon('./icons/folder-open.png'))
        self.save_action = action_file.addAction("Save")
        self.save_action.setIcon(QIcon('./icons/diskdark.png'))
        action_file.addSeparator()
        quit_action = action_file.addAction("Quit")
        quit_action.setIcon(QIcon('./icons/power.png'))
        action_edit = self.addMenu("Edit")
        prefer = action_edit.addAction("Preferences")
        prefer.setIcon(QIcon('./icons/slider.png'))
        action_view = self.addMenu("View")
        view3 = action_view.addAction("3D View")
        view3.triggered.connect(self.toggle_view)
        action_run = self.addMenu("Program")
        action_run.addAction("Generate Marker Set")
        action_run.addAction("Run FK-IK")
        action_help = self.addMenu("Help")
        about = action_help.addAction("About")
        about.setIcon(QIcon('./icons/info.png'))
        about.triggered.connect(self.open_splash)

    def run(self):
        pass

    def toggle_view(self):
        self.view3d = not self.view3d
        if self.view3d:
            self.par.setwith_3dwidth()
        else:
            self.par.setno_3dwidth()
        self.view.setVisible(self.view3d)
        print(self.view3d)

    def open_splash(self):
        if self.splash is not None:
            self.splash.show()


class MyTabWidget(QWidget):

    def __init__(self, grand, parent):
        super().__init__(grand)
        self.the_window = parent

        self.layout = QVBoxLayout(self)
        self.tabs = QTabWidget()
        self.tabs.setStyleSheet(BasicIO.read_as_block("./defaults/tabwidget.qss"))
        t = TabBar()
        t.listener = grand.qw
        self.tabs.setTabBar(t)
        t.left_clicked.connect(self.on_tab_click)
        self.tabs.setFont(QFont('Calibri', 12))
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()

        self.tabs.addTab(self.tab1, "C3D to TRC")
        self.tabs.addTab(self.tab2, "Batch IK")
        self.tabs.addTab(self.tab3, "Scaler")

        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

        self.home_layout = QHBoxLayout()
        h0 = Home(self.tab1, C3DConsole(None))
        h0.set_console()
        self.home_layout.addWidget(h0)
        self.tab1.setLayout(self.home_layout)

        h1 = Home(self.tab2, IKConsole(None))
        h1.set_console()
        self.home_layout2 = QHBoxLayout()
        self.home_layout2.addWidget(h1)
        self.tab2.setLayout(self.home_layout2)

        h2 = Home(self.tab3, ScalerConsole(None))
        h2.set_console()
        self.home_layout3 = QHBoxLayout()
        self.home_layout3.addWidget(h2)
        self.tab3.setLayout(self.home_layout3)

    def on_tab_click(self, index):
        pass

    def on_focus(self):
        pass


class TabBar(QTabBar):
    left_clicked = Signal(int)

    def __init__(self):
        super().__init__()
        self.previousIndex = -1
        self.listener = None

    def mousePressEvent(self, mouseEvent):
        p = mouseEvent.position()
        p_int = QPoint(int(np.round(p.x())), int(np.round(p.y())))
        if mouseEvent.button() == Qt.MouseButton.LeftButton:
            self.previousIndex = self.tabAt(p_int)
        QTabBar.mousePressEvent(self, mouseEvent)

    def mouseReleaseEvent(self, mouseEvent):
        p = mouseEvent.position()
        p_int = QPoint(int(np.round(p.x())), int(np.round(p.y())))
        if mouseEvent.button() == Qt.MouseButton.LeftButton and self.previousIndex == self.tabAt(p_int):
            self.left_clicked.emit(self.previousIndex)
        self.previousIndex = -1
        QTabBar.mouseReleaseEvent(self, mouseEvent)


class OpensimHelperApp(QMainWindow):
    def closeEvent(self, event):
        self.qw.on_close()

    def __init__(self, screen, splash=None):
        super().__init__()
        self.setFocusPolicy(Qt.StrongFocus)
        self.title = 'Opensim Helper'
        self.splash = splash
        size: QSize = screen.size()
        self.width = int(0.8 * size.width())
        self.height = int(0.8 * size.height())
        self.left = int((size.width()-self.width)/2)
        self.top = int((size.height()-self.height)/2)
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.main_widget = MainWidget(self)
        self.main_widget.no_3d = [self.width, self.height]
        self.main_widget.with_3d = [int(0.8 * size.width()), self.height]
        self.qw = WorldView(self.main_widget)

        q = QWidget()
        hv = QHBoxLayout()
        self.table_widget = MyTabWidget(self, self.main_widget)
        self.table_widget.setMinimumWidth(int(0.35 * size.width()))
        self.table_widget.setMaximumWidth(int(0.4 * size.width()))
        hv.addWidget(self.table_widget)

        hv.addWidget(self.qw)
        q.setLayout(hv)
        self.main_widget.add(q)
        self.main_widget.menu_bar.set_view(self.qw)

        self.setCentralWidget(self.main_widget)
        self.setStyleSheet(BasicIO.read_as_block("./defaults/main_window.qss"))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    current_screen = app.primaryScreen()
    ex = OpensimHelperApp(current_screen)
    ex.show()
    sys.exit(app.exec())
    pass
