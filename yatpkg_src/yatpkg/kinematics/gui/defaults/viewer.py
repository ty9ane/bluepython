import vtk
from PySide6.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout
from vtkmodules.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

from PySide6.QtWidgets import (QLabel, QPushButton, QGraphicsOpacityEffect)
from PySide6.QtGui import QIcon, QPainterPath, QRegion, QPainter, QPixmap
from PySide6.QtCore import QPoint, Qt

from yatpkg.kinematics.gui.defaults.tools import BasicIO
from yatpkg.kinematics.gui.defaults.misc import OpenFiles
from yatpkg.util.data import VTKMeshUtl, TRC
import os
import numpy as np


class World:
    def __init__(self, frame):
        self.__key_mods__ = []
        self.__other_keys__ = []
        self.control_pressed = False
        self.shift_pressed = False

        self.frame = frame
        self.central_widget = QWidget()
        self.central_widget.setStyleSheet("border-radius: 10px;")
        self.vl = QVBoxLayout()
        self.central_widget.setLayout(self.vl)
        self.vtk_widget = QVTKRenderWindowInteractor()
        self.vl.addWidget(self.vtk_widget)
        self.ren = vtk.vtkRenderer()
        self.ren.SetBackground(0.25, 0.25, 0.25)
        self.vtk_widget.GetRenderWindow().AddRenderer(self.ren)
        self.iren = self.vtk_widget.GetRenderWindow().GetInteractor()
        self.vtk_widget.AddObserver("keyPressEvent", self.key_pressed)
        self.vtk_widget.AddObserver("keyReleaseEvent", self.keyrelease)

        self.actors = {}
        self.axes = vtk.vtkAxesActor()
        self.axes.SetTotalLength(100, 100, 100)
        self.axes.SetNormalizedTipLength(0.2, 0.2, 0.2)

        self.axesw = vtk.vtkOrientationMarkerWidget()
        self.axesw.SetOrientationMarker(self.axes)
        self.axesw.SetInteractor(self.iren)
        self.axesw.SetViewport(0.8, 0.8, 1, 1)
        self.axesw.EnabledOn()
        self.axesw.InteractiveOff()

        self.txt = vtk.vtkTextActor()
        self.txt.SetDisplayPosition(20, 20)

        txtprop = self.txt.GetTextProperty()
        txtprop.SetFontSize(20)
        txtprop.BoldOn()
        txtprop.SetColor(0.80, 0.80, 0.80)

        self.picker = vtk.vtkPropPicker()

        self.style_interactor = vtk.vtkInteractorStyleTrackballCamera()
        self.vtk_widget.SetInteractorStyle(self.style_interactor)

        self.vtk_widget.Initialize()
        self.vtk_widget.Start()

    def reset_view(self):
        self.ren.ResetCamera()
        self.vtk_widget.update()

    def remove_actor(self, actor_name):
        try:
            a = self.actors.pop(actor_name)
            self.ren.RemoveActor(a)
            self.vtk_widget.update()
        except KeyError:
            pass

    def remove_all(self):
        keys = [a for a in self.actors]
        for a in keys:
            self.ren.RemoveActor(self.actors[a])
            self.actors.pop(a)
        self.vtk_widget.update()


    def add_actor(self, actor_name: str = None, actor: vtk.vtkActor = None, filename: str = None):
        if actor_name is not None and actor is not None:
            self.actors[actor_name] = actor
            self.ren.AddActor(self.actors[actor_name])
        elif filename is not None:
            path_elements = filename.split("\\")
            if len(path_elements) == 1:
                path_elements = filename.split("/")

            polydata = VTKMeshUtl.load(filename)
            mapper = vtk.vtkPolyDataMapper()
            if vtk.VTK_MAJOR_VERSION <= 5:
                mapper.SetInput(polydata)
            else:
                mapper.SetInputData(polydata)
            actor = vtk.vtkActor()
            actor.SetMapper(mapper)
            actor.GetProperty().SetColor(0, 100/255, 200/255)
            actor_name = path_elements[-1][:path_elements[-1].rindex(".")]
            self.actors[actor_name] = actor
            self.ren.AddActor(self.actors[actor_name])
        else:
            print("Class World > Method Add Actor > No Actor added")
        self.vtk_widget.update()
        return actor

    def start_world(self):
        # self.ren.AddActor(self.axes)
        self.ren.AddActor(self.txt)
        self.ren.ResetCamera()

    def key_pressed(self, iren, event):
        key = iren.GetKeySym()
        print("Pressed " + key)
        if key == 'Control_L' or key == 'Control_R':
            self.control_pressed = True
            self.__key_mods__.append('Control')

        elif key == 'Shift_L' or key == 'Shift_R':
            self.shift_pressed = True
            self.__key_mods__.append("Shift")
        else:
            if key not in self.__other_keys__:
                self.__other_keys__.append(key)
                print(self.__other_keys__)
        str_list = ""
        if len(self.__key_mods__) > 0:
            for i in self.__key_mods__:
                str_list += i + " + "
            str_list = str_list[:-3]
        if len(self.__other_keys__) > 0:
            str_list += ""
            if len(self.__key_mods__) > 0:
                str_list += " + "
            for i in range(0, len(self.__other_keys__)):
                str_list += self.__other_keys__[i] + " + "
            str_list = str_list[:-3]
        self.txt.SetInput(str_list)
        self.vtk_widget.update()

    def keyrelease(self, iren, event):
        key = iren.GetKeySym()
        print("Released " + key)

        if key == 'Control_L' or key == 'Control_R':
            self.control_pressed = False
            self.__key_mods__.remove("Control")

        elif key == 'Shift_L' or key == 'Shift_R':
            self.shift_pressed = False
            self.__key_mods__.remove("Shift")
        else:
            if key in self.__other_keys__:
                self.__other_keys__.remove(key)
        str_list = ""
        if len(self.__key_mods__) > 0:
            for i in self.__key_mods__:
                str_list += i + " + "
            str_list = str_list[:-3]
        if len(self.__other_keys__) > 0:
            str_list += " + "
            for i in self.__other_keys__:
                str_list += i + " + "
            str_list = str_list[:-3]
        self.txt.SetInput(str_list)
        self.vtk_widget.update()



class ExampleModels:

    def __init__(self, world):
        self.world = world
        files = [m for m in os.listdir('./defaults/geom/')]
        for f in files:
            offset = np.array([0, 0, 0])
            a_name = f
            print(f)
            # if 'pelvis' in f:
            #     a_name = 'pelvis'
                # offset = np.array([-0.039300399999999999, -0.081331600000000004, 0.084597400000000003])

            if 'r_femur' in f:
                a_name = 'r_femur'
                offset = np.array([-0.03874, -0.083679, 0.08984])

            if 'l_femur' in f:
                a_name = 'l_femur'
                offset = np.array([-0.04074, -0.084428, -0.092887])

            if 'r_tibia' in f:
                a_name = 'r_tibia'
                offset = np.array([-0.03874, -0.525511, 0.08984])

            if 'r_fibula' in f:
                a_name = 'r_fibula'
                offset = np.array([-0.03874, -0.525511, 0.08984])

            if 'l_tibia' in f:
                a_name = 'l_tibia'
                offset = np.array([-0.03874, -0.525511, -0.08984])

            if 'l_fibula' in f:
                a_name = 'l_fibula'
                offset = np.array([-0.03874, -0.525511, -0.08984])

            p = VTKMeshUtl.load('./defaults/geom/' + f)
            ps = VTKMeshUtl.extract_points(p)
            ps2 = ps + offset
            VTKMeshUtl.update_poly_w_points(ps2, p)
            mapper = vtk.vtkPolyDataMapper()
            if vtk.VTK_MAJOR_VERSION <= 5:
                mapper.SetInput(p)
            else:
                mapper.SetInputData(p)
            actor = vtk.vtkActor()
            actor.SetMapper(mapper)
            actor.GetProperty().SetColor(0, 100 / 255, 200 / 255)
            self.world.add_actor(actor_name=a_name, actor=actor)
        pass


class WorldView(QWidget):
    def __init__(self, parent):
        super().__init__()
        self.world = World(parent)
        vh = QVBoxLayout()
        vh.addWidget(self.world.vtk_widget)
        self.example = ExampleModels(self.world)
        self.setLayout(vh)
        self.world.reset_view()
        self.l0 = QLabel("", parent=self)
        self.l0.setFixedSize(38, 30)
        self.expanded_button = WorldMenuWidget(None, self)
        self.expanded_button.move(QPoint(60, 25))
        self.expanded_button.setVisible(False)
        self.button_loc = QPoint(25, 30)
        self.l0.move(self.button_loc)
        self.button = QPushButton("", parent=self)
        self.button.setIcon(QIcon("icons/slider.png"))
        self.button.setFixedSize(30, 30)
        self.button.move(self.button_loc)
        self.button.setCheckable(True)
        self.button.clicked.connect(self.menu_trigger)
        self.menu_boo = False
        self.closed = False

    def on_close(self):
        self.expanded_button.close()
        pass

    def clear_view(self):
        self.world.remove_all()

    def add_mesh(self, file_name):
        self.world.add_actor(filename=file_name)
        self.world.reset_view()

    def on_focus(self):
        self.closed = True
        self.menu_boo = False
        self.expanded_button.setVisible(False)
        op = QGraphicsOpacityEffect(self)
        op.setOpacity(1.0)
        self.l0.setGraphicsEffect(op)
        self.button.setChecked(False)

    def menu_trigger(self):
        if self.closed:
            self.closed = False
            self.button.setChecked(False)
            return
        print(self.button.mapToGlobal(self.button.pos()))
        self.menu_boo = not self.menu_boo
        self.button.setChecked(self.menu_boo)
        p = self.button.mapToGlobal(self.button.pos())
        self.expanded_button.move(p.x() + 15, p.y() - 40)

        self.expanded_button.setVisible(self.menu_boo)
        if self.menu_boo:
            op = QGraphicsOpacityEffect(self)
            op.setOpacity(0.5)
            self.l0.setGraphicsEffect(op)
        else:
            op = QGraphicsOpacityEffect(self)
            op.setOpacity(1.0)
            self.l0.setGraphicsEffect(op)

        print("pressed - trigger {0}".format(self.button.isChecked()))

    @staticmethod
    def label_landmark(p_name, p):
        caption_actor = vtk.vtkCaptionActor2D()
        caption_actor.SetCaption(p_name)
        caption_actor.SetAttachmentPoint(p)
        caption_actor.BorderOff()
        text_property = caption_actor.GetTextActor().GetTextProperty()
        text_property.SetFontSize(12)
        caption_actor.GetTextActor().SetTextScaleModeToNone()
        caption_actor.GetTextActor().GetTextProperty().SetColor(235.0 / 255.0, 223.0 / 255.0, 195.0 / 255.0)
        return p_name + "_l", caption_actor


class WorldMenuWidget(QWidget):
    def closeEvent(self, event):
        self.listener.on_focus()
        print("closed")

    def __init__(self, parent, listener):
        super().__init__(parent)
        self.listener = listener
        self.setWindowOpacity(0.65)
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.Popup)
        self.blank = QLabel(self)
        self.w = 250
        self.h = 200
        self.blank.setFixedHeight(self.h)
        self.blank.setFixedWidth(self.w)
        self.setFixedWidth(self.w)
        self.setFixedHeight(self.h)
        vl = QVBoxLayout()

        self.mesh_button = QPushButton("")
        self.mesh_button.setIcon(QIcon("icons/folder-open.png"))
        self.mesh_button.setFixedWidth(30)
        vl.addWidget(QLabel("Info and Settings"))
        hl = QHBoxLayout()
        self.mhl = QLabel("[Mesh (stl, ply, obj, vtp)]")
        self.mhl.setObjectName("mesh_q")
        hl.addWidget(self.mhl)
        hl.addWidget(self.mesh_button)
        mesh_q = QWidget()
        mesh_q.setObjectName("mesh_q")
        mesh_q.setLayout(hl)
        vl.addWidget(mesh_q)

        self.marker_button = QPushButton("")
        self.marker_button.setIcon(QIcon("icons/folder-open.png"))
        self.marker_button.setFixedWidth(30)
        hl = QHBoxLayout()
        self.ml = QLabel("[Markers (trc, c3d)]")
        self.ml.setObjectName("mesh_q")
        hl.addWidget(self.ml)
        hl.addWidget(self.marker_button)
        markers_q = QWidget()
        markers_q.setObjectName("mesh_q")
        markers_q.setLayout(hl)
        vl.addWidget(markers_q)

        vl.addStretch(5)
        self.setLayout(vl)
        self.sheet = BasicIO.read_as_block("./defaults/drop_menu.qss")
        self.setStyleSheet(self.sheet)
        self.mesh_button.clicked.connect(self.mesh_load_trigger)
        self.marker_button.clicked.connect(self.marker_load_trigger)

        # Create a QPixmap with the same size as the window, filled with a transparent color
        mask = QPixmap(self.size())
        mask.fill(Qt.transparent)

        # Create a QPainter to draw onto the QPixmap
        painter = QPainter(mask)
        painter.setRenderHint(QPainter.Antialiasing)
        path = QPainterPath()
        path.addRoundedRect(self.rect(), 16, 16)  # radius of the corners
        # Draw the path onto the QPixmap
        painter.fillPath(path, Qt.white)
        painter.end()

        # Set the QPixmap as the mask for the window
        self.setMask(mask.createMaskFromColor(Qt.transparent))
        pass

    def mesh_load_trigger(self):
        print("load mesh")
        of = OpenFiles()
        file_filter = 'Mesh (*.stl *.ply);; All File (*.*)'
        the_mesh = of.get_file(file_filter)
        print(the_mesh)
        self.listener.clear_view()
        self.listener.add_mesh(the_mesh)
        mesh_name = os.path.split(the_mesh)
        self.mhl.setText("Mesh: {0}".format(mesh_name[1]))

    # @staticmethod
    # def label_landmark(p_name, p):
    #     caption_actor = vtk.vtkCaptionActor2D()
    #     caption_actor.SetCaption(p_name)
    #     caption_actor.SetAttachmentPoint(p)
    #     caption_actor.BorderOff()
    #     text_property = caption_actor.GetTextActor().GetTextProperty()
    #     text_property.SetFontSize(12)
    #     caption_actor.GetTextActor().SetTextScaleModeToNone()
    #     caption_actor.GetTextActor().GetTextProperty().SetColor(235.0/255.0, 223.0/255.0, 195.0/255.0)
    #     return p_name+"_l", caption_actor

    def marker_load_trigger(self):
        print("load marker")
        of = OpenFiles()
        file_filter = 'Mocap (*.trc *.c3d);; All File (*.*)'
        the_file = of.get_file(file_filter)
        print(the_file)
        if the_file.endswith('.trc'):
            trc = TRC.read(the_file)
        else:
            trc = TRC.create_from_c3d(the_file)
        print(trc)
        markers = [m for m in trc.marker_set if '*' not in m and 'ignore' not in m.lower()]
        spheres = {}
        frame = 1335
        self.listener.clear_view()
        for m in markers:
            point = trc.marker_set[m].iloc[frame, :].to_list()
            if trc.headers['Units'] == 'm':
                point = [1000.0*p for p in point]
            print(point)
            spheres[m] = VTKMeshUtl.make_sphere(point)
            self.listener.world.add_actor(actor_name=m, actor=spheres[m][0])
            label, mark = self.listener.label_landmark(m, point)
            self.listener.world.add_actor(actor_name=label, actor=mark)
        self.listener.world.reset_view()
        mesh_name = os.path.split(the_file)
        self.ml.setText("Mocap: {0}".format(mesh_name[1]))
