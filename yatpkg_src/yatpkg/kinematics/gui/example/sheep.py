import os
import numpy as np
import pandas as pd
from scipy.spatial.transform import Rotation
from tqdm import tqdm

from yatpkg.math.transformation import Cloud
from yatpkg.util.data import JSONSUtl, TRC


def ref_maker(root):
    po = [f for f in os.listdir(root) if f.endswith('.npy')]
    sample = ['S{0}'.format(i) for i in range(29, 34)]
    corrs = {s: {p: np.load(root + p) for p in po if p.startswith(s)} for s in sample}
    ref = JSONSUtl.load_json(root + "landmarkers.json")
    ref_col = {s: {c for c in ref if c.startswith(s)} for s in sample}
    lumber = ['L3', 'L4', 'L5']
    for s in sample:
        rs = [ref[r] for r in ref_col[s]][0]
        origin = {o.split("_")[1]: np.load(root + o) for o in corrs[s] if "origin" in o}
        markers = [m for m in rs if m[:2] in lumber]
        sample_corr = {o.split("_")[1]: np.load(root + o) for o in corrs[s] if "corr" in o}
        L3_markers = {m: rs[m] - origin['L3'] for m in markers if 'L3' in m}
        L4_markers = {m: rs[m] - origin['L4'] for m in markers if 'L4' in m}
        L5_markers = {m: rs[m] - origin['L5'] for m in markers if 'L5' in m}
        sample_corr_inv = {o.split("_")[1]: np.linalg.inv(sample_corr[o.split("_")[1]]) for o in corrs[s] if
                           "corr" in o}
        L3x = pd.DataFrame(L3_markers)
        L3_ref = np.matmul(sample_corr_inv['L3'], L3x.to_numpy())
        L3_ref_pd = pd.DataFrame(data=L3_ref, columns=L3x.columns)
        L4x = pd.DataFrame(L4_markers)
        L4_ref = np.matmul(sample_corr_inv['L4'], L4x.to_numpy())
        L4_ref_pd = pd.DataFrame(data=L4_ref, columns=L4x.columns)
        L5x = pd.DataFrame(L5_markers)
        L5_ref = np.matmul(sample_corr_inv['L5'], L5x.to_numpy())
        L5_ref_pd = pd.DataFrame(data=L5_ref, columns=L5x.columns)
        L3_ref_pd.to_csv("{0}{1}_L3_ref_markers.csv".format(root, s), index=False)
        L4_ref_pd.to_csv("{0}{1}_L4_ref_markers.csv".format(root, s), index=False)
        L5_ref_pd.to_csv("{0}{1}_L5_ref_markers.csv".format(root, s), index=False)
        pass


if __name__ == '__main__':
    sheep_root = "E:/bones/sheep/"
    # ref_maker(sheep_root)
    labelled_data = sheep_root+"labelleddata/"
    tracking_files = [f for f in os.listdir(labelled_data) if f.endswith('.trc') and 'Compression' in f and 'Labelled' in f]
    tracking = {f[:3]: TRC.read(labelled_data+f) for f in tracking_files}
    vertebrae = {"L3": ["L3Left", "L3Mid", "L3Right"],
                 "L4": ["L4Left", "L4Mid", "L4Right"],
                 "L5": ["L5Left", "L5Mid", "L5Right"]
                 }
    corr = {f: np.load(sheep_root+f) for f in os.listdir(sheep_root) if f.endswith('corr.npy')}
    origin = {f: np.load(sheep_root+f) for f in os.listdir(sheep_root) if f.endswith('origin.npy')}
    ref_m = {f: pd.read_csv(sheep_root+f) for f in os.listdir(sheep_root) if f.endswith('markers.csv')}
    df_ref = {}
    for rm in ref_m:
        df_ref[rm[:3]] = {}
    for rm in ref_m:
        df_ref[rm[:3]][rm[4:6]] = ref_m[rm].to_numpy()

    for s in tracking:
        sample = tracking[s]
        min_frame = np.inf
        # scan
        p = [m for m in sample.marker_names if 'l' in m.lower()]

        for m in p:
            frame1 = np.isnan(sample.marker_set[m].to_numpy())
            frame2 = np.sum(frame1, axis=1)
            frame3 = np.where(frame2 > 0)
            try:
                pid = frame3[0][0]
                if min_frame > pid:
                    min_frame = pid
            except IndexError:
                continue
        if min_frame == np.inf:
            min_frame = sample.data.shape[0]
        marker_list = {}
        for m in p:
            marker_list[m] = sample.marker_set[m].iloc[:min_frame, :]
        ref_frame = None
        calibrate = {}
        bar = tqdm(range(min_frame), desc="Tracking {0}".format(s), ascii=False, ncols=100, colour="#6e5b5b")
        current = 0
        sq = {}
        for v in vertebrae:
            sq[v] = np.zeros([min_frame, 7])

        for i in range(0, min_frame):
            stuff = {}
            for v in vertebrae:
                m = np.zeros([3, 3])
                indx = 0
                for mx in vertebrae[v]:
                    m[:, indx] = marker_list[mx].iloc[i, :].to_list()
                    indx += 1
                stuff[v] = m

            if ref_frame is None:
                ref_frame = stuff
                spl = df_ref[s]
                t_L3 = Cloud.rigid_body_transform(spl["L3"], ref_frame["L3"])
                calibrate["L3"] = t_L3
                t_L4 = Cloud.rigid_body_transform(spl["L4"], ref_frame["L4"])
                calibrate["L4"] = t_L4
                t_L5 = Cloud.rigid_body_transform(spl["L5"], ref_frame["L5"])
                calibrate["L5"] = t_L5
            pass

            for v in vertebrae:
                t = Cloud.rigid_body_transform(ref_frame[v], stuff[v])
                k = np.matmul(t, calibrate[v])
                r = Rotation.from_matrix(k[:3, :3])
                q = r.as_quat(False)    # x,y,z,w
                sq[v][i, 0:4] = q
                sq[v][i, 4:] = k[:3, 3]
                pass
            bar.update(1)
        bar.close()
        for i in sq:
            out_file = '{2}{0}_{1}_tracking.csv'.format(s, i, sheep_root)
            oi = pd.DataFrame(data=sq[i], columns=['i', 'j', 'k', 'w', 'x', 'y', 'z'])
            oi.to_csv(out_file, index=False)
        st = {'l3l4r': np.zeros([min_frame, 3]),
              'l4l5r': np.zeros([min_frame, 3])
              }
        bar = tqdm(range(min_frame), desc="Processing angles {0}".format(s), ascii=False, ncols=100, colour="#6e5b5b")
        for i in range(0, min_frame):
            l3 = Rotation.from_quat(sq['L3'][i, :4])
            l4 = Rotation.from_quat(sq['L4'][i, :4])
            l5 = Rotation.from_quat(sq['L5'][i, :4])
            l3l4 = l3 * l4.inv()
            l3l4r = l3l4.as_euler('xyz', True)
            st['l3l4r'][i, :] = l3l4r
            l4l5 = l4 * l5.inv()
            l4l5r = l4l5.as_euler('xyz', True)
            st['l4l5r'][i, :] = l4l5r
            bar.update(1)

        for i in st:
            out_file = '{2}{0}_{1}_angles.csv'.format(s, i, sheep_root)
            oi = pd.DataFrame(data=st[i], columns=['x', 'y', 'z'])
            oi.to_csv(out_file, index=False)
        bar.close()
    pass
