from yatpkg.kinematics.gait import Analysis
import os

if __name__ == '__main__':
    print("try")
    # gk = Analysis.find_stride(ikfile='H:/My Drive/Work/gait Data/IKs/Baseline01.sto',
    #                           model_file='H:/My Drive/Work/gait Data/participant model/Participant_02_gait2392_simbody.osim',
    #                           percentage_cpu=0.25,
    #                           frame_number_st=259)

    # gk = Analysis.find_stride(ikfile='H:/My Drive/Work/gait Data/IKs/LeftBellyVib1min20Hz03.sto',
    #                           model_file='H:/My Drive/Work/gait Data/participant model/Participant_02_gait2392_simbody.osim',
    #                           percentage_cpu=0.25)

    # gk = Analysis.find_stride(ikfile='H:/My Drive/Work/gait Data/IKs/RightTendonVib1min20Hz01.sto',
    #                           model_file='H:/My Drive/Work/gait Data/participant model/Participant_02_gait2392_simbody.osim',
    #                           percentage_cpu=0.25,
    #                           frame_number_st=223)

    # gk = Analysis.find_stride(ikfile='H:/My Drive/Work/gait Data/IKs/WBV1min20Hz07.sto',
    #                           model_file='H:/My Drive/Work/gait Data/participant model/Participant_02_gait2392_simbody.osim',
    #                           percentage_cpu=0.25)

    # gk = Analysis.find_stride(ikfile='H:/My Drive/Work/gait Data/IKs/WBV1min20HzPost03.sto',
    #                           model_file='H:/My Drive/Work/gait Data/participant model/Participant_02_gait2392_simbody.osim',
    #                           percentage_cpu=0.25)
    # gk = Analysis.find_stride(ikfile='H:/My Drive/Work/gait Data/IKs/RightTendonVib1min20Hz07.sto',
    #                           model_file='H:/My Drive/Work/gait Data/participant model/Participant_02_gait2392_simbody.osim',
    #                           percentage_cpu=0.25)
    # extracted_gait_parameters = Analysis.process_stride_heel(gk)
    filelist = [f for f in os.listdir('H:/My Drive/Work/gait Data/IKs/') if "static" not in f]
    for f in filelist:
        gk = Analysis.find_stride(ikfile='H:/My Drive/Work/gait Data/IKs/{0}'.format(f),
                                  model_file='H:/My Drive/Work/gait Data/participant model/Participant_02_gait2392_simbody.osim',
                                  percentage_cpu=0.25,
                                  is_debug=False)
        extracted_gait_parameters = Analysis.process_stride_heel(gk)
    print("done")
