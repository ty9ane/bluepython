import os

if __name__ == '__main__':
    print("Getting Requirement:")
    os.system('conda install -c opensim-org opensim')
    os.system('python -m pip install pandas scipy numpy scikit-learn Pillow vtk h5py tables PySide6 scikit-kinematics tsfresh pymeshlab')
    print("Done!")
