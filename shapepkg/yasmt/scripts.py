import numpy as np

from yatpkg.util.data import Mesh
from modelling.analysis import MeshTools
import pymeshlab as ml
import os


def align():
    wk_dir = "C:/Users/tyeu008/OneDrive/Work/Bones/Shape/"
    wk_dir1 = "C:/Users/tyeu008/OneDrive/Work/Bones/testing/rawSegmentations/flipped/VRS028/"
    sour = Mesh(wk_dir1 + "VRS_028_raw_Scapula.stl")
    target = Mesh(wk_dir + "mean_scapula_Scapula.stl")
    a = MeshTools.align_points_to_principle(sour.points)
    sour.points = a
    sour.update_points_vtk()
    sour.write_mesh("C:/Users/tyeu008/OneDrive/Work/Bones/Shape/test_sour_align.ply")
    e0, v0, centered_data0, cm0 = MeshTools.principle_axes(sour.points, sour.cog)
    e1, v1, centered_data1, cm1 = MeshTools.principle_axes(target.points, target.cog)
    align = np.matmul(v0.T, centered_data0.T)
    sour.points = align.T
    sour.update_points_vtk()
    sour.write_mesh("C:/Users/tyeu008/OneDrive/Work/Bones/Shape/test_sour.ply")
    new_points = np.matmul(v0, np.matmul(v1.T, centered_data1.T)) + np.atleast_2d(cm0).T
    target.points = new_points.T
    target.update_points_vtk()
    target.write_mesh("C:/Users/tyeu008/OneDrive/Work/Bones/Shape/test_tar.ply")


def matching():
    patientlist = [p for p in os.listdir("D:/Model/VRS/VRS-Original/")]
    patient_pred = [p for p in os.listdir('D:/Model/VRS/VRS-Predcited-HumerusScapula_flipped/stl/')]
    patient_data = {}
    for f in patientlist:
        try:
            patient_data[f.split("_")[0]].append(f)
        except KeyError:
            patient_data[f.split("_")[0]] = [f]
    for p in patient_pred:
        try:
            patient_data[p.split("_")[0]].append(p)
        except KeyError:
            continue
    pass


def flip_normals():
    # Load the mesh file
    indir = 'D:/Model/VRS/VRS-Predcited-HumerusScapula/'
    outdirply = 'D:/Model/VRS/VRS-Predcited-HumerusScapula_flipped/ply/'
    outdirstl = 'D:/Model/VRS/VRS-Predcited-HumerusScapula_flipped/stl/'
    files = [f for f in os.listdir(indir)]
    if not os.path.exists(outdirply):
        os.makedirs(outdirply)
    if not os.path.exists(outdirstl):
        os.makedirs(outdirstl)
    for f in files:
        print(f[:-3]+"ply")
        mesh = ml.MeshSet()
        mesh.load_new_mesh(indir+f)

        # Compute vertex normals
        # mesh.compute_normal_for_point_clouds()

        # Invert faces orientation
        mesh.meshing_invert_face_orientation()

        # Save the mesh file
        mesh.save_current_mesh(outdirply+f[:-3]+"ply")
        mesh.save_current_mesh(outdirstl + f)
    pass


if __name__ == '__main__':
    # align()
    # flip_normals()
    matching()
    pass
